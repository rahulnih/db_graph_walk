# Contig extension in genome assembly

This is the final step in our genome assembly pipeline. This software is to be used in conjunction with the [bruno](http://github.com/ParBLiSS/bruno) graph construction software. This software is currently under development.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

* C++ 5.3 or higher
* Python 3.4 or higher


### Installing

Run "make"

### Running

```
./walk <config_file> <tinterface_dir> <tidentifier> <output_dir_name>
```
### Example

```
./runBench.sh
```


## Authors

* [Rahul Nihalani](https://rahulnih.bitbucket.io)


## Acknowledgments

* An immense thanks to Tony Pan for his contributions. 
