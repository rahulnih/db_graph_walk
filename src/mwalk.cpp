#include "mwalk.h"

// constructor
Walk_Queue_Mul::Walk_Queue_Mul(Params* P, DB_Node_Table_Type& list_nodes, KU_Table_Type& KU_Table, contig_data_str& CDS, std::ofstream& log_file, unsigned int NUM_THREADS) : NUM_THREADS_(NUM_THREADS)
{
	WBptr = new Walk_Base(P, list_nodes, KU_Table, CDS, LP_map_, log_file);
	Q_vec.resize(NUM_THREADS_);
	
#pragma omp parallel for num_threads(NUM_THREADS_)
	for (unsigned int i=0; i < NUM_THREADS; i++) {
		Q_vec[i] = new Walk_Queue(P, list_nodes, KU_Table, CDS, LP_map_, log_file, WBptr);
	}
	
	// initialize in_use vector
	in_use.resize(list_nodes.size());
	for (unsigned int i=0; i < in_use.size(); i++) {
		in_use[i] = false;
	}
}

// the main parallel walk function
void Walk_Queue_Mul::mwalk_db_graph()
{
	WBptr->init_lmer_id_vec();
	
	// var assists in parallel selection for starting nodes
	std::atomic<unsigned int> sel_start_node_id(0);
		
#pragma omp parallel num_threads(NUM_THREADS_)
	{
		int id = omp_get_thread_num();
		Walk_Queue& WQ = *Q_vec[id];
		
		// start producing contigs
		while(true) {
			
			WQ.clear();
			WQ.CCS.curr_contig="";
			unsigned int start_index =mselect_starting_node(WQ, &sel_start_node_id);
#pragma omp critical(sel_start)
			{
				std::cout << "Tid: " << id << ", start index: " << start_index << std::endl;
			}
#pragma omp barrier
// #pragma omp single
// 			{
// 				exit(0);
// 			}
			
			if (start_index==std::numeric_limits<unsigned int>::max()) {
				break;
			}

			WQ.push_starting_node(start_index);
			
			unsigned long int num_iterations=0;
			
			// produce one contig
			while(true) {
				Walk_Queue_Element& current_WQE = WQ.CurrElement();
				
				// set s_dir for all WHITE nodes in current_WQE based on end
				WQ.set_sdir( current_WQE );
				
				// check if there is enough space for further nodes
				WQ.buff_check(current_WQE, WBptr->walk_queue_preserve_history);
				
				// add neighbors of curr WHITE node, and make them BROWN
				WQ.set_neigh_nodes( current_WQE );
				
				// add dist const of curr WHITE node, and make them BLACK
				unsigned int m = WQ.set_next_dist_const( current_WQE );
				
				// log some information
				if (num_iterations >= WBptr->P_->info_after_iterations) {
					unsigned int l = WBptr->ref_db_node_table.ElementAt(
									current_WQE.candidate_nodes[ current_WQE.white_index ].tar_lmer_id
									).len_lmer;
					WQ.print_snapshot(WQ.curr, (WQ.curr + std::max(l,m))%WQ.Q.size(), WBptr->ostr_.log_file );
				}
				
				// move the current pointer to the next BROWN list
				WQ.move_pointer( current_WQE );
				
				// again get the current element after the pointer has been moved
				Walk_Queue_Element& next_WQE = WQ.CurrElement();
				
				mselect_next_hop( WQ, next_WQE, id );
				
			}
		}
		
	} // END: pragma omp parallel num_threads(NUM_THREADS_)
}

// select multiple starting nodes for parallel walk
// TODO: have a length threshold for selecting starting node.
unsigned int Walk_Queue_Mul::mselect_starting_node(Walk_Queue& WQ, std::atomic<unsigned int>* sel_var)
{
	while(true){
		unsigned int i = (*sel_var).fetch_add(1, std::memory_order_relaxed);
		if (i >= WBptr->sorted_lmer_id_vec.size()) {
			break;
		}
		unsigned int index = WBptr->sorted_lmer_id_vec[i];
		DB_Node_Type& D = WBptr->ref_db_node_table.ElementAt(index);
		// test if there is at least one negative dist const
		bool negative_exists = false;
		for (unsigned int j=0; j < D.Dist_Const_List.size(); j++) {
			if (D.Dist_Const_List[j].distance < 0) {
				negative_exists = true;
			}
		}
		if (negative_exists and (WBptr->UD.expected_node_usage_left[index] > 0) and (not in_use[index])) {
			WBptr->ostr_.log_file << "Starting node " << index << " selected" << std::endl;
			WQ.CCS.white_node_vec.push_back(index);
			WQ.exp_covg = WBptr->init_expected_coverage(index);
			in_use[index] = true;
			return index;
		}
	}
	return std::numeric_limits<unsigned int>::max();
}

void Walk_Queue_Mul::mselect_next_hop(Walk_Queue& WQ, Walk_Queue_Element& WQE, int id)
{
	select_next_hop_data SD(WQ);
	
	bool found = WQ.SNHptr->find_best_candidate(WQE, SD, PAR);
// 	found = not found;
	
	// if found, candidate present in SD.best_cand_position
	unsigned int lmer_id = std::numeric_limits<unsigned int>::max();
	if (found) {
		lmer_id = WQE.candidate_nodes[ SD.best_cand_position ].tar_lmer_id;
	}
#pragma omp critical(sel_next)
	{
		std::cout << "Tid: " << id << ", next index: " << lmer_id << std::endl;
	}
#pragma omp barrier
#pragma omp single
	{
		exit(0);
	}

}
