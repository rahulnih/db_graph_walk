#include "all_classes.h"
#include "db_graph.h"
#include "ku_table.h"
#include "misc.h"
#include "walk.h"
#include "mwalk.h"
#include "pwalk.h"
#include "contig_data.h"
#include "post_walk.h"
#include "tinterface.h"
#define DEBUG false

std::ofstream debug_file;
wall_time_wrap W_base;
int main(int argc, char** argv) {

	if (argc!=5) {
		std::cerr << "Usage: " << argv[0] << " <config_file> <tinterface_dir> <tidentifier> <output_dir_name>" << std::endl;
		exit(0);
	}
	
	int dont_care;
	dont_care = std::system("echo '### git log ###\n' >> gitinfo");
	dont_care = std::system("git log -1 >> gitinfo");
	dont_care = std::system("echo '\n### git diff ###\n' >> gitinfo");
	dont_care = std::system("git diff >> gitinfo");
	
	// open the config file
	std::ifstream config_file(argv[1]);
	if (not config_file.good()) {
		std::cerr << "Config file " << argv[1] << " doesn't exist" << std::endl;
		exit(0);
	}
	
	// read params from config file
	Params *P = new Params;
	P->getParams(config_file);
	
	W_base=P->base_wall_time;
	
	stat_str SS;
	
	// open the tinterface directory
	DIR *tDir;
	tDir = opendir(argv[2]);
	if (tDir == NULL) {
		std::cerr << "Tinterface directory doesn't exist" << std::endl;
		exit(0);
	}
	closedir(tDir);
	
	// copy the config file in the working directory
	std::string copy_command = "cp " + std::string(argv[1]) + " " + std::string(argv[2]) + "/copy_" + std::string(argv[1]);
// 	std::cout << copy_command << std::endl;
	dont_care = std::system(copy_command.c_str());
	
	std::string move_command = "mv gitinfo " + std::string(argv[2]);
	dont_care = std::system(move_command.c_str());
	
	if (chdir(argv[2])!=0) {
		std::cerr << "Unable to switch to tinterface directory" << std::endl;
		exit(0);
	}
	
	// prefix of various input files
	std::string input_id = std::string(argv[3]);
	
	// open various t-input files
	tinfile_str TinS;
	TinS.read_tinfiles(input_id);
	
	// file containing partition ids of partitioned de-Bruijn graph
	// TODO: test the kahip checking logic
	std::ifstream partition_file;
	if (P->wt == PAR and P->print_kahip_graph==0) {
		partition_file.open(P->partition_file);
		if (not partition_file.is_open()) {
			std::cerr << "Unable to open partition id file" << std::endl;
			exit(0);
		}
	}
	
	// output main directory
	DIR *ODir;
	ODir = opendir(argv[4]);
	if (ODir == NULL) {
		std::string mkdir_command = "mkdir " + std::string(argv[4]);
		dont_care = std::system(mkdir_command.c_str());
	}
	closedir(ODir);

	move_command = "mv copy_" + std::string(argv[1]) + " " + std::string(argv[4]);
// 	std::cout << move_command << std::endl;
	dont_care = std::system(move_command.c_str());
	
	move_command = "mv gitinfo " + std::string(argv[4]);
// 	std::cout << move_command << std::endl;
	dont_care = std::system(move_command.c_str());
	
	if (chdir(argv[4])!=0) {
		std::cerr << "Unable to switch to output directory" << std::endl;
		exit(0);
	}
	
	// read output files
// 	open_output_files(out_file, log_file, stat_file, P);
	outfile_str ostr;
	ostr.open_output_files(P);
	std::ofstream& log_file = ostr.log_file;
	debug_file.open("debug.txt");
	
	// log directory for parallel walk
	DIR *lDir;
	lDir = opendir("log_dir");
	if (lDir == NULL) {
		dont_care = std::system("mkdir log_dir");
	}
	closedir(lDir);
	
	// all necessary files and directoriesopen
	
	P->print(log_file);

	// the kmer length
	unsigned int k;
	time_str T(P);
	
	// clock vars
	std::clock_t c_start, c_end;
	std::chrono::high_resolution_clock::time_point t_start, t_end;
	
	/****************************************stage 1********************************************/
	
	// start clock for prewalk
	c_start = std::clock();
	t_start = std::chrono::high_resolution_clock::now();
	
	// build the graph
	DB_Node_Table_Type list_nodes(log_file);
	kmer_unitig_mapping_table_type KUMTT(log_file);
	// TODO: clear kumtt?
	wall_time_wrap W;
	
	if (P->read_kgp) {
		std::cout << "Reading graph and params from file..." << std::flush;
		W.start();
		
		// TODO: param directory name
		
		std::ifstream gin;
		gin.open("reading_dir/graph.txt");
		if (not gin.is_open()) {
			std::cerr << "\nUnable to open the graph input file" << std::endl;
			exit(0);
		}
		
		std::ifstream pin;
		pin.open("reading_dir/params.txt");
		if (not pin.is_open()) {
			std::cerr << "\nUnable to open the param input file" << std::endl;
			exit(0);
		}
		
		list_nodes.read_from_file(gin);
		
		P->read_graph_params(pin);
		
		W.stop();
		std::cout << "done, time: " << W  << std::endl; 
	}
	else {
		list_nodes.build_graph_from_tinterface(P, TinS, ostr, KUMTT);
	}
	
	if (P->write_kgp) {
		std::cout << "Writing graph and params to file..." << std::flush;
		W.start();
		
		DIR *rDir;
		rDir = opendir("reading_dir");
		if (rDir == NULL) {
			std::string mkdir_command = "mkdir reading_dir";
			dont_care = std::system(mkdir_command.c_str());
		}
		closedir(rDir);
		
		std::ofstream gout;
		gout.open("reading_dir/graph.txt");
		if (not gout.is_open()) {
			std::cerr << "Unable to open the graph output file" << std::endl;
			exit(0);
		}
		
		std::ofstream pout;
		pout.open("reading_dir/params.txt");
		if (not pout.is_open()) {
			std::cerr << "Unable to open the graph output file" << std::endl;
			exit(0);
		}
		
		list_nodes.write_to_file(gout);
		
		P->print_graph_params(pout);
		
		W.stop();
		std::cout << "done, time: " << W  << std::endl; 
	}
	
	k = P->k;
	SS.num_singletons = list_nodes.singleton_count;
	SS.num_nodes = list_nodes.size();
	SS.num_edges = list_nodes.num_edges/2;
	
	// output stats
	ostr.stat_file << SS;
	
	KU_Table_Type KU_Table;
	if (P->read_kgp) {
		std::cout << "Reading KU table from file..." << std::flush;
		W.start();
		
		std::ifstream kin;
		kin.open("reading_dir/ku_table.txt");
		if (not kin.is_open()) {
			std::cerr << "\nUnable to open the ku table input file" << std::endl;
			exit(0);
		}
		
		KU_Table.read_from_file(kin);
		
		W.stop();
		std::cout << "done, time: " << W  << std::endl; 
		
	}
	else {
		// build KU table
		std::cout << "Creating the kmer to unitig transformation table..." << std::flush;
		W.start();
		
		KU_Table.Create_Kmer_2_Unitig_Table (list_nodes, k);
		
		W.stop();
		std::cout << "done, time: " << W  << std::endl; 
	}

	if (P->write_kgp) {
		std::cout << "Writing ku table to file..." << std::flush;
		W.start();

		DIR *rDir;
		rDir = opendir("reading_dir");
		if (rDir == NULL) {
			// by now, the directory should be there
			std::cerr << "reading directory does not exist" << std::endl;
			exit(0);
		}
		closedir(rDir);

		std::ofstream kout;
		kout.open("reading_dir/ku_table.txt");
		if (not kout.is_open()) {
			std::cerr << "Unable to open the ku table output file" << std::endl;
			exit(0);
		}
		
		KU_Table.write_to_file(kout);
		
		W.stop();
		std::cout << "done, time: " << W  << std::endl; 
	}
	
	if (P->wt == PAR and P->print_kahip_graph==1) {
		std::cout << "Quitting" << std::endl;
		exit(0);
	}
	
	// ku table and kumtt comparison
// 	bool compare_check=compare_kut_kumtt(KUMTT.KUM_Table, KU_Table, log_file);
// 	std::cout << "Compare check: " << BOOL(compare_check) << std::endl;
// 	exit(0);
	
	// end prewalk clock and put time
	c_end = std::clock();
	t_end = std::chrono::high_resolution_clock::now();	
	T.c_prewalk = c_end - c_start;
	T.t_prewalk = std::chrono::duration_cast<std::chrono::milliseconds >(t_end - t_start);
	
	/***********************************End: stage 1********************************************/
	
	// log stuff if needed
	log_file << "kmer size: " << k << std::endl;
	if (P->print_db_graph) {
		std::cout << "Printing the input DB graph to log file... " << std::flush;
		log_file << "The DB graph read from the input file (lmer_id's initialized to max unsigned int): \n\n";
		list_nodes.print(log_file);
		std::cout << "done"  << std::endl;
	}
	else {
		log_file << "Skipping printing the input DB graph:\n\n";
	}

	if (P->print_KU_table) {
	  std::cout << "Printing the KU Table to log file... " << std::flush;
		log_file << "The KU Table:\n\n";
		KU_Table.print(log_file);
		std::cout << "done"  << std::endl;
	}
	else {
		log_file << "Skipping printing the KU Table:\n\n";
	}
	
	/****************************************stage 2********************************************/
	
	// start walk time
	c_start = std::clock();
	t_start = std::chrono::high_resolution_clock::now();
	
	
	// now start the walk part
	contig_data_str CDS;
	std::vector<lmer_partition_map> LP_map;
	std::cout << "Starting the graph walk..." << std::flush;
	Walk_Base *WBptr = nullptr; 
	if (P->wt == SER) {
		WBptr = new Walk_Base(P, list_nodes, KU_Table, CDS, LP_map, ostr);
		Walk_Queue wok(P, list_nodes, KU_Table, CDS, LP_map, ostr, WBptr);
		wok.walk_db_graph();
	}
	else if (P->wt == PAR) {
		init_LP_map(list_nodes, P->k, LP_map, P->num_threads, partition_file,
			ostr.stat_file, ostr.log_file, P->walk_info_level);

		Walk_Queue_Par pwok(P, list_nodes, KU_Table, CDS, LP_map, ostr, partition_file, T);
		pwok.pwalk_db_graph();
	}
	else if (P->wt == MUL) {
// 		Walk_Queue_Mul mwok(P, list_nodes, KU_Table, CDS, log_file, 16 /*MAX_THREADS*/);
// 		mwok.mwalk_db_graph();
		std::cerr << "Currently MUL option is unsupported" << std::endl;
		exit(0);
	}
	else {
		
	}
	
	std::cout << "done" << std::endl;
	
	// end walk part and put time
	c_end = std::clock();
	t_end = std::chrono::high_resolution_clock::now();
	T.c_walk = c_end - c_start;
	T.t_walk = std::chrono::duration_cast<std::chrono::milliseconds>(t_end - t_start);
	
	/***********************************End: stage 2********************************************/
	
	std::cout << CDS.data.size() <<" initial contigs produced" << std::endl;
	std::cout << CDS.isolated_contigs << " are isolated" << std::endl;
	
	/****************************************stage 3********************************************/
	
	// start post walk clock
	c_start = std::clock();
	t_start = std::chrono::high_resolution_clock::now();
	
	// do some post walk processing
	Post_Walk PS(P, CDS, log_file);
	std::cout << "Post walk processing..." << std::flush;
// 	PS.elongate_contigs();
	std::cout << "done" << std::endl;
	
	// postwalk routine done, calculate time
	c_end = std::clock();
	t_end = std::chrono::high_resolution_clock::now();	
	T.c_postwalk =c_end - c_start;
	T.t_postwalk = std::chrono::duration_cast<std::chrono::milliseconds >(t_end - t_start);
	
	/***********************************End: stage 3********************************************/
	
	// write the contigs to the output file
	std::cout << "output contigs..." << std::flush;
	CDS.write_contigs(P->min_contig_len, ostr.out_file);
	std::cout << "done" << std::endl;
	
	// log the timing of various stages
	log_file << T;
	
	dont_care++;

	if (WBptr != nullptr) {
		delete WBptr;
	}
	if (P != nullptr) delete P;
	return 0;
}
