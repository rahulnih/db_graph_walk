#ifndef MISC_H
#define MISC_H

#include "all_classes.h"
#include <mutex>
// #include <typeinfo>

// misc functions used by various classes to follow
// TODO: separate params for preprocessing (tinterface), and graph walking
class Params {
public:
	
	// file params
	std::string log_file;
	std::string output_file;
	std::string stat_file;
	
	// general params
	
	bool print_contigs;
	// minimum contig length to be written to output
	unsigned int min_contig_len;
	// flag for printing kahip input
	bool print_kahip_graph;
	
	// input reading params
	
	// flags to denote if we should read/write graph and ku table from/to files
	bool read_kgp;
	bool write_kgp;
	
	// parallelization params
	
	// Var to denote the type of walk
	Walk_Type wt;
	// partition file name, only if walk type is PAR
	std::string partition_file;
	// number of threads for parallel walk
	unsigned int num_threads;

	// walk params
	
	// maximum permissible error in paired read distance (not in config file, calculated later) (tinterface)
	int max_dist_error;
	// used to calculate support score for a candidate node, used in func evidence_score
	float support_power;
	// how much to scale down support for intra read dist const
	float intra_support_scale;
	// while deciding which brown node to pick, min score diff needed between best and second best
	float sufficient_score_diff_abs;
	float sufficient_score_diff_norm;
	// if current usage is within the fudge factor, it will get absorbed
	float coverage_fudge;
	// max iterations for any loop
	unsigned long int max_iterations;
	// will be read in the main file from DB graph input file (tinterface)
	unsigned int k;
	// read length
// 	unsigned int read_length (tinterface) // TODO: remove them from here
	unsigned int read1_length;
	unsigned int read2_length;
	
	// avg mate pair distance (TODO: useless var, remove) (tinterface)
	unsigned int pair_distance;
	
	// avg insert size (tinterface)
	unsigned int insert_size;
	// if true, we will produce a reverse contig after producing a forward contig
	bool enabRevWalk;
	// enforce presence of a neg constraint while picking start node in walk
	bool enforce_neg_const;
	// depth to which we do all paths in case a decision can't be made on the next hop
	unsigned int all_paths_dist;
	// max num of nodes in all paths tree
	unsigned int all_paths_size;
	// max difference in length of adjoint seqs to be considered a bubble
	unsigned int max_bubble_diff;
	
	// limits of magic_ratio
	float min_magic_ratio;
	float max_magic_ratio;
	
	// error tolerance = var * std_dev
	float error_scaling;
	
	// tolerable error for intra read dist const
	int intra_error;
	
	// min tolerable distance for inserting dist const
	int min_dist_const;
	
	int min_start_node_len;
	
	Evidence_Scaledown_Type evidence_scaledown_fn;
	
	// whether do scaffolding by continuing traversal via N's
	bool doScaff;
	
	//debug params
	
	unsigned int info_after_iterations;
	// how much info in log file, 1:min, 3:max
	unsigned int walk_info_level;
	
	// wall time in ms above which function timings will be printed
	unsigned int base_wall_time;
	// whether to print db graph
	bool print_db_graph;
	// whether to print ku table
	bool print_KU_table;
	// true if graph to be printed after distance constraints are transformed from lmer-kmer to lmer-lmer
	bool print_transformed_constraints;
	// true if graph to be printed after identical distance are merged in the transformed graph
	bool print_transformed_and_merged_constraints;
	
	bool print_start_node_stats;
	bool print_score_diff_stats;
	bool print_mini_all_paths_stats;
	bool print_mem_map;
	unsigned int print_mem_map_freq;
	
	// FUNCTIONS
	
	// read the parameters from an input stream
	void getParams(std::ifstream& is);
	
	// print all the vars
	void print(std::ostream& os=std::cout);
	
	void print_graph_params(std::ostream& os);
	
	void read_graph_params(std::istream& is);
};

// structure to manage tinterface input files
class tinfile_str {
public:
	std::ifstream kum_file;
	std::ifstream unitig_nodes_file;
	std::ifstream lmer_sequences_file;
	std::ifstream junction_nodes_file;
	std::ifstream read_truncation_file;
	std::ifstream read_file1;
	std::ifstream read_file2;
	
	// read tinterface input files
	void read_tinfiles(std::string input_id);
};

class outfile_str {
public:
	std::ofstream out_file;
	std::ofstream log_file;
	std::ofstream stat_file;
	std::ofstream insert_size_hist_file;
	// data about contig lens used for estimating insert size
	std::ofstream contig_len_hist_file;
	std::ofstream start_node_stats_file;
	std::ofstream score_diff_hist_file;
	std::ofstream map_stat_file;
	std::ofstream warnings;
	std::ofstream bub_file;

	// mutexes for common files
	std::mutex warnings_m;
	std::mutex log_file_m;
	
	void open_output_files(Params* P);
};


// misc utility functions

// // open output files
// void open_output_files(std::ofstream& out_file, 
// 					std::ofstream& log_file, 
// 					std::ofstream& stat_file, 
// 					Params *P);

// split a string str based on delimiter and return a vector of split elements
std::vector<std::string> split(std::string str, char delimiter);

// checks if the strand is lex smaller than it's reverse complement
bool is_positive(std::string & strand);

// check if mid is between start and end
bool check_mid(unsigned int start, unsigned int mid, unsigned int end);

// given a strand (kmer or lmer), return the lesser of it and it's reverse complement, return true if no change needed
bool MakePositive(std::string& strand);

// given a strand (kmer or lmer), return the greater of it and it's reverse complement, return true if no change needed
bool MakeNegative(std::string& strand);

// change the string to get it's reverse complement
void reverse_complement(std::string& s);

// see func definition
unsigned int reverse_alphabet_index(char x);


template<typename T> size_t vecsize(const typename std::vector<T>& vec) {
	return sizeof(T)*vec.size();
}

std::string bytes2h(size_t bytes);

std::string num2h(long int num);

extern std::string alphabet;

// /dev/null stream to redirect useless output
extern std::ofstream nullstream;


// TODO: GET THE DEFINITION TO CPP FILE
// TODO: make vector const
// print a general vector
template <typename T> std::ostream& operator<< (std::ostream& os, std::vector<T>& v) {
	os << '[';
	for (unsigned int i=0; i < v.size(); i++) {
		os << v[i] << " ";
	}
	os << ']';
	return os;
}

// reduce var by val, but only uptil zero
template <typename T> bool reduce_to_zero (T& var, T val, std::ostream& log_file, bool log_put) {
	if (var==0) {
		if (log_put) {
			log_file << "Warning, zero variable being reduced by " << int(val) << std::endl;
		}
		return false;
	}
	if (var > val) {
		var-=val;
	}
	else {
		var=0;
	}
	return true;
}

template <typename T> void clearVecMem(std::vector<T>& V) {
	std::vector<T>().swap(V);
}

template <typename T> void reset_vec_to_zero(std::vector<T>& v) {
	for (unsigned int i=0; i < v.size(); i++) {
		v[i] = 0;
	}
}

template <typename T> void box_print_vec(std::vector<T>& v, unsigned int num_cols, std::ofstream& out_file) {
	for (unsigned int i=0; i < v.size(); i++) {
		out_file << i << ":" << v[i];
		if (i%num_cols==num_cols-1) {
			out_file << std::endl;
		}
		else {
			out_file << "\t";
		}
	}
}


// structure to map a given lmer_id to partition id, useful for partitioned walk.
class lmer_partition_map {
public:
	// lmer id by Tony's representation
	std::string lmer_id;
	// useful for partitioning singletons
	bool is_singleton;
	// index in the DB_Node_Table
	unsigned int index;
	// partition id of the node
	unsigned int partition_id;
};

void calc_lp_stats(	std::vector<lmer_partition_map>& LP_vec, 
				unsigned int num_par, 
				std::vector<std::size_t>& par_size);

void print_lp_stats(std::vector<std::size_t>& par_size, std::ofstream& out_file);

std::ostream& operator<<(std::ostream& os, lmer_partition_map& L);

// structure to hold some stats about the input data
class stat_str {
public:
	unsigned int num_nodes;
	unsigned int num_edges;
	unsigned int num_singletons;
	
	/*************************
	 *  TODO: add these:
	 * num intra read rejected by various criteria
	 * 
	 * ************************/
	
};

std::ostream& operator<<(std::ostream& os, stat_str& S);

class time_str {
public:
	std::clock_t c_prewalk;
	std::clock_t c_walk;
	std::clock_t c_postwalk;
	
	std::chrono::milliseconds t_prewalk;
	std::chrono::milliseconds t_walk;
	std::chrono::milliseconds t_postwalk;
	
	std::vector<std::chrono::milliseconds> t_threads;
	Params *P_;
	
	time_str(Params *P);
};

// output operator for time_str
std::ostream& operator<<(std::ostream& os, time_str& T);


// easy wrapper class to compute wall time
class wall_time_wrap {
public:
	
	// vars
	
	// start time of a process
	std::chrono::high_resolution_clock::time_point t_start;
	
	// end time of a process
	std::chrono::high_resolution_clock::time_point t_end;
	
	// time elapsed during process
	std::chrono::milliseconds t_elapsed;
	
	// constructor
	wall_time_wrap() {}
	wall_time_wrap(unsigned int t){t_elapsed = std::chrono::milliseconds(t);}
	wall_time_wrap& operator=(const unsigned int t) {
		t_elapsed = std::chrono::milliseconds(t);
		return *this;
	}
	
	// functions
	
	// start the wall clock
	void start();
	
	// stop the wall clock
	void stop();
	
	// convert the ticks into a time string
// 	std::string convert_t2s(std::chrono::milliseconds msec);
	
	void reset();
	
};

// convert the ticks into a time string
std::string convert_t2s(std::chrono::milliseconds msec);


// print the time elapsed string
std::ostream& operator<<(std::ostream& os, wall_time_wrap& W);

// comparison operators for wall time
bool operator<(const wall_time_wrap& w1, const wall_time_wrap& w2);
bool operator>(const wall_time_wrap& w1, const wall_time_wrap& w2);
bool operator>=(const wall_time_wrap& w1, const wall_time_wrap& w2);

extern wall_time_wrap W_base;


// TODO: move the following to walk_base.h
/******************************** score diff strs ******************************/

// structure to store score diff observed in data
class score_diff_str {
public:
	float score_diff_abs;
	float score_diff_norm;
	bool pass;
};

std::ostream& operator<<(std::ostream& os, score_diff_str& SDS);

// vector for score diff in data
class score_diff_vec {
private:
	
	std::mutex m;
	std::vector<score_diff_str> V;
	
public:
	
	// push values in vector
	void push(float sabs, float snorm, bool pass);
		
	// output operator
	friend std::ostream& operator<<(std::ostream& os, score_diff_vec& SDV);
};

/*****************************END: score diff strs ****************************/

/******************************** mini all paths stats strs ******************************/

// structure to store mini all paths stats
class map_stat_str {
public:
	unsigned short num_nodes_returned;
	unsigned short buff_used;
};

std::ostream& operator<<(std::ostream& os, map_stat_str& MSS);

// vector for map stats in data
class map_stat_vec {
private:
	
	std::mutex m;
	std::vector<map_stat_str> V;
	
public:
	
	// push values in vector
	void push(unsigned short num_nodes_returned, unsigned short buff_used);
	
	// sort the vector on buff used
	void sort();
		
	// output operator
	friend std::ostream& operator<<(std::ostream& os, map_stat_vec& MSV);
};

/*****************************END: mini all paths stat str ****************************/

/******************************** start node len str ******************************/

class start_node_len_vec {
private:
	
	std::mutex m;
	std::vector<unsigned int> V;
	
public:
	
	// push len in vec
	void push(unsigned int len);
	
	// sort in ascending order
	void sort();
	
	// output operator
	friend std::ostream& operator<<(std::ostream& os, start_node_len_vec& SLV);
};

/*****************************END: start node len str ****************************/


extern std::vector<std::string> Contig_Dir_String;

// TODO: add 5th param as custom w_base
// if cond and W >= W_base, print W
void conditional_time_print(bool cond, wall_time_wrap W, std::ofstream& os, std::string fn_name);


void read_file_to_vec(std::ifstream& infile, std::vector<std::string>& vec, unsigned long int max_iter);

/******************************** Debug functions, currently not in use*******************/

bool compare_kut_kumtt(std::vector<kmer_unitig_mapping_type>& KXK, KU_Table_Type& KU_Table, std::ofstream& log_file);

/***************************** End: Debug functions, currently not in use*****************/

void init_LP_map(DB_Node_Table_Type const & list_nodes, unsigned int k, std::vector<lmer_partition_map> & LP_map_,
 unsigned int nthreads, std::ifstream& partition_file_, std::ofstream & stat_file, std::ofstream& log_file_, unsigned int& walk_info_level_);


#endif // MISC_H
