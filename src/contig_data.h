#ifndef CONTIG_DATA_H
#define CONTIG_DATA_H

#include "all_classes.h"
#include "misc.h"

// arbit assignments
// #define FW 10
// #define REV 20

class black_dist {
public:
	unsigned int lmer_id;
	unsigned int dist;
};

// structure useful in glueing forward and reverse contigs (in serial mode)
class fr_contig_glue_str {
public:
	unsigned int src_contig_id;
	unsigned int tar_contig_id;
	unsigned int overlap_length;
	unsigned int overlap_node_count;
	
	// functions
	size_t size() {return sizeof(fr_contig_glue_str);}
};
std::ostream& operator<<(std::ostream& os, fr_contig_glue_str& FR);


// metadata associated with each contig
class contig_data {
public:
	// TODO: more information (unique label, contig part) to identify parts of same contig
	
	// vars
	
	std::string contig;
	bool no_neigh;
	std::vector<unsigned int> white_node_vec;
	unsigned int white_node_count;
	int  partition_id;
 //	std::vector<black_dist> black_dist_vec;
	
	// vars for multi piece stitching
	
	// contig id
	unsigned int contig_label;
	
	// piece number of contig
	unsigned int contig_part;
	
	// FW or REV
	contig_dir d;
	
	// functions
	
	contig_data() : contig(""), no_neigh(false), white_node_vec(0), white_node_count(0), partition_id(-1), contig_label(std::numeric_limits<unsigned int>::max()),
		contig_part(std::numeric_limits<unsigned int>::max()), d(FWD) {}
	
	contig_data(contig_data const & other) = default;
	contig_data(contig_data && other) = default;
	contig_data& operator=(contig_data const & other) = default;
	contig_data& operator=(contig_data && other) = default;

	size_t dynamic_size();
	size_t size() {return sizeof(contig_data) + dynamic_size();}
};
std::ostream& operator<<(std::ostream& os, contig_data& CD);

// sorting comparator for multi piece stitching of contig (only for parallel graph walk)
struct multi_piece_comparator {
	inline bool operator() (const contig_data& C1, const contig_data& C2) {
		if (C1.contig_label not_eq C2.contig_label) {
			return C1.contig_label < C2.contig_label;
		}
		if (C1.d not_eq C2.d) {
			return C1.d < C2.d;
		}
		assert(C1.contig_part not_eq C2.contig_part);
		return C1.contig_part < C2.contig_part;
	}
};

std::ostream& operator<<(std::ostream& os, contig_data& C);

// metadata for all the contigs
class contig_data_str {
public:
	// vars
	
	std::vector<contig_data> data;
	std::vector<fr_contig_glue_str> fr_contig_glue_vec;
	unsigned int isolated_contigs=0;
 	
	// functions
	
	// push contig data in serial mode
	void push_contig_data(const currContStr & CCS_, int  partition_id_);
	
	// push contig data in job mode
	void push_contig_data(Walk_Queue& WQ, const job_str *curr_job_ptr, unsigned int contig_id);
	
	// TODO: more generic function, that stitches parts of same contig ....... should produce same result as merge_fw_rev_contigs when only one fw and rev part
	// merge fw and reverse contigs
	void merge_fw_rev_contigs();
	
	// stitch pieces of contigs produced by different threads (only for parallel walk)
	void stitch_contig_pieces(unsigned int k);
	
	// remove data entries that have empty white vec
	void remove_empty();
	
	// sort data in descending order of contig length
	void sort_data();
	
	void write_contigs(unsigned int min_len, std::ofstream& os);
	
	size_t dynamic_size();
	size_t size() {return sizeof(contig_data_str) + dynamic_size();}
	
};

std::ostream& operator<<(std::ostream& os, contig_data_str& C_vec);

#endif // CONTIG_DATA_H