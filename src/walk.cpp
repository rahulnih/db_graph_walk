#include "walk.h"
#include <exception>

/**************************PRIMARY TODO LIST
 * 2. When walking forward, save the initial coverage somewhere, which can be used for reverse walking as well
 * 3. When ending a contig, save the dist const left somewhere. It will be used for creating longer contigs with N's in between (OR, just continue the walk by inserting few N's till you see some dist constraints)
 * 5. Read errors and read mutations
 * 6. In clear_buffer, add a sanity check that for all nodes, ends and starts should be opposite (DONE), and others if you can think of
 * 7. Remove s_ed
 * 8. Remove prints and do operator<< everywhere
 * 10. Add a separate dist_const_usage_left for intra read, revise the coverage calculation model. Also check usage before putting dist const
 * 15. Try to update node_usage_left and dist_const_usage_left on the spot, instead of later
 * 16. When a long lmer encountered, take it's frequency to be the coverage
 * 17. If a large number of nodes want to see a particular node, that counts for something (even if the total count is less than another node whose count is more, but has less variety of source nodes)
 * 18. Get rid of 5th entry in prev_neigh and next_neigh
 * 19. Put all the knob vars and functions at one place
 * 20. Check what happens to unupdated usage str when contig is being wrapped up.
 * 22. Put '$' at the end of path if no neighbors, they won't participate in post walk
 * 23. Calculate stats: how many times mini_all_paths called
 * 26. Add desctuctors
 * 27. Check for places to make long int
 * 28. change counters to size_t
 * 29. Create assertions.h/cpp to include all assertions, and a flag to turn them on
 * 30. Change size to memsize
 * 31. Fix reading_dir error
 * 32. while traversing, automatically calculate score diff threshold based on coverage
 * 33. Streamingly modify mu and sigma for insert size, by real observations everytime you make a decision.
 * 34. for scoring function, put params for different distributions
 * 35. In mini all paths, see if two strong nodes are actually the same, and if yes, it's a bubble, take one.
 * 36. While scoring options, if two nodes have different lengths, expand the shorter one to make length equal sort off, and then score two paths.
 * 37. Bubble traversal
 * *****************************************/

/**************************SECONDARY TODO LIST
 * 1. Remove automatic params from config file
 * 2. Error buffer, put last n entries in error buffer and print if error occurs
 * *****************************************/

/************* functions of class DB_Node_Table_Type **************/

// go through the list_nodes, and transform the dist const from target kmer to target lmer
void DB_Node_Table_Type::Transform_dist_const(KU_Table_Type & Table, unsigned int k) {
	
	for (std::size_t i=0; i<DB_Node_Table.size(); i++) {
		DB_Node_Type & D = DB_Node_Table.at(i);
		
		for (std::size_t j=0; j<D.Dist_Const_List.size(); j++) {
			
			Dist_Type& curr_dc = D.Dist_Const_List[j];
			
			KU_TableEntry const & r = Table.search_table(curr_dc.tar_kmer);
			curr_dc.lmer_id = r.list_nodes_index;
			
			// first see if the target kmer is actually a junction node, if yes, nothing more to be done
			if (not isUnitig(DB_Node_Table.at(r.list_nodes_index))) {
				continue;
			}
			
			/*******************************
			* assume now that the lmer is a unitig
			* r.first is a reference to the KU_TableEntry for the tar_kmer.
			* r.second is the index in KU_Table where that entry is
			* now transform target, that will use pos from KU_TableEntry and e_dir and e_ed from the dist const entry itself
			* separate out e_dir and e_edge
			*************************************/
			
			unsigned int e_edge = curr_dc.end & 0x7;
			unsigned int e_dir = (curr_dc.end - e_edge) >> 3;
			unsigned int pos = r.pos;
			unsigned int l = r.lmer_length;
			
			// if the tar_kmer is on the positive strand of the corresponding lmer
			if (r.PositiveLmer) {
				// bring at the beginning of the positive lmer
				e_edge = FindUnitigEdge(DB_Node_Table.at(r.list_nodes_index), e_dir);
				if (e_dir==0) {
					// no change in e_dir
					curr_dc.distance -= pos;
				}
				// bring at the end of the positive lmer
				else if (e_dir==1) {
					// again no change in e_dir
					curr_dc.distance -= l-k-pos;
				}
				else {
					std::cout << "Incorrect e_dir" << std::endl;
					exit(0);
				}
			}
			// if the target kmer is on the negative strand
			else {
				// bring at the end of the positive lmer
				e_edge = FindUnitigEdge(DB_Node_Table.at(r.list_nodes_index), 1-e_dir);
				if (e_dir==0) {
					curr_dc.distance -= pos;
				}
				// bring at the beginning of the positive lmer
				else if (e_dir==1) {
					curr_dc.distance -= l-k-pos;
				}
				else {
					std::cout << "Incorrect e_dir" << std::endl;
					exit(0);
				}
				e_dir=1-e_dir;
			}
			curr_dc.end = (e_dir << 3) + e_edge;
			// at this point, tar_kmer has become irrelevant
		}
	}
	for (std::size_t i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type & D=DB_Node_Table.at(i);
		std::sort(D.Dist_Const_List.begin(), D.Dist_Const_List.end(), lmer_comparator());
	}
}

void Walk_Node_Element::revWalkInit(reverse_seed_structure& R)
{
	src_lmer_id = MAX_UINT; // irrelevant
	tar_lmer_id = R.lmer_id;
	error=0;
	end = (R.end + 8) & 0xF;  // %16
	is_intra_read = false; // irrelevant
	color = WHITE;
	revWaiver=true;
}



/************* END: functions of class DB_Node_Table_Type **************/

Traversal_support_str::Traversal_support_str(Walk_Queue& WQ)
{
	saved_base_num_dc=0;
	hanging_seq="";
	last_update_pos = WQ.beg;
	start_of_walk = true;
}

void Traversal_support_str::reset()
{
	saved_base_num_dc=0;
	hanging_seq="";
	last_update_pos = 0; 
	start_of_walk = true;
}


size_t Traversal_support_str::dynamic_size()
{
	return  hanging_seq.size();
}


/************************* currContStr functions******************/

currContStr::currContStr()
{
	no_neigh=false;
	fw_contig_id = MAX_UINT;
	white_node_count = 0;
}


void currContStr::reset()
{
	no_neigh=false;
	curr_contig.clear();
	white_node_vec.clear();
	white_node_count = 0;
}


/*********************** END:currContStr functions***************/
/************************* revWalkStr functions******************/

revWalkStr::revWalkStr()
{
	walkRev = false;
	revSeedLen = 0;
	revSeedCount=0;
}

void revWalkStr::reset()
{
	revSeedVec.clear();
	revSeedLen = 0;
	revSeedCount = 0;
	walkRev=false;
}


/*********************** END:revWalkStr functions*****************/


void decidedHopStr::reset()
{
	hops.clear();
	decided_dc_inter.clear();
	decided_dc_intra.clear();
}

std::size_t decidedHopStr::dynamic_size()
{
	return 	vecsize(hops) 
			+ vecsize(decided_dc_inter) 
			+ vecsize(decided_dc_intra);
}


/************************* Walk_Queue functions******************/

// Walk_Queue constructor
Walk_Queue::Walk_Queue(Params* P, 
						DB_Node_Table_Type const & list_nodes, 
						KU_Table_Type const & KU_Table,  
						contig_data_str& CDS, 
						std::vector<lmer_partition_map> const & LP_map,
						outfile_str& ostr,
						Walk_Base *ptr) : 	walk_info_level_(P->walk_info_level), 
										log_file_(ostr.log_file)
						/*Walk_Base(P, list_nodes, KU_Table, log_file), CDS_(CDS)*/ 
						{
									
	if (P == nullptr) {
		throw ::std::invalid_argument("Params ptr is null");
	}
	
	if (ptr == nullptr) {
// 		WBptr = new Walk_Base(P, list_nodes, KU_Table, CDS, LP_map, ostr);
		throw ::std::invalid_argument("Walk_Base ptr is null.");
	}
	else {
		WBptr = ptr;
	}
	
	thread_id = omp_get_thread_num();
	if (thread_id==0 and walk_info_level_ >= 2) {
		log_file_ << "walk queue constructor" << std::endl;
	}
	
	// init vars
	beg=0;
	curr=0;
	print_level=0;
	mini_all_paths_mem=0;
	sel_start_node_resume=0;
	if (P->min_start_node_len < 0) {
		min_start_node_len=P->insert_size;
	}
	else {
		min_start_node_len = P->min_start_node_len;
	}

	TSS = Traversal_support_str(*this);
	

	//WBptr->estimate_parameters();    // move out of OMP parallel region.
	
	if (thread_id==0) {
		log_file_ << "magic_ratio: " << WBptr->magic_ratio << std::endl;
	}
	
	unsigned int buf=WBptr->walk_queue_size;
	Q.resize(buf);
	
	t_log_file_ptr = nullptr;
	
	// for parallel case, open the thread log files
	if (P->wt==SER) {
		t_log_file_ptr = &(ostr.log_file);
	}
	else if (P->wt==PAR) {
		t_log_file_ptr = new std::ofstream();
		(*t_log_file_ptr).open(("log_dir/" + P->log_file + "_t" + std::to_string(thread_id) + ".txt").c_str());
		if (not (*t_log_file_ptr).is_open()) {
			std::cerr << "Unable to open thread log file " << thread_id << std::endl;
			delete t_log_file_ptr;
			exit(0);
		}
	}
	
	SNHptr = new select_next_hop_class(*this);
	
#pragma omp parallel for
	for (std::size_t i=0; i<buf; i++) {
		Q[i].color=GREEN;
	}
	
	dirty_index_vec_dcul.clear();
	dirty_index_vec_nul.clear();
	
	if (thread_id==0 and walk_info_level_ >= 2) {
		log_file_ << "DONE walk queue constructor" << std::endl;
	}
	
// 	scin_ptr = nullptr;
	
} // END Walk_Queue constructor

size_t Walk_Node_Element::dynamic_size()
{
	return 	vecsize(freq_range) 
			+ vecsize(sat_const_vec_back) 
			+ vecsize(sat_const_vec_fw);
}


void Walk_Queue_Element::clear() {
	candidate_nodes.clear();
	color = GREEN;
	white_index = MAX_UINT;
}

size_t Walk_Queue_Element::dynamic_size()
{
	size_t bytes=0;
	for (size_t i=0; i < candidate_nodes.size(); i++) {
		bytes += candidate_nodes[i].size();
	}
	return bytes;
}

/**
 * @brief ... MAIN WALKING FUNCTION
 * walk db graph to form contigs and write in the output file
 * 
 * @return void
 */
void Walk_Queue::walk_db_graph() {
	
	// initiate sorted_lmer_id_vec, to enable picking starting nodes
	WBptr->init_lmer_id_vec();
	
	wall_time_wrap W;
	W.start();
	
	// main contig loop, each iteration produces a contig
	unsigned long int out_num_iterations=0;
	while(true) {
		// clear the queue for producing a new contig
		clear();
		CCS.curr_contig="";
		
		// find a starting node for the walk
		unsigned int start_index = select_start_node();
		if (start_index==MAX_UINT) {
			break;
		}
		
		// picked the index, now start the walk
		log_msg_8(out_num_iterations);
		
		// push the starting node in the walk queue, AND ALSO SET THE WHITE INDEX????
		push_start_node( start_index );
		
		// queue initiated, now start the walk loop
		unsigned long int num_iterations=0;
		
		while (true) {
			
			log_msg_9(num_iterations);
			
			Walk_Queue_Element& current_WQE = CurrElement();
			
			// set s_dir for all WHITE nodes in current_WQE based on end
			set_sdir( current_WQE );
			
			// check if there is enough space for further nodes
			buff_check(current_WQE, WBptr->walk_queue_preserve_history);
			
			// add neighbors of curr WHITE node, and make them BROWN
			unsigned int neigh_count = set_neigh_nodes( current_WQE );
			
			// add dist const of curr WHITE node, and make them BLACK
			unsigned int m = 0;
			if (neigh_count not_eq 0) {
				m = set_next_dist_const( current_WQE );
			}
			
			log_msg_10(m, num_iterations, current_WQE);
			
			// move the current pointer to the next BROWN list
			move_pointer( current_WQE );
			
			// again get the current element after the pointer has been moved
			Walk_Queue_Element& next_WQE = CurrElement();
			
			// go through the BROWN list and select the next hop, make it WHITE
			hop_return H;
			
			H = SNHptr->select_next_hop( next_WQE, neigh_count );
			
			log_msg_11(num_iterations);
			
			// if nothing selected, either reverse walk or quit
			if (H==NO_HOP) {
				
				update_contig_info();
				reset_vars();
				
				// if the contig just produced is forward, set up the queue for reverse walking
				if (WBptr->P_->enabRevWalk and (not RWS.walkRev)) {
					if (walk_info_level_ >= 2) {
						*t_log_file_ptr << "Setting up the queue for reverse walking" << std::endl;
					}
					
					// do what you got to do to set up reverse walking
					reverse_walking_init();
					
					// oh yeah, now we walking reverse
					RWS.walkRev=true;
					if (walk_info_level_ >= 2) {
						print_snapshot(0, Q.size()-1, *t_log_file_ptr);
					}
				}
				else{
					square_zero_init();
					
					break;
				}
			} // END: if (not hop_selected) {
			
			if (num_iterations++ > WBptr->P_->max_iterations) {
				std::cerr << "Iterations exceeded maximum limit in walk_db_graph inner loop" << std::endl;
				exit(0);
			}
		} // end: inner while
		if (out_num_iterations++ > WBptr->P_->max_iterations) {
			std::cerr << "Iterations exceeded maximum limit in walk_db_graph outer loop" << std::endl;
			break;
		}
	}// end: outer while
	
	W.stop();
	std::cout << "Pure walk time: " << W << std::endl;
	
	log_msg_12();
	
	// initial contigs produced, now merge fw and rev contigs
	WBptr->CDS_.merge_fw_rev_contigs();
	
	WBptr->print_start_node_stats();
	
	WBptr->print_score_diff_stats();
	
	WBptr->print_map_stats();
	
	WBptr->ostr_.bub_file << WBptr->BSV;
	
	// update visit_counts in list_nodes, just for analysis purpose
	for (std::size_t i=0; i < WBptr->CDS_.data.size(); i++) {
		std::vector<unsigned int>& white_vec_ref = WBptr->CDS_.data[i].white_node_vec;
		for (std::size_t j=0; j < white_vec_ref.size(); j++) {
			unsigned int lmer_id = white_vec_ref[j];
			DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(lmer_id);
			D.visit_count++;
		}
	}
	
	// sort the contigs, and white_node_vec_vec by decreasing size
	WBptr->CDS_.sort_data();
	
	log_msg_13();
	
} // END walk_db_graph()

// select a starting node to walk the graph, or else return the max unsigned int
unsigned int Walk_Queue::select_start_node () {
	
	atomic::atomic_array<long int> & exp_node_usg_left_vec = WBptr->UD.expected_node_usage_left;
	
	// we want to preferably select long lmers, which are at the end of the list, that's why going in reverse direction
	for (std::size_t i=sel_start_node_resume; i < WBptr->sorted_lmer_id_vec.size() ; ) {
		unsigned int index = WBptr->sorted_lmer_id_vec[i];
		DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(index);
		
		// default
		bool negative_exists = true;
		
		// if neg const has to be enforced, then enforce it
		if (WBptr->P_->enforce_neg_const) {
			// test if there is at least one negative dist const
			negative_exists = false;
			for (std::size_t j=0; j < D.Dist_Const_List.size(); j++) {
				if (D.Dist_Const_List[j].distance < 0) {
					negative_exists = true;
				}
			}
		}
		
		if (!negative_exists || (D.len_lmer < int(min_start_node_len))) {
			++i;
			continue;
		}

		long exp_usg_left_entry = exp_node_usg_left_vec[index].load();
		if (exp_usg_left_entry <= 0) {
			++i;
			continue;
		}

		exp_covg = WBptr->init_expected_coverage(index);

		// reduce to zero
		long new_val = exp_usg_left_entry > (long int)exp_covg ? exp_usg_left_entry - (long int)exp_covg : 0;
		if (exp_node_usg_left_vec[index].compare_exchange_strong(exp_usg_left_entry, new_val) ) {

			if (walk_info_level_ >= 2) {
				*t_log_file_ptr << "Starting node " << index << " selected" << std::endl;
			}
			
			if (WBptr->P_->print_start_node_stats) {
				WBptr->SLV.push((unsigned int)D.len_lmer);
			}
			
			CCS.white_node_vec.push_back(index);
			
			// update dirty index
			dirty_index_vec_nul.push_back(index);
			
			sel_start_node_resume = i;
			
			// return
			return index;
			
		}  // else repeat the same iteration.

	}
	return MAX_UINT;
}

// put the first node in the start queue, which will get things started
inline void Walk_Queue::push_start_node(unsigned int start_index, int queue_index /*= 0*/) {
	DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(start_index);
	
	// you want to start with at least one negative dist const (if it exists), to well oil the machine. 
	// Find a negative dist const.
	std::size_t neg_dist_const_index= MAX_SIZE_T;
	for (std::size_t i=0; i < D.Dist_Const_List.size(); i++) {
		if (D.Dist_Const_List[i].distance < 0) {
			neg_dist_const_index = i;
			break;
		}
	}
	
	// if we are enforcing neg dist const, then we must have found one
	if (WBptr->P_->enforce_neg_const) {
		assert(neg_dist_const_index != MAX_SIZE_T);
	}
	
	// create the first node and push in the walk queue
	Walk_Node_Element WNE;
	
	// who put me here
	WNE.src_lmer_id = MAX_UINT;
	
	// who am I
	WNE.tar_lmer_id = start_index;
	
	WNE.error=0;
	
	// set the e_dir opposite to s_dir in the DB node, so that the start would be set accordingly (in set_sdir) and would be sure to encounter at least one negative dist const
	if (neg_dist_const_index==MAX_SIZE_T) {
		WNE.end = 0;
	}
	else if ((D.Dist_Const_List[ neg_dist_const_index ].start & 0x8)==0x8) {
		WNE.end = 0; // no particular preference for end
	}
	else {
		WNE.end = 8;
	}
	WNE.is_intra_read = false; // some default value
	// intention to copy the data, not just pointer assignment
	WNE.freq_range = D.freq_range;
	WNE.color = WHITE; 
	// just entering the queue
	EntryPush(WNE,queue_index);
	// all occupied entries are red
	Q[jump_ahead(queue_index)].color = RED;
	Q[jump_ahead(queue_index)].white_index = 0;
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr 	<< "Pushed node " << start_index 
					<< " at queue position " << queue_index 
					<< std::endl;
	}
} // END push_starting_node()

// for all WQE nodes, set the s_dir based on end
void Walk_Queue::set_sdir (Walk_Queue_Element& WQE) {
		
// 	ASSERT_WITH_MESSAGE(WQE.white_index not_eq MAX_UINT, 
// 						"\nError in set_sdir: No white node");
	if (WQE.white_index == MAX_UINT) {
		std::cerr << "Error in set_sdir: No white node" << std::endl;
		if (WBptr->P_->wt == PAR) {
			std::cerr << "Thread id: " << thread_id << std::endl;
			exit(0);
		}
	}
	
	if ((WQE.candidate_nodes[ WQE.white_index ].end & 0x8) == 0) {
		// ending dir is 0, so setting s_dir to 1
		WQE.candidate_nodes[ WQE.white_index ].start = 8;
	}
	else if ((WQE.candidate_nodes[ WQE.white_index ].end & 0x8) == 0x8) {
		// ending dir is 1, so setting s_dir to 0
		WQE.candidate_nodes[ WQE.white_index ].start = 0;
	}
	else {
		std::cerr << "Error in set_sdir: Incorrect end in the candidate nodes" << std::endl;
		exit(0);
	}
	
	log_msg_17(WQE);
	
}

// check if there is enough buffer for further dist constraints. If not, clear the buffer and write curr seq etc
void Walk_Queue::buff_check(Walk_Queue_Element& WQE, int shift ) {
	wall_time_wrap W;
	W.start();
	
	log_msg_14();
	

	// first check the buffer space left
	unsigned int empty_space;
	if (curr >= beg) {
		empty_space = (unsigned int)(Q.size() - (unsigned int)(curr - beg));
	}
	else {
		empty_space = beg - curr;
	}
	
	// calculate the space needed
	ASSERT_WITH_MESSAGE(WQE.white_index not_eq MAX_UINT, 
						"\nError in buff_check, no white index for current candidate");
	
	DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(
						WQE.candidate_nodes[ WQE.white_index ].tar_lmer_id);
	
	int max_dist=0;
	// go over all the distance constraints in appropriate direction
	for (std::size_t j=0; j < D.Dist_Const_List.size(); j++) {
		
		// check the direction, if direction appropriate, create an entry and push to the queue
		if ((D.Dist_Const_List[j].start & 0x8) == (WQE.candidate_nodes[ WQE.white_index ].start & 0x8)) {
			if (D.len_lmer - 1 + D.Dist_Const_List[j].distance > max_dist) {
				max_dist = D.len_lmer - 1 + D.Dist_Const_List[j].distance;
			}
		}
	}
	unsigned int space_needed = (unsigned int)max_dist;
	
	// if the there is sufficient empty space, return
	if (empty_space > std::max((unsigned int)WBptr->walk_queue_low_size, space_needed)) {
		if (walk_info_level_ >=2) {
			*t_log_file_ptr << "OK, " << empty_space << " number of slots left" << std::endl;
		}
		return;
	}
	
	// else, clear up the queue and reset the beg
	log_msg_15(empty_space, space_needed);
	
	// end position in the buffer till which clearence has to be done
	unsigned int end_pos = (unsigned int)((int)curr - shift)%Q.size(); 
	
	// call the clearing function
	clear_buffer(beg, end_pos);
	
	beg = end_pos;
	
	log_msg_16(end_pos);
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "BUFF CHECK");
} //end buff_check


// set neigh nodes in walk function, set as BROWN
unsigned int Walk_Queue::set_neigh_nodes(Walk_Queue_Element& WQE) {
	
	wall_time_wrap W;
	W.start();
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\nSET NEIGH NODES\n" << std::endl;
	}
	
	// find out the WHITE size, it should be equal to 1
	assert(WQE.candidate_nodes.size()>0);
	unsigned int white_index=MAX_UINT;
	unsigned int white_size = count_color(WQE, WHITE, white_index);
	ASSERT_WITH_MESSAGE(white_size==1, "Error in set_neigh_nodes: white_size==1 unsatisfied");
	ASSERT_WITH_MESSAGE(white_index not_eq MAX_UINT, 
						"\nError in set_neigh_nodes: white_size = " + std::to_string(white_size));
	
	// find current white node
	DB_Node_Type const & white_db_node = WBptr->ref_db_node_table.ElementAt(
									WQE.candidate_nodes[ white_index ].tar_lmer_id);
	
	// find neighbors of the current white node
	std::vector<Walk_Node_Element> neighbors;
	
	// int because subtracted from int
	int k = WBptr->P_->k;
	
	// check the white node size against the queue space
	ASSERT_WITH_MESSAGE(white_db_node.len_lmer - (k-1) < (signed)WBptr->walk_queue_low_size 
						and white_db_node.len_lmer - (k-1) > -(signed)WBptr->walk_queue_preserve_history, 
						"Error in set_neigh_nodes, brown distance more than it should be, vals: len_lmer: " 
						+ std::to_string(white_db_node.len_lmer) 
						+ ", lmer id: " + std::to_string(WQE.candidate_nodes[ white_index ].tar_lmer_id));
	
	// get neighbors
	unsigned int neigh_count = get_neighbors(neighbors, white_db_node, WQE.candidate_nodes[ white_index ].start);
	
	// push all neighbors at appropriate distance
	for (unsigned int i=0; i < neighbors.size(); i++) {
		EntryPush(neighbors[i], white_db_node.len_lmer - (k-1));
		if (walk_info_level_ >=2) {
			*t_log_file_ptr << "Pushing candidate node " << neighbors[i].tar_lmer_id 
					<< " at " << white_db_node.len_lmer - (k-1) 
					<< " positions ahead with end as " << std::bitset<8>(neighbors[i].end) << std::endl;
		}
	}
	
	// now make the space RED
	for (int i=0; i<=white_db_node.len_lmer - (k-1); i++) {
		unsigned int pos = jump_ahead(i);
		Q[pos].color=RED;
	}
	
	neighbors.clear();
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "END SET NEIGH NODES" << std::endl;
	}
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "SET NEIGH NODES");
	return neigh_count;
} // END: set_neigh_nodes()

// TODO: CHECK IF THE SPACE IS GREEN, AND MAKE IT RED AFTER USING
// For all good nodes in WQE, set further dist constraints
unsigned int Walk_Queue::set_next_dist_const(Walk_Queue_Element& WQE) {
	wall_time_wrap W;
	W.start();
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\nSET NEXT DIST CONST\n" << std::endl;
	}
	
	// find out the WHITE size, it should be equal to 1
	assert(WQE.candidate_nodes.size()>0);
	unsigned int white_index=MAX_UINT;
	unsigned int white_size = count_color(WQE, WHITE, white_index);
	ASSERT_WITH_MESSAGE(white_size==1 and 
						white_index not_eq MAX_UINT, 
						"\nError in set_next_dist_const: white_size==1 unsatisfied, white_size = " 
						+ std::to_string(white_size));
	
	// white node at current pos
	Walk_Node_Element* curr_white_node_ptr;
	curr_white_node_ptr = &WQE.candidate_nodes[ white_index ];
	
	// white db node at curr pos
	DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(curr_white_node_ptr->tar_lmer_id);
	
	
	// used to color the space RED
	int max_dist=0;
	
	// go over all the distance constraints in appropriate direction
// #pragma omp parallel for
	for (std::size_t j=0; j < D.Dist_Const_List.size(); j++) {
		curr_white_node_ptr = &WQE.candidate_nodes[ white_index ];
		
		// if direction is appropriate, create an entry and push to the queue
		if ((D.Dist_Const_List[j].start & 0x8) == (curr_white_node_ptr->start & 0x8)) {
			
			// if not enough usage left for dist const, continue
			

			if (WBptr->UD.expected_dist_const_usage_left[curr_white_node_ptr->tar_lmer_id][j].load() <= 0) {
				continue;
			}
			
			// construct the Walk_Node_Element to be pushed in the walk structure
			Walk_Node_Element WNE;
			
			// set the WNE vars
			WNE.is_intra_read = D.Dist_Const_List[j].is_intra_read();
			WNE.end = D.Dist_Const_List[j].end;
			WNE.src_lmer_id = curr_white_node_ptr->tar_lmer_id;
			WNE.tar_lmer_id = D.Dist_Const_List[j].lmer_id;
			// TODO: Carefully think how to handle error propagation
			WNE.error = D.Dist_Const_List[j].error;
			// this distance is used for tracking back the original node and updating the outgoing dist const if the node pushed turns out to be good
			WNE.distance = D.len_lmer - 1 + D.Dist_Const_List[j].distance;
			
// 			WBptr->UD.expected_dist_const_usage_left_atomic[WQE.candidate_nodes[ white_index ].tar_lmer_id][j].lock();
			WNE.freq = std::min(D.Dist_Const_List[j].freq, 
							(unsigned int)WBptr->UD.expected_dist_const_usage_left[
								WQE.candidate_nodes[ white_index ].tar_lmer_id
														][j].load());
// 			WBptr->UD.expected_dist_const_usage_left_atomic[WQE.candidate_nodes[ white_index ].tar_lmer_id][j].unlock();
			
			// intention to copy the data, not just pointer assignment
			WNE.freq_range = D.freq_range;
			WNE.color = BLACK;
			
			int dc_len = D.len_lmer - 1 + D.Dist_Const_List[j].distance;
			int WQ_low_size = (signed)WBptr->walk_queue_low_size;
			int WQ_pres_hist = (signed)WBptr->walk_queue_preserve_history;
			
			// make sure of enough buffer space
			ASSERT_WITH_MESSAGE(dc_len < WQ_low_size and dc_len > -WQ_pres_hist, 
								"Error in set_next_dist_const, black distance more than it should be, vals: len_lmer: " + std::to_string(D.len_lmer) 
								+ ", distance const: " + std::to_string(D.Dist_Const_List[j].distance) 
								+ ", lmer id: " + std::to_string(WQE.candidate_nodes[ white_index ].tar_lmer_id));
			
			EntryPush(WNE, D.len_lmer - 1 + D.Dist_Const_List[j].distance);
			
			if (walk_info_level_ >=2) {
				*t_log_file_ptr 	<< "Pushing dist const " << WNE.tar_lmer_id 
							<< " at " << D.len_lmer - 1 + D.Dist_Const_List[j].distance 
							<< " positions ahead " 
							<< std::endl;
			}
			
			// update max_dist
			if (WNE.distance > max_dist) {
				max_dist = WNE.distance;
			}
		}
	}
	
	// now color the space RED
	for (int i=0; i <= max_dist; i++) {
		unsigned int pos = jump_ahead(i);
		Q[pos].color = RED;
	}
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "END SET NEXT DIST CONST" << std::endl;
	}
	ASSERT_WITH_MESSAGE(max_dist >= 0, 
						"Error in set_next_dist_const, max_dist coming out to be negative");
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "SET NEXT DIST CONST");
	return (unsigned int)max_dist;
} //END: set_next_dist_const()

// move the current pointer to the next brown list
void Walk_Queue::move_pointer(Walk_Queue_Element& WQE) {
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\nMOVE POINTER" << std::endl;
	}
	
	// assert that there is a white node at current pos
	ASSERT_WITH_MESSAGE(WQE.white_index not_eq MAX_UINT, 
						"\nError in move_pointer, white index not set");
	
	// white node at current pos
	Walk_Node_Element& curr_white_node = WQE.candidate_nodes[ WQE.white_index ];
	
	// length of the current white node
	int l = WBptr->ref_db_node_table.ElementAt(curr_white_node.tar_lmer_id).len_lmer;
	l = l - (WBptr->P_->k-1);
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\nMoving curr from " << curr << " to " << jump_ahead(l) << std::endl;
	}
	
	// move the pointer
	set_curr(jump_ahead(l));
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\nEND MOVE POINTER" << std::endl;
	}
}



// clear buffer from beg to end (not including end), update the the curr_contig and the node usage
void Walk_Queue::clear_buffer(unsigned int beg, unsigned int end) {
	wall_time_wrap W;
	W.start();
	
	/************ calculate a coverage estimate ***************/
	print_level++;
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr 	<< std::string(print_level, '\t') 
					<< "IN clear_buffer..." 
					<< std::endl;
	}	
	// data structure for clear_buffer
	clear_buffer_data CBD(*this);
	
	// first find out a base coverage
	CBD.calculate_base_coverage(beg, end, *this);
		
	// go through each white node, update usage and overlap_lmers vector
	unsigned int iterations=0;
	for (unsigned int pos = beg; pos not_eq end; pos = (pos+1)%Q.size()) {
		
		// fetch the current white node
		Walk_Queue_Element& Wq = getElement(pos);
		if (Wq.white_index==MAX_UINT) {
			continue;
		}
		Walk_Node_Element& Wn = Wq.candidate_nodes[ Wq.white_index ];
		
		// update the node usage and dist const usage
		CBD.update_usage(Wn, *this);
		
		CCS.white_node_count++;
		
		// put the white node in overlap_lmers
		CBD.update_overlap_lmers(Wn, *this);
		
		// if this is a forward walk and the reverse seed not sufficient, update it
		CBD.update_reverse_seed_str(Wn, *this);
		
		// something going wrong, quit
		if (iterations++ > WBptr->P_->max_iterations) {
			std::cout << "\nIterations exceeded maximum limit in buff_check while coverage update" << std::endl;
			exit(0);
		}
			
	} // end: for (unsigned int pos = beg; pos not_eq end; pos = (pos+1)%Q.size())
		
	// update the expected structures
	WBptr->UD.equate_dist_const_vec(dirty_index_vec_dcul, dirty_index_vec_nul);
	
	// basic sanity check, also update hanging seq
	CBD.update_hanging_seq_n_sanity_check(*this);
	
	std::string & curr_contig = CCS.curr_contig;
	
	// check the contig max limit
	ASSERT_WITH_MESSAGE(curr_contig.size() + Q.size() < curr_contig.max_size(), 
						"\nError in buff_check: Cannot update contig, too close to max capacity");
	
	// use overlap lmers to update current contig
	CBD.update_current_contig(curr_contig, walk_info_level_);
	
	// clear walk queue
	for (unsigned int pos = beg; pos not_eq end; pos = (pos+1)%Q.size()) {
		Q[ pos ].clear();
	}
	
	// wrap up
	CBD.clear(*this);
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') << "DONE clear_buffer" << std::endl;
	}
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "CLEAR BUFFER");

	print_level--;
} // end clear_buffer()

// after producing a contig, update some info about it
void Walk_Queue::update_contig_info() {
	
	wall_time_wrap W;
	W.start();
	
	WBptr->contig_count++;
	
	log_msg_1();
	
	// white nodes can be zero only in reverse contig
	assert(RWS.walkRev or CCS.white_node_count!=0);
	
	WBptr->CDS_.push_contig_data(CCS, thread_id);
	
	// if false, then first param has no relevance
	if (WBptr->P_->enabRevWalk) {
		if (RWS.walkRev) {
			assert (CCS.fw_contig_id < WBptr->contig_count - 1);
			
			fr_contig_glue_str F;
			
			F.src_contig_id = CCS.fw_contig_id;
			F.tar_contig_id = WBptr->contig_count-1;
			F.overlap_length = RWS.revSeedLen;
			F.overlap_node_count = RWS.revSeedCount;
			
			WBptr->CDS_.fr_contig_glue_vec.push_back(F);
			
			CCS.fw_contig_id = MAX_UINT;
		}
		else {
			assert(CCS.fw_contig_id==MAX_UINT);
			CCS.fw_contig_id = WBptr->contig_count - 1;
		}
	}
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "UPDATE CONTIG INFO");
}

// after producing contig (fw or rev), reset vars for the next walk;
void Walk_Queue::reset_vars() {
	
	// reset variables
	beg=0;
	curr=0;
	
	CCS.reset();
	TSS.reset();
	DHS.reset();
	
	// clear up the entire queue
	clear();
} // end reset_vars

// after producing a forward contig, set up the queue for reverse walk
inline void Walk_Queue::reverse_walking_init() {
	//TODO: initiate queue for reverse walking
	
	ASSERT_WITH_MESSAGE(RWS.revSeedVec.size()==RWS.revSeedCount, "");
	
	log_msg_2();
	
	// set the seed nodes, but in reverse. Also set the reverse_waiver to be true
	// pipeline, for the nodes, skip buff_check (assumption that buff is large enough that it won't fill up during this), set_neigh_nodes, select_next_hop.
	for (int i=RWS.revSeedVec.size()-1; i >=0; i--) {
		
		reverse_seed_structure& R = RWS.revSeedVec[i];
		
		Walk_Node_Element WNE;
		WNE.revWalkInit(R);
		
		EntryPush(WNE, 0);
		
		TSS.last_update_pos = curr;
		
		Walk_Queue_Element& current_WQE = CurrElement();
		current_WQE.white_index=current_WQE.candidate_nodes.size()-1;
		
		ASSERT_WITH_MESSAGE(current_WQE.candidate_nodes[current_WQE.white_index].color==WHITE, 
				    "Error in reverse_walking_init, white_index not set up properly");
		
		// don't move when the last white is being put, set_sdir wants that white
		// TODO: Check logic
		if (i != 0) {
			set_sdir(current_WQE);
			set_next_dist_const(current_WQE);
			move_pointer(current_WQE);
			exp_covg = R.coverage;
		}
		
		CCS.white_node_vec.push_back(R.lmer_id);
	}
	log_msg_3();

} //END: reverse_walking_init()

void Walk_Queue::square_zero_init() {
	
	// just finished with reverse walking (if enabled), now start producing the next contig
	RWS.reset();
	
	CCS.fw_contig_id=MAX_UINT;
}




/**
 * @brief ...given a node and a direction, find all the neighbors in that direction
 * 
 * @param neighbors ...vector in which all neighbors will be put
 * @param D ...node whose neighbors are needed
 * @param start ...direction in which neighbors are needed
 * @return unsigned int ...number of neighbors found
 */
unsigned int Walk_Queue::get_neighbors(
						std::vector<Walk_Node_Element>& neighbors, 
						DB_Node_Type const & D, char start) {
	
	print_level++;
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') << "IN get_neighbors..." << std::endl;
	}
	
	unsigned int k = WBptr->P_->k;
	
	// create the common k-1 string
	std::string common_mer;
	std::string common_mer_alt;
	
	// neighboring vector
	std::vector<unsigned int> neigh_vec;
	
	if ((start & 0x8) == 0x8) {
		neigh_vec = D.next_neigh;
		std::string const & strand = D.get_lmer();
		common_mer = strand.substr(D.len_lmer-(k-1), k-1);
	}
	else {
		neigh_vec = D.prev_neigh;
		// works because reverse vector also gives the complement vector
		std::reverse(neigh_vec.begin(), neigh_vec.end()-1);
		std::string const & strand = D.get_lmer();
		
		common_mer = strand.substr(0, k);
		reverse_complement(common_mer);
		common_mer = common_mer.substr(common_mer.size() - (k-1), k-1);
	}
	if (walk_info_level_ >= 3) {
		*t_log_file_ptr << std::string(print_level+1, '\t') 
				<< "common_mer: " << common_mer 
				<< std::endl;
	}
	
	ASSERT_WITH_MESSAGE(common_mer.size()==k-1, 
						"\nError in set_neigh_nodes, the common mer length is incorrect");
	
	unsigned int neigh_count=0;
	
	for (unsigned int i=0; i < neigh_vec.size()-1; i++) {
		if (neigh_vec[i]==0) {
			continue;
		}
		Walk_Node_Element WNE;
		WNE.src_lmer_id = MAX_UINT; // irrelevant
		// TODO: CHECK AGAIN IF CORRECT CHAR IS BEING APPENDED
		std::string search_candidate = common_mer + alphabet.substr(i, 1);
		if (walk_info_level_ >= 3) {
			*t_log_file_ptr << std::string(print_level+1, '\t') << "Search candidate: " << search_candidate << std::endl;
		}
		KU_TableEntry const & E = WBptr->ref_ku_table.search_table(search_candidate);
		WNE.tar_lmer_id = E.list_nodes_index;
		WNE.error = 0; 
		// not sure about e_ed
		if (E.PositiveLmer) {
			WNE.end = 0 + i;
		}
		else {
			WNE.end = 8 + i;
		}
		if (search_candidate.compare(E.kmer)!=0) {
			WNE.end = (WNE.end + 8) & 0xF; // %16;
		}
		WNE.is_intra_read = false; // irrelevant
		// put max frequency, to be used as coverage
		WNE.freq = D.freq_range[1]; // freq is irrelevant for brown node
		WNE.color = BROWN;
		
		neighbors.push_back(WNE);
		neigh_count++;
	}
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') << "DONE get_neighbors..." << std::endl;
	}
	print_level--;
	return neigh_count;
} // END: get_neighbors()

void Walk_Queue::gather_cand_support(
					std::vector<support_str>& support, 
					std::vector<dc_str>& dist_const,
					std::vector<support_str>& support_intra_read, 
					std::vector<dc_str>& dist_const_intra_read,
					Walk_Node_Element& current_WNE) {
	
	// basic sanity checks regarding parameters
	assert(support.empty());
	assert(support_intra_read.empty());
	assert(dist_const.empty());
	assert(dist_const_intra_read.empty());
	assert(current_WNE.in_dc_inter==0);
	assert(current_WNE.in_dc_intra==0);
	
	print_level++;
	log_msg_7(dist_const);
	
	const int & max_dist_err = WBptr->P_->max_dist_error;

	//go through the error range to gather evidence
	for (int j = -max_dist_err; j < max_dist_err; j++) {
		
		// position where evidence is being collected
		unsigned int pos = jump_ahead(j);
		Walk_Queue_Element& some_WQE = getElement(pos);
		
		// go through all the black nodes at the evidence position
		for (unsigned int m=0; m < some_WQE.candidate_nodes.size(); m++) {
			Walk_Node_Element& some_node = some_WQE.candidate_nodes[m];

			if (some_node.color==BLACK) {
				
				// if the node is a support for the current candidate
				if (match_evidence(some_node, current_WNE, j)) {
					
					// push in the support vector
					
					// check if the constraint is inter read. if yes, push info that will be useful for outgoing_dist_const and incoming_dist_const, and eventually for coverage
					
					if (some_node.is_intra_read) {
						
						support_intra_read.emplace_back(j, some_node.freq);
						
						// updating the candidate incoming_dist_const_intra_read
						current_WNE.in_dc_intra += some_node.freq;
						
						dist_const_intra_read.emplace_back(some_node.distance - j, some_node.freq);
					}
					// otherwise, update the intra_read data
					else {
						support.emplace_back(j, some_node.freq);
						
						// updating the candidate incoming_dist_const
						current_WNE.in_dc_inter += some_node.freq;
						
						dist_const.emplace_back(some_node.distance - j, some_node.freq);
					}
				}
			}
		}
	}
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') << "DONE gather_cand_support" << std::endl;
	}
	print_level--;
} // END: gather_cand_support()






unsigned int Walk_Queue::count_color(	Walk_Queue_Element& WQE , 
								char color, 
								unsigned int& color_index)
{
	unsigned int count=0;
	for (unsigned int i=0; i < WQE.candidate_nodes.size(); i++) {
		if (WQE.candidate_nodes[i].color==color) {
			count++;
			color_index=i;
		}
	}
	return count;
}


size_t Walk_Queue::dynamic_size()
{
	size_t bytes=0;
	
	for (size_t i=0; i < Q.size(); i++) {
		bytes += Q[i].size();
	}
	
	bytes += CCS.curr_contig.size()
			+ TSS.dynamic_size()
			+ vecsize(RWS.revSeedVec) 
			+ vecsize(CCS.white_node_vec) 
			+ DHS.dynamic_size()
			+ vecsize(dirty_index_vec_dcul) 
			+ vecsize(dirty_index_vec_nul);
	
	if (WBptr->P_->wt==SER) {
		bytes += WBptr->size();
	}
	bytes += mini_all_paths_mem;
	
	return bytes;
}








/**************************************** END: Walk_Queue functions**************************/




/******************************** operator overloads *********************/

std::ostream& operator<<(std::ostream& os, Walk_Node_Element& WNE) {
	os << "Src:" << WNE.src_lmer_id << "\tTar:" << WNE.tar_lmer_id << "\t";
	os << "err:" << WNE.error << "\t";
	os << "start:" << (WNE.start & 0x8) << "\tend:" << (WNE.end & 0x8) << "\t";
	os <<"dist:" << WNE.distance << "\tis_intra_read:" << BOOL(WNE.is_intra_read) << "\tfreq:" << WNE.freq << "\t";
	os << "in_dc_inter:" << WNE.in_dc_inter << "\t"; 
	os << "out_dc_inter:" << WNE.out_dc_inter << "\t";
	os << "in_dc_intra:" << WNE.in_dc_intra << "\t";
	os << "out_dc_intra:" << WNE.out_dc_intra << "\t";
	os << "sat_con" << WNE.sat_const_vec_fw << "\t" << RET_COLOR(WNE.color);
	os << std::endl;
	return os;
}

std::ostream& operator<<(std::ostream& os, Walk_Queue_Element& WQE) {
	os << "Color: " << RET_COLOR(WQE.color) << "\tWhite index: " << WQE.white_index << std::endl;
	for (unsigned int j=0; j < WQE.candidate_nodes.size(); j++) {
		os << "\t";
		os << WQE.candidate_nodes[j];
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, Walk_Queue& WQ) {
	WQ.print_snapshot(0, WQ.Q.size()-1, os);
	return os;
}

std::ostream& operator<<(std::ostream& os, reverse_seed_structure& RSD) {
	os << "lmer id: " << RSD.lmer_id << "\tend" << RSD.end << "\tcoverage" << RSD.coverage << std::endl; 
	return os;
}


std::ostream& operator<<(std::ostream& os, support_str& cdist) {
	os << "(d=" << cdist.distance << ", f=" << cdist.freq << ")";
	return os;
}


bool operator==(const support_str& lhs, const support_str& rhs) {
	if (lhs.distance == rhs.distance and lhs.freq == rhs.freq) {
		return true;
	}
	return false;
}

bool operator==(const Traversal_support_str& T1, const Traversal_support_str& T2)
{
	if (	T1.last_update_pos == T2.last_update_pos
		and T1.start_of_walk == T2.start_of_walk
		and T1.hanging_seq == T2.hanging_seq
		and T1.saved_base_num_dc == T2.saved_base_num_dc) {
			return true;
	}
	return false;
}


/************************* All class print functions******************/


// print a snapshot of the queue in the range [print_start, print_end]
void Walk_Queue::print_snapshot(unsigned int print_start, unsigned int print_end, std::ostream& os /*= std::cout*/) {
	os << "\n-------------------------------------------------------------------------\n";
	os << "\nPRINTING QUEUE SNAPSHOT in range (" 
		<< print_start << ":" << print_end << ") \n";
	os << "Curr: " << curr << "\tbeg: " << beg 
		<< "\texp_cov: " << exp_covg 
		<< "\tQueue size: " << Q.size() << std::endl;
	
	bool green_region=false;
	bool red_empty_region=false;
	unsigned int green_start, green_end;
	unsigned int red_empty_start, red_empty_end;
	
	ASSERT_WITH_MESSAGE(print_start <= Q.size()-1 and print_end <= Q.size()-1, 
						"Error in print_snapshot, indices out of bounds");
	
	for (unsigned int i=print_start;; i = (i+1)%Q.size()) {
		if (Q[i].color==GREEN) {
			green_end=i;
			if (not green_region) {
				green_start=i;
				green_region=true;
				red_empty_region=false;
			}
			if (red_empty_region) {
				os << "\n" << red_empty_start << " : " << red_empty_end << " is RED\n" << std::endl;
				red_empty_region=false;
			}
		}
		if (Q[i].color==RED) {
			if (green_region) {
				os << "\n" <<  green_start << " : " << green_end << " is GREEN\n" << std::endl;
				green_region=false;
			}
			if (Q[i].candidate_nodes.empty()) {
				red_empty_end=i;
				if (not red_empty_region) {
					red_empty_region=true;
					red_empty_start=i;
				}
			}
			else {
				if (red_empty_region) {
					os << "\n" << red_empty_start << " : " << red_empty_end << " is RED\n" << std::endl;
					red_empty_region=false;
				}
				os << i << "\t";
				os << Q[i];
// 				Q[i].print(os);
			}
		}
		if (i==print_end) {
			if (green_region) {
				os << "\n" <<  green_start << " : " << green_end << " is GREEN\n" << std::endl;
			}
			if (red_empty_region) {
				os << "\n" << red_empty_start << " : " << red_empty_end << " is RED\n" << std::endl;
			}
			break;
		}
	}
}


/************************* END: All class print functions******************/



bool operator==(const Walk_Node_Element& w1, const Walk_Node_Element& w2)
{
	// TODO just got lazy, add more when you get time
	if (w1.src_lmer_id == w2.src_lmer_id and w1.tar_lmer_id == w2.tar_lmer_id and w1.color == w2.color and w1.error == w2.error and w1.end == w2.end and w1.distance == w2.distance and w1.is_intra_read == w2.is_intra_read and w1.freq == w2.freq and w1.freq_range == w2.freq_range and w1.start == w2.start) {
		return true;
	}
	return false;
}

/******************************** END operator overloads *********************/

bool comparable_candidate_lengths(unsigned int min_len, unsigned int max_len, Params* P)
{
	min_len = std::min(min_len, P->insert_size);
	max_len = std::min(max_len, P->insert_size);
	unsigned int k = P->k;
	ASSERT_WITH_MESSAGE(min_len>=k and max_len>=k, "lmer length less than k");
	unsigned int min_k = min_len - k + 1;
	unsigned int max_k = max_len - k + 1;
	
	// arbit criteria
	// TODO: add this to knobs
	if (min_k >= (unsigned int)std::round(0.8*float(max_k))) {
		return true;
	}
	if (max_k <= 10*min_k) {
		return true;
	}
	return false;
}

/************* knobs ******************/

bool check_acceptable_distance(int given_dist, int base_intra_dist, int base_inter_dist, int max_intra_err, int max_inter_err, bool is_intra_read)
{
	if (is_intra_read) {
		if (abs(given_dist - base_intra_dist) <= max_intra_err) {
			return true;
		}
	}
	else {
		if (abs(given_dist - base_inter_dist) <= max_inter_err) {
			return true;
		}
	}
	return false;
}




/************************* END: knobs ***********************/


/*************************** all logger functions **************/

void Walk_Queue::log_msg_1()
{
	if (WBptr->contig_count <= 20000) {
		log_file_ << "Tid: " << thread_id << ", num white nodes: " << CCS.white_node_count << std::endl;
	}
	if (walk_info_level_ >= 1) {
		// TODO: test summing unsigned ints to long ints
		unsigned long int j=0;
		unsigned int z=0;
		if (WBptr->contig_count%1000==0) {
			wall_time_wrap W_local;
			W_local.start();
			for (std::size_t i =0; i < WBptr->UD.node_usage_left.size(); i++) {
				long val = WBptr->UD.node_usage_left[i].load();
				j += val;
				if (val not_eq 0) {
					z++;
				}
			}
			W_local.stop();
			log_file_ << "Tid: " << thread_id 
							<< "\tcontig: " << WBptr->contig_count 
							<< "\t usage sum: " << j 
							<< "\tnum nodes left: " << z 
							<< ", calculation time: " << W_local 
							<< std::endl;
		}
		for (unsigned int i=0; i < CCS.white_node_vec.size(); i++) {
			*t_log_file_ptr << CCS.white_node_vec[i] << "(" << WBptr->ref_db_node_table.ElementAt(CCS.white_node_vec[i]).len_lmer << ") ";
		}
		*t_log_file_ptr << std::endl;	
	}

}

void Walk_Queue::log_msg_2()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr 	<< "In reverse_walking_init" << std::endl;
		*t_log_file_ptr 	<< "filling the queue with " 
					<< RWS.revSeedVec.size() 
					<< " nodes with a length of " 
					<< RWS.revSeedLen 
					<< std::endl;
	}

}

void Walk_Queue::log_msg_3()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Done: reverse_walking_init" << std::endl;
	}
}


void Walk_Queue::log_msg_7(std::vector< dc_str >& dist_const)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "IN gather_cand_support" 
				<< std::endl;
		
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "dist_const size " << dist_const.size() 
				<< " capacity " << dist_const.capacity() 
				<< std::endl;
	}
}

void Walk_Queue::log_msg_8(long unsigned int out_num_iterations)
{
		if (walk_info_level_ >= 2) {
			*t_log_file_ptr 	<< "--------------- Producing contig number : " 
						<< out_num_iterations+1 
						<< "-----------------" 
						<< std::endl;
		}

}

void Walk_Queue::log_msg_9(long unsigned int num_iterations)
{
			if (walk_info_level_ >= 2) {
				*t_log_file_ptr 	<< "\n###############Begin iteration: " 
							<< num_iterations 
							<< ", curr: " << curr 
							<< "######################\n" 
							<< std::endl;
				*t_log_file_ptr 	<< "White nodes: " 
							<< CCS.white_node_vec 
							<< std::endl;
			}

}

void Walk_Queue::log_msg_10(unsigned int m, long unsigned int num_iterations, Walk_Queue_Element& current_WQE)
{
	// log some information
	if (num_iterations >= WBptr->P_->info_after_iterations) {
		unsigned int l = WBptr->ref_db_node_table.ElementAt(
						current_WQE.candidate_nodes[ current_WQE.white_index ].tar_lmer_id
						).len_lmer;
		if (walk_info_level_ > 1) {
			print_snapshot(curr, (curr + std::max(l,m))%Q.size(), *t_log_file_ptr );
		}
	}

}

void Walk_Queue::log_msg_11(long unsigned int num_iterations)
{
			if (num_iterations >= WBptr->P_->info_after_iterations and walk_info_level_ > 1) {
				print_snapshot((curr +Q.size() - WBptr->P_->max_dist_error)%Q.size(), 
							(curr + WBptr->P_->max_dist_error)%Q.size(), 
							*t_log_file_ptr );
			}
			
			if (walk_info_level_ >=2) {
				*t_log_file_ptr << "######################End of 		iteration####################" << std::endl;
			}

}

void Walk_Queue::log_msg_12()
{
	if (walk_info_level_ >= 2) {
		log_file_ << "Contigs before stitching fw and reverse" << std::endl;
		for (unsigned int i=0; i < WBptr->CDS_.data.size(); i++) {
			log_file_ << i << ": " 
					<< WBptr->CDS_.data[i].contig 
					<< "\twhite nodes: " << WBptr->CDS_.data[i].white_node_count 
					<<  std::endl;
		}
		log_file_ << std::endl;
		log_file_ << WBptr->CDS_ << std::endl;
		
		for (unsigned int i=0; i < WBptr->CDS_.fr_contig_glue_vec.size(); i++) {
			log_file_ << WBptr->CDS_.fr_contig_glue_vec[i];
		}
	}
}

void Walk_Queue::log_msg_13()
{
	if (walk_info_level_ >= 2) {
		log_file_ << "Contigs after merging fw and reverse and sorting" << std::endl;
		for (unsigned int i=0; i < WBptr->CDS_.data.size(); i++) {
			log_file_ << i << ": " << WBptr->CDS_.data[i].contig <<  std::endl;
		}
		log_file_ << WBptr->CDS_ << std::endl;
		
		for (unsigned int i=0; i < WBptr->ref_db_node_table.size(); i++) {
			DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(i);
			log_file_ << i << "\t" <<  BOOL(D.is_singleton) << "\t" << D.visit_count << std::endl;
		}
	}

}

void Walk_Queue::log_msg_14()
{
	if (walk_info_level_ >=2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\nBUFF CHECK: ";
	}
}


void Walk_Queue::log_msg_15(unsigned int empty_space, unsigned int space_needed)
{
	if (walk_info_level_ >=2) {
		*t_log_file_ptr << "clearing buffer: " 
				<< space_needed << " needed, " 
				<< empty_space << " available" << std::endl;
	}

}

void Walk_Queue::log_msg_16(unsigned int end_pos)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Cleared buffer from " << beg << " till " << end_pos << std::endl;
		*t_log_file_ptr << "END BUFF CHECK: " << std::endl;
	}

}

void Walk_Queue::log_msg_17(Walk_Queue_Element& WQE)
{
	if (walk_info_level_ >=2) {
		*t_log_file_ptr << "SET SDIR: \nSet direction of the white node at queue position " 
					<< curr 
					<< " as start: " 
					<< std::bitset<8>(WQE.candidate_nodes[ WQE.white_index ].start) 
					<< " based on end: " 
					<< std::bitset<8>(WQE.candidate_nodes[ WQE.white_index ].end) 
					<< std::endl;
	}

}


void Walk_Queue::log_msg_24()
{

}

void Walk_Queue::log_msg_25()
{

}


void Walk_Queue::beg_salutation(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\n" << fn_name << "\n" << std::endl;
	}

}

void Walk_Queue::end_salutation(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\nEND " << fn_name <<"" << std::endl;
	}
}




