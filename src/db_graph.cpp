#include "db_graph.h"

//Dist_Type

void Dist_Type::print(std::ostream& os /*=std::cout*/) const {
	os << "target: " << tar_kmer << "\tlmer_id: " << lmer_id << "\tDistance: " << distance << "\terror: " << error << "\ts_ed: " << (int(start))%8 << "\ts_dir: " << (int(start))/8 << "\te_ed: " << (int(end))%8 << "\te_dir: " << (int(end))/8  << "\tfreq: " << int(freq) << std::endl;
// std::cout << std::bitset<8>(int(DB_Node_Table[i].Dist_Const_List[j].start)) << std::bitset<8>(int(DB_Node_Table[i].Dist_Const_List[j].end)) << std::endl;
}

// DB_Node_Type

void DB_Node_Type::print(std::ostream& os /*=std::cout*/) const {
	os << "Lmer:" << get_lmer() << ":" << len_lmer << "\tprev_neigh:";
	for (unsigned int j=0; j<prev_neigh.size(); j++) {
		os << prev_neigh[j] << ",";
	}
	os << "\tnext_neigh:";
	for (unsigned int j=0; j<next_neigh.size(); j++) {
		os << next_neigh[j] << ",";
	}
	os << "\tfreq:" << freq << "\tfreq_range:";
	for (unsigned int j=0; j<freq_range.size(); j++) {
		os << freq_range[j] << ",";
	}
// 	os << "\tvisit_bounds:";
// 	for (unsigned int j=0; j<visit_bounds.size(); j++) {
// 		os << visit_bounds[j] << ",";
// 	}
	os << std::endl;
	// now print the distance constraints
	for (unsigned int j=0; j<Dist_Const_List.size(); j++) {
		Dist_Const_List[j].print(os);
	}
	os << std::endl;
}

void DB_Node_Table_Type::print(std::ostream& os /*= std::cout*/) const {
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		os << i << ". ";
		DB_Node_Table[i].print(os);
	}
}

void DB_Node_Table_Type::read_graph(Params* P, std::ifstream& graph_file) {
	std::string line;
	
	//read length
	std::getline(graph_file, line);
	P->read1_length = atoi(line.c_str());
	P->read2_length = P->read1_length;
	
	//mate pair distance
	std::getline(graph_file, line);
	P->pair_distance = atoi(line.c_str());
	
	//kmer
	std::getline(graph_file, line);
	P->k = atoi(line.c_str());
	kmer_length=P->k;

	// number of graph nodes
	std::getline(graph_file, line);
	unsigned int num_graph_nodes=atoi(line.c_str());
	
	std::vector<std::string> main_elements_string;
	std::vector<std::string> dist_const_string;
	std::vector<std::string> individual_element_string;
	std::string temp_string;
	if (DEBUG) {std::cout << "Num nodes: " << num_graph_nodes << std::endl;}
	for (unsigned int node_count=0; node_count<num_graph_nodes; node_count++) {
		DB_Node_Type Node;
		// line containing all node info except distance constraints
		std::getline(graph_file, line);
		if (DEBUG) {std::cout << "Read line: " << line << std::endl;}
		main_elements_string=split(line, '\t');
		
		//lmer
		individual_element_string=split(main_elements_string[2], ':');
// 		Node.lmer = individual_element_string[1];
		Node.put_lmer(individual_element_string[1]);
		Node.len_lmer = atoi(individual_element_string[2].c_str());
		
		// prev neigh
		individual_element_string=split(main_elements_string[3], ':');
		temp_string = individual_element_string[1];
		temp_string=temp_string.substr(1, temp_string.length()-2);
		individual_element_string=split(temp_string,',');
//		Node.prev_neigh.resize(5);
		for (unsigned int i=0; i<5; i++) {
			Node.prev_neigh.push_back(atoi(individual_element_string[i].c_str()));
		}
		
		// next neigh
		individual_element_string=split(main_elements_string[4], ':');
		temp_string = individual_element_string[1];
		temp_string=temp_string.substr(1, temp_string.length()-2);
		individual_element_string=split(temp_string,',');
//		Node.next_neigh.resize(5);
		for (unsigned int i=0; i<5; i++) {
			Node.next_neigh.push_back(atoi(individual_element_string[i].c_str()));
		}
		
		// frequency
		individual_element_string=split(main_elements_string[5], ':');
		Node.freq = atoi(individual_element_string[1].c_str());
		
		//frequency range
		individual_element_string=split(main_elements_string[6], ':');
		temp_string = individual_element_string[1];
		temp_string=temp_string.substr(1, temp_string.length()-2);
		individual_element_string=split(temp_string,',');
//		Node.next_neigh.resize(5);
		for (unsigned int i=0; i<2; i++) {
			Node.freq_range.push_back(atoi(individual_element_string[i].c_str()));
		}
		
		// Main data done, now dist constraints
		std::getline(graph_file, line);
		if (DEBUG) {std::cout << "Read line: " << line << std::endl;}
		unsigned int num_dist_constraints=atoi(line.c_str());
//		Node.Dist_Const_List.resize(num_dist_constraints);
		for (unsigned int constraint_count=0; constraint_count<num_dist_constraints; constraint_count++) {
			Dist_Type Dist;
			
			std::getline(graph_file, line);
			if (DEBUG) {std::cout << "Read line: " << line << std::endl;}
			main_elements_string=split(line, '\t');
			
			// target kmer
			individual_element_string=split(main_elements_string[0], ':');
			Dist.tar_kmer=individual_element_string[1];
			
			// initialization
			Dist.lmer_id=std::numeric_limits<unsigned int>::max();
			
			// distance
			individual_element_string=split(main_elements_string[1], ':');
			Dist.distance=atoi(individual_element_string[1].c_str());

			// error
			individual_element_string=split(main_elements_string[2], ':');
			Dist.error=atoi(individual_element_string[1].c_str());
			
			// starting edge
			individual_element_string=split(main_elements_string[3], ':');
			int s_ed=atoi(individual_element_string[1].c_str());

			// starting dir
			individual_element_string=split(main_elements_string[4], ':');
			int s_dir=atoi(individual_element_string[1].c_str());
			
			// combined representation, 3 LSBs represent s_ed and 4th LSB represents s_dir
			Dist.start=8*s_dir+s_ed;

			// Ending edge
			individual_element_string=split(main_elements_string[5], ':');
			int e_ed=atoi(individual_element_string[1].c_str());

			// starting dir
			individual_element_string=split(main_elements_string[6], ':');
			int e_dir=atoi(individual_element_string[1].c_str());
			
			// combined representation, 3 LSBs represent e_ed and 4th LSB represents e_dir
			Dist.end=8*e_dir+e_ed;

			// frequency
			individual_element_string=split(main_elements_string[7], ':');
			Dist.freq=atol(individual_element_string[1].c_str());
			
			Node.Dist_Const_List.push_back(Dist);
		}
		// empty line
		std::getline(graph_file, line);
		
		DB_Node_Table.push_back(Node);
	}
}

size_t DB_Node_Type::dynamic_size() const
{
	return 	lmer.size() 
			+ vecsize(prev_neigh) 
			+ vecsize(next_neigh) 
			+ vecsize(freq_range) 
			+ vecsize(Dist_Const_List);
}

// DB_Node_Table_Type

void DB_Node_Table_Type::build_graph_from_tinterface(Params* P, tinfile_str& TinS, outfile_str& ostr, kmer_unitig_mapping_table_type& KUMTT)
{
	// the input file handlers
	std::ifstream& kmer_unitig_map_file = TinS.kum_file;
	std::ifstream& unitig_nodes_file = TinS.unitig_nodes_file;
	std::ifstream& lmer_sequences_file = TinS.lmer_sequences_file;
	std::ifstream& junction_nodes_file = TinS.junction_nodes_file;
	std::ifstream& read_truncation_file = TinS.read_truncation_file;
	std::ifstream& fasta_readfile1 = TinS.read_file1;
	std::ifstream& fasta_readfile2 = TinS.read_file2;
	
	std::vector<std::string> glob_jn_file_vec;
	
	// read the kmer to unitig mapping table, provided by Tony, and deduce the kmer length
// 	kmer_unitig_mapping_table_type KUMTT(log_file_);
	KUMTT.read_mapping_file(	kmer_unitig_map_file, 
							junction_nodes_file, 
							glob_jn_file_vec, 
							kmer_length);
	
	P->k = kmer_length;
	
	
	// read all the lmer (unitig) sequences
	lmer_table_type LTT(P->k, log_file_);
	LTT.read_input(lmer_sequences_file, glob_jn_file_vec);

// 	for (std::size_t i=0; i < LTT.Table.size(); i++) {
// 		debug_file << LTT.Table[i].lmer << std::endl;
// 	}
	
	// construct the dist const structure from the read files, also set up pair distance and read length in the params
	dist_const_table_type DCTT(P, KUMTT, LTT, ostr);
	DCTT.read_input_fasta(	fasta_readfile1, 
						fasta_readfile2, 
						read_truncation_file);
	
	// transform the kmer-kmer dist const to lmer-lmer in the dist const table
	DCTT.kk_to_ll_transformation(KUMTT, LTT);
	
	// accummulate identical dist const in the dist const table
	DCTT.accummulate_dist_const();
	
	// dist const table built, now read the graph from files 2 and 4 and add dist const. 
	unsigned int num_unitig_nodes = read_unitig_nodes_file(unitig_nodes_file, DCTT, LTT);
	unsigned int num_junc_nodes = read_junction_nodes_file(DCTT, LTT, glob_jn_file_vec);
	
	glob_jn_file_vec.clear();
	
	assert(num_junc_nodes + num_unitig_nodes == DB_Node_Table.size());
	
	// now set the lmer_id in distance constraints (conversion from Tony's lmer id's to my lmer id's)
	std::vector<lmer_id_mapping_type> lmer_id_mapping;
	
	// cannot use parallel for, lmer_id_mapping order has to be preserved, unless you completely sort them
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		lmer_id_mapping_type L;
		std::string& node_lmer = DB_Node_Table[i].get_lmer();
// 		L.lmer_id = DB_Node_Table[i].lmer.substr(0,kmer_length);
		L.lmer_id = node_lmer.substr(0,kmer_length);
		L.num_lmer_id = i;
		lmer_id_mapping.push_back(L);
	}
	
	// merge the two sorted parts of array, to get a sorted sequence
	std::inplace_merge(	lmer_id_mapping.begin(), 
					lmer_id_mapping.begin() + num_unitig_nodes, 
					lmer_id_mapping.end(), 
					[](	lmer_id_mapping_type l1, 
						lmer_id_mapping_type l2){
							return l1.lmer_id < l2.lmer_id;
						});
	
	// now go through all the dist const and put the lmer_id in position
#pragma omp parallel for
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type & D = DB_Node_Table[i];
// 		log_file_ << D.lmer_size() << ":" << D.get_lmer() << std::endl;
		for (unsigned int j=0; j < D.Dist_Const_List.size(); j++) {
			Dist_Type & DT = D.Dist_Const_List[j];
			std::string & tkey = DT.tar_kmer;
			lmer_id_mapping_type const & LIMT = search_lmer_id(lmer_id_mapping, tkey);
			DT.lmer_id = LIMT.num_lmer_id;
		}
	}
	
	
	// do kahip stuff
	if (P->print_kahip_graph) {
		// kahip stuff here
		std::vector< std::vector< std::string > > adj_list;
		std::vector< std::string > label_ref;
		unsigned int num_edges;
		
		create_kahip_input(P, KUMTT, adj_list, label_ref, num_edges);
		
		write_kahip_input(adj_list, label_ref, num_edges);
		
		std::cout << "Created and written Kahip input" << std::endl;
		
// 		exit(0);
	}
} // END: build_graph_from_tinterface()

unsigned int DB_Node_Table_Type::read_unitig_nodes_file(	std::ifstream& infile, 
										dist_const_table_type& DCTT, 
										lmer_table_type& LTT)
{
	wall_time_wrap W;
	std::string line;
	std::cout << "Tint: Reading unitig nodes file to add nodes into DB graph ... ";
	W.start();
	unsigned int iter_count=0;
	while(true) {
		
		std::getline(infile, line);
		
		// end of file reached, break
		if (line.empty()) {
			break;
		}
		
				
		// put an entry in the DB_Node_Table
		std::vector<std::string> line_elements;
		line_elements = split(line, '\t');
		
		std::string lmer_key = line_elements[0];
		lmer_entry_type& L = LTT.search_table(lmer_key);
		
		DB_Node_Type Node;
		Node.put_lmer(L.lmer);
		Node.len_lmer = L.lmer.size();
		
		// push prev_neigh
		for (unsigned int i=2; i < 6; i++) {
			Node.prev_neigh.push_back(atoi(line_elements[i].c_str()));
		}
		Node.prev_neigh.push_back(0);
		
		// push next_neigh
		for (unsigned int i=6; i < 10; i++) {
			Node.next_neigh.push_back(atoi(line_elements[i].c_str()));
		}
		Node.next_neigh.push_back(0);

		update_sing_n_edges(Node);
		
		Node.freq = atol(line_elements[10].c_str());
		
		Node.freq_range.push_back(atol(line_elements[11].c_str()));
		Node.freq_range.push_back(atol(line_elements[12].c_str()));
		
		// main entries created, now add dist const
		
		// find out the range
		dist_const_type key_obj;
		key_obj.src = lmer_key;
		std::pair<std::vector<dist_const_type>::iterator, std::vector<dist_const_type>::iterator> dist_const_range;
		dist_const_range = std::equal_range(DCTT.DC_Table.begin(), 
								DCTT.DC_Table.end(), 
								key_obj, 
								[](	dist_const_type D1, 
									dist_const_type D2){
										return D1.src < D2.src;
									});
		
		// now add all the constraints in the range
		for (std::vector<dist_const_type>::iterator it = dist_const_range.first; it != dist_const_range.second; it++) {
			Dist_Type D;
			D.tar_kmer = (*it).tar;
			// will be put later
			D.lmer_id = std::numeric_limits<unsigned int>::max();
			D.distance = (*it).distance;
			D.error = (*it).error;
			D.start = int((*it).s_dir)*8;
			D.end = int((*it).e_dir)*8;
			D.freq = (*it).freq;
			
			Node.Dist_Const_List.push_back(D);
		}
		
		DB_Node_Table.push_back(Node);

		if (++iter_count == std::numeric_limits<decltype(iter_count)>::max()) { // hundred billion is a big number
			std::cerr << "Max iterations exceeded while reading input kmer to unitig file";
			break;
		}

	} // END: for (unsigned int iter_count=0;;) {
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
	return iter_count;
}

unsigned int DB_Node_Table_Type::read_junction_nodes_file(	/*std::ifstream& infile,*/ 
										dist_const_table_type& DCTT, 
										lmer_table_type& LTT,
										std::vector<std::string>& glob_jn_file_vec)
{
	wall_time_wrap W;
	std::string line;
	std::cout << "Tint: Reading junction nodes file to add nodes into DB graph ... ";
	W.start();
	for (unsigned int i=0; i < glob_jn_file_vec.size(); i++ ) {
		
		
		// put an entry in the DB_Node_Table
		std::vector<std::string> line_elements;
		line_elements = split(glob_jn_file_vec[i], '\t');
		
		
		std::string lmer_key = line_elements[0];
		lmer_entry_type& L = LTT.search_table(lmer_key);
		
		DB_Node_Type Node;
		Node.put_lmer(L.lmer);
		Node.len_lmer = L.lmer.size();
		
		// push prev_neigh
		for (unsigned int i=1; i < 5; i++) {
			Node.prev_neigh.push_back(atoi(line_elements[i].c_str()));
		}
		Node.prev_neigh.push_back(0);
		
		// push next_neigh
		for (unsigned int i=5; i < 9; i++) {
			Node.next_neigh.push_back(atoi(line_elements[i].c_str()));
		}
		Node.next_neigh.push_back(0);
		
		update_sing_n_edges(Node);
		
		Node.freq = atol(line_elements[9].c_str());
		
		Node.freq_range.push_back(atol(line_elements[9].c_str()));
		Node.freq_range.push_back(atol(line_elements[9].c_str()));
		
		// main entries created, now add dist const
		
		// find out the range
		dist_const_type key_obj;
		key_obj.src = lmer_key;
		std::pair<std::vector<dist_const_type>::iterator, std::vector<dist_const_type>::iterator> dist_const_range;
		dist_const_range = std::equal_range(DCTT.DC_Table.begin(), 
								DCTT.DC_Table.end(), 
								key_obj, 
								[](	dist_const_type D1, 
									dist_const_type D2){
										return D1.src < D2.src;
									});
		
		// now add all the constraints in the range
		for (std::vector<dist_const_type>::iterator it = dist_const_range.first; it != dist_const_range.second; it++) {
			Dist_Type D;
			D.tar_kmer = (*it).tar;
			D.lmer_id = std::numeric_limits<unsigned int>::max();
			D.distance = (*it).distance;
			D.error = (*it).error;
			D.start = int((*it).s_dir)*8;
			D.end = int((*it).e_dir)*8;
			D.freq = (*it).freq;
			
			Node.Dist_Const_List.push_back(D);
		}
		
		DB_Node_Table.push_back(Node);
	} // END: for (unsigned int iter_count=0;;) {
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
	return glob_jn_file_vec.size();
}

void DB_Node_Table_Type::create_kahip_input(Params* P, 
										kmer_unitig_mapping_table_type& KUMTT_,  
										std::vector< std::vector< std::string > >& adj_list, 
										std::vector<std::string>& label_ref, 
										unsigned int& num_edges)
{
	// TODO: Modify it to write input to multiple files, for parallel kahip
	// TODO: Remove two copies of singleton nodes (local copy for the function and the class copy)
	// you are making nodes singleton here, when you run walk, they won't become singleton and therefore there is inconsistancy
	std::cout << "Creating Kahip input..." << std::endl;
	unsigned int k = P->k;
	unsigned int singletons_count=0;
	unsigned int self_edge_count=0;
	std::vector<std::pair<std::string, std::string> > edge_list;

	std::cout << "Creating edge list..." << std::endl;
	
	// add all the edges in the edge list
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		if (DB_Node_Table[i].is_singleton) {
			singletons_count++;
			continue;
		}
		
		std::string& l = DB_Node_Table[i].get_lmer();
		std::string current_node_id = l.substr(0,k);
		
		std::string current_kmer;
		std::string common_mer;

		// add prev neighbor edges
		current_kmer = l.substr(0,k);
		common_mer = current_kmer.substr(0, k-1);
		unsigned int curr_node_edges_added=0;
		for (unsigned int j=0; j < 4; j++) {
			if (DB_Node_Table[i].prev_neigh[j] not_eq 0) {

				// find the neighboring node
				std::string neigh_mer = alphabet.substr(j,1) + common_mer;
				
				// following 3 lines added
// 				std::string neigh_mer_rc = neigh_mer;
// 				reverse_complement(neigh_mer_rc);
// 				if (current_kmer == neigh_mer or current_kmer == neigh_mer_rc) {
				MakePositive(neigh_mer);
				kmer_unitig_mapping_type& K = KUMTT_.search_table(neigh_mer);
				if (current_node_id == K.lmer_id) {
					log_file_ << "Prev self edge: " << current_node_id << std::endl;
					self_edge_count++;
					continue;
				}
				
				// make the edge and push
				std::pair<std::string, std::string> edge = std::make_pair(current_node_id, K.lmer_id);
				edge_list.push_back(edge);
				curr_node_edges_added++;
			}
		}
		
		// add next neighbor edges
		current_kmer = l.substr(l.size()-k, k);
		common_mer = current_kmer.substr(1, k-1);
		for (unsigned int j=0; j < 4; j++) {
			if (DB_Node_Table[i].next_neigh[j] not_eq 0) {
				std::string neigh_mer = common_mer + alphabet.substr(j,1);
				MakePositive(neigh_mer);
				kmer_unitig_mapping_type& K = KUMTT_.search_table(neigh_mer);
				if (current_node_id == K.lmer_id) {
					log_file_ << "Next self edge: " << current_node_id << std::endl;
					self_edge_count++;
					continue;
				}
				
				// make the edge and push
				std::pair<std::string, std::string> edge = std::make_pair(current_node_id, K.lmer_id);
				edge_list.push_back(edge);
				curr_node_edges_added++;
			}
		}
		if (curr_node_edges_added==0) {
			DB_Node_Table[i].is_singleton=true;
			singletons_count++;
		}
	}

	ASSERT_WITH_MESSAGE(	singletons_count==singleton_count, 
					"singletons_count: " 
					+ std::to_string(singletons_count) 
					+ ", singleton_count: " 
					+ std::to_string(singleton_count));
	
	
	adj_list.resize(DB_Node_Table.size() - singletons_count);
	
	std::cout 	<< "Edge list created, " 
			<< num2h( edge_list.size() )
			<< " edges, (sing = " 
			<< num2h( singletons_count )
			<< "), self edges = " 
			<< self_edge_count 
			<< ", now sorting by second vertex....." 
			<< std::endl;

	// now sort edge list by the second
// 	std::sort(edge_list.begin(), edge_list.end(), [](std::pair<std::string, std::string> e1, std::pair<std::string, std::string> e2){return e1.second < e2.second;});
	

	ALGO_NS::sort(	edge_list.begin(), 
					edge_list.end(), 
					[](	std::pair<std::string, std::string> e1, 
						std::pair<std::string, std::string> e2){
							return (e1.second).compare(e2.second)!=0 ? e1.second < e2.second : e1.first < e2.first;
						});


	std::cout 	<< "Num edges before duplicate removal: " 
			<< num2h( std::distance(edge_list.begin(), edge_list.end()) )
			<< std::endl;
	
	// remove duplicates
	std::vector<std::pair<std::string, std::string> >::iterator it;
	it = std::unique(	edge_list.begin(), 
				edge_list.end(), 
				[](	std::pair<std::string, std::string> e1, 
					std::pair<std::string, std::string> e2){
						return (e1.second == e2.second) and (e1.first == e2.first);
					});
	std::cout << num2h( std::distance(it, edge_list.end()) ) << " parallel edges removed" << std::endl;
	edge_list.resize(std::distance(edge_list.begin(), it));
	
	std::cout 	<< "Num edges after duplicate removal: " 
			<< num2h( std::distance(edge_list.begin(), edge_list.end()) )
			<< std::endl;
	
	assert(edge_list.size()%2==0);
	num_edges = edge_list.size()/2;
	std::cout << "Sorted, now relabling second vertex and creating label ref ....." << std::endl;
	
	// label nodes and create a reference
	// TODO: right now the labels are from 0 to n-1, check if it needs to be changed to 1 to n
	unsigned int label=1;
	
	unsigned int low=0, high=1;
	while(low <edge_list.size()) {
		while(true) {
			if (high>=edge_list.size()) {
				break;
			}
			if ((edge_list[low].second).compare((edge_list[high].second))==0) {
				high++;
			}
			else {
				break;
			}
		}
		assert (high>low);
		// [low,high) is the desired range of same second nodes
		
		// first push the id into the reference
		label_ref.push_back(edge_list[low].second);
		
		// now relabel the id
		for (unsigned int i=low; i < high; i++) {
			edge_list[i].second = std::to_string(label);
		}
		low=high;
		label++;
	}
	
	unsigned int label_copy = label;
	
	// basic check
	ASSERT_WITH_MESSAGE(adj_list.size()==label-1, 
					"adj list size: " 
					+ std::to_string(adj_list.size()) 
					+ ", label: " 
					+ std::to_string(label) 
					+ ", DB table size: " 
					+ std::to_string(DB_Node_Table.size()) 
					+ ", label ref size: " 
					+ std::to_string(label_ref.size()));
	
	std::cout << "Now sorting by first vertex ....." << std::endl;
	
	// now sort by first edge, relabel and create adjacency list

	ALGO_NS::sort(	edge_list.begin(), 
					edge_list.end(), 
					[](std::pair<std::string, std::string> e1, 
					   std::pair<std::string, std::string> e2){
						return /*(e1.first).compare(e2.first) < 0*/ e1.first < e2.first;
					});


	std::cout << "Sorted, now relabling first vertex and creating adjacency list ....." << std::endl;
	
	low=0;
	label=1;
	high=1;
	unsigned int edge_count=0;
	while(low < edge_list.size()) {
		while(true) {
			if (high>=edge_list.size()) {
				break;
			}
			if ((edge_list[low].first).compare((edge_list[high].first))==0) {
				high++;
			}
			else {
				break;
			}
		}
		assert (high>low);
		// [low,high) is the desired range of same second nodes
		
		// relabel the id
		for (unsigned int i=low; i < high; i++) {
			edge_list[i].first = std::to_string(label);
			adj_list[label-1].push_back(edge_list[i].second);
			edge_count++;
		}
		low=high;
		label++;
	}
	
	ASSERT_WITH_MESSAGE(	label == label_copy, 
					"label = " 
					+ std::to_string(label) 
					+ ", label copy = " 
					+ std::to_string(label_copy));
	
	ASSERT_WITH_MESSAGE(	2*num_edges==edge_count, 
					"num_edges: " 
					+ std::to_string(num_edges) 
					+ ", edge_count: " 
					+ std::to_string(edge_count));
	std::cout << "Done creating kahip input ....." << std::endl;
	
} // END: create_kahip_input()

void DB_Node_Table_Type::write_kahip_input(std::vector< std::vector< std::string > >& adj_list, 
										std::vector< std::string >& label_ref, 
										unsigned int& num_edges)
{
	std::ofstream adj_list_file;
	std::ofstream label_ref_file;
	
	adj_list_file.open("adj_list.txt");
	label_ref_file.open("kmer_ref.txt");
	
	
	// write adj list header
	std::cout << "Writing adj list ...." << std::endl;
	adj_list_file << adj_list.size() << "\t" << num_edges << std::endl;
	
	// write remaining adj list
	for (unsigned int i=0; i < adj_list.size(); i++) {
		assert(adj_list[i].size()>0);
		if (adj_list[i].size()>1) {
			for (unsigned int j=0; j < adj_list[i].size()-1; j++) {
				adj_list_file << adj_list[i][j] << "\t";
			}
		}
		adj_list_file << adj_list[i][adj_list[i].size()-1] << std::endl;
	}
	
	// write kmer ref
	std::cout << "Writing label ref" << std::endl;
	for (unsigned int i=0; i < label_ref.size(); i++) {
		label_ref_file << label_ref[i] << std::endl;
	}
	std::cout << "Written kahip input" << std::endl;
	
} // END: write_kahip_input


// Given a unitig node, find the prev or the next based on if the side variable is 0 or 1. 
unsigned int DB_Node_Table_Type::FindUnitigEdge (DB_Node_Type const & Node, unsigned int side) const {
	// find the prev neighbor
	if (side==0) {
		for (unsigned int i=0; i < 5; i++) {
			if (Node.prev_neigh[i]!=0) {
				return i;
			}
		}
		std::cout << "Error in FindUnitigEdge: No prev neigh found" << std::endl;
		exit(0);
	}
	// find the next neighbor
	else if (side==1) {
		for (unsigned int i=0; i < 5; i++) {
			if (Node.next_neigh[i]!=0) {
				return i;
			}
		}
		std::cout << "Error in FindUnitigEdge: No next neigh found" << std::endl;
		exit(0);
	}
	else {
		std::cout << "Error in FindUnitigEdge: invalid value of variable side" << std::endl;
		exit(0);
	}
}

// check if a given node is a unitig or not
bool DB_Node_Table_Type::isUnitig(DB_Node_Type const & Node) const {
	unsigned int prev_count=0, next_count=0;
	for (unsigned int i=0; i<4; i++) {
		if (Node.prev_neigh[i]!=0) {
			prev_count++;
		}
		if (Node.next_neigh[i]!=0) {
			next_count++;
		}
	}
	if (prev_count<=1 and next_count<=1) {
		return true;
	}
	return false;
}

// given a DB node and indices of two distance constraints in it's structure, check if they are two occurances of the same dist constraint
bool DB_Node_Table_Type::check_identical_constraints(	DB_Node_Type const & D, 
									unsigned int index1, 
									unsigned int index2) const {
	if (	D.Dist_Const_List[index1].lmer_id == D.Dist_Const_List[index2].lmer_id 
		and D.Dist_Const_List[index1].distance == D.Dist_Const_List[index2].distance 
		and D.Dist_Const_List[index1].error == D.Dist_Const_List[index2].error 
		and D.Dist_Const_List[index1].start == D.Dist_Const_List[index2].start 
		and D.Dist_Const_List[index1].end == D.Dist_Const_List[index2].end 
		and D.Dist_Const_List[index1].is_intra_read() == D.Dist_Const_List[index2].is_intra_read()) {
			return true;
	}
	return false;
}

// go through the list of nodes and merge identical constraints within each node
void DB_Node_Table_Type::merge_distance_constraints() {
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type & D=DB_Node_Table.at(i);
// 		std::sort(D.Dist_Const_List.begin(), D.Dist_Const_List.end(), lmer_comparator());
		unsigned int start_index=0;
		unsigned int end_index=0;
		while(true) {
			if (start_index>=D.Dist_Const_List.size()) {
				break;
			}
			while(end_index<D.Dist_Const_List.size()) {
				if (check_identical_constraints(D, start_index, end_index)) {
					end_index+=1;
				}
				else {
					break;
				}
			}
			for (unsigned int j=start_index+1;j<end_index;j++) {
				D.Dist_Const_List[start_index].freq+=D.Dist_Const_List[j].freq;
				D.Dist_Const_List[j].freq=0;
			}
			start_index=end_index;
		}
		// remove constraints within same strand (separate handling for inter and intra read dist const since they might need different handling in future version of algorithm
		for (unsigned int j=0; j<D.Dist_Const_List.size(); j++) {
			// intra read
			if (	D.Dist_Const_List[j].lmer_id==i 
				and D.Dist_Const_List[j].distance==-int(D.lmer_size()-1) 
				and D.Dist_Const_List[j].error==0) {
					D.Dist_Const_List[j].freq=0;
			}
			// inter read
			else if (	D.Dist_Const_List[j].lmer_id==i 
					and D.Dist_Const_List[j].distance<=-int(D.lmer_size()-1) +int(D.Dist_Const_List[j].error) 
					and D.Dist_Const_List[j].distance>=-int(D.lmer_size()-1) -int(D.Dist_Const_List[j].error)) {
						D.Dist_Const_List[j].freq=0;
			}
		}
		unsigned int j=0;
		while(true) {
			if (j>=D.Dist_Const_List.size()) {
				break;
			}
			if (D.Dist_Const_List[j].freq==0) {
				D.Dist_Const_List.erase(D.Dist_Const_List.begin()+j);
			}
// 			j=j+1;
			else {
				j=j+1;
			}
		}
	}
}

unsigned int DB_Node_Table_Type::calc_longest_dist_const() const 
{
	unsigned int longest_const_len = std::numeric_limits<unsigned int>::min();
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		for (unsigned int j=0; j < DB_Node_Table[i].Dist_Const_List.size(); j++) {
			unsigned int const_len = (unsigned int)(DB_Node_Table[i].Dist_Const_List[j].distance + DB_Node_Table[i].Dist_Const_List[j].error);
			if (const_len > longest_const_len) {
				longest_const_len = const_len;
			}
		}
	}
	return longest_const_len;
}

unsigned int DB_Node_Table_Type::calc_longest_lmer_len() const {
	unsigned int longest_lmer_len = std::numeric_limits<unsigned int>::min();
	for (unsigned int i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type const & D = DB_Node_Table[i];
		ASSERT_WITH_MESSAGE((unsigned int)D.len_lmer>0, "Error in revise_parameters: lmer lengths of node " + std::to_string(i) + " is negative");
		
		if (longest_lmer_len < (unsigned int)D.len_lmer) {
			longest_lmer_len = (unsigned int)D.len_lmer;
		}
	}
	return longest_lmer_len;
}

// From Node, update the singleton_count and num edges
void DB_Node_Table_Type::update_sing_n_edges(DB_Node_Type & Node)
{
	Node.is_singleton=false;
	unsigned int k = kmer_length;
	unsigned int neigh_sum=0;
	
	std::string& l = Node.get_lmer();
	
	std::string current_kmer;
	std::string common_mer;

	// prev neighbor edges
	current_kmer = l.substr(0,k);
	common_mer = current_kmer.substr(0, k-1);
	
	for (unsigned int i=0; i < 4; i++) {
		if (Node.prev_neigh[i] not_eq 0) {
		
			num_edges++;
			std::string neigh_mer = alphabet.substr(i,1) + common_mer;
			
			// following 3 lines added
			std::string neigh_mer_rc = neigh_mer;
			reverse_complement(neigh_mer_rc);
			if (current_kmer == neigh_mer or current_kmer == neigh_mer_rc) {
				continue;
			}
			neigh_sum++;
		}
	}
	// add next neighbor edges
	current_kmer = l.substr(l.size()-k, k);
	common_mer = current_kmer.substr(1, k-1);
	
	for (unsigned int i=0; i < 4; i++) {
		if (Node.next_neigh[i] not_eq 0) {
		
			num_edges++;
			std::string neigh_mer = common_mer + alphabet.substr(i,1);
			
			// following 3 lines added
			std::string neigh_mer_rc = neigh_mer;
			reverse_complement(neigh_mer_rc);
			if (current_kmer == neigh_mer or current_kmer == neigh_mer_rc) {
				continue;
			}
			neigh_sum++;
		}
	}

	if (neigh_sum==0) {
		Node.is_singleton=true;
		singleton_count++;
	}
}

void Dist_Type::read_from_file(std::ifstream& is)
{
	std::string line;
	std::getline(is, line);
	std::vector<std::string> elements = split(line, '\t');
	assert(elements.size()==7);
	
	tar_kmer = elements[0];
	lmer_id = atoi(elements[1].c_str());
	distance = atoi(elements[2].c_str());
	error = atoi(elements[3].c_str());
// 	start = char(atoi(elements[4].c_str()));
// 	end = char(atoi(elements[5].c_str()));
	start = (elements[4].c_str())[0];
	end = (elements[5].c_str())[0];
	freq = atoi(elements[6].c_str());
}

void Dist_Type::write_to_file(std::ofstream& os) const
{
	os 	<< tar_kmer << "\t" 
		<< lmer_id << "\t"
		<< distance << "\t"
		<< error << "\t"
// 		<< int(start) << "\t"
// 		<< int(end) << "\t"
		<< start << "\t"
		<< end << "\t"
		<< freq << std::endl;
}

void DB_Node_Type::read_from_file(std::ifstream& is)
{
	std::string line;
	std::vector<std::string> elements;
	
	std::getline(is, line);
	elements = split(line, '\t');
	assert(elements.size()==4);
	lmer = elements[0].c_str();
	len_lmer = atoi(elements[1].c_str());
	is_singleton = atoi(elements[2].c_str());
	visit_count = atoi(elements[3].c_str());
	
	std::getline(is, line);
	size_t s = atoi(line.c_str());
	std::getline(is, line);
	elements = split(line, '\t');
	assert(elements.size()==s);
	prev_neigh.resize(s);
	for (unsigned int i=0; i < s; i++) {
		prev_neigh[i] = atoi(elements[i].c_str());
	}

	std::getline(is, line);
	s = atoi(line.c_str());
	std::getline(is, line);
	elements = split(line, '\t');
	assert(elements.size()==s);
	next_neigh.resize(s);
	for (unsigned int i=0; i < s; i++) {
		next_neigh[i] = atoi(elements[i].c_str());
	}

	std::getline(is, line);
	elements = split(line, '\t');
	assert(elements.size()==3);
	freq_range.resize(2);
	freq = atoi(elements[0].c_str());
	freq_range[0] = atoi(elements[1].c_str());
	freq_range[1] = atoi(elements[2].c_str());
	
	std::getline(is, line);
	s = atoi(line.c_str());
	for (size_t i=0; i < s; i++) {
		Dist_Type DT;
		DT.read_from_file(is);
		Dist_Const_List.push_back(DT);
	}
}

void DB_Node_Type::write_to_file(std::ofstream& os) const
{
	os 	<< lmer << "\t" 
		<< len_lmer << "\t"
		<< is_singleton << "\t"
		<< visit_count << std::endl;
		
	os << prev_neigh.size() << std::endl;
	for (unsigned int i=0; i < prev_neigh.size(); i++) {
		os << prev_neigh[i] << "\t";
	}
	os << std::endl;

	os << next_neigh.size() << std::endl;
	for (unsigned int i=0; i < next_neigh.size(); i++) {
		os << next_neigh[i] << "\t";
	}
	os << std::endl;
	
	os << freq << "\t" << freq_range[0] << "\t" << freq_range[1] << std::endl;
	
	os << Dist_Const_List.size() << std::endl;
	for (unsigned int i=0; i < Dist_Const_List.size(); i++) {
		Dist_Type const & DT = Dist_Const_List[i];
		DT.write_to_file(os);
	}

}

void DB_Node_Table_Type::read_from_file(std::ifstream& is)
{
	std::string line;
	
	std::getline(is, line);
	kmer_length = atoi(line.c_str());

	std::getline(is, line);
	singleton_count = atoi(line.c_str());

	std::getline(is, line);
	num_edges = atoi(line.c_str());
	
	std::getline(is, line);
	size_t s = atoi(line.c_str());
	for (size_t i=0; i < s; i++) {
		DB_Node_Type D;
		D.read_from_file(is);
		DB_Node_Table.push_back(D);
	}
}

void DB_Node_Table_Type::write_to_file(std::ofstream& os) const
{
	os << kmer_length << std::endl;
	os << singleton_count << std::endl;
	os << num_edges << std::endl;
	
	os << DB_Node_Table.size() << std::endl;
	for (size_t i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type const & D = DB_Node_Table[i];
		D.write_to_file(os);
	}
}


std::size_t DB_Node_Table_Type::dynamic_size() const
{
	size_t bytes=0;
	for (size_t i=0; i < DB_Node_Table.size(); i++) {
		DB_Node_Type const & D = DB_Node_Table[i];
		bytes += D.size();
	}
	return bytes;
}


std::ostream& operator<<(std::ostream& os, Dist_Type const & D) {
	D.print(os);
	return os;
}

std::ostream& operator<<(std::ostream& os, DB_Node_Type const & D)  {
	D.print(os);
	return os;
}

/*********** void Transform_dist_const(KU_Table_Type& Table, unsigned int k); implemented in walk.cpp**********/
