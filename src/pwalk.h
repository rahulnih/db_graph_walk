#ifndef PWALK_H
#define PWALK_H

#include "all_classes.h"
#include "walk.h"
#include "misc.h"
#include "job_queue.h"

/********** TODO list**********************
 * 1. Concurrency of queues, pushing and popping simultaniously 
 * 2. separate file for jobs
 * 
 * *****************************************/


class Walk_Queue_Par_Mem {
	
	private:
		// vars
		
		// supporting vars
		
		Walk_Queue_Par& wqp_ref_;
		
		unsigned int NUM_THREADS_;
		
		std::vector<size_t> iter_vec;
		
// 		std::vector<bool> mem_print;
		std::atomic<unsigned int> mem_print_counter;
		
		std::mutex shared_mem_m;
		
		// memory vars
		
		// shared
		
		size_t WB_mem;
		
		size_t db_graph_mem;
		
		size_t ku_table_mem;
		
		size_t contigs_data_str_mem;
		
		size_t LP_map_mem;
		
		size_t no_start_node_vec_mem;
		
		size_t timing_vec_mem;
		
		// per thread
		std::vector<size_t> Q_vec_mem;
		
		std::vector<size_t> JQS_vec_mem;
		std::vector<size_t> JQS_vec_num;
		
		std::vector<size_t> curr_job_vec_mem;
		
		// total vars
		
// 		size_t Q_vec_mem_t;
// 		size_t JQS_vec_mem_t;
// 		size_t curr_job_vec_mem_t;
// 		size_t total_mem;
		
	public:
		
		// functions
		
		Walk_Queue_Par_Mem(Walk_Queue_Par& wqp_ref);
		
		Walk_Queue_Par_Mem(Walk_Queue_Par_Mem& other);
		
// 		void calc_mem_map();
		
		void calc_mem_map_parallel();
		
		void print_mem_map(std::ofstream& os);
		
		unsigned int inc_mem_print() {return mem_print_counter.fetch_add(1, std::memory_order_relaxed);}
		
		size_t Q_vec_sum() {return std::accumulate(	Q_vec_mem.begin(), 
												Q_vec_mem.end(), 
												static_cast<size_t>(0));}
		
		size_t JQS_vec_sum() {return std::accumulate(	JQS_vec_mem.begin(), 
												JQS_vec_mem.end(), 
												static_cast<size_t>(0));}
		
		size_t curr_job_vec_sum() {return std::accumulate(	curr_job_vec_mem.begin(), 
													curr_job_vec_mem.end(), 
													static_cast<size_t>(0));}
		
// 		friend void Walk_Queue_Par_Mem::print_mem_map(std::ofstream& os);
};

// Partitioned graph walk wrapper class
class Walk_Queue_Par {

	private:
		// vars
		
		Walk_Base *WBptr;
		
		// number of threads, TODO: redundant, remove it
		unsigned int NUM_THREADS_;
		
		std::vector<lmer_partition_map> const & LP_map_;
		
		std::ifstream& partition_file_;
		
		std::ofstream& log_file_;
		
		// TODO: put all these vectors of structs into one vector of struct ("parallel job")
		
		// vector of pointers to queue objects
		std::vector<Walk_Queue*> Q_vec;   // as many queues as there are threads, not shared or cross-accessed.
		
		// structure to manage jobs put by various threads
		std::vector<job_queue_str*> JQS_vec;  // as many queues as there are threads
		
		// holds current job of each thread
		std::vector<job_str*> curr_job_vec;   // one per thread, not shared or cross-accessed.
		
		// true if all local start nodes are exhausted
		std::vector<bool> no_start_node_vec;
		
		// true if there is currently no ongoing traversal in a thread
		std::vector<bool> no_ongoing_traversal;
		
		// timings of various threads
		std::vector<std::chrono::milliseconds>& timing_vec;
		
		// the usual walk info level
		unsigned int& walk_info_level_;
		
		// useful in generating unique contig id whenever needed
		std::atomic<unsigned int> unique_contig_id;
		
		Walk_Queue_Par_Mem* Mem_str_ptr;
		
		// TODO: put all these locks in a set of locks
		
		// lock to control std::cout, so that we don't have gibberish output
		std::mutex console;
		
		// lock to examin no_start_node_vec to determine if it's time to exit
		std::mutex exit_cond;
		
		std::mutex push_contig_m;
		
		std::mutex push_fr_glue_str_m;
		
		std::mutex log_file_m;
		
		std::mutex debug_file_m;
		
	public:
		
		// constructor
		Walk_Queue_Par(Params* P, 
						DB_Node_Table_Type const & list_nodes, 
						KU_Table_Type const & KU_Table, 
						contig_data_str& CDS, 
						std::vector<lmer_partition_map> const & LP_map, 
						outfile_str& ostr,
						std::ifstream& partition_file, 
						time_str& T);
		
		// functions
		~Walk_Queue_Par() {
			for (auto x : Q_vec) {
				if (x != nullptr) delete x;
			}
			for (auto x : JQS_vec) {
				if (x != nullptr) delete x;
			}
			for (auto x : curr_job_vec) {
				if (x != nullptr) delete x;
			}
			if (Mem_str_ptr != nullptr) delete Mem_str_ptr;
			if (WBptr != nullptr) delete WBptr;
		}
		
		
		// the main parallel walk function
		void pwalk_db_graph();
		
		// select multiple starting nodes for parallel walk
// 		unsigned int pselect_starting_node(Walk_Queue& WQ, int id);
		unsigned int pselect_starting_node(Walk_Queue& WQ, int id);
		
		hop_return pselect_next_hop(Walk_Queue& WQ, 
							Walk_Queue_Element& WQE, 
							unsigned int neigh_count,
							unsigned int& tar_par_id,
							int id);
		
		void print_paths(std::ostream& os);
		
		//returns a unique id
		unsigned int get_unique_contig_id() {return unique_contig_id.fetch_add(1, std::memory_order_relaxed);}
		
		// returns true if no start node anywhere, and thus exit
		bool check_exit_cond();
		
		// update contig info in job mode
		void jupdate_contig_info(	Walk_Queue& WQ, 
								job_str *curr_job_ptr, 
								unsigned int contig_id);
		
		void jupdate_contig_log_print(Walk_Queue& WQ);
		
		// update contig info in job mode
		void jpush_fw_rev_stitch_info(	Walk_Queue& WQ, 
									job_str *curr_job_ptr, 
									unsigned int contig_id);
		
		void jcalculate_RSD(Walk_Queue& WQ);
		
		void calc_start_node_eligibility(	std::vector<std::size_t>& eligibility_vec, 
									unsigned int num_par);
		
		void pre_merge_fw_rev_contigs();
		
		friend class Walk_Queue_Par_Mem;
};

#endif // PWALK_H
