#ifndef SELECT_NEXT_HOP_H
#define SELECT_NEXT_HOP_H

#include "../all_classes.h"
#include "mini_all_paths.h"
// #include "../walk.h"

class select_next_hop_data {
private:
	// relative positions where current candidate has support (separate vectors of inter and intra read support)
	std::vector<dc_str> cand_support_inter_read;
	std::vector<dc_str> cand_support_intra_read;

	// dist consts for the current candidate, used to update their outgoing dist const (weird structure of alternating the distance and frequency
	std::vector<dc_str> cand_dc_inter;
	std::vector<dc_str> cand_dc_intra;
	
	// score, calculated using cand_support
	float cand_score_abs;
	float cand_score_norm;
	
	// data for the best candidate
	
	// candidate score for best candidate
	float best_cand_score_norm;
	float best_cand_score_abs;
	
	// candidate score for second best candidate
	float sec_best_cand_score_norm;
	float sec_best_cand_score_abs;
	
	// in the current WNE array, position of the best candidate
	// WHAT IF THE POSITION NEVER CHANGES, POS 0 MIGHT BE NON BROWN
	std::size_t best_cand_position;
	
	// candidate dist const for the best candidate
	std::vector<dc_str> best_cand_dc_inter;
	std::vector<dc_str> best_cand_dc_intra;
	
	std::vector<unsigned int> candidate_lengths;
	
	std::ofstream & log_file_;
	
public:
	
	// FUNCTIONS
	
	// constructor
	select_next_hop_data(Walk_Queue& WQ);
	
	// based on curr cand score, update the best and second best candidate data
	void update_best_cand_data(std::size_t pos);
	
	// pick decided hop from mini_all_paths, and mark that in WQE
	void pick_decided_hop(Walk_Queue& WQ, Walk_Queue_Element& WQE);
	
	// regather support for best candidate 
	void regather_best_cand_support(Walk_Queue& WQ, Walk_Queue_Element& WQE);
	
	// clear all the vectors
	void clear();
	
	// clear support vecs
	inline void clear_support_vecs() {
		cand_support_inter_read.clear();
		cand_support_intra_read.clear();
	}
	
	// clear dist const vecs
	inline void clear_dc_vecs() {
		cand_dc_inter.clear();
		cand_dc_intra.clear();
	}
	
	friend class select_next_hop_class;
// 	friend class Walk_Queue;
	friend class Walk_Queue_Par;
};

class select_next_hop_class {
	
private:
	
	// VARS
	
	Walk_Queue& WQ;
	unsigned int& walk_info_level_;
// 	unsigned int& infoLevel;
	std::ofstream * t_log_file_ptr;
	
	std::ofstream& log_file_;
	
	unsigned int & print_level;
	
	// FUNCTIONS
	
	/************************* main function 1*******************************/
	bool find_best_candidate(Walk_Queue_Element& WQE, 
							select_next_hop_data& SD, 
							Walk_Type wt);
	/************************* end : main function 1*******************************/
	

	/**************** HELPER FUNCTIONS: find_best_candidate*******************/
	
	bool check_decided_stuff(	Walk_Queue_Element& WQE, 
							select_next_hop_data & SD);
	
	bool update_allCand_n_best_scores(	std::vector<Walk_Node_Element> & cand_list, 
									select_next_hop_data & SD, 
									unsigned int & last_brown_pos);
	
		bool check_cand_usage(Walk_Node_Element& current_WNE);
	
		void update_cand_supp_dc (select_next_hop_data & SD, 
							Walk_Node_Element& current_WNE);
	
		void update_cand_scores (	select_next_hop_data & SD, 
							Walk_Node_Element& current_WNE);
	
	void single_brown_cand_adjust(	Walk_Queue_Element& WQE, 
								select_next_hop_data& SD, 
								unsigned int & last_brown_pos);
	
	
	bool allPaths(	Walk_Queue_Element& WQE, 
				select_next_hop_data& SD);
	
	
		// our last savior when a hop can't be selected
		bool mini_all_paths(Walk_Queue_Element& WQE);
			
			void update_decided_strs(	const mini_all_paths_data& MAPD, 
									const std::vector< unsigned int >& path_indices);
		
			void map_wrap_up(mini_all_paths_data & MAPD, wall_time_wrap & W);
	

	/************************* main function 2*******************************/
	void select_best_candidate(Walk_Queue_Element& WQE, 
							select_next_hop_data& SD);
	/************************* end : main function 2*******************************/
	

	/**************** HELPER FUNCTIONS: find_best_candidate*******************/
	
	
	// method for updating the expected usage at back nodes, used in select_next_hop
	// TODO: is it only backnode?
	void update_dc_usage_backnode(std::vector<dc_str>& dc_vec,
										Walk_Queue_Element& WQE, 
										bool is_intra_read);
	
		// helper function for update_dist_const_usage_backnode
		void accumulate_dc(std::vector< dc_str >& dc_vec);
	
		void update_exp_dc_usage(	unsigned int usage, 
								Walk_Node_Element& updatable_white_node, 
								Walk_Node_Element& ref_white_node, 
								dc_str& dc_entry, 
								bool is_intra_read, 
								int prev_lmer_len);	
	
	// update expected usage as recent as possible, and mark the unupdated area
	void update_expected_usage( Walk_Queue_Element& WQE );
	
		// get the length of white node at pos
		Walk_Node_Element& get_white_node(unsigned int pos);
		
		unsigned int next_white_pos(const unsigned int pos);
		unsigned int next_white_pos(const unsigned int pos, const unsigned int white_len);
		
		bool check_dc_overshoot(const unsigned int next_unup_pos);
		
		void make_unupdated(Walk_Node_Element& WNE);
		
		bool reduce_usage_left(Walk_Node_Element& WNE);
		
		void make_updated(Walk_Node_Element& WNE);
	
// 	const unsigned int get_WNE_len(Walk_Node_Element& WNE);
	
		void refine_init_covg(Walk_Node_Element & WNE);
	
		void update_exp_covg(Walk_Node_Element & WNE);
	

	/****************************** function 3 of select_next_hop *********************/
	void reset_unupdated_vec();
	
	
	void beg_salutation(std::string fn_name);
	void end_salutation(std::string fn_name);
	
	void beg_salutation_indent(std::string fn_name);
	void end_salutation_indent(std::string fn_name);
	
	// LOGGER FUNCTIONS
	
	void log_msg_1(const float score_diff_norm, const float score_diff_abs);
	void log_msg_2();
	void log_msg_3();
	void log_msg_4(Walk_Node_Element& prev_white_node, sat_const_str& S);
	void log_msg_5(const unsigned int max_dist_const_dest, const unsigned int next_unup_pos);
	void log_msg_6();
	void log_msg_7();
	void log_msg_8();
	void log_msg_9(const unsigned int next_unup_pos);
	void log_msg_10(const Walk_Queue_Element& WQE, const select_next_hop_data& SD);
	void log_msg_11(select_next_hop_data& SD);
	void log_msg_12(select_next_hop_data& SD);
	void log_msg_13(const unsigned int d);
	void log_msg_14();
	void log_msg_15(	const unsigned int d_depth_node_count, 
					const unsigned int d, 
					const mini_all_paths_data& MAPD, 
					const unsigned int current_depth_beg);
	void log_msg_16(std::vector< unsigned int >& path_indices, mini_all_paths_data& MAPD);
	void log_msg_17();
	void log_msg_18(unsigned int brown_size);
	void log_msg_19();
	void log_msg_20(const Walk_Node_Element& current_WNE);
	void log_msg_21(const Walk_Node_Element& current_WNE);
	void log_msg_22(std::size_t i, select_next_hop_data& SD, Walk_Node_Element& current_WNE);
	void log_msg_23();
	


public:
	
	select_next_hop_class(Walk_Queue& WQ);
	
	// go through the brown list and select the next hop, crucial function in the walk
	hop_return select_next_hop(		Walk_Queue_Element& WQE, 
								unsigned int neigh_count);
	
// 	friend class Walk_Queue;
	friend class Walk_Queue_Par;
	
};

#endif // SELECT_NEXT_HOP_H