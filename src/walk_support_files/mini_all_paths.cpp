#include "mini_all_paths.h"
#include "../walk.h"

mini_all_paths_data::mini_all_paths_data(Walk_Queue& WQ):	
								log_file_(WQ.WBptr->ostr_.log_file), 
								bub_file_(WQ.WBptr->ostr_.bub_file),
								print_level(WQ.print_level), 
								infoLevel(WQ.WBptr->P_->walk_info_level), 
								exp_nul_vec(WQ.WBptr->UD.expected_node_usage_left), 
								exp_nul_unup_vec(WQ.WBptr->UD.expected_node_usage_left_unupdated), 
								Pref  (*(WQ.WBptr->P_)), 
								BSV_(WQ.WBptr->BSV)
{
	max_dist = WQ.WBptr->P_->all_paths_dist;
	max_num_nodes = WQ.WBptr->P_->all_paths_size;
	k = WQ.WBptr->P_->k;
	curr_copy = WQ.curr;
	bubble_found = false;
	max_depth_accessed=0;
}

void mini_all_paths_data::assert_equal_sizes()
{
	assert(depth.size() == parent.size());
	assert(depth.size() == head_offset.size());
	assert(depth.size() == is_leaf.size());
	assert(depth.size() == score_vec.size());
	assert(depth.size() == nodes.size());
	assert(depth.size() == local_node_usage_left.size());
}

unsigned int mini_all_paths_data::add_root_brown_nodes(	Walk_Queue_Element& WQE, 
												Walk_Queue& WQ)
{
	beg_salutation("add_root_brown_nodes");
	
	ASSERT_WITH_MESSAGE(nodes.empty(), "");
	
	for (unsigned int i=0; i < WQE.candidate_nodes.size(); i++) {
		// basic check
		if (i > max_num_nodes) {
			log_msg4();
			break;
		}
		
		const Walk_Node_Element & curr_WNE = WQE.candidate_nodes[i];
		
		long exp_nul_entry;
		
		// brown and usage check, also get exp_nul_entry
		bool valid = check_root_node(curr_WNE, exp_nul_entry, WQ);
		if (not valid) {
			continue;
		}
		
		
		// push the brown node 
		nodes.push_back(curr_WNE);
		
		// set data about the root node pushed
		set_node_data();
		
		local_node_usage_left.push_back(exp_nul_entry);
		for (unsigned int j=0; j < local_node_usage_left.size(); j++) {
			local_node_usage_left[j] = (WQ.exp_covg > local_node_usage_left[j]) ? 0 : local_node_usage_left[j] - WQ.exp_covg;
		}
		
		// calc score for added node, also set it's dist const in vecs
		float root_score = calc_root_score(WQ);
		
		// set up complementry structures corr. to pushed root node
		set_MAP_strs_rt(i, root_score);
		
		assert(nodes.size() <=4);
		
	}
	assert_equal_sizes();
	ASSERT_WITH_MESSAGE(nodes.size() == root_cand_dc_inter_read.size(), "");
	ASSERT_WITH_MESSAGE(nodes.size() == root_cand_dc_intra_read.size(), "");
	
	end_salutation("add_root_brown_nodes");
	
	return nodes.size();
}

/**
 * @brief ...add brown nodes at depth d, and keep updating add_node_pos, return number of nodes added
 * 
 * @param curr_depth ...depth at which nodes are being added
 * @param add_node_pos ...node position whose children are being added
 * @param WQ ...Walk queue instance
 * @return unsigned int
 */
unsigned int mini_all_paths_data::add_current_brown_nodes(	unsigned int curr_depth, 
													unsigned int& add_node_pos, 
													Walk_Queue& WQ)
{
	beg_salutation("add_current_brown_nodes");
	
	// total nodes added on this depth
	unsigned int node_count=0;
	
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	
	// for all nodes at current depth, add children
	while(depth[add_node_pos]==curr_depth-1) {
		
		// get the current node at add_node_pos
		unsigned int & curr_lmer_id = nodes[add_node_pos].tar_lmer_id;
		DB_Node_Type const & D = DB_Table.ElementAt(curr_lmer_id);
		
		// tree depth of current node
		const unsigned int tail_offset = head_offset[add_node_pos] + D.len_lmer - (k-1);
		
		// only add children if bp depth and buff size have not exceeded limit
		if (tail_offset < max_dist and nodes.size() <= max_num_nodes) {
			
			// get the correct direction
			char start = nodes[add_node_pos].start;
			
			// add all neighbors of add_node_pos
			unsigned int new_nodes = WQ.get_neighbors(nodes, D, start);
			
			assert(nodes.size() >= new_nodes);
			
			// delete all nodes with no usage, and on the side, also set the start and update new_nodes var
			remove_no_usg_nodes(new_nodes, add_node_pos, WQ);
			
			// non usage nodes deleted
			
			assert(nodes.size() == local_node_usage_left.size());
			
			// update the total count
			node_count += new_nodes;
			
			set_MAP_strs_gen(new_nodes, curr_depth, add_node_pos, tail_offset);
			
		} // END: if (offset[ add_node_pos ] < max_dist) {
		add_node_pos++;
		if (add_node_pos >= depth.size()) {
			break;
		}
	} //END: while(depth[add_node_pos]==d-1)
	
	end_salutation("add_current_brown_nodes");
	
	return node_count;
}


void mini_all_paths_data::calc_curr_depth_scores(	unsigned int curr_depth_node_count, 
											Walk_Queue& WQ)
{
	beg_salutation("calc_curr_depth_scores");
	
	unsigned int currDepBeg=nodes.size() - curr_depth_node_count;
	unsigned int currDepEnd=nodes.size()-1;

	// calculate score for each candidate at depth d
	for (unsigned int i=currDepBeg; i<=currDepEnd; i++) {
		Walk_Node_Element & curr_WNE = nodes[i];
		
		// candidate support is gathered around curr
		WQ.curr = (curr_copy + head_offset[i])%WQ.Q.size();
		
		float score = calc_gen_score(curr_WNE, WQ);
		
		// add the score of parent and push
		score += score_vec[ parent[i] ];
		score_vec.push_back(score);
		
	}
	
	assert_equal_sizes();
	
	end_salutation("calc_curr_depth_scores");
}


bool mini_all_paths_data::attempt_decision(unsigned int& best_score_index, Walk_Queue& WQ)
{
	beg_salutation("attempt_decision");
	
	bool decision = false;
	std::vector<unsigned int> leaf_path_length;
	
	
	scoreData best;
	scoreData secBest;
	
	best.root=1000;
	secBest.root = 2000;
	secBest.index = MAX_UINT;
	best.index = best_score_index;
	
	// calculate two best leaves in the current tree
	calc_two_best_leaves(best, secBest, WQ);
	
	unsigned int sec_best_score_index=secBest.index;
	best_score_index = best.index;
	
	// decision time
	if (best.root == secBest.root) {
		assert(sec_best_score_index not_eq MAX_UINT);
		assert(best_score_index not_eq MAX_UINT);
		
		unsigned int lca_index = find_lca(best_score_index, sec_best_score_index);
		
		unsigned int lca_len = WQ.WBptr->ref_db_node_table.ElementAt(nodes[lca_index].tar_lmer_id).len_lmer;
		unsigned int reasonable_l = std::min(head_offset[lca_index] + lca_len, WQ.WBptr->P_->insert_size);
		
		float lca_nscore = score_vec[lca_index]/float(reasonable_l - k + 1);
		
		if (WQ.is_good_score(lca_nscore, 0, 100, 0)) {
			
			decision = true;
			
			best_score_index = lca_index;
			
			log_msg3(best_score_index, WQ);
		}
		else {
			if (WQ.is_good_score(best.normScore, secBest.normScore, best.absScore, secBest.absScore)) {
				decision = true;
				
				best_score_index = best.index;
				
				log_msg3(best_score_index, WQ);
				
			}
		}
	}
	else {
		// broot not_eq sbroot
		bubble_found = false;
		unsigned int bubble_index = findBubble(best_score_index, sec_best_score_index);
		
		if (bubble_found) {
			
			assert(sec_best_score_index not_eq MAX_UINT);
			assert(best_score_index not_eq MAX_UINT);
			assert(bubble_index not_eq MAX_UINT);
			
			unsigned int lca_index = bubble_index;
			
			unsigned int lca_len = WQ.WBptr->ref_db_node_table.ElementAt(nodes[lca_index].tar_lmer_id).len_lmer;
			unsigned int reasonable_l = std::min(head_offset[lca_index] + lca_len, WQ.WBptr->P_->insert_size);
			
			float lca_nscore = score_vec[lca_index]/float(reasonable_l - k + 1);
			
			if (WQ.is_good_score(lca_nscore, 0, 100, 0)) {
				
				decision = true;
				
				best_score_index = lca_index;
				
				log_msg3(best_score_index, WQ);
			}
			
		}
		else {
			// no bubble
			if (WQ.is_good_score(best.normScore, secBest.normScore, best.absScore, secBest.absScore)) {
				decision = true;
				
				best_score_index = best.index;
				
				log_msg3(best_score_index, WQ);
				
			}
			
		}
		
	}
	
	
	
	
	// decision time
// 	if (best.root == secBest.root and WQ.is_good_score(best.normScore, 0, 100,0)) {
// 		assert(sec_best_score_index not_eq MAX_UINT);
// 		assert(best_score_index not_eq MAX_UINT);
// 		
// 		// have the same ancestor, find the lca and you are done
// 		decision = true;
// 		
// 		best_score_index = find_lca(best_score_index, sec_best_score_index);
// 		
// 		log_msg3(best_score_index, WQ);
// 	}
// 	else if (WQ.is_good_score(best.normScore, secBest.normScore, best.absScore, secBest.absScore)) {
// 		decision = true;
// 	}
// 	else {
// 		
// 		bubble_found = false;
// 		unsigned int bubble_index = findBubble(best_score_index, sec_best_score_index);
// 		
// 		if (bubble_found) {
// 			if (WQ.is_good_score(best.normScore, 0, 100,0)) {
// 				assert(bubble_index not_eq MAX_UINT);
// 				best_score_index = bubble_index;
// 				decision=true;
// 			}
// 		}
// 		else {
// 			assert(bubble_index == MAX_UINT);
// 		}
// 	}
	
	end_salutation("attempt_decision");
	return decision;
}

void mini_all_paths_data::trace_ancestor_path(	std::vector< unsigned int >& path_indices, 
										const unsigned int index)
{
	beg_salutation("trace_ancestor_path");
	
	unsigned int curr_index=index;
	// trace back the path, and put nodes in path_indices
	while(true) {
		path_indices.push_back(curr_index);
		curr_index = parent[curr_index];
		if (curr_index==MAX_UINT) {
			break;
		}
	}
	
	end_salutation("trace_ancestor_path");

}

void mini_all_paths_data::print_vecs(std::ostream& os)
{
	if (infoLevel >=3) {
		os << std::string(print_level, '\t') << "Nodes:\n" << nodes << std::endl;
		os << std::string(print_level, '\t') << "Depth:\n" << depth << std::endl;
		os << std::string(print_level, '\t') << "Parent:\n" << parent << std::endl;
		os << std::string(print_level, '\t') << "Head Offset:\n" << head_offset << std::endl;
	}
}

void mini_all_paths_data::clear(Walk_Queue& WQ)
{
	// restore back curr
	WQ.curr = curr_copy;
	
	// clear structures
	nodes.clear();
	local_node_usage_left.clear();
	depth.clear();
	parent.clear();
	head_offset.clear();
	is_leaf.clear();
	score_vec.clear();
	
	root_cand_dc_inter_read.clear();
	root_cand_dc_intra_read.clear();
}

size_t mini_all_paths_data::size()
{
	size_t bytes=0;
	bytes += 5*sizeof(int) 
			+ sizeof(int&) 
			+ vecsize(depth) 
			+ vecsize(parent) 
			+ vecsize(root) 
			+ vecsize(head_offset) 
			+ vecsize(is_leaf) 
			+ vecsize(score_vec) 
			+ vecsize(local_node_usage_left);
	
	for (size_t i=0; i < nodes.size(); i++) {
		bytes += nodes[i].size();
	}
	
	for (size_t i=0; i < root_cand_dc_inter_read.size(); i++) {
		bytes += vecsize(root_cand_dc_inter_read);
	}
	
	for (size_t i=0; i < root_cand_dc_intra_read.size(); i++) {
		bytes += vecsize(root_cand_dc_intra_read);
	}

	return bytes;
}


/******************************** HELPER FUNCTIONS *************************************/

bool mini_all_paths_data::check_ancestor_match(unsigned int& modNodeUsgLeft, const unsigned int & add_node_pos, const unsigned int & brown_lid)
{
	// check all ancestors to see if current node is present in the tree
	
	unsigned int usage_checking_pos = add_node_pos;
	while (true) {
		if (nodes[ usage_checking_pos ].tar_lmer_id == brown_lid) {
			// match found
			
			modNodeUsgLeft = local_node_usage_left[ usage_checking_pos ];
			return true;
			break;
		}
		if (parent[usage_checking_pos] == MAX_UINT) {
			break;
		}
		usage_checking_pos = parent[usage_checking_pos];
	}
	return false;
}

void mini_all_paths_data::accept_node(const unsigned int modNodeUsgLeft, const unsigned int i, Walk_Queue & WQ)
{
	nodes[i].start = (nodes[i].end + 8)%16;
	local_node_usage_left.push_back(modNodeUsgLeft);
	assert(local_node_usage_left.size()==i+1);
	local_node_usage_left[i] = (WQ.exp_covg > local_node_usage_left[i]) ? 0 : local_node_usage_left[i] - WQ.exp_covg;
}


unsigned int mini_all_paths_data::find_lca(const unsigned int best_score_index, const unsigned int sec_best_score_index)
{
	std::vector<bool> lca(root.size(), false);
	unsigned int curr_index = sec_best_score_index;
	while(true) {
		lca[curr_index] = true;
		curr_index = parent[curr_index];
		if (curr_index==MAX_UINT) {
			break;
		}
	}
	curr_index = best_score_index;
	while(true){
		if (curr_index==MAX_UINT or lca[curr_index]) {
			break;
		}
		curr_index = parent[ curr_index ];
	}
	return curr_index;
}

unsigned int mini_all_paths_data::findBubble(const unsigned int best_index, const unsigned int sbest_index)
{
	if (best_index==MAX_UINT or sbest_index==MAX_UINT) {
		return MAX_UINT;
	}
	
	if (not bubble_found) {
		std::vector<unsigned int> anc_idx_best;
		std::vector<unsigned int> anc_idx_sbest;
		
		trace_ancestor_path(anc_idx_best, best_index);
		trace_ancestor_path(anc_idx_sbest, sbest_index);
		
		return checkBubble_bf(anc_idx_best, anc_idx_sbest);
		
	}
	return MAX_UINT;
}

unsigned int mini_all_paths_data::checkBubble_bf(std::vector< unsigned int >& path1, std::vector< unsigned int >& path2)
{
	for (int i=path1.size() - 1; i>=0; i--) {
		if (bubble_found) {
			break;
		}
		for (int j=path2.size() - 1; j>=0; j--) {
			
			if (bubble_found) {
				break;
			}
			
			unsigned int idx1 = path1[i];
			unsigned int idx2 = path2[j];
		
			// head offsets
			unsigned int ho1 = head_offset[idx1];
			unsigned int ho2 = head_offset[idx2];
	
			// if their length diff is within bounds
			if (std::max(ho1, ho2) - std::min(ho1, ho2) <= Pref.max_bubble_diff) {
				// compare the WNE's
				Walk_Node_Element& w1 = nodes[idx1];
				Walk_Node_Element& w2 = nodes[idx2];
				
				if (w1.tar_lmer_id == w2.tar_lmer_id and w1.end == w2.end) {
					
					
					bubble_str B;
					make_bubble_str(ho1, ho2, path1, path2, nodes, B);
					BSV_.push(B);
					
					
// 					bub_file_ << "=====Beg=====" << std::endl;
// 					for (int x=path1.size()-1; x>=0; x--) {
// 						bub_file_ << nodes[path1[x]].tar_lmer_id << ", ";
// 					}
// 					bub_file_ << " || offset: " << ho1 << std::endl;
// 					for (int x=path2.size()-1; x>=0; x--) {
// 						bub_file_ << nodes[path2[x]].tar_lmer_id << ", ";
// 					}
// 					bub_file_ << " || offset: " << ho2 << std::endl;
// 					
// 					bub_file_ << "=====End=====" << std::endl;
					
					bubble_found=true;
					
					return idx1;
				}
			}
		}
	}
	return MAX_UINT;
}

void mini_all_paths_data::make_bubble_str(unsigned int ho1, unsigned int ho2, std::vector< unsigned int >& path1, std::vector< unsigned int >& path2, std::vector< Walk_Node_Element >& nodes, bubble_str& B)
{
	for (int x=path1.size()-1; x>=0; x--) {
		B.tar_lmer_id_vec1.push_back(nodes[path1[x]].tar_lmer_id);
	}
	B.head_offset1 = ho1;
	for (int x=path2.size()-1; x>=0; x--) {
		B.tar_lmer_id_vec2.push_back(nodes[path2[x]].tar_lmer_id);
	}
	B.head_offset2 = ho2;
}


void mini_all_paths_data::checkBubble_eff(std::vector< unsigned int >& path1, std::vector< unsigned int >& path2)
{
// 		int beg_success_idx = anc_idx_sbest.size()-1;
// 		bool success_region;
// 		
// 		for (int i=anc_idx_best.size() - 1; i>=0; i--) {
// 			
// 			int j = beg_success_idx;
// 			success_region=false;
// 			while(j>=0) {
// 				
// 				unsigned int idx1 = anc_idx_best[i];
// 				unsigned int idx2 = anc_idx_sbest[j];
// 				
// 				// head offsets
// 				unsigned int ho1 = head_offset[idx1];
// 				unsigned int ho2 = head_offset[idx2];
// 				
// 				// if their length diff is within bounds
// 				if (std::max(ho1, ho2) - std::min(ho1, ho2) <= Pref.max_bubble_diff) {
// 					
// 					if (not success_region) {
// 						beg_success_idx = j;
// 						success_region=true;
// 					}
// 					
// 					// compare the WNE's
// 					Walk_Node_Element& w1 = nodes[idx1];
// 					Walk_Node_Element& w2 = nodes[idx2];
// 					
// 					if (w1.tar_lmer_id == w2.tar_lmer_id/* and w1.end == w2.end*/) {
// 						
// 						std::cout << "Bubble found" << std::endl;
// 						
// 						// etc etc
// 						bubble_found=true;
// 					}
// 					
// 				}
// 				else {
// 					if (success_region) {
// 						break;
// 					}
// 				}
// 				j--;
// 			}
// 			if (bubble_found) {
// 				break;
// 			}
// 		}

}


// void mini_all_paths_data::get_ancestor_nodes(std::vector< Walk_Node_Element& >& anc_nodes, const unsigned int idx)
// {
// 	// vector for ancestor path
// 	std::vector<unsigned int> anc_idx_vec;
// 	
// 	// trace indices
// 	trace_ancestor_path(anc_idx_vec, idx);
// 	
// 	// push corr nodes
// 	for (unsigned int i=0; i < anc_idx_vec.size(); i++) {
// 		unsigned int anc_idx = anc_idx_vec[i];
// 		anc_nodes.push_back(nodes[anc_idx]);
// 	}
// }

// void mini_all_paths_data::get_idx_nodes(std::vector< Walk_Node_Element& >& nodes, const std::vector< unsigned int >& idx_vec)
// {
// 	// push corr nodes
// 	for (unsigned int i=0; i < idx_vec.size(); i++) {
// 		unsigned int idx = idx_vec[i];
// 		nodes.push_back(nodes[idx]);
// 	}
// 
// }



bool mini_all_paths_data::check_root_node(const Walk_Node_Element& curr_WNE, long int& exp_nul_entry, Walk_Queue& WQ)
{
	
	const unsigned int curr_lmer_id = curr_WNE.tar_lmer_id;
	
	// brown check
	if (curr_WNE.color not_eq BROWN) {
		return false;
	}
	
	exp_nul_entry =  exp_nul_vec[ curr_lmer_id ].load();
	const int exp_nul_unup_entry = exp_nul_unup_vec[ curr_lmer_id ].load();
	
	// usage check
	if (exp_nul_entry <= 0) {
		return false;
	}
	if (exp_nul_unup_entry > 0 and exp_nul_entry < (long int)std::round((1.0 - Pref.coverage_fudge)*(float)WQ.exp_covg)) {
		return false;
	}
	return true;
}


void mini_all_paths_data::set_node_data()
{
	// the most recent root node pushed
	const unsigned int last_index=nodes.size() - 1;
	Walk_Node_Element & last_WNE = nodes[last_index];
	
	// set stuff in the node just pushed
	last_WNE.start = (last_WNE.end + 8)%16;
	last_WNE.in_dc_inter=0;
	last_WNE.out_dc_inter=0;
	last_WNE.in_dc_intra=0;
	last_WNE.out_dc_intra=0;
}

float mini_all_paths_data::calc_root_score(Walk_Queue& WQ)
{
	std::vector<support_str> root_support;
	std::vector<support_str> root_support_intra_read;
	
	root_cand_dc_inter_read.push_back(std::vector<dc_str>());
	root_cand_dc_intra_read.push_back(std::vector<dc_str>());
	
	
	const unsigned int last_index=nodes.size() - 1;
	WQ.gather_cand_support(	root_support, 
							root_cand_dc_inter_read[ last_index ], 
							root_support_intra_read, 
							root_cand_dc_intra_read[ last_index ], 
							nodes[last_index]);
	
	float score = WQ.evidence_score(root_support, root_support_intra_read);
	
	return score;

}

void mini_all_paths_data::set_MAP_strs_rt(const unsigned int idx, float root_score)
{
	is_leaf.push_back(true);
	depth.push_back(0);
	parent.push_back(MAX_UINT);
	root.push_back(idx);
	head_offset.push_back(0);
	score_vec.push_back(root_score);

}


void mini_all_paths_data::remove_no_usg_nodes(	unsigned int& new_nodes, 
											const unsigned int add_node_pos, 
											Walk_Queue& WQ)
{
	for (unsigned int i=nodes.size() - new_nodes;;) {
		if (i>=nodes.size()) {
			break;
		}
		
		const unsigned int & brown_lid = nodes[i].tar_lmer_id;
		
		// check if the node exists as an ancestor, if yes, then use that usage
		unsigned int estNodeUsgLeft = exp_nul_vec[brown_lid].load();
		bool ancestor_match_found = check_ancestor_match(	estNodeUsgLeft, 
														add_node_pos, 
														brown_lid);
		
		// if usage left (either through ancestor or otherwise) is zero, no further questions
		if (estNodeUsgLeft==0) {
			nodes.erase(nodes.begin() + i);
			new_nodes--;
		}
		else {
			// usage left non zero and an ancestor found, so just take from there
			if (ancestor_match_found) {
				// accept the node and set start
				accept_node(estNodeUsgLeft, i, WQ);
				i++;
			}
			// usage left non zero and no ancestor found
			else {
				const int unupdated_usage_entry = exp_nul_unup_vec[ brown_lid ].load();
				
				// it's not the latest value
				if (unupdated_usage_entry > 0) {
					// if the available value is quite less, then surely no usage left for this time, so delete
					if (estNodeUsgLeft < WQ.exp_covg) {
						nodes.erase(nodes.begin() + i);
						new_nodes--;
					}
					// if the available value is enough, go ahead and take it
					else {
						// accept the node and set start
						accept_node(estNodeUsgLeft, i, WQ);
						i++;
					}
				}
				// it is the latest value
				else {
					accept_node(estNodeUsgLeft, i, WQ);
					i++;
				}
				
			}
		}
	} // END: for (unsigned int i=nodes.size() - new_nodes;;) {

}

void mini_all_paths_data::set_MAP_strs_gen(const unsigned int new_nodes, const unsigned int curr_depth, const unsigned int add_node_pos, const unsigned int next_offset)
{
	// update the supporting structures
	for (unsigned int j=0; j<new_nodes; j++) {
		
		depth.push_back(curr_depth);
		parent.push_back(add_node_pos);
		root.push_back(root[add_node_pos]);
		head_offset.push_back(next_offset);
		is_leaf.push_back(true);
		
		max_depth_accessed = std::max(max_depth_accessed, next_offset);
	}
	// parent is no more a leaf
	is_leaf[ add_node_pos ] = false;
}

float mini_all_paths_data::calc_gen_score(Walk_Node_Element& curr_WNE, Walk_Queue& WQ)
{
	curr_WNE.in_dc_inter=0;
	curr_WNE.out_dc_inter=0;
	
	curr_WNE.in_dc_intra=0;
	curr_WNE.out_dc_intra=0;
	
	std::vector<dc_str> m1;
	std::vector<dc_str> m2;
	std::vector<support_str> cand_support;
	std::vector<support_str> cand_support_intra_read;
	
	// gather support
	WQ.gather_cand_support(cand_support, m1, cand_support_intra_read, m2, curr_WNE);
	
	float score = WQ.evidence_score( cand_support, cand_support_intra_read );
	
	return score;

}

void mini_all_paths_data::calc_two_best_leaves(scoreData& best, scoreData& secBest, Walk_Queue& WQ)
{
	for (unsigned int i=0; i < score_vec.size(); i++) {
		if (is_leaf[i]) {
			unsigned int leaf_len = WQ.WBptr->ref_db_node_table.ElementAt(nodes[i].tar_lmer_id).len_lmer;
			unsigned int reasonable_l = std::min(head_offset[i] + leaf_len, WQ.WBptr->P_->insert_size);
			
			float norm_score = score_vec[i]/float(reasonable_l - k + 1);
			
			scoreData currLeafData;
			
			currLeafData.normScore = norm_score;
			currLeafData.absScore = score_vec[i];
			currLeafData.root = root[i];
			currLeafData.index = i;
			
			if (norm_score > secBest.normScore) {
				secBest = currLeafData;
			}
			if (norm_score > best.normScore) {
				secBest = best;
				best = currLeafData;
			}
			
			log_msg1(i, leaf_len, norm_score, WQ);
		}
	}
}


// LOGGER FUNCTIONS

void mini_all_paths_data::log_msg1(const unsigned int i, const unsigned int leaf_len, const float norm_score, Walk_Queue& WQ)
{
	if (infoLevel >=3) {
		log_file_ << std::string(print_level+1, '\t') << "leaf tar_lmer_id: " << nodes[i].tar_lmer_id << ", leaf_len: " << i << ", par: " << parent[i] << ", root: " << root[i] << ",  head offset: " << head_offset[i] << ", score: " << score_vec[i] << ", n_score: " << norm_score << std::endl;
			}
}

void mini_all_paths_data::log_msg2(	const float score_diff_norm, 
								const float min_score_diff_norm, 
								const float score_diff_abs, 
								const float min_score_diff_abs, 
								Walk_Queue& WQ)
{
	if (infoLevel >=3) {
		log_file_ 	<< std::string(print_level+1, '\t') 
				<< "NORM: score_diff_norm, min_score_diff_norm: " 
				<< score_diff_norm << ", " << min_score_diff_norm  
				<< std::endl;
				
		log_file_ 	<< std::string(print_level+1, '\t') 
				<< "ABS: score_diff_abs, min_score_diff_abs: " 
				<< score_diff_abs << ", " << min_score_diff_abs << std::endl;
	}
}

void mini_all_paths_data::log_msg3(	const unsigned int best_score_index, 
								Walk_Queue& WQ)
{
	if (infoLevel >=3) {
		log_file_ 	<< std::string(print_level, '\t') 
				<< "best and second best have same ancestor, returning best index: " 
				<< best_score_index << std::endl;
	}
}

void mini_all_paths_data::log_msg4()
{
	if (infoLevel >=3) {
		log_file_ << "Too many nodes in mini_all_paths root posn, quitting" << std::endl;
	}

}

void mini_all_paths_data::log_msg5()
{

}

void mini_all_paths_data::log_msg6()
{

}

void mini_all_paths_data::beg_salutation(std::string fn_name)
{
	print_level++;
	if (infoLevel >=3) {
		log_file_ 	<< std::string(print_level, '\t') 
				<< "IN " << fn_name 
				<< std::endl;
	}
}

void mini_all_paths_data::end_salutation(std::string fn_name)
{
	if (infoLevel >=3) {
		log_file_ 	<< std::string(print_level, '\t') 
				<< "DONE " << fn_name 
				<< std::endl;
	}
	print_level--;
}
