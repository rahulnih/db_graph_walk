#include "select_next_hop.h"
#include "../walk.h"
#include "mini_all_paths.h"


select_next_hop_data::select_next_hop_data(Walk_Queue& WQ) : log_file_(WQ.WBptr->ostr_.log_file) {
	cand_score_abs = 0;
	cand_score_norm = 0;
	best_cand_score_norm = MIN_FLOAT;
	sec_best_cand_score_norm = MIN_FLOAT;
	best_cand_score_abs = MIN_FLOAT;
	sec_best_cand_score_abs = MIN_FLOAT;
	best_cand_position = 0;
}

void select_next_hop_data::update_best_cand_data(std::size_t pos) {
	if (cand_score_norm > sec_best_cand_score_norm) {
		sec_best_cand_score_norm = cand_score_norm;
		sec_best_cand_score_abs = cand_score_abs;
	}
	if (cand_score_norm > best_cand_score_norm) {
		sec_best_cand_score_norm = best_cand_score_norm;
		sec_best_cand_score_abs = best_cand_score_abs;
		
		best_cand_score_norm = cand_score_norm;
		best_cand_score_abs = cand_score_abs;
		
		best_cand_position = pos;
		best_cand_dc_inter = cand_dc_inter;
		best_cand_dc_intra = cand_dc_intra;
	}
}

void select_next_hop_data::pick_decided_hop(	Walk_Queue& WQ, 
										Walk_Queue_Element& WQE) {
	// get the data from decided nodes, and proceed
	best_cand_position = MAX_SIZE_T;
	
	Walk_Node_Element best_WNE = WQ.DHS.hops[WQ.DHS.hops.size()-1];
	WQ.DHS.hops.pop_back();
	
	// find the best candidate position
	for (std::size_t i=0; i < WQE.candidate_nodes.size(); i++) {
		
		Walk_Node_Element& curr_WNE = WQE.candidate_nodes[i];
		
		if ((curr_WNE.tar_lmer_id == best_WNE.tar_lmer_id) and (curr_WNE.end==best_WNE.end) and (curr_WNE.color==BROWN)) {
			best_cand_position = i;
			// no need to update the incoming_dist_const, they have been updated previously while gathering support
			curr_WNE.in_dc_inter = best_WNE.in_dc_inter;
			curr_WNE.in_dc_intra = best_WNE.in_dc_intra;
			break;
		}
	}
	
	// decided_hops didn't choose anything
	assert(best_cand_position not_eq MAX_SIZE_T);
	
}

// regather support for best candidate 
void select_next_hop_data::regather_best_cand_support(	Walk_Queue& WQ, 
												Walk_Queue_Element& WQE)
{
	// Error in regather_best_cand_support, best_cand_position not up to date
	assert(best_cand_position not_eq MAX_SIZE_T);
	Walk_Node_Element & best_WNE = WQE.candidate_nodes[ best_cand_position ];

	// create the useless vectors
	std::vector<support_str> useless_vector;
	std::vector<support_str> another_useless_vector;
	
	// clear the dist const vectors
	best_cand_dc_inter.clear();
	best_cand_dc_intra.clear();
	
	// reset the incoming dist const to 0
	best_WNE.in_dc_inter=0;
	best_WNE.in_dc_intra=0;
	
	WQ.gather_cand_support(	useless_vector, 
							best_cand_dc_inter, 
							another_useless_vector, 
							best_cand_dc_intra, 
							best_WNE);
	
	// clear the useless vectors
	useless_vector.clear();
	another_useless_vector.clear();
}

// clear all vecs
void select_next_hop_data::clear() {
	clear_support_vecs();
	clear_dc_vecs();
	best_cand_dc_inter.clear();
	best_cand_dc_intra.clear();
}



/*******************************SELECT NEXT HOP CLASS *********************/


select_next_hop_class::select_next_hop_class(Walk_Queue& WQ):
											WQ(WQ), 
											walk_info_level_(WQ.walk_info_level_), 
											log_file_(WQ.log_file_), 
											print_level(WQ.print_level)
{
	t_log_file_ptr = WQ.t_log_file_ptr;
}



// MANAGE is_intra_read IN THE DIST CONST, AND USE THEM
// go through the brown list and select the next hop, crucial function in the walk
hop_return select_next_hop_class::select_next_hop(	Walk_Queue_Element& WQE, 
											unsigned int neigh_count) {
	
	wall_time_wrap W;
	W.start();
	
	beg_salutation("SELECT NEXT HOP");
	
	// initiate data for the function
	select_next_hop_data SD(WQ);
	
	bool found;
	hop_return ret_val;
	
	// sum of all neighbors of current candidates
	if (neigh_count==0) {
		
		assert(not WQ.CCS.no_neigh);
		
		// this is used only for calculating isolated contigs, so not much effect on output contigs
		WQ.CCS.no_neigh=true;
	}
	
	
	found = find_best_candidate(WQE, SD, SER);
	
	if (found) {
		// some decision has come out
		select_best_candidate(WQE, SD);
		ret_val = SAME_PAR_HOP;
	}
	else {
		reset_unupdated_vec();
		// TODO: assert unupdated_vec 0?
		WQ.clear_buffer(WQ.beg, (WQ.curr+1)%WQ.Q.size());
		WQ.CCS.curr_contig.append(WQ.TSS.hanging_seq);
		ret_val = NO_HOP;
	}
	
	end_salutation("SELECT NEXT HOP");
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr, "SELECT NEXT HOP");
	
	return ret_val;
	
} // End select_next_hop()

bool select_next_hop_class::find_best_candidate(	Walk_Queue_Element& WQE, 
											select_next_hop_data& SD, 
											Walk_Type wt)
{
	bool found;
	
	// start timing
	wall_time_wrap W;
	W.start();
	
	// check if prev all paths had left something
	found = check_decided_stuff(WQE, SD);
	
	// nothing is decided, need to do all the work
	if (not found) {
	
		// check if there is some usage left in proceeding forward
		SD.best_cand_position = MAX_SIZE_T;
		unsigned int last_brown_pos=MAX_UINT;
		
		// update scores for all cands, best score, last_brown_position
		bool no_usage=update_allCand_n_best_scores(	WQE.candidate_nodes, 
												SD, 
												last_brown_pos);
		
		if (no_usage) {
			
			// no way to proceed, wrap up the contig
			log_msg_23();
			found = false;
		}
		
		else {
			
			// usage is there, but no brown node
			assert( last_brown_pos not_eq MAX_UINT);
			
			// make special SD adjustment if there's only one brown candidate
			single_brown_cand_adjust(WQE, SD, last_brown_pos);
			
// 			log_msg_1(score_diff_norm, score_diff_abs);
			bool success = WQ.is_good_score(	SD.best_cand_score_norm, 
										SD.sec_best_cand_score_norm, 
										SD.best_cand_score_abs, 
										SD.sec_best_cand_score_abs);
			
			if (success) {
				
				// We got usage, but no position for best candidate
				assert(SD.best_cand_position not_eq MAX_SIZE_T);
				
				found = true;
			}
			else {
				
				// Call our savior mini_all_paths
				found = allPaths(WQE, SD);
				
			}
			
		}
		
	} // END: else: if (not decided_hops.empty())
	
	W.stop();
	conditional_time_print(	PRINT_FN_TIMES, 
						W, 
						*t_log_file_ptr, 
						"FIND BEST CANDIDATE");
// 	return true;
	return found;
} // END: find_best_candidate

void select_next_hop_class::select_best_candidate(	Walk_Queue_Element& WQE, 
											select_next_hop_data& SD)
{
	wall_time_wrap W;
	W.start();
	
	// prelims
	std::vector<Walk_Node_Element> & cand_list = WQE.candidate_nodes;
	Walk_Node_Element & best_WNE = cand_list[ SD.best_cand_position ];
	
	// function
	
	// the best candidate must be a brown node
	assert( best_WNE.color == BROWN);
	
	log_msg_10(WQE, SD);
	
	WQ.CCS.white_node_vec.push_back(best_WNE.tar_lmer_id);
	
	// make the best candidate WHITE
	best_WNE.color = WHITE;
	
	// now remove all the BROWN nodes
	cand_list.erase(
							std::remove_if(
											cand_list.begin(), 
											cand_list.end(), 
											[](Walk_Node_Element& W){
												return W.color==BROWN;
											}
										), 
							cand_list.end()
							);
	
	// update the white index in the queue
	for (unsigned int i=0; i < WQE.candidate_nodes.size(); i++) {
		if (WQE.candidate_nodes[i].color == WHITE) {
			WQE.white_index = i;
			break;
		}
	}
	
	log_msg_11(SD);
	
	// now update the outgoing dist const (inter read) in the prev nodes
	update_dc_usage_backnode(SD.best_cand_dc_inter, WQE, false);
	
	log_msg_12(SD);
	
	// now update the outgoing dist const (intra read) in the prev nodes
	update_dc_usage_backnode(SD.best_cand_dc_intra, WQE, true);
	
	// now update the expected coverage
	update_expected_usage(WQE);
	
	// clear everything
	SD.clear();
	
	W.stop();
	conditional_time_print(	PRINT_FN_TIMES, 
						W, 
						*t_log_file_ptr, 
						"SELECT BEST CANDIDATE");
	
} // END: select_best_candidate


/**
 * @brief ...called from select_next_hop when a decision cannot be made on current brown list: open a tree, make a decision, leave a white trail and a corresponding trail of cand_dist_const
 * 
 * @param WQE ...the current walk element
 * @return bool ..... true if a path was found, false otherwise
 */
bool select_next_hop_class::mini_all_paths(Walk_Queue_Element& WQE) {
	
	wall_time_wrap W;
	W.start();
	
	if (WQ.WBptr->P_->wt == PAR) {
		assert(WQ.WBptr->LP_map_.size()==WQ.WBptr->ref_db_node_table.size());
	}
	
	print_level++;
	beg_salutation_indent("mini_all_paths");
	
	// all decided structures should be empty
	assert(WQ.DHS.hops.empty());
	
	bool pathFound=false;
	
	// data structure for mini_all_paths
	mini_all_paths_data MAPD(WQ);
	
	// first, add the brown nodes at the current position
	unsigned int num_root_nodes = MAPD.add_root_brown_nodes(WQE, WQ);
	
	if (num_root_nodes==0) {
		
		pathFound = false;
	}
	else {
		
		// now go through levels and gather evidence at each depth
		
		// positions whose all children are being added
		unsigned int add_node_pos=0;
		
		// for all depths d
		for (unsigned int d=1;; d++) {
			
			// TODO: parametrize mem calculation
	// 		mini_all_paths_mem=MAPD.size();
			
			log_msg_13(d);
			
			// add nodes at depth d, and get the number of nodes added
			unsigned int d_depth_node_count = MAPD.add_current_brown_nodes(d, add_node_pos, WQ);
			
			// if no new node added at depth d, break
			if (d_depth_node_count==0) {
				
				log_msg_14();
				
				pathFound = false;
				break;
				
			}
			
			// print the nodes of current depth
			unsigned int current_depth_beg=MAPD.nodes.size() - d_depth_node_count;
			
			log_msg_15(d_depth_node_count, d, MAPD, current_depth_beg);
			
			// now calculate scores for all added candidates
			MAPD.calc_curr_depth_scores(d_depth_node_count, WQ);
			
			// try to see if there is a clear winner at current depth
			unsigned int best_score_idx=MAX_UINT;
			
			pathFound = MAPD.attempt_decision(best_score_idx, WQ);
			
			if (pathFound) {
				
				// Decision made and still no best_score_index
				assert(best_score_idx not_eq MAX_UINT);
				
				// root at the end, so the nodes will have to be pulled from the end.
				std::vector<unsigned int> path_indices;
				
				// calculate the path decided
				MAPD.trace_ancestor_path(path_indices, best_score_idx);
				
				log_msg_16(path_indices, MAPD);
				
				update_decided_strs(MAPD, path_indices);
				
				break;
			} //END: if (decision_made)
			
			if (d > WQ.WBptr->P_->max_iterations) {
				std::cerr << "Iterations exceeded maximum limit in mini_all_paths" << std::endl;
				exit(0);
			}
		} //END: for (unsigned int d=1;; d++)
	} // if num_root_nodes != 0
	
	// clear up everything
	end_salutation_indent("mini_all_paths");
	print_level--;
	
	map_wrap_up(MAPD, W);
	
	return pathFound;
	
} // END: mini_all_paths()

void select_next_hop_class::update_decided_strs(	const mini_all_paths_data& MAPD, 
											const std::vector<unsigned int> & path_indices)
{
	// update the decision structures
	assert (WQ.DHS.hops.empty());
	for (unsigned int i=0; i < path_indices.size(); i++) {
		WQ.DHS.hops.push_back(MAPD.nodes[path_indices[i]]);
	}
	
	log_msg_17();
	
	// calculate the decided dist const for root
	unsigned int root_index = path_indices[ path_indices.size() - 1];
	
	// Something wrong with path indices at root
	assert (root_index <= MAPD.root_cand_dc_inter_read.size());
	
	WQ.DHS.decided_dc_inter = MAPD.root_cand_dc_inter_read[ root_index ];
	WQ.DHS.decided_dc_intra = MAPD.root_cand_dc_intra_read[ root_index ];

}


void select_next_hop_class::map_wrap_up(mini_all_paths_data & MAPD, wall_time_wrap & W)
{
	unsigned int buff_used = MAPD.nodes.size();
	MAPD.clear(WQ);
	
	WQ.mini_all_paths_mem=0;
	
	W.stop();
	
	if (WQ.WBptr->P_->print_mini_all_paths_stats) {
		WQ.WBptr->MSV.push(WQ.DHS.hops.size(), buff_used);
	}
	if (PRINT_FN_TIMES and W >= W_base) {
		*t_log_file_ptr 	<< "MINI ALL PATHS: " << W  
					<< " (" << WQ.DHS.hops.size() 
					<< " nodes, buff used: " << buff_used 
					<< ", depth accessed: " << MAPD.max_depth_accessed 
					<< ")" << std::endl;
	}
}


// accumulate dist const by the distance
void select_next_hop_class::accumulate_dc(std::vector< dc_str >& dc_vec)
{
	// sort dist_const_vector based on distance;
	std::sort(	dc_vec.begin(), dc_vec.end(),[](dc_str c1, dc_str c2) {return c1.distance > c2.distance;});
	
	// now accummulate constraints with identical distance
	unsigned int start_idx=0;
	unsigned int end_idx=0;
	while (start_idx < dc_vec.size()) {
		while(end_idx < dc_vec.size()) {
			if (dc_vec[ start_idx ].distance == dc_vec[ end_idx ].distance) {
				end_idx++;
			}
			else {
				break;
			}
		}
		for (unsigned int j = start_idx+1; j < end_idx; j++) {
			dc_vec[ start_idx ].freq += dc_vec[ j ].freq;
			dc_vec[ j ].freq = 0;
		}
		start_idx = end_idx;
	}
	
	// remove the zero frequency entries
	dc_vec.erase(std::remove_if(dc_vec.begin(),dc_vec.end(),[](dc_str& c){return c.freq==0;}),dc_vec.end());

}

// reduce dc_left list of updatable_white_node using dc_entry and matching with ref_white_node ref_white_node 
void select_next_hop_class::update_exp_dc_usage(	unsigned int usage, 
											Walk_Node_Element& updatable_white_node, 
											Walk_Node_Element& ref_white_node, 
											dc_str& dc_entry, 
											bool is_intra_read, 
											int prev_lmer_len)
{
	DB_Node_Type const & tar_db_node = WQ.WBptr->ref_db_node_table.ElementAt( updatable_white_node.tar_lmer_id );
	
	std::vector<Dist_Type> const & tar_dc_list = tar_db_node.Dist_Const_List;
	
	atomic::atomic_array<int >& tar_dc_left_list = WQ.WBptr->UD.expected_dist_const_usage_left[ updatable_white_node.tar_lmer_id ];
	
	// usage and dist const sizes should match
	ASSERT_WITH_MESSAGE(tar_dc_left_list.size()== tar_dc_list.size(), 
						"Dist const usage size and dist const list size different");
	
	// now go thorough all the dist const of prev node and find the current lmer and appropriate start, and update the expected usage left of the backnode
	for (unsigned int j=0; (j < tar_dc_list.size()) and (usage > 0);) {
		
		// if the lmer there is the current lmer and the start directions match
		if ((tar_dc_list[j].lmer_id != ref_white_node.tar_lmer_id) 
			|| ((tar_dc_list[j].start & 0x8) != (updatable_white_node.start & 0x8))) {
			++j;
			continue;
		}
			
			// check if distance is within acceptable range
		int db_entry_len = prev_lmer_len - 1 
						+ tar_dc_list[j].distance;
			
		if (! check_acceptable_distance(dc_entry.distance, 
									db_entry_len, db_entry_len, 
									0, 
									WQ.WBptr->P_->max_dist_error, 
									is_intra_read)) {
			++j;
			continue;
		}

		int tar_dc_left_entry = tar_dc_left_list[ j ].load();
		if (tar_dc_left_entry==0) {
			++j;
			continue;
		}
				
		unsigned int min_val = std::min((unsigned int)tar_dc_left_entry, usage);
		unsigned int new_val = tar_dc_left_entry - min_val;
		if ( tar_dc_left_list[ j ].compare_exchange_strong(tar_dc_left_entry, new_val)) {
			if (walk_info_level_ >= 3) {
				log_file_ << std::string(print_level+2, '\t') 
						<< "Match found: " << tar_dc_list[j] 
						<< std::endl;
			}
			
			WQ.dirty_index_vec_dcul.push_back(updatable_white_node.tar_lmer_id);
			usage -= min_val;
			if (usage==0) {
				break;
			} else {
				++j;
			}
		}  // else repeat the same iteration.
	}
}




// TODO: Revise the function
void select_next_hop_class::update_dc_usage_backnode(
				std::vector< dc_str >& dc_vec, Walk_Queue_Element& WQE, bool is_intra_read) {
	
	print_level++;
	beg_salutation_indent("update_dc_usage_backnode");
	
	accumulate_dc(dc_vec);
	
	Walk_Node_Element& curr_white_node = WQE.candidate_nodes[ WQE.white_index ];
	
	// go over the compressed list, and update the expected dist const usage of each backnode
	for (unsigned int i=0; i < dc_vec.size(); i++) {
		
		dc_str& dc_entry = dc_vec[i];
		assert(dc_entry.freq>=0);
		
		// jump to the prev node
		unsigned int prev_pos = WQ.jump_ahead(-dc_entry.distance);
		Walk_Queue_Element& prev_WQE = WQ.getElement(prev_pos);
		
		assert (prev_WQE.white_index not_eq MAX_UINT);
		
		// previous connected walk node that is white
		Walk_Node_Element& prev_white_node = prev_WQE.candidate_nodes[ prev_WQE.white_index ];
		
		// corresponding prev db node
		DB_Node_Type const & prev_db_node = WQ.WBptr->ref_db_node_table.ElementAt( prev_white_node.tar_lmer_id );
		
		// update the outgoing_dist_const, and calculate usage that needs to be updated to back node
		unsigned int my_usage;
		if (is_intra_read) {
			prev_white_node.out_dc_intra += (unsigned int)dc_entry.freq;
			my_usage = std::min((unsigned int)dc_entry.freq, WQ.exp_covg);
		}
		else {
			prev_white_node.out_dc_inter+=(unsigned int)dc_entry.freq;
			my_usage = dc_entry.freq;
		}
		assert(my_usage>0);
		
		
		// push the current current lmer id and freq in the sat_const in prev node
		sat_const_str S;
		S.tar_lmer_id = curr_white_node.tar_lmer_id;
		S.freq = my_usage;
		S.is_intra_read = is_intra_read;
		S.distance = dc_entry.distance;
		
		prev_white_node.sat_const_vec_fw.push_back(S);
		
		S.tar_lmer_id = prev_white_node.tar_lmer_id;
		curr_white_node.sat_const_vec_back.push_back(S);
		
		
		// above info will be used to update the real usage in clear buffer, but for now, make some changes in expected usage and move on.
		
		log_msg_4(prev_white_node, S);
		
		// reduce dc_left on prev_white_node
		update_exp_dc_usage(	my_usage, 
							prev_white_node, 
							curr_white_node, 
							dc_entry, 
							is_intra_read, 
							prev_db_node.len_lmer);
		
		// reduce dc_left on curr_white_node
		update_exp_dc_usage(	my_usage, 
							curr_white_node, 
							prev_white_node, 
							dc_entry, 
							is_intra_read, 
							prev_db_node.len_lmer);
	}
	
	end_salutation_indent("update_dc_usage_backnode");
	print_level--;
} // END: update_dist_const_usage_backnode()

Walk_Node_Element & select_next_hop_class::get_white_node(unsigned int pos)
{
	Walk_Queue_Element& WQE = WQ.getElement(pos);
	unsigned int & white_index = WQE.white_index;
	assert(white_index not_eq MAX_UINT);
	Walk_Node_Element& White_Node = WQE.candidate_nodes[white_index];
	return White_Node;

}


unsigned int select_next_hop_class::next_white_pos(const unsigned int pos)
{
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	const unsigned int k = WQ.WBptr->P_->k;
	
	Walk_Node_Element& white_WNE = get_white_node(pos);
	unsigned int white_len = DB_Table.ElementAt(white_WNE.tar_lmer_id).len_lmer;
	unsigned int next_pos = (pos + white_len - (k-1))%WQ.Q.size();
	
	return next_pos;
}

unsigned int select_next_hop_class::next_white_pos(	const unsigned int pos, 
											const unsigned int white_len)
{
	const unsigned int k = WQ.WBptr->P_->k;
	unsigned int next_pos = (pos + white_len - (k-1))%WQ.Q.size();
	
	return next_pos;

}


bool select_next_hop_class::check_dc_overshoot(const unsigned int pos)
{
	// prelims
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	const Params & P_ref = *(WQ.WBptr->P_);
	const unsigned int k_ = P_ref.k;
	unsigned int max_dc_jump = P_ref.insert_size + P_ref.max_dist_error - 2*k_;
	
	// function
	Walk_Node_Element& next_WNE = get_white_node(pos);
	
	
	const unsigned int tar_lmer_id = next_WNE.tar_lmer_id;
	const unsigned int tar_lmer_len = DB_Table.ElementAt(tar_lmer_id).len_lmer;
	
	// get an upper bound on farthest dist constraint from tar_lmer_id
	unsigned int max_dist_const_dest = (pos 
									+ tar_lmer_len 
									+ max_dc_jump)%WQ.Q.size();
	
	// this should be before curr, else queue size not configured properly
	if (not check_mid(pos, max_dist_const_dest, WQ.curr)) {
		log_msg_5(max_dist_const_dest, pos);
		return false;
	}
	return true;
}


void select_next_hop_class::make_unupdated(Walk_Node_Element& WNE)
{
	unsigned int lmer_id = WNE.tar_lmer_id;
	WQ.WBptr->UD.expected_node_usage_left_unupdated[lmer_id]++;
}

bool select_next_hop_class::reduce_usage_left(Walk_Node_Element& WNE)
{
	atomic::atomic_array<long int> & exp_nul_vec = WQ.WBptr->UD.expected_node_usage_left;
	
	const unsigned int tar_lmer_id = WNE.tar_lmer_id;
	
	std::atomic<long int> & tar_exp_nul = exp_nul_vec[ tar_lmer_id ];
	long tar_exp_nul_curr =  tar_exp_nul.load();
	long new_val = tar_exp_nul_curr > (long int)WQ.exp_covg ? tar_exp_nul_curr - (long int)WQ.exp_covg : 0;
	
	// tar_exp_nul = new_val is to be achieved, but atomically: a.ces(b,c): if a==b then a=c, else b=a
	if ( tar_exp_nul.compare_exchange_strong(tar_exp_nul_curr, new_val)) {
		WQ.dirty_index_vec_nul.push_back(tar_lmer_id);
		return true;
	}
	return false;

}



void select_next_hop_class::make_updated(Walk_Node_Element& WNE)
{
	atomic::atomic_array<int> & exp_nul_unup_vec = WQ.WBptr->UD.expected_node_usage_left_unupdated;
	
	const unsigned int tar_lmer_id = WNE.tar_lmer_id;
	
	int unupdated_entry = exp_nul_unup_vec[ tar_lmer_id ].load();
	
	assert(unupdated_entry > 0);
	
	// update the usage info array
	exp_nul_unup_vec[ tar_lmer_id ]--;
	
}




void select_next_hop_class::refine_init_covg(Walk_Node_Element & WNE)
{
	// if the current walk has just begun, start off expected_coverage
	if (WQ.TSS.start_of_walk) {
		WQ.exp_covg = std::max(WQ.exp_covg, 
								(unsigned int)std::round(WQ.WBptr->magic_ratio * WNE.out_dc_inter));
		WQ.TSS.start_of_walk = false;
	}

}

// void select_next_hop_class::update_exp_covg(Walk_Node_Element& WNE)
// {
// 	// coverage should be positive
// 
// 	// TODO: Temporary fix, find out when it occurs
// 	const float mag_ratio = WQ.WBptr->magic_ratio;
// 	if (float(WQ.exp_covg)/mag_ratio + (float)WNE.out_dc_inter > (float)WNE.in_dc_inter) {
// 		
// 		// coverage
// 		WQ.exp_covg = (unsigned int)std::round( 
// 										mag_ratio
// 										*(
// 											float(WQ.exp_covg)/mag_ratio 
// 											+ (float)WNE.out_dc_inter 
// 											- (float)WNE.in_dc_inter
// 										) 
// 											);
// 		
// 		if (WQ.exp_covg == 0) {
// 			WQ.exp_covg = 1;
// 		}
// 	}
// 	else {
// 		WQ.exp_covg = 1;
// 	}
// 
// }

void select_next_hop_class::update_expected_usage(Walk_Queue_Element& WQE) {
	
	print_level++;
	
	beg_salutation_indent("update_expected_usage");
	
	// prelims
	
	const Params & P_ref = *(WQ.WBptr->P_);
	const unsigned int k_ = P_ref.k;
	
	assert(P_ref.insert_size + P_ref.max_dist_error >= 2*k_);
	
	// function
	
	// current node usage goes to unupdated
	Walk_Node_Element& curr_white_node = WQE.candidate_nodes[ WQE.white_index ];
	make_unupdated(curr_white_node);
	
		
	// TODO: If just started the walk, revise expected coverage at last updated data, else no need
	
	// last updated data
	unsigned int & last_updt_pos = WQ.TSS.last_update_pos;
	Walk_Node_Element& last_updt_white_node = get_white_node(last_updt_pos);
	
	// next jump
	unsigned int next_unup_pos = next_white_pos(last_updt_pos);
	
	// next jump should not cross curr
	bool basic = check_mid(WQ.TSS.last_update_pos, next_unup_pos, WQ.curr);
	assert(basic);
		
	// if the current walk has just begun, start off expected_coverage
	refine_init_covg(last_updt_white_node);
	
	log_msg_6();
	
	// now begin updating
	unsigned int num_iterations=0;
	while(true) {
		
		
		bool success = check_dc_overshoot(next_unup_pos);
		if (not success) {
			break;
		};
		
		// now update the coverage at current position
		Walk_Node_Element& next_WNE = get_white_node(next_unup_pos);
		
		update_exp_covg(next_WNE);
		
		// reduce coverage from usage
		
		success = reduce_usage_left(next_WNE);
		
		// tar_exp_nul = new_val is to be achieved, but atomically: a.ces(b,c): if a==b then a=c, else b=a
		if (success) {

			make_updated(next_WNE);
			
			WQ.TSS.last_update_pos = next_unup_pos;
			
			log_msg_9(next_unup_pos);
			
			// now move forward the position.
			next_unup_pos = next_white_pos(next_unup_pos/*, tar_lmer_len*/);
			
			if (num_iterations++ > WQ.Q.size()) {
				std::cerr << "Quitting the loop, exceeded max iterations" << std::endl;
				exit(0);
			}
		}
		else {
			// TODO: if program never goes here, remove the if else
			// error
			printf("Error in select next hop class\n");
			exit(0);
		}
	}  // while true
	
	end_salutation_indent("update_expected_usage");
	
	print_level--;
} // END: update_expected_usage()


bool select_next_hop_class::check_decided_stuff(Walk_Queue_Element& WQE, select_next_hop_data& SD)
{
	if (not WQ.DHS.hops.empty()) {
		
		log_msg_19();
		
		// pick the decided hops and move on
		SD.pick_decided_hop(WQ, WQE);
		
		// data was lost in mini all paths, so regather for coverage calculations
		SD.regather_best_cand_support(WQ, WQE);
		
		return true;
		
	} 
	return false;
}


bool select_next_hop_class::update_allCand_n_best_scores(std::vector< Walk_Node_Element >& cand_list, select_next_hop_data& SD, unsigned int& last_brown_pos)
{
	bool no_usage=true;
	
	// now for each brown node, gather the evidence
	for (std::size_t i=0; i < /*WQE.candidate_nodes*/cand_list.size(); i++) {
		
		// current candidate node
		Walk_Node_Element& current_WNE = /*WQE.candidate_nodes*/cand_list[i];
		
		// if candidate node is brown
		if (current_WNE.color==BROWN) {
			
			last_brown_pos=i;
			
			bool suff_usg = check_cand_usage(current_WNE);
			if (not suff_usg) {
				continue;
			}
			
			// at least one node has usage
			no_usage=false;
			
			update_cand_supp_dc(SD, current_WNE);
			
			update_cand_scores(SD, current_WNE);
			
			log_msg_22(i, SD, current_WNE);
			
			// update the best and second best score, and related data
			SD.update_best_cand_data(i);
			
		} // END: if (WQE.candidate_nodes[i].color==BROWN)
	} // END: for (unsigned int i=0; i < WQE.candidate_nodes.size(); i++)
	
	return no_usage;

}


bool select_next_hop_class::check_cand_usage(Walk_Node_Element& current_WNE)
{
	// expected usage left of current node
	long curr_exp_usage_left = WQ.WBptr->UD.expected_node_usage_left[ current_WNE.tar_lmer_id ].load();
	
	// if current_WNE has no expected usage, continue
	if (curr_exp_usage_left <=0) {
		log_msg_20(current_WNE);
		return false;
	}
	
	// if there is an unupdated coverage instance of current_WNE, check for enough usage
	int curr_exp_usage_unup = WQ.WBptr->UD.expected_node_usage_left_unupdated[ current_WNE.tar_lmer_id ].load();
	
	// node usage must be >= c*curr covg (c ~ 1)
	long int usage_cutoff = (long int)std::round((1.0 - WQ.WBptr->P_->coverage_fudge)*(float)WQ.exp_covg);
	
	if ((curr_exp_usage_unup > 0) and (curr_exp_usage_left < usage_cutoff)) {
		log_msg_21(current_WNE);
		return false;
	}
	return true;

}

void select_next_hop_class::update_cand_supp_dc(	select_next_hop_data& SD, 
											Walk_Node_Element& current_WNE)
{
	// clear vectors for gathering support
	SD.clear_support_vecs();
	SD.clear_dc_vecs();
	
	// gather candidate support, also update the in_dist_const
	WQ.gather_cand_support( 	SD.cand_support_inter_read, 
							SD.cand_dc_inter, 
							SD.cand_support_intra_read, 
							SD.cand_dc_intra, 
							current_WNE );
}

void select_next_hop_class::update_cand_scores(	select_next_hop_data& SD, 
											Walk_Node_Element& current_WNE)
{
	// length of current_WNE
	int l = WQ.WBptr->ref_db_node_table.ElementAt(current_WNE.tar_lmer_id).len_lmer;
	
	// basic assertion
	ASSERT_WITH_MESSAGE(l>=int(WQ.WBptr->P_->k), 
				"lmer length coming out to be less than kmer length");
	
	// reasonable length to normalize the candidate score
	unsigned int reasonable_l = std::min((unsigned int)l, WQ.WBptr->P_->insert_size);
	
	// candidate score and normalized score
	SD.cand_score_abs = WQ.evidence_score(SD.cand_support_inter_read, SD.cand_support_intra_read);
	SD.cand_score_norm = SD.cand_score_abs/float(reasonable_l - WQ.WBptr->P_->k + 1);
}

void select_next_hop_class::single_brown_cand_adjust(	Walk_Queue_Element& WQE, 
												select_next_hop_data& SD, 
												unsigned int& last_brown_pos)
{
	// find out the BROWN size, it should be greater than zero. Also initialize the best_cand_position
	unsigned int dont_care;
	unsigned int brown_size=WQ.count_color(WQE, BROWN, dont_care);
	
	log_msg_18(brown_size);
	
	// if just one brown size and there is some usage, go with it
	if (brown_size==1) {
		if (walk_info_level_ >= 2) {
			*t_log_file_ptr << "Just one brown node, going with that" << std::endl;
		}
		SD.best_cand_position = last_brown_pos;
		SD.sec_best_cand_score_abs=0;
		SD.sec_best_cand_score_norm=0;
	}

}


bool select_next_hop_class::allPaths(Walk_Queue_Element& WQE, select_next_hop_data& SD)
{
	log_msg_2();
	
	// Call our savior mini_all_paths
	bool success = mini_all_paths(WQE);
	
	log_msg_3();
	
	// If mini_all_paths gave us something, happy times
	if (success) {
		
		//assert that something came out of mini_all_paths
		assert(not WQ.DHS.hops.empty());
		
		log_msg_7();
		
		SD.pick_decided_hop(WQ, WQE);
		
		// DC's for the first hop, for later hops will be re-evaluated
		SD.best_cand_dc_inter = WQ.DHS.decided_dc_inter;
		SD.best_cand_dc_intra = WQ.DHS.decided_dc_intra;
		
		return true;
		
	} 
	else {
		// end of current contig, wrap up
		log_msg_8();
		return false;
	}
}

void select_next_hop_class::reset_unupdated_vec()
{
	
	const unsigned int last_updt_pos = WQ.TSS.last_update_pos;
	
	// next jump
	unsigned int next_unup_pos = next_white_pos(last_updt_pos);
									
	// next jump should not cross curr
	bool basic = check_mid(last_updt_pos, next_unup_pos, WQ.curr);
	assert(basic);
	
	// go through each white node, update usage and overlap_lmers vector
	unsigned int iterations=0;
	for (unsigned int pos = next_unup_pos; pos not_eq WQ.curr; pos = (pos+1)%WQ.Q.size()) {
		
		// fetch the current white node
		Walk_Queue_Element& Wq = WQ.getElement(pos);
		if (Wq.white_index==MAX_UINT) {
			continue;
		}
		Walk_Node_Element& Wn = Wq.candidate_nodes[ Wq.white_index ];
		
		make_updated(Wn);
		
		if (iterations++ > WQ.WBptr->P_->max_iterations) {
			std::cout << "\nIterations exceeded maximum limit in reset_unupdated_vec" << std::endl;
			exit(0);
		}
	}
}


void select_next_hop_class::beg_salutation(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\n-------------------------------------------------------------------------\n";
		*t_log_file_ptr << "\n" << fn_name << "\n" << std::endl;
	}

}

void select_next_hop_class::end_salutation(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "\nEND " << fn_name <<"" << std::endl;
	}
}

void select_next_hop_class::beg_salutation_indent(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "IN " << fn_name << "..." 
				<< std::endl;
	}

}

void select_next_hop_class::end_salutation_indent(std::string fn_name)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "DONE " << fn_name 
				<< std::endl;
	}

}


/********************************** LOGGER FUNCTIONS *********************************/

void select_next_hop_class::log_msg_1(const float score_diff_norm, const float score_diff_abs)
{
	float min_score_diff_norm = WQ.WBptr->P_->sufficient_score_diff_norm;
	float min_score_diff_abs = WQ.WBptr->P_->sufficient_score_diff_abs;

	// log information
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "More than one candidate" << std::endl;
		*t_log_file_ptr << "NORM: score_diff, min_score_diff: " 
				<< score_diff_norm << ", " << min_score_diff_norm 
				<< std::endl;
		*t_log_file_ptr << "ABS: score_diff, min_score_diff: " 
				<< score_diff_abs << ", " << min_score_diff_abs 
				<< std::endl;
	}

}

void select_next_hop_class::log_msg_2()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Calling mini_all_paths" << std::endl;
	}

}

void select_next_hop_class::log_msg_3()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Back in select_next_hop" << std::endl;
	}

}


void select_next_hop_class::log_msg_4(Walk_Node_Element& prev_white_node, sat_const_str& S)
{
	if (walk_info_level_ >= 3) {
		*t_log_file_ptr << std::string(print_level+1, '\t') 
				<< "curr tar_lmer_id: " << prev_white_node.tar_lmer_id 
				<< std::endl;
		*t_log_file_ptr << std::string(print_level+1, '\t') 
				<< "Processing entry: " << S 
				<< std::endl;
	}
}

void select_next_hop_class::log_msg_5(const unsigned int max_dist_const_dest, const unsigned int next_unup_pos)
{
	if (walk_info_level_ >= 2) {
		log_file_ << std::string(print_level+1, '\t') 
						<< "Quitting: max_dist_const_dest: " 
						<< max_dist_const_dest 
						<< " not in the mid of next_unupdated_pos and curr: (" 
						<< next_unup_pos 
						<< ", " 
						<< WQ.curr 
						<< ")" 
						<< std::endl;
	}

}

void select_next_hop_class::log_msg_6()
{
	if (walk_info_level_ >= 3) {
		*t_log_file_ptr << std::string(print_level+1, '\t') 
					<< "last_update_position: " 
					<< WQ.TSS.last_update_pos 
					<< std::endl;
	}

}

void select_next_hop_class::log_msg_7()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Here is what we got from mini_all_paths: " << std::endl;
		*t_log_file_ptr << WQ.DHS.hops << std::endl;
	}

}

void select_next_hop_class::log_msg_8()
{
	if (walk_info_level_ >= 2) {
		log_file_ 	<< "Okay, mini_all_paths gave up, wrapping up the contig" 
				<< std::endl;
	}

}


void select_next_hop_class::log_msg_9(const unsigned int next_unup_pos)
{
	if (walk_info_level_ >= 3) {
		*t_log_file_ptr << std::string(print_level+1, '\t') 
				<< "Updated usage at position: " << next_unup_pos 
				<< std::endl;
	}
}

void select_next_hop_class::log_msg_10(	const Walk_Queue_Element& WQE, 
									const select_next_hop_data& SD)
{
	// now we have the best candidate
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr 	<< "Best candidate chosen with position " 
					<< SD.best_cand_position 
					<< ", with tar_lmer_id: " 
					<< WQE.candidate_nodes[ SD.best_cand_position ].tar_lmer_id 
					<< std::endl;
	}
}

void select_next_hop_class::log_msg_11(select_next_hop_data& SD)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Updating the inter read constraints usage" << std::endl;
		*t_log_file_ptr << SD.best_cand_dc_inter << std::endl;
	}

}

void select_next_hop_class::log_msg_12(select_next_hop_data& SD)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Updating the intra read constraints usage" << std::endl;
		*t_log_file_ptr << SD.best_cand_dc_intra << std::endl;
	}

}

void select_next_hop_class::log_msg_13(const unsigned int d)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "Evaluating nodes at depth " << d 
				<< std::endl;
	}

}

void select_next_hop_class::log_msg_14()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "no new nodes added, quitting" 
				<< std::endl;
	}

}

void select_next_hop_class::log_msg_15(	const unsigned int d_depth_node_count, 
									const unsigned int d, 
									const mini_all_paths_data& MAPD, 
									const unsigned int current_depth_beg)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< d_depth_node_count << " nodes added at depth " << d 
				<< std::endl;
	}
	std::vector<Walk_Node_Element> copy_vector(MAPD.nodes.begin()+current_depth_beg, MAPD.nodes.end());
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << copy_vector;
	}
	copy_vector.clear();
}

void select_next_hop_class::log_msg_16(	std::vector< unsigned int >& path_indices, 
									mini_all_paths_data& MAPD)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') << "Decision has been made, printing everything" << std::endl;
		*t_log_file_ptr << std::string(print_level, '\t') << path_indices.size() << " nodes selected in mini_all_paths" << std::endl;
		*t_log_file_ptr << std::string(print_level, '\t') << "path indices:\n" << path_indices << std::endl;
		MAPD.print_vecs(*t_log_file_ptr);
	}

}

void select_next_hop_class::log_msg_17()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << std::string(print_level, '\t') 
				<< "decided_hops:\n" << WQ.DHS.hops << std::endl;
	}

}


void select_next_hop_class::log_msg_18(unsigned int brown_size)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << brown_size << " brown candidates to choose from" << std::endl;
	}

}

void select_next_hop_class::log_msg_19()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "We have decided hops, yay" << std::endl;
		*t_log_file_ptr << WQ.DHS.hops << std::endl;
	}

}

void select_next_hop_class::log_msg_20(const Walk_Node_Element& current_WNE)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr 	<< "No usage for tar_lmer_id: " 
					<< current_WNE.tar_lmer_id 
					<< std::endl;
	}

}

void select_next_hop_class::log_msg_21(const Walk_Node_Element& current_WNE)
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Unupdated small usage for tar_lmer_id: " 
				<< current_WNE.tar_lmer_id << std::endl;
	}

}

void select_next_hop_class::log_msg_22(std::size_t idx, select_next_hop_data& SD, Walk_Node_Element& current_WNE)
{
	int l = WQ.WBptr->ref_db_node_table.ElementAt(current_WNE.tar_lmer_id).len_lmer;
	
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Candidate pos: " << idx 
				<< ", tar lmer id: " << current_WNE.tar_lmer_id 
				<< ", score: " << SD.cand_score_abs 
				<< ", length: " << l 
				<<", n_score: " << SD.cand_score_norm 
				<< std::endl;
	}

}

void select_next_hop_class::log_msg_23()
{
	if (walk_info_level_ >= 2) {
		*t_log_file_ptr << "Ending the current contig, no usage" << std::endl;
	}

}


