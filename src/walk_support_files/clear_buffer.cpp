#include "clear_buffer.h"
#include "../walk.h"

// HELPER FUNCTION DECLARATIONS

// in update_dc_usage_core: check if curr_dc matches the distant_str
bool sat_const_match(	const sat_const_str & distant_str, 
					const char Wn_dir, 
					const Dist_Type & curr_dc);

// in update_dc_usage_core: check if the distance of distant_str is acceptable wrt curr_dc
bool sat_dist_acceptable(	const int correct_node_len, 
						const Dist_Type & curr_dc, 
						const sat_const_str & distant_str, 
						const int max_dist_error);

// in update_dc_usage_core: update the curr_dc_left_entry based on distant_str
short unsigned int update_curr_dc_left(	std::atomic< int >& curr_dc_left_entry, 
									sat_const_str& distant_str, 
									std::vector< long unsigned int >& dirty_index_vec_dcul, 
									Walk_Node_Element& Wn);

// CLEAR BUFFER DATA FUNCTIONS

// constructor
clear_buffer_data::clear_buffer_data(Walk_Queue& WQ):	log_file_(*(WQ.t_log_file_ptr)), 
												ostr_(WQ.WBptr->ostr_), 
												print_level(WQ.print_level)
{
	// TODO: shouldn't base num dist const be reset from prev clear buffer
	base_num_dc=0;
	longest_node_freq=0;
	longest_node_len=0;
	hopeless=false;
	
	curr_cov = 0;
	k = WQ.WBptr->P_->k;
}


// calc base covg as the fall back option, in case a more accurate covg can't be estimated
void clear_buffer_data::calculate_base_coverage(	unsigned int beg, 
											unsigned int end, 
											Walk_Queue& WQ)
{
	const float mag_ratio = WQ.WBptr->magic_ratio;
	// try to get a handle on base coverage, and update the white node list
	
	std::vector<unsigned int> hovering_dc_vec;
	
	update_hovering_dc(hovering_dc_vec, beg, end, WQ);
	
	unsigned int sum_num_dc = std::accumulate(hovering_dc_vec.begin(), 
									hovering_dc_vec.end(), 
									0);
	unsigned int non_zero_cands = std::count_if(	hovering_dc_vec.begin(), 
									hovering_dc_vec.end(), 
									[](unsigned int a){
										return a>0;
									});
	
	bool success = calc_base_num_dc(sum_num_dc, non_zero_cands, WQ);
	
	if (not success)
		hopeless = true;
	
	base_cov = hopeless ? WQ.exp_covg : (float)base_num_dc*mag_ratio;
} // END: calculate_base_coverage()

void clear_buffer_data::update_usage(Walk_Node_Element & Wn, Walk_Queue & WQ)
{
	// prelims
	const Params & Pref = *(WQ.WBptr->P_);
	atomic::atomic_array<long int> & nul_vec = WQ.WBptr->UD.node_usage_left;
	
	// if reverse walking is enabled, and we are currently walking reverse and walking on seed nodes, do nothing

	// the good case
	if (not hopeless) {
		
		float prev_cov = curr_cov;
		
		curr_cov += 	( (float)Wn.out_dc_inter 
					- (float)Wn.in_dc_inter 
					) * WQ.WBptr->magic_ratio ;
		
		// warning msg if curr_cov less than 0
		log_msg_1(prev_cov, Wn);
		
		// if it's one of the reverse seed nodes, no need to go further
		if (Pref.enabRevWalk and WQ.RWS.walkRev and Wn.revWaiver) 
			return;
		
		curr_cov = (curr_cov > 0.0) ? curr_cov : 0.0;
		
		// when curr_cov is zero, use the base coverage
		unsigned int my_usage = (curr_cov <= 0.0) ? std::round(base_cov) : std::round(curr_cov);
		
		log_msg_2(my_usage, WQ);

		unsigned int my_fudge_usage =std::max( 
									(unsigned int)std::round( 
										((float)my_usage) * (1.0 + Pref.coverage_fudge)
									)
									, 
									(unsigned int)(my_usage+1) 
								);
		
		// forward walking.
		std::atomic<long int> & nul_entry = nul_vec[ Wn.tar_lmer_id ];
		
		update_nul_entry_hope(nul_entry, my_usage, my_fudge_usage, Wn, WQ);
		
		// update dist const usage
		update_dist_const_usage(Wn, WQ);
	
	} //END: if (not hopeless) {
	else {  // hopeless

		if (Pref.enabRevWalk and WQ.RWS.walkRev and Wn.revWaiver) 
			return;
		
		// not reverse walking
		nul_vec[Wn.tar_lmer_id].store(0);
		
		WQ.dirty_index_vec_nul.push_back(Wn.tar_lmer_id);
		
	
	} //END: else: if (not hopeless) {
	
} //END: update_usage()

// while clearing buffer, update the dist const usage of Wn (both: in Wn, and in other nodes with tar as Wn) :: used in update_usage
void clear_buffer_data::update_dist_const_usage(Walk_Node_Element& Wn, Walk_Queue& WQ)
{
	update_dc_usage_core(Wn, WQ, Wn.sat_const_vec_fw, true);
	
	Wn.sat_const_vec_fw.clear();
	
	update_dc_usage_core(Wn, WQ, Wn.sat_const_vec_back, false);
	
	Wn.sat_const_vec_back.clear();
	
} //END update_dist_const_usage()


void clear_buffer_data::update_overlap_lmers(Walk_Node_Element& Wn, Walk_Queue& WQ)
{
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	
	unsigned int l = DB_Table.ElementAt(Wn.tar_lmer_id).len_lmer;
	assert(l > k-1);
	
	// push lmer to the array
	if ((Wn.end & 0x8) ==0) {
		olap_lmers.push_back(DB_Table.ElementAt(Wn.tar_lmer_id).get_lmer());
	}
	else {
		std::string s = DB_Table.ElementAt(Wn.tar_lmer_id).get_lmer();
		MakeNegative(s);
		olap_lmers.push_back(s);
	}

} //END update_overlap_lmers()

void clear_buffer_data::update_reverse_seed_str(Walk_Node_Element& Wn, Walk_Queue& WQ)
{
	// prelims
	const Params* P = WQ.WBptr->P_;
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	revWalkStr& RWS_ = WQ.RWS;
	
	unsigned int l = DB_Table.ElementAt(Wn.tar_lmer_id).len_lmer;
	if (P->enabRevWalk) {
		if ((not RWS_.walkRev) and RWS_.revSeedLen < P->insert_size) {
			//TODO: update information about reverse walking
			
			reverse_seed_structure RSD;
			RSD.lmer_id = Wn.tar_lmer_id;
			RSD.end = Wn.end;
			RSD.coverage = std::round(curr_cov);
			RWS_.revSeedVec.push_back(RSD);
			
			if (RWS_.revSeedLen==0) {
				RWS_.revSeedLen+=l;
			}
			else {
				RWS_.revSeedLen+=l - (k - 1);
			}
			RWS_.revSeedCount++;
		}
	}

} //END update_reverse_seed_str()

void clear_buffer_data::update_hanging_seq_n_sanity_check(Walk_Queue& WQ)
{
	std::string & hang_seq = WQ.TSS.hanging_seq;
	std::size_t hang_size = hang_seq.size();
	
	for (unsigned int i=0; i < olap_lmers.size(); i++) {
		
		std::string & curr_lmer = olap_lmers[i];
		
		// compare the first overlapping lmer with the prev hanging seq
		if (i==0 and hang_size not_eq 0) {
			assert(curr_lmer.substr(0, hang_size) == hang_seq);
		}
		
		// routine sanity check for all the lmers
		if (i < olap_lmers.size()-1) {
			
			std::string & next_lmer = olap_lmers[i+1];
			
			assert(curr_lmer.substr(curr_lmer.size()-k+1, k-1) == next_lmer.substr(0, k-1));
		}
		
		// if it is the last lmer, update the hanging sequence
		if (i==olap_lmers.size()-1) {
			hang_seq = curr_lmer.substr(curr_lmer.size()-k+1, k-1);
		}
	}
}// END update_hanging_seq_n_sanity_check()

void clear_buffer_data::update_current_contig(	std::string & curr_contig, 
										unsigned int & infoLevel)
{
	// initialize the hopeless contig differently
	if (hopeless) {
		curr_contig="";
	}
	
	// update the current contig
	for (unsigned int i=0; i < olap_lmers.size(); i++) {
		curr_contig.append(olap_lmers[i], 
						0, 
						olap_lmers[i].size() - (k-1));
	}
	
	if (infoLevel >=3) {
		log_file_ << "current contig: " << curr_contig << std::endl;
	}
} //END update_current_contig()

void clear_buffer_data::clear(Walk_Queue& WQ)
{
	if (not hopeless) {
		WQ.TSS.saved_base_num_dc = base_num_dc;
	}
	else {
		WQ.TSS.saved_base_num_dc=0;
	}
	olap_lmers.clear();
} // END clear()

/***********************Helper Functions ********************/

void clear_buffer_data::update_hovering_dc(	std::vector< unsigned int >& hovering_dc_vec, 
										unsigned int beg, 
										unsigned int end,  
										Walk_Queue& WQ)
{
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	
	// try to get a handle on base coverage, and update the white node list
	for (unsigned int pos = beg; pos not_eq end; pos = (pos + 1)%WQ.Q.size()) {
		Walk_Queue_Element& Wq = WQ.getElement(pos);
		
		if (Wq.white_index==MAX_UINT) {
			continue;
		}
		Walk_Node_Element& Wn = Wq.candidate_nodes[ Wq.white_index ];
		
		// a basic sanity check for a white node: s_dir and e_dir should be opposite
		ASSERT_WITH_MESSAGE(((Wn.start+8) & 0x8) == (Wn.end & 0x8), "");
		
		const DB_Node_Type & curr_db_node = DB_Table.ElementAt(Wn.tar_lmer_id);
		int curr_lmer_len = curr_db_node.len_lmer;
		
		// needed just for corner cases where there are no dist constraints
		if (curr_lmer_len > (int)k) {
			
			// if it is the longest node seen yet, and not one of the seed nodes for reverse walking
			if (curr_lmer_len > longest_node_len and (not Wn.revWaiver)) {
				longest_node_freq = Wn.freq;
				longest_node_len = curr_lmer_len;
			}
		}
		else {
			ostr_.warnings_m.lock();
			ostr_.warnings << "lmer found that is less than k: calculate_base_coverage" << std::endl;
			ostr_.warnings_m.unlock();
		}
		
		base_num_dc = base_num_dc 
							+ Wn.out_dc_inter 
							- Wn.in_dc_inter;
							
		hovering_dc_vec.push_back(base_num_dc);
	}

}


void clear_buffer_data::update_dc_usage_core(	Walk_Node_Element& Wn, 
										Walk_Queue& WQ, 
										std::vector<sat_const_str> & sat_const_vec, 
										bool updating_forward)
{
	// prelims
	const DB_Node_Table_Type & DB_Table = WQ.WBptr->ref_db_node_table;
	std::vector<atomic::atomic_array<int> > & dcul_vec = WQ.WBptr->UD.dist_const_usage_left;
	const Params & Pref = *(WQ.WBptr->P_);
	
	// curr node data
	
	// current db node (corr to Wn)
	DB_Node_Type const & curr_db_node = DB_Table.ElementAt(Wn.tar_lmer_id);
		
	// dist const list for curr db node
	std::vector<Dist_Type> const & curr_dc_list = curr_db_node.Dist_Const_List;
		
	// usage left list for curr db node
	
	atomic::atomic_array<int> & curr_dc_left_list = dcul_vec[ Wn.tar_lmer_id ];
		
	// dist const usage vector size compatible with current node dist const size
	ASSERT_WITH_MESSAGE(curr_dc_left_list.size() == curr_dc_list.size(), "");
	
	// dir of curr node to which other dc's will be compared
	char Wn_dir = updating_forward ? Wn.start : Wn.end;
	
	// update dist const usage of db node corr to Wn, using fw vector :: tar nodes that have been satisfied forward
	for (unsigned int i=0; i < sat_const_vec.size(); i++) {
		
		sat_const_str & distant_str = sat_const_vec[i];
		
		// the forward node lmer id
		unsigned int dist_lmer_id = distant_str.tar_lmer_id;
		
		int correct_node_len = updating_forward ? curr_db_node.len_lmer : DB_Table.ElementAt(dist_lmer_id).len_lmer;
			
		// go through dist const of current db node, and make deductions from matching entries
		for (unsigned int j=0; j < curr_dc_list.size() and distant_str.freq > 0;) {
			
			const Dist_Type & curr_dc = curr_dc_list[j];
			
			// if the target lmer id's match and the is_intra_read flag match, proceed
			if (not sat_const_match(distant_str, Wn_dir, curr_dc)) {
				++j;
				continue;
			}
			
			if (not sat_dist_acceptable(correct_node_len, curr_dc, distant_str, Pref.max_dist_error)) {
				++j;
				continue;
				
			}
			
			std::atomic<int> & curr_dc_left_entry =  curr_dc_left_list[j];
			
			int curr_dc_left_currVal = curr_dc_left_entry.load();
			// if current node has no usage left, continue
			if (curr_dc_left_currVal<=0) {
				++j;
				continue;
			}
			
			short unsigned int flag = update_curr_dc_left(curr_dc_left_entry, distant_str, WQ.dirty_index_vec_dcul, Wn);
			
			if (flag==0) {
				++j;
				continue;
			} else if (flag==1) {
				break;
			}
		}
	}
}


void clear_buffer_data::update_nul_entry_hope(	std::atomic< long int >& nul_entry, 
										const unsigned int my_usage, 
										const unsigned int my_fudge_usage, 
										Walk_Node_Element& Wn, 
										Walk_Queue& WQ)
{
	// update the entry in node_usage_left vector
	long nul_entry_curr_val = nul_entry.load();
	long new_val = 
		((long int)my_fudge_usage > nul_entry_curr_val) ? 
		0 
		: 
		nul_entry_curr_val - (long int)my_usage;

//TCP OK?  what if <= 0? --> new_val would be set to 0, but tar_lmer_id would be pushed_back.
	while ((nul_entry_curr_val > 0) && (! nul_entry.compare_exchange_strong(nul_entry_curr_val, new_val))) {
		new_val = 
		((long int)my_fudge_usage > nul_entry_curr_val) ? 
		0 
		: 
		nul_entry_curr_val - (long int)my_usage;
	}
	// the node_usage_left_entry is now updated to the value just before replacement.
	if (nul_entry_curr_val <= 0) {
		log_msg_3(my_usage, WQ);
	}
	else {   // only push_back if originally has some usage_left.
		WQ.dirty_index_vec_nul.push_back(Wn.tar_lmer_id);
	}
}

bool clear_buffer_data::calc_base_num_dc(	const unsigned int sum_num_dc, 
									const unsigned int non_zero_cands, 
									Walk_Queue& WQ)
{
	// TODO: Here you are using average, try to vectorize it individually for each white node
	
	// ideal case
	if ((sum_num_dc not_eq 0) and (non_zero_cands not_eq 0)) {
		base_num_dc = (unsigned int)std::round(float(sum_num_dc)/float(non_zero_cands));
	}
	// no candidates to play with, use something
	else {
		// history to the rescue
		if (WQ.TSS.saved_base_num_dc not_eq 0) {
			base_num_dc = WQ.TSS.saved_base_num_dc;
		}
		// getting worse
		else {
			// thank goodness, longest lmer acting as the savior
			if (longest_node_freq not_eq 0) {
				base_num_dc = longest_node_freq;
			}
			else {
				// hopeless case, just clean up everything and move on. 
				log_msg_4(WQ);
				return false;
			}
		}
	}
	return true;
}

bool sat_const_match(const sat_const_str& distant_str, const char Wn_dir, const Dist_Type& curr_dc)
{
	if (	(distant_str.tar_lmer_id != curr_dc.lmer_id) ||  
		((Wn_dir & 0x8) != (curr_dc.start & 0x8)) ||
		(distant_str.is_intra_read != curr_dc.is_intra_read())) {
		
		return false;
		
	}
	return true;
}

bool sat_dist_acceptable(const int correct_node_len, const Dist_Type & curr_dc, const sat_const_str & distant_str, const int max_dist_error) {
	// dist from start of curr_db_node to start of tar evidence
	int db_entry_len = correct_node_len - 1 + curr_dc.distance;
	
	// check the distance acceptibility
	if (!check_acceptable_distance(distant_str.distance, 
							db_entry_len, 
							db_entry_len, 
							0, 
							max_dist_error, 
							distant_str.is_intra_read)) {
								return false;;
							}
							return true;
	
}

short unsigned int update_curr_dc_left(std::atomic<int> & curr_dc_left_entry, sat_const_str & distant_str, std::vector<long  unsigned int> & dirty_index_vec_dcul, Walk_Node_Element & Wn) {
	
	int curr_dc_left_currVal = curr_dc_left_entry.load();
	
	// freq is actually usage
	// since Wn.sat_const_vec_back[i].freq is modified in the other branch, there could be a race condition here.
	unsigned int min_freq = ::std::min(
								(unsigned int)curr_dc_left_currVal, 
								distant_str.freq
								);
	int new_val = curr_dc_left_currVal - min_freq;
	if ( curr_dc_left_entry.compare_exchange_strong(curr_dc_left_currVal, new_val)) {
		distant_str.freq -= min_freq;
		dirty_index_vec_dcul.push_back(Wn.tar_lmer_id);
		if (new_val == 0) {
			return 0;
		} else {
			return 1;
		}
	}
	return 2;
}


/*********************** logger functions ********************/




void clear_buffer_data::log_msg_1(float prev_cov, Walk_Node_Element& Wn)
{
	// if covg is negative (0.01 is an approximation for 0), issue warning
	if (curr_cov < -0.01) {
		
		ostr_.warnings_m.lock();
		
		ostr_.warnings << "Negative coverage" << std::endl;
		ostr_.warnings << "Thread id: " << omp_get_thread_num() << std::endl;
		ostr_.warnings << "prev cov: " << prev_cov << std::endl;
		ostr_.warnings << "out: " << Wn.out_dc_inter << std::endl;
		ostr_.warnings << "in: " << Wn.in_dc_inter << std::endl;
		ostr_.warnings << "curr cov: " << curr_cov << std::endl;
		
		ostr_.warnings_m.unlock();
	}
}

void clear_buffer_data::log_msg_2(unsigned int my_usage, Walk_Queue& WQ)
{
	if (( ( (float)my_usage ) * (1.0 + WQ.WBptr->P_->coverage_fudge) ) > 5000) {
		ostr_.warnings_m.lock();
		
		ostr_.warnings << "Overflow, my_usage: " << my_usage << std::endl;
		ostr_.warnings << "Thread id: " << omp_get_thread_num() << std::endl;
		
		ostr_.warnings_m.unlock();
	}
}

void clear_buffer_data::log_msg_3(unsigned int my_usage, Walk_Queue& WQ)
{
	if (WQ.walk_info_level_ >= 2) {
		log_file_ 	<< std::string(print_level, '\t') 
				<< "Warning: zero variable being reduced by " 
				<< my_usage << std::endl;
	}

}

void clear_buffer_data::log_msg_4(Walk_Queue& WQ)
{
	if (WQ.walk_info_level_ >= 2) {
		log_file_ 	<< std::string(print_level, '\t') 
				<< "Clear buffer: No usages to play with, clearing up everyting and proceeding" 
				<< std::endl;
	}

}

std::ostream& operator<<(std::ostream& os, sat_const_str& scs) {
	os << "tar_lmer_id: " << scs.tar_lmer_id << "\tfreq: " << scs.freq << "\tis_intra_read: " << BOOL(scs.is_intra_read);
	return os;
}

