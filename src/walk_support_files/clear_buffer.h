#ifndef CLEAR_BUFFER_H
#define CLEAR_BUFFER_H

#include "../all_classes.h"
#include "../misc.h"

class sat_const_str {
public:
	unsigned int tar_lmer_id;
	unsigned int freq;
	bool is_intra_read;
	int distance;
};
std::ostream& operator<<(std::ostream& os, sat_const_str& scs);


class clear_buffer_data {
private:
	unsigned int base_num_dc;
	float base_cov;
	unsigned int longest_node_freq;
	int longest_node_len;
	
	// let's hope that we don't become hopeless and there is some hope to have hope.
	bool hopeless;
	
	float curr_cov;
	unsigned int k;
	std::vector<std::string> olap_lmers;
	
	std::ofstream& log_file_;
	outfile_str & ostr_;
	unsigned int& print_level;
	
	// HELPER FUNCTIONS
	
	void update_hovering_dc(	std::vector< unsigned int >& hovering_dc_vec, 
							unsigned int beg, 
							unsigned int end, 
							Walk_Queue& WQ);
	
	void update_nul_entry_hope(std::atomic<long int> & nul_entry, const unsigned int my_usage, const unsigned int my_fudge_usage, Walk_Node_Element & Wn, Walk_Queue & WQ);
	
	void update_dc_usage_core(Walk_Node_Element & Wn, 
							Walk_Queue & WQ, 
							std::vector< sat_const_str > & sat_const_vec, 
							bool updating_forward);
	
	bool calc_base_num_dc(const unsigned int sum_num_dc, const unsigned int non_zero_cands, Walk_Queue& WQ);
	
	
public:
	
	// constructor
	clear_buffer_data(Walk_Queue & WQ);
	
	// go from beg to end, and calculate the base coverage
	void calculate_base_coverage(unsigned int beg, unsigned int end, Walk_Queue & WQ);
	
	// update usages of Wn, and also update curr_cov
	void update_usage(Walk_Node_Element & Wn, Walk_Queue & WQ);
	
	// update the dist const usage of Wn, used in update_usage
	void update_dist_const_usage(Walk_Node_Element & Wn, Walk_Queue & WQ);
	
	// add Wn to overlap_lmers
	void update_overlap_lmers(Walk_Node_Element & Wn, Walk_Queue & WQ);
	
	void update_reverse_seed_str(Walk_Node_Element & Wn, Walk_Queue & WQ);
	
	// sanity check the the overlap_lmers actually overlap
	void update_hanging_seq_n_sanity_check(Walk_Queue & WQ);
	
	// use overlap_lmers to update curr_contig in WQ
	void update_current_contig(std::string & curr_contig, unsigned int & infoLevel);
// 	void update_current_contig(Walk_Queue& WQ);
	
	// update saved_base_num_dist_const in WQ
	void clear(Walk_Queue& WQ);
	
	/************************ logger functions *****************/
	
	void log_msg_1(float prev_cov, Walk_Node_Element& Wn);
	void log_msg_2(unsigned int my_usage, Walk_Queue& WQ);
	void log_msg_3(unsigned int my_usage, Walk_Queue& WQ);
	void log_msg_4(Walk_Queue& WQ);
	
	friend class Walk_Queue_Par;
	
};

#endif // CLEAR_BUFFER_H