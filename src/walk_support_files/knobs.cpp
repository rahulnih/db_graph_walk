//#include "knobs.h"
#include "../walk.h"
#include "select_next_hop.h"
#include "../tinterface.h"

/***************************** GRAPH CONSTRUCTION KNOBS ********************************/


void dist_const_table_type::update_maxdc_error()
{
// 	P_->max_dist_error = std::min(int(est_ins_size_dev), (int)std::ceil(float(est_ins_size_mean)/10));
	P_->max_dist_error = est_ins_size_dev;
}

// push the estimated insert size and dev into relevant DC table entries
void dist_const_table_type::update_inter_read_data()
{
	for (unsigned int i=0; i < DC_Table.size(); i++) {
		if (not DC_Table[i].is_intra_read) {
			DC_Table[i].distance += est_ins_size_mean - insert_size_init;
			
			DC_Table[i].error = (int)std::ceil(float(DC_Table[i].distance)/10);
// 			DC_Table[i].error = (int)(P_->max_dist_error);
		}
	}
}


/******************************* GRAPH TRAVERSAL KNOBS **********************************/


// given the support vector for a candidate, calculate the score
float Walk_Queue::evidence_score(std::vector<support_str>& cand_support_inter_read, std::vector<support_str>& cand_support_intra_read) {
	float score=0;
	float p = WBptr->P_->support_power;
	int max_err = WBptr->P_->max_dist_error;
	float intra_support_scale_down = WBptr->P_->intra_support_scale;
	
	if (WBptr->P_->evidence_scaledown_fn == LIN) {
		// add support for inter read dist const
		for (unsigned int i=0; i < cand_support_inter_read.size(); i++) {
			float x = float(cand_support_inter_read[i].freq) * pow( float(max_err - abs(cand_support_inter_read[i].distance))/float(max_err), p);
			score += x;
		}
		
	}
	else if (WBptr->P_->evidence_scaledown_fn == GAUSS) {
		// add support for inter read dist const
		for (unsigned int i=0; i < cand_support_inter_read.size(); i++) {
			float x = float(cand_support_inter_read[i].freq) * pow( WBptr->gaussian_scaledown[abs(cand_support_inter_read[i].distance)], p);
			score += x;
		}
	}
	else {
		// error
	}
	
	
	// add support for intra read dist const
	for (unsigned int i=0; i < cand_support_intra_read.size(); i++) {
		float x = float(cand_support_intra_read[i].freq)/float(abs(cand_support_intra_read[i].distance)+1);
		score += x * intra_support_scale_down;
	}
	
	return score;
}

// THINK how to modify when read errors come into picture
// given two candidate nodes, find if one is an evidence other's occurance
bool Walk_Queue::match_evidence(	const Walk_Node_Element& ev_node, 
								const Walk_Node_Element& fixed_node, 
								const int obs_error) {
	// right now, just matching the dir, ed will be considered later
	return ( (ev_node.tar_lmer_id == fixed_node.tar_lmer_id) 
			and ((ev_node.end & 0x8) == (fixed_node.end & 0x8)) and (ev_node.error > abs(obs_error)) );
}


bool Walk_Queue::is_good_score(const float norm_score_b, const float norm_score_sb, const float abs_score_b, const float abs_score_sb) {
	
	const Params& Pref = *(WBptr->P_);
	const float min_score_diff_norm = Pref.sufficient_score_diff_norm;
	const float min_score_diff_abs = Pref.sufficient_score_diff_abs;
	
	bool decision=false;
	
	float score_diff_norm = norm_score_b - norm_score_sb;
	float score_diff_abs = abs_score_b - abs_score_sb;
	
	if (score_diff_norm >= min_score_diff_norm and score_diff_abs >= min_score_diff_abs) {
		decision = true;
	}
	
	if (Pref.print_score_diff_stats) {
		WBptr->SDV.push(score_diff_abs, score_diff_norm, decision);
	}
	
	return decision;
}

void select_next_hop_class::update_exp_covg(Walk_Node_Element& WNE)
{
	// coverage should be positive

	// TODO: Temporary fix, find out when it occurs
	const float mag_ratio = WQ.WBptr->magic_ratio;
	if (float(WQ.exp_covg)/mag_ratio + (float)WNE.out_dc_inter > (float)WNE.in_dc_inter) {
		
		// coverage
		WQ.exp_covg = (unsigned int)std::round( 
										mag_ratio
										*(
											float(WQ.exp_covg)/mag_ratio 
											+ (float)WNE.out_dc_inter 
											- (float)WNE.in_dc_inter
										) 
											);
		
		if (WQ.exp_covg == 0) {
			WQ.exp_covg = 1;
		}
	}
	else {
		WQ.exp_covg = 1;
	}

}


// float Walk_Queue::evidence_score(std::vector<support_str>& cand_support_inter_read, std::vector<support_str>& cand_support_intra_read) {
// 	print_level++;
// 	log_file_ << std::string(print_level, '\t') << "IN evidence_score" << std::endl;
// 	log_file_ << std::string(print_level+1, '\t') << "Support: " << cand_support_inter_read << std::endl;
// 	log_file_ << std::string(print_level+1, '\t') << "Intra read: " << cand_support_intra_read << std::endl;
// 	
// 	float score=0;
// 	float p = WBptr->P_->support_power;
// 	int max_err = WBptr->P_->max_dist_error;
// 	for (unsigned int i=0; i < cand_support_inter_read.size(); i++) {
// 		float x = float(cand_support_inter_read[i].freq) *pow( float(max_err - abs(cand_support_inter_read[i].distance))/float(max_err), p);
// 		score += x;
// 	}
// 	for (unsigned int i=0; i < cand_support_intra_read.size(); i++) {
// 		float x = float(cand_support_intra_read[i].freq)/float(abs(cand_support_intra_read[i].distance)+1);
// 		score += x/2;
// 	}
// 	log_file_ << std::string(print_level, '\t') << "DONE evidence_score" << std::endl;
// 	print_level--;
// 	return score;
// }
