#ifndef MINI_ALL_PATHS_H
#define MINI_ALL_PATHS_H

#include "../all_classes.h"
#include "../atomic_array.hpp"
#include "../misc.h"
#include "../walk_base.h"

class AP_node_info {
private:
	unsigned int depth;
	unsigned int parent;
	unsigned int root;
	unsigned int offset;
	bool is_leaf;
	float cum_score;
// 	Walk_Node_Element WNE;
// 	unsigned int local_node_usage_left;
	
	
	friend class mini_all_paths_data;
};

// used in attempt decision()
typedef struct {
	float normScore=0;
	float absScore=0;
	unsigned int root=0;
	unsigned int index;
} scoreData;


class mini_all_paths_data {
private:
	
	// VARS
	
	// depth till which all paths has to be done.
	unsigned int max_dist;
	unsigned int max_num_nodes;
	unsigned int k;
	
	// a copy of WQ.curr, will be needed when resetting back.
	unsigned int curr_copy;
	
	std::vector<AP_node_info> MAP_tree;
	
	// necessary structures for mini_all_paths, all vecs have equal node, one entry per all paths node
	
	// depth of nodes in terms of num nodes, root depth=0
	std::vector<unsigned int> depth;
	
	// index of the parent in this very array
	std::vector<unsigned int> parent;
	
	// index of root in this very array
	std::vector<unsigned int> root;
	
	// offset in terms of bp from root to the head of the node
	std::vector<unsigned int> head_offset;
	std::vector<bool> is_leaf;
	// carries the cummulative score, not the normalized score
	std::vector<float> score_vec;
	std::vector<Walk_Node_Element> nodes;
	std::vector<unsigned int> local_node_usage_left;
	
	// only for the root nodes, one will hopefully be passed as the root support to the WQ instance
	
	std::vector<std::vector< dc_str> > root_cand_dc_inter_read;
	std::vector<std::vector< dc_str> > root_cand_dc_intra_read;
	
	// only for statistical purposes, max num bases depth searched by mini all paths
	unsigned int max_depth_accessed;
	
	bool bubble_found;
	
	// vars imported from WQ
	std::ofstream& log_file_;
	std::ofstream& bub_file_;
	unsigned int& print_level;
	const unsigned int& infoLevel;
	
	atomic::atomic_array<long int> & exp_nul_vec;
	atomic::atomic_array<int> & exp_nul_unup_vec;
	const Params & Pref;
	bubble_str_vec& BSV_;
	
	// HELPER FUNCTIONS
	
	bool check_ancestor_match(unsigned int& modNodeUsgLeft, 
							const unsigned int& add_node_pos, 
							const unsigned int& brown_lid);
	
	void accept_node( const unsigned int modNodeUsgLeft, 
					const unsigned int i, 
					Walk_Queue & WQ);
	
	unsigned int find_lca(	const unsigned int best_score_index, 
						const unsigned int sec_best_score_index);
	
	unsigned int findBubble(const unsigned int best_index, const unsigned int sbest_index);
	
		unsigned int checkBubble_bf(std::vector<unsigned int> & path1, std::vector<unsigned int> & path2);
	
		void checkBubble_eff(std::vector<unsigned int> & path1, std::vector<unsigned int> & path2);
		
		void make_bubble_str(	unsigned int ho1, 
							unsigned int ho2, 
							std::vector<unsigned int> & path1, 
							std::vector<unsigned int> & path2,
							std::vector< Walk_Node_Element >& nodes, 
							bubble_str& B);
		
// 		void get_ancestor_nodes(std::vector<Walk_Node_Element&> & anc_nodes, const unsigned int idx);
		
// 		void get_idx_nodes(std::vector<Walk_Node_Element&> & nodes, const std::vector<unsigned int> & idx_vec);
	
	bool check_root_node(	const Walk_Node_Element & curr_WNE, long & exp_nul_entry,
						Walk_Queue & WQ);
	
	void set_node_data();
	
	float calc_root_score(Walk_Queue & WQ);
	
	void set_MAP_strs_rt(const unsigned int idx, float root_score);
	
	
	void remove_no_usg_nodes(unsigned int & new_nodes, const unsigned int add_node_pos, Walk_Queue& WQ);
	
	void set_MAP_strs_gen(const unsigned int new_nodes, 
						const unsigned int curr_depth, 
						const unsigned int add_node_pos, 
						const unsigned int next_offset);
	
	float calc_gen_score(Walk_Node_Element & curr_WNE, Walk_Queue & WQ);
	
	// calc two best leaves wrt norm score
	void calc_two_best_leaves(scoreData & best, scoreData & secBest, Walk_Queue & WQ);
	
	// LOGGER FUNCTIONS
	
	void log_msg1(	const unsigned int i, 
					const unsigned int leaf_len, 
					const float norm_score, 
					Walk_Queue & WQ);
	void log_msg2(	const float score_diff_norm, 
					const float min_score_diff_norm, 
					const float score_diff_abs, 
					const float min_score_diff_abs, 
					Walk_Queue & WQ);
	void log_msg3(const unsigned int best_score_index, Walk_Queue & WQ);
	void log_msg4();
	void log_msg5();
	void log_msg6();
	
	void beg_salutation(std::string fn_name);
	void end_salutation(std::string fn_name);
	
public:
	
	// functions
	
	// constructor
	mini_all_paths_data(Walk_Queue& WQ);
	
	// assert that all vector sizes are equal
	void assert_equal_sizes();
	
	// add all the brown nodes at the starting position into the vectors
	unsigned int add_root_brown_nodes(Walk_Queue_Element& WQE, Walk_Queue& WQ);
	
	// add brown nodes at depth d, and keep updating add_node_pos, return number of nodes added
	unsigned int add_current_brown_nodes(unsigned int d, unsigned int& add_node_pos, Walk_Queue& WQ);
	
	// calculate scores for all nodes at current depth
	void calc_curr_depth_scores (unsigned int curr_depth_node_count, Walk_Queue& WQ);
	
	// try to make a decision by looking at current depth scores
	bool attempt_decision (unsigned int& best_score_index, Walk_Queue& WQ);
	
	// after decision is made, trace the decided path
	void trace_ancestor_path(std::vector< unsigned int >& path_indices, const unsigned int index);
	
	// print all vectors
	void print_vecs(std::ostream& os);
	
	// clear all vectors, and reset back WQ.curr
	void clear(Walk_Queue& WQ);
	
	size_t size();
	
// 	friend class Walk_Queue;
	friend class select_next_hop_class;
};	


#endif // MINI_ALL_PATHS_H