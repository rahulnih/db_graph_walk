#include "job_queue.h"

/********************** Contig extention jobs***************************/

// TODO: consider blatently copying WQ to and from Job, introduce walk queue var in job, to change extract_WQ_from_job and create job from Walk_Queue

void Walk_Queue_Job_Info::clear()
{
	curr_contig.clear();
	Q.clear();
	white_node_vec.clear();
	decided_hops.clear();
	decided_dist_const_inter_read.clear();
	decided_dist_const_intra_read.clear();
	
	dirty_index_vec_dcul.clear();
	dirty_index_vec_nul.clear();
}


size_t Walk_Queue_Job_Info::dynamic_size()
{
	size_t bytes = 0;
	for (size_t i=0; i < Q.size(); i++) {
		bytes += Q[i].size();
	}
	bytes += curr_contig.size() 
			+ TSS.dynamic_size() 
// 			+ vecsize(reverse_seed_vector) 
			+ vecsize(white_node_vec) 
			+ vecsize(decided_hops) 
			+ vecsize(decided_dist_const_inter_read) 
			+ vecsize(decided_dist_const_intra_read);
			
	return bytes;
}


// JOB_STR

// default constructor
job_str::job_str(std::ofstream* t_log_file_ptr)
{
	src_thread_id = std::numeric_limits<int>::max();
	d = FWD;
	contig_num = std::numeric_limits<unsigned int>::max();
	contig_part = std::numeric_limits<unsigned int>::max();
	WQJI.clear();
	t_log_file_ptr_ = t_log_file_ptr;
}

void job_str::copy_WQ(Walk_Queue& WQ)
{
	// start time
	wall_time_wrap W;
	W.start();
	
	// some refs for readibility
	Walk_Queue_Job_Info & my_WQ = this->WQJI;
	currContStr & CCS_ = WQ.CCS;
	decidedHopStr& DHS_ = WQ.DHS;
	
	// now do the copying
	(my_WQ.curr_contig).swap( CCS_.curr_contig);
	my_WQ.curr_cont_no_neigh = CCS_.no_neigh;
	my_WQ.fw_contig_id = CCS_.fw_contig_id;
	
	my_WQ.Q.resize(WQ.Q.size());
	(my_WQ.Q).swap(WQ.Q);
	
	my_WQ.beg = WQ.beg;
	my_WQ.curr = WQ.curr;
	my_WQ.expected_coverage = WQ.exp_covg;
	my_WQ.TSS = WQ.TSS;
	my_WQ.walking_reverse = WQ.RWS.walkRev;
	my_WQ.white_node_count = CCS_.white_node_count;
	
	(my_WQ.white_node_vec).swap(CCS_.white_node_vec);
	(my_WQ.decided_hops).swap(DHS_.hops);
	(my_WQ.decided_dist_const_inter_read).swap(DHS_.decided_dc_inter);
	(my_WQ.decided_dist_const_intra_read).swap(DHS_.decided_dc_intra);
	
	(my_WQ.dirty_index_vec_dcul).swap(WQ.dirty_index_vec_dcul);
	(my_WQ.dirty_index_vec_nul).swap(WQ.dirty_index_vec_nul);
	
	my_WQ.print_level = WQ.print_level;
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "COPY WQ");
}


// TODO: review this code
// take data from job and put it in walk queue WQ
void job_str::extract_WQ_from_job(Walk_Queue& WQ)
{
	wall_time_wrap W;
	W.start();
	
	Walk_Queue_Job_Info& my_WQ = this->WQJI;
	currContStr & CCS_ = WQ.CCS;

	(CCS_.curr_contig).swap(my_WQ.curr_contig);
	CCS_.no_neigh = my_WQ.curr_cont_no_neigh;
	CCS_.fw_contig_id = my_WQ.fw_contig_id;
	(WQ.Q).swap(my_WQ.Q);
	WQ.beg = my_WQ.beg;
	WQ.curr = my_WQ.curr;
	WQ.exp_covg = my_WQ.expected_coverage;
	WQ.TSS = my_WQ.TSS;
	WQ.RWS.walkRev = my_WQ.walking_reverse;
	CCS_.white_node_count = my_WQ.white_node_count;
	
	(CCS_.white_node_vec).swap(my_WQ.white_node_vec);
	(WQ.DHS.hops).swap(my_WQ.decided_hops);
	(WQ.DHS.decided_dc_inter).swap(my_WQ.decided_dist_const_inter_read);
	(WQ.DHS.decided_dc_intra).swap(my_WQ.decided_dist_const_intra_read);
	
	(WQ.dirty_index_vec_dcul).swap(my_WQ.dirty_index_vec_dcul);
	(WQ.dirty_index_vec_nul).swap(my_WQ.dirty_index_vec_nul);
	
	WQ.print_level = my_WQ.print_level;
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "EXTRACT WQ FROM JOB");
}

void job_str::clear()
{
	src_thread_id = std::numeric_limits<int>::max();
	d = FWD;
	contig_num = std::numeric_limits<unsigned int>::max();
	contig_part = std::numeric_limits<unsigned int>::max();
	WQJI.clear();
}

void job_str::create_job_from_walk_queue(Walk_Queue& WQ, job_str *curr_job_ptr, unsigned int contig_id)
{
	wall_time_wrap W;
	W.start();
	
	src_thread_id = omp_get_thread_num();
	
	if ((*curr_job_ptr).no_job()) {
		assert(contig_id not_eq std::numeric_limits<unsigned int>::max());
		
		d = WQ.RWS.walkRev ? REV : FWD;
		contig_num = contig_id;
		
		// the current contig will get part 0, so this is part 1 really
		contig_part = 1;
	}
	else {
		d = (*curr_job_ptr).d;
		contig_num = (*curr_job_ptr).contig_num;
		contig_part = (*curr_job_ptr).contig_part + 1;
	}
	
	// copy the walk queue for the job
	copy_WQ(WQ);
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "CREATE JOB FROM WQ");
}

size_t job_str::dynamic_size()
{
	return WQJI.dynamic_size();
}


// JOB_QUEUE_STR

void job_queue_str::push_job(job_str *J)
{
	wall_time_wrap W;
	
	W.start();
	
	// get exclusive access on queue
	q_access.lock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_ , "PUSH JOB: LOCK");
	
	W.start();
	
	JQD_ptr.push_back(J);
		
	// release exclusive access
	q_access.unlock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "PUSH JOB: ACTUAL");
}

job_str * job_queue_str::pop_job_to_walk_queue(Walk_Queue& WQ)
{
	wall_time_wrap W;
	W.start();
	
	q_access.lock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "POP JOB 2 WQ: LOCK");
	
	W.start();
	
	job_str *J_ptr = JQD_ptr.front();

	(*J_ptr).extract_WQ_from_job(WQ);

	JQD_ptr.pop_front();
	
	q_access.unlock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *t_log_file_ptr_, "POP JOB 2 WQ: ACTUAL");
	
	return J_ptr;
}

size_t job_queue_str::dynamic_size()
{
	size_t bytes=0;
	for (size_t i = 0; i < JQD_ptr.size(); i++) {
		bytes += JQD_ptr[i]->size();
	}
	return bytes;
}

/********************** END: Contig extention jobs***************************/



/********************************* operator<< ******************************/

std::ostream& operator<<(std::ostream& os, job_queue_str& JQS)
{
	os << "";
	return os;
}

std::ostream& operator<<(std::ostream& os, job_str& J)
{
	os << "src_thread_id: " << J.src_thread_id << std::endl;
	os << "Direction: " << Contig_Dir_String[J.d] << std::endl;
	os << "contig_num: " << J.contig_num << std::endl;
	os << "contig_part: " << J.contig_part << std::endl;
	os << "contig_len: " << J.WQJI.curr_contig.size() << std::endl;
	os << "white node vec size: " << J.WQJI.white_node_vec.size() << " " << J.WQJI.white_node_count << std::endl;
	return os;
}

bool operator==(const Walk_Queue_Job_Info& w1, const Walk_Queue_Job_Info& w2)
{
	bool result=true;
	if (		w1.curr_contig not_eq w2.curr_contig
		or 	w1.curr_cont_no_neigh not_eq w2.curr_cont_no_neigh
		or	w1.fw_contig_id not_eq w2.fw_contig_id
		or	w1.Q.size() not_eq w2.Q.size()
		or	w1.beg not_eq w2.beg
		or	w1.curr not_eq w2.curr
		or	w1.expected_coverage not_eq w2.expected_coverage
		or	not (w1.TSS == w2.TSS)
		or	w1.walking_reverse not_eq w2.walking_reverse
		or	w1.white_node_count not_eq w2.white_node_count
		or	w1.white_node_vec.size() not_eq w2.white_node_vec.size()
		or	w1.decided_hops.size() not_eq w2.decided_hops.size()
		or	w1.decided_dist_const_inter_read.size() not_eq w2.decided_dist_const_inter_read.size()
		or	w1.decided_dist_const_intra_read.size() not_eq w2.decided_dist_const_intra_read.size()
		or	w1.dirty_index_vec_dcul.size() not_eq w2.dirty_index_vec_dcul.size()
		or	w1.dirty_index_vec_nul.size() not_eq w2.dirty_index_vec_nul.size()
		or	w1.print_level not_eq w2.print_level) {
			result = false;
	}
	// TODO: add more conditions here
	return result;
}

bool operator==(const job_str& J1, const job_str& J2)
{
	if (J1.src_thread_id == J2.src_thread_id
		and J1.d == J2.d
		and J1.contig_num == J2.contig_num
		and J1.contig_part == J2.contig_part
		and J1.WQJI == J2.WQJI
		and J1.t_log_file_ptr_ == J2.t_log_file_ptr_) {
			return true;
	}
	return false;
}


/******************************END: operator<< *****************************/

