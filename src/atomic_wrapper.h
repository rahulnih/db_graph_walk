#ifndef ATOMIC_WRAPPER_H
#define ATOMIC_WRAPPER_H

#include "all_classes.h"
#include <mutex>
#include <typeinfo>

/*Wrapper struct to create vector of atomic objects
 * copied from : https://stackoverflow.com/questions/13193484/how-to-declare-a-vector-of-atomic-in-c
 */
// my wrapper

template <typename T>struct identity { typedef T type; };

template <typename T>
struct my_atomic_wrapper
{
	// vars
	
	T a_;
	std::mutex var_access;
	
	// functions

	my_atomic_wrapper():a_() {}

	my_atomic_wrapper(const T &a):a_(a) {}
	
	// copy constructor
	my_atomic_wrapper(const my_atomic_wrapper &other):a_(other.a_) {}

	// move/assignment constructor
	void lock() {var_access.lock();}
	
	void unlock() {var_access.unlock();}
	
	my_atomic_wrapper& operator=(const my_atomic_wrapper& other) {
		a_ = other.a_;
		return *this;
	}
	
	// addition operators
	
	my_atomic_wrapper& operator+=(const my_atomic_wrapper& other) {
		a_+=other.a_;
		return *this;
	}
	
	my_atomic_wrapper& operator+=(const T& other) {
		a_+=other;
		return *this;
	}
		
	// subtraction operators
	
	my_atomic_wrapper& operator-=(const my_atomic_wrapper &other) {
		a_-=other.a_;
		return *this;
	}
	
	my_atomic_wrapper& operator-=(const T &other) {
		a_-=other;
		return *this;
	}
	
	// +1 operators
	
	// ++obj
	my_atomic_wrapper& operator++() {
		if (a_ == std::numeric_limits<T>::max()) {
			std::cout << "Warning, incrementing max, type: " << typeid(T).name() << std::endl;
		}
		a_ += 1;
		return *this;
	}
	
	// obj++
	my_atomic_wrapper operator++(int) {
		my_atomic_wrapper result(*this);
		if (a_ == std::numeric_limits<T>::max()) {
			std::cout << "Warning, incrementing max, type: " << typeid(T).name() << std::endl;
		}
		a_ += 1;
		return result;
	}
	
	// --obj
	my_atomic_wrapper& operator--() {
		a_ -= 1;
		return *this;
	}
	
	// obj--
	my_atomic_wrapper operator--(int) {
		my_atomic_wrapper result(*this);
		a_ -= 1;
		return result;
	}
	
	T val() {return a_>0?a_:0;}
	T true_val() {return a_;}
	
};

// output operator for a
template <typename T> std::ostream& operator<<(std::ostream& os, const my_atomic_wrapper<T> A) {
	os << A.a_;
	return os;
}

// equality operator
template <typename T> bool operator==(const my_atomic_wrapper<T> A, const my_atomic_wrapper<T> B) {
	return A.a_ == B.a_;
}

template <typename T> bool operator==(const my_atomic_wrapper<T> A, typename identity<T>::type B) {
	return A.a_ == B;
}

// less than operator
template <typename T> bool operator<(const my_atomic_wrapper<T> A, const my_atomic_wrapper<T> B) {
	return A.a_ < B.a_;
}

template <typename T> bool operator<(const my_atomic_wrapper<T> A, typename identity<T>::type B) {
	return A.a_ < B;
}

// greater than operator
template <typename T> bool operator>(const my_atomic_wrapper<T> A, const my_atomic_wrapper<T> B) {
	return A.a_ > B.a_;
}

template <typename T> bool operator>(const my_atomic_wrapper<T> A, typename identity<T>::type B) {
	return A.a_ > B;
}

// greater than or equal to
template <typename T> bool operator>=(const my_atomic_wrapper<T> A, const my_atomic_wrapper<T> B) {
	return A.a_ >= B.a_;
}

template <typename T> bool operator>=(const my_atomic_wrapper<T> A, typename identity<T>::type B) {
	return A.a_ >= B;
}

// less than or equal to
template <typename T> bool operator<=(const my_atomic_wrapper<T> A, const my_atomic_wrapper<T> B) {
	return A.a_ <= B.a_;
}

template <typename T> bool operator<=(const my_atomic_wrapper<T> A, typename identity<T>::type B) {
	return A.a_ <= B;
}


#endif // ATOMIC_WRAPPER_H
