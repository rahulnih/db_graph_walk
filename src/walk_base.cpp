#include "walk_base.h"


std::ostream& operator<<(std::ostream& os, bubble_str& B)
{
	os << "=====Beg=====" << std::endl;
	
	os << B.tar_lmer_id_vec1 << " || offset: " << B.head_offset1 << std::endl;
	os << B.tar_lmer_id_vec2 << " || offset: " << B.head_offset2 << std::endl;
	
	os << "=====End=====" << std::endl;

	return os;

}


void bubble_str_vec::push(bubble_str& B)
{
	m.lock();
	V.push_back(B);
	m.unlock();
}

std::ostream& operator<<(std::ostream& os, bubble_str_vec& BSV)
{
	for (long unsigned int i=0; i < BSV.V.size(); i++) {
		os << BSV.V[i] << std::endl;
	}
	return os;

}


// constructor
usage_data_str::usage_data_str(DB_Node_Table_Type const & DB_Table, const Params * P, std::ofstream& log_file) : 
	node_usage_left(DB_Table.size()), 
	expected_node_usage_left(DB_Table.size()), 
	expected_node_usage_left_unupdated(DB_Table.size(), 0)
{
	// MASTER THREAD ONLY. int thread_id = omp_get_thread_num();
	
	if (P->walk_info_level >=2) {
		log_file << "usage_data_str constructor" << std::endl;
	}

	
	size_t count = DB_Table.size();

	// initiate the usage left structure
	dist_const_usage_left.reserve(count);  dist_const_usage_left.clear();
	expected_dist_const_usage_left.reserve(count);  expected_dist_const_usage_left.clear();
	for (size_t i=0; i < count; i++) {
		DB_Node_Type const & D = DB_Table.ElementAt(i);

		dist_const_usage_left.emplace_back(atomic::atomic_array<int>(D.Dist_Const_List.size(),0));
		expected_dist_const_usage_left.emplace_back(atomic::atomic_array<int>(D.Dist_Const_List.size(),0));

		for (size_t j=0; j< D.Dist_Const_List.size(); j++) {
			dist_const_usage_left.back()[j] = D.Dist_Const_List[j].freq;
			expected_dist_const_usage_left.back()[j] = D.Dist_Const_List[j].freq;
		}
	}
	expected_dist_const_usage_left = dist_const_usage_left;
	
	// initiate the structure for walk, and create sorted list of lmer ids
	for (size_t i=0; i < count; i++) {
		node_usage_left[i] = DB_Table.ElementAt(i).freq;
		expected_node_usage_left[i] = DB_Table.ElementAt(i).freq;
	}
	
	if (P->walk_info_level >=2) {
		log_file << "DONE usage_data_str constructor" << std::endl;
	}
	
}


void usage_data_str::equate_dist_const_vec(	std::vector<long unsigned int>& dcul_vec, 
										std::vector<long unsigned int>& nul_vec)
{
	// TODO: sort and unique
	// TODO: if parallel walk, assert that only my partition nodes are being touched
	
// 	std::vector<unsigned int>& dcul_vec = WQ.dirty_index_vec_dcul;
// 	std::vector<unsigned int>& nul_vec = WQ.dirty_index_vec_nul;
	
// 	expected_dist_const_usage_left = dist_const_usage_left;
	for (std::size_t i=0; i < dcul_vec.size(); i++) {
		std::size_t index = dcul_vec[i];
		for (std::size_t j=0; j < expected_dist_const_usage_left[index].size(); j++) {
			
			// actual is assigned to expected.
			expected_dist_const_usage_left[index][j].store(dist_const_usage_left[index][j].load());
		}
	}
	for (std::size_t i=0; i < nul_vec.size(); i++) {
		std::size_t index = nul_vec[i];

		expected_node_usage_left[ index ].store(node_usage_left[ index ].load());
	}
	
	dcul_vec.clear();
	nul_vec.clear();
}

size_t usage_data_str::dynamic_size()
{
	size_t unupdated_size = expected_node_usage_left_unupdated.mem_size();
	size_t nul_size = node_usage_left.mem_size();
	size_t dcul_size = 0;
	for (size_t i = 0; i < dist_const_usage_left.size(); i++) {
		dcul_size += dist_const_usage_left[i].mem_size();
	}
	
	return unupdated_size + 2* nul_size + 2*dcul_size;
}



Walk_Base::Walk_Base(
				Params* P, 
				DB_Node_Table_Type const & list_nodes, 
				KU_Table_Type const & KU_Table, 
				contig_data_str& CDS, 
				std::vector<lmer_partition_map> const & LP_map, 
				outfile_str& ostr) : ref_db_node_table(list_nodes), 
									ref_ku_table(KU_Table), 
									CDS_(CDS), 
									LP_map_(LP_map), 
									ostr_(ostr),
									contig_count(0),
									P_(P)
{
	magic_ratio = (float)(P_->read1_length + P_->read2_length)/ (float)(P_->insert_size);
	magic_ratio = std::max(magic_ratio, P_->min_magic_ratio);
	magic_ratio = std::min(magic_ratio, P_->max_magic_ratio);
	
	// usage data constructor
	UD = usage_data_str(this->ref_db_node_table, this->P_, this->ostr_.log_file);

	this->estimate_parameters();
	
	// fill in the gaussian vector
	float std_dev = float(P_->max_dist_error)/ P_->error_scaling;
	
	for (int i=0; i <= P_->max_dist_error; i++) {
		gaussian_scaledown.push_back( exp( -float(i*i) / (2*std_dev*std_dev) ) );
	}
	
}

void Walk_Base::estimate_parameters()
{
	if (P_->walk_info_level >= 2) {
		ostr_.log_file << "Estimating parameters...";
	}
	
	unsigned int max_len = ref_db_node_table.calc_longest_lmer_len();
	unsigned int max_dist_const = 2*max_len + P_->insert_size + P_->max_dist_error;
	
	if (P_->walk_info_level >= 2) {
		ostr_.log_file 		<< "Longest lmer length: " << max_len 
						<< "\tmax_dist_const: " << max_dist_const 
						<< std::endl;
	}
	
	walk_queue_size = 4*max_dist_const;
	walk_queue_low_size = max_dist_const;
	walk_queue_preserve_history = max_dist_const;
	
	if (P_->walk_info_level >= 1) {
		ostr_.log_file 	<< "New params: walk_queue_size = " << walk_queue_size 
					<< "\twalk_queue_low_size = " << walk_queue_low_size 
					<< "\tpreserve_history = " << walk_queue_preserve_history 
					<< std::endl 
					<< "Done" 
					<< std::endl;
	}
}


void Walk_Base::init_lmer_id_vec()
{
	ostr_.log_file << "Init lmer id vec...." << std::flush;
	
	wall_time_wrap W;
	W.start();
	
	// vector storing lmer id's of all lmers in sorted order of lmer length
	std::vector<std::pair<unsigned int, int> > sorted_lmer_id_tup;
	sorted_lmer_id_tup.resize(ref_db_node_table.size());
	
	sorted_lmer_id_vec.resize(ref_db_node_table.size());
	
	// criteria for sorting sorted_lmer_id_vec
// 	std::vector<int> lmer_length_vector(ref_db_node_table.size(),0);
	
#pragma omp parallel for
	for (std::size_t i=0; i < ref_db_node_table.size(); i++) {
		sorted_lmer_id_tup[i].first = i;
		sorted_lmer_id_tup[i].second = ref_db_node_table.ElementAt(i).len_lmer;
	}
	
	ALGO_NS::sort(sorted_lmer_id_tup.begin(), 
				sorted_lmer_id_tup.end(), 
				[](std::pair<unsigned int, int> a, std::pair<unsigned int, int> b)
				{
					return a.second > b.second;
				});
	
	for (std::size_t i=0; i < ref_db_node_table.size(); i++) {
		sorted_lmer_id_vec[i] = sorted_lmer_id_tup[i].first;
	}
	
	// print if necessary
	if (P_->walk_info_level>=2) {
		ostr_.log_file << "Sorted lmer id's: " << sorted_lmer_id_vec << std::endl;
	}
	
	W.stop();
	ostr_.log_file << "done, time:" << W << std::endl;
}

size_t Walk_Base::dynamic_size()
{
	return vecsize(sorted_lmer_id_vec) + UD.dynamic_size();
}

void Walk_Base::print_start_node_stats()
{
	wall_time_wrap W;
	if (P_->print_start_node_stats) {
		std::cout << "Sorting start node lens..." << std::flush;
		W.start();
		SLV.sort();
		W.stop();
		std::cout << "Done, time: " << W << std::endl << std::flush;
		
		std::cout << "Writing start node lens to file..." << std::flush;
		W.start();
		ostr_.start_node_stats_file << SLV;
		W.stop();
		std::cout << "Done, time: " << W << std::endl << std::flush;
	}
}


void Walk_Base::print_score_diff_stats()
{
	wall_time_wrap W;
	if (P_->print_score_diff_stats) {
		
		std::cout << "Writing score diff to file..." <<  std::flush;
		W.start();
		
		ostr_.score_diff_hist_file << P_->sufficient_score_diff_abs << std::endl;
		ostr_.score_diff_hist_file << P_->sufficient_score_diff_norm << std::endl;
		ostr_.score_diff_hist_file << SDV;
		
		W.stop();
		std::cout << "Done, time: " << W << std::endl << std::flush;
	}
}


void Walk_Base::print_map_stats()
{
	wall_time_wrap W;
	if (P_->print_mini_all_paths_stats) {
		
		std::cout << "Sorting MAP stat vec..." <<  std::flush;
		W.start();
		MSV.sort();
		W.stop();
		std::cout << "Done, time: " << W << std::endl << std::flush;
		
		
		std::cout << "Writing MAP stats to file..." <<  std::flush;
		W.start();
		ostr_.map_stat_file << P_->all_paths_size << std::endl;
		ostr_.map_stat_file << MSV;
		W.stop();
		std::cout << "Done, time: " << W << std::endl << std::flush;
	}
}


unsigned int Walk_Base::init_expected_coverage(unsigned int index)
{
	DB_Node_Type const & D = ref_db_node_table.ElementAt(index);
// 	if ((unsigned int)D.len_lmer > P_->insert_size) {
		return (unsigned int)std::max((long unsigned int)UD.expected_node_usage_left[index].load(), D.freq_range[1]);
// 	}
// 	else {
// 		return std::min(UD.expected_node_usage_left[index], D.freq_range[1]);
// 	}
}
