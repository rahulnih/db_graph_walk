#ifndef JOB_QUEUE_H
#define JOB_QUEUE_H

#include "all_classes.h"
#include "walk.h"
#include "misc.h"
#include <mutex>

/********************** Contig extention jobs***************************/

class Walk_Queue_Job_Info {
public:
	
	// vars
	
	std::string curr_contig;
	bool curr_cont_no_neigh=false;
	unsigned int fw_contig_id;
	std::vector<Walk_Queue_Element> Q;
	unsigned int beg;
	unsigned int curr;
	unsigned int expected_coverage;
	Traversal_support_str TSS;
	bool walking_reverse=false;
	unsigned int white_node_count;
	std::vector<unsigned int> white_node_vec;
	std::vector<Walk_Node_Element> decided_hops;
	std::vector<dc_str> decided_dist_const_inter_read;
	std::vector<dc_str> decided_dist_const_intra_read;
	
	std::vector<unsigned long int> dirty_index_vec_dcul;
	std::vector<unsigned long int> dirty_index_vec_nul;
	
	unsigned int print_level;
	
	// functions
	void clear();
	
	size_t dynamic_size();
	size_t size(){return sizeof(Walk_Queue_Job_Info) + dynamic_size();}
};

bool operator==(const Walk_Queue_Job_Info& w1, const Walk_Queue_Job_Info& w2);

// str to manage a particular job put by a different thread
class job_str {
private:
	
	// contig metadata
	
	// id of thread that put this job
	int src_thread_id;
	
	// whether the contig was fwd or rev
	contig_dir d;
	
	// contig number
	unsigned int contig_num;
	
	// part of the contig
	unsigned int contig_part;
		
	// queue metadata
	
	// job information needed from walk queue
	Walk_Queue_Job_Info WQJI;
	
	std::ofstream* t_log_file_ptr_;

	
public:
	
	// constructor
	job_str(std::ofstream* t_log_file_ptr);
	
	// Not writing explicity copy constructor, relying on implicit one
	
	void copy_WQ(Walk_Queue& WQ);
	
	// take data from job and put it in walk queue WQ
	void extract_WQ_from_job (Walk_Queue& WQ);
	
	void create_job_from_walk_queue(Walk_Queue& WQ, job_str *curr_job_ptr, unsigned int contig_id);
	
	bool no_job() const {return contig_num==std::numeric_limits<unsigned int>::max();}
	
	void clear();
	
	size_t dynamic_size();
	size_t size() {return sizeof(job_str) + dynamic_size();}
	
// 	job_str& operator=(const job_str& other);
	
	friend class contig_data_str;
	friend class Walk_Queue_Par;
	friend std::ostream& operator<<(std::ostream& os, job_str& J);
	friend bool operator==(const job_str& J1, const job_str& J2);
};



// output operator for job
// std::ostream& operator<<(std::ostream& os, job_str& J);

// str to manage jobs that threads put for each other
class job_queue_str {
private:
// 	std::queue<job_str> JQ;

// 	std::deque<job_str> JQD;
	std::deque<job_str*> JQD_ptr;
	
		
	std::mutex q_access;
	
	std::ofstream* t_log_file_ptr_;
	
public:
	// constructor
	job_queue_str(std::ofstream* t_log_file_ptr):t_log_file_ptr_(t_log_file_ptr){};
	
	void lock() {q_access.lock();}
	
	void unlock() {q_access.unlock();}
	
	// push a given job to the job queue
	void push_job(job_str *J);
	
	// pop the front job and place it on walk queue
	job_str * pop_job_to_walk_queue(Walk_Queue& WQ/*, job_str* J*/);
	
	bool empty() {return JQD_ptr.empty();}
	
	unsigned int num_jobs(){return JQD_ptr.size();};
	
	size_t dynamic_size();
	size_t size() {return sizeof(job_queue_str) + dynamic_size();}
};

std::ostream& operator<<(std::ostream& os, job_queue_str& JQS);

/********************** END: Contig extention jobs***************************/


#endif // JOB_QUEUE_H