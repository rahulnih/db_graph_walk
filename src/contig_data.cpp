#include "contig_data.h"
#include "walk.h"
#include "job_queue.h"

size_t contig_data::dynamic_size()
{
	return contig.size() + vecsize(white_node_vec);
}


// used for serial pipeline
void contig_data_str::push_contig_data(const currContStr & CCS_, int  partition_id_)
{
	contig_data C;
	
	C.contig = CCS_.curr_contig;
	C.no_neigh = CCS_.no_neigh;
	C.white_node_vec = CCS_.white_node_vec;
	C.white_node_count = CCS_.white_node_count;
	C.partition_id = partition_id_;
	
	data.push_back(C);
}

void contig_data_str::push_contig_data(	Walk_Queue& WQ, 
									const job_str *curr_job_ptr, 
									unsigned int contig_id)
{
	contig_data C;
	
// 	C.contig = WQ.curr_contig;
	(C.contig).swap(WQ.CCS.curr_contig);
	C.no_neigh = WQ.CCS.no_neigh;
	(C.white_node_vec).swap(WQ.CCS.white_node_vec);
	C.white_node_count = WQ.CCS.white_node_count;
	C.partition_id = WQ.thread_id;
	
	// now stitch info
	if ((*curr_job_ptr).no_job()) {
		C.contig_label = contig_id;
		C.contig_part = 0;
		C.d = WQ.RWS.walkRev ? REV : FWD;
	}
	else {
		C.contig_label = (*curr_job_ptr).contig_num;
		C.contig_part = (*curr_job_ptr).contig_part;
		C.d = (*curr_job_ptr).d;
	}
	
	data.push_back(C);

}


void contig_data_str::remove_empty()
{
	data.erase(	
				std::remove_if(
					data.begin(), 
					data.end(), 
					[](contig_data& C){
						return C.contig=="";
					}), 
				data.end()
			);
}


void contig_data_str::merge_fw_rev_contigs() {
	
	// first merge the contigs
	for (unsigned int i=0; i < fr_contig_glue_vec.size(); i++) {
		
		unsigned int curr_src_index = fr_contig_glue_vec[i].src_contig_id;
		unsigned int curr_tar_index = fr_contig_glue_vec[i].tar_contig_id;
		
		unsigned int curr_overlap_len = fr_contig_glue_vec[i].overlap_length;
		
		std::string& src_contig = data[curr_src_index].contig;
		std::string& tar_contig = data[curr_tar_index].contig;
		
		// reverse the target for stitching
		reverse_complement(tar_contig);
		
		// some sanity checks, overlap length should be smaller than the contig sizes
		ASSERT_WITH_MESSAGE (curr_overlap_len <= src_contig.size(), 
							"overlap length bigger than the source contig");
		ASSERT_WITH_MESSAGE (curr_overlap_len <= tar_contig.size(), 
							"overlap length bigger than the target contig");
// 		ASSERT_WITH_MESSAGE (data[curr_src_index].partition_id == data[curr_tar_index].partition_id, 
// 							"part ids of fw and rev contigs don't match");
		
		// calculate the overlapping parts
		
		// src prefix
		std::string stitch_part_1 = src_contig.substr(0, curr_overlap_len);
		
		// tar suffix
		std::string stitch_part_2 = tar_contig.substr(tar_contig.size() - curr_overlap_len, curr_overlap_len);
		
		// the overlap parts should match
		ASSERT_WITH_MESSAGE(stitch_part_1.compare(stitch_part_2)==0, 
				    "Overlap part not matching for contigs: ");
		
		// merge the fw and rev contig, make one empty
		tar_contig = tar_contig.substr(0, tar_contig.size() - curr_overlap_len) + src_contig;
		src_contig = "";
		
		data[curr_tar_index].no_neigh = data[curr_tar_index].no_neigh and data[curr_src_index].no_neigh;
		if (data[curr_tar_index].no_neigh) {
			isolated_contigs++;
		}
	}
	
	// now merge the white node list
	for (unsigned int i=0; i < fr_contig_glue_vec.size(); i++) {
		unsigned int curr_src_index = fr_contig_glue_vec[i].src_contig_id;
		unsigned int curr_tar_index = fr_contig_glue_vec[i].tar_contig_id;
		
		unsigned int num_overlap_nodes = fr_contig_glue_vec[i].overlap_node_count;
		
		std::vector<unsigned int>& src_white_vec = data[curr_src_index].white_node_vec;
		std::vector<unsigned int>& tar_white_vec = data[curr_tar_index].white_node_vec;
		
		unsigned int l = tar_white_vec.size();
		
		// reverse tar vector
		std::reverse(tar_white_vec.begin(), tar_white_vec.end());
		
		// sanity check, overlapping white nodes should match
		for (unsigned int j=0; j < num_overlap_nodes; j++) {
			ASSERT_WITH_MESSAGE(tar_white_vec[ l - 1 - j ] == src_white_vec[num_overlap_nodes -1 -j], 
								"white_node_vec_vec not agreeable " 
								+ std::to_string(tar_white_vec.size()) 
								+ " " 
								+ std::to_string(src_white_vec.size()));
		}
		
		// push the tail src nodes into tar
		for (unsigned int j=num_overlap_nodes; j < src_white_vec.size(); j++) {
			tar_white_vec.push_back( src_white_vec[j] );
		}
		
		// sanity check, the src contig should already have been cleared in prev loop
		std::string& src_contig = data[curr_src_index].contig;
		assert(src_contig=="");
		
		// empty the white node vec for src
		src_white_vec.clear();
	}
	
	// remove empty contigs that appear as a result of merge
	data.erase(std::remove_if(data.begin(), data.end(), [](contig_data& C){return C.contig=="";}), data.end());
	
} // END: merge_fw_rev_contigs()

void contig_data_str::stitch_contig_pieces(unsigned int k)
{
	// TODO: check the code
	wall_time_wrap W;
	std::cout << "Sorting contigs to stitch pieces ... " << std::flush;
	W.start();
	
	ALGO_NS::sort(data.begin(), data.end(), multi_piece_comparator());

	W.stop();
	std::cout << "Done sorting: " << W << std::endl;
	
	std::cout << "Stitching pieces together ... " << std::flush;
	W.start();
	
	// TODO: depending on timing, this can be parallelized
	unsigned int start_index=0;
	unsigned int end_index=0;
	
	while(true) {
		if ( start_index >= data.size() ) {
			break;
		}
		while( end_index<data.size() ) {
			if ((data[start_index].contig_label == data[end_index].contig_label) and(data[start_index].d == data[end_index].d)) {
				end_index += 1;
			}
			else {
				break;
			}
		}
		// [start_index, end_index) is the required range
		unsigned int contig_part_check=1; // TODO: or 0, depending on convention
		for (unsigned int i=start_index; i < end_index; i++) {
			assert(data[i].contig_part == contig_part_check);
			contig_part_check++;
			
			if (i==start_index) {
				continue;
			}
			std::string& first_piece = data[start_index].contig;
			std::string& prev_piece = data[i-1].contig;
			std::string& curr_piece = data[i].contig;
			assert(prev_piece.substr(prev_piece.size()-k+1, k-1) == curr_piece.substr(0, k-1));
			first_piece.append(curr_piece, k, curr_piece.size() - k + 1);
			curr_piece="";
		}
	}
	
	// remove empty contigs
	data.erase(std::remove_if(data.begin(), data.end(), [](contig_data& C){return C.contig=="";}), data.end());
	
	W.stop();
	std::cout << "Done stitching: " << W << std::endl;
	// TODO: make fr_contig_glue_str accordingly

}


// sort contigs (and related data str) in dec order of size
void contig_data_str::sort_data()
{
	// sort data in descending order of contig length
	ALGO_NS::sort(	data.begin(), 
			data.end(), 
			[](contig_data a, contig_data b)
			{return a.contig.size() > b.contig.size(); }
			);

}

void contig_data_str::write_contigs(unsigned int min_len, std::ofstream& os)
{
	for (unsigned int i=0; i < data.size(); i++) {
		std::string& contig_i = data[i].contig;
		if (contig_i.size() < min_len) {
			continue;
		}
		os << ">Contig_" << i << " size = " << contig_i.size() << std::endl;
		os << contig_i << std::endl;
	}
}

size_t contig_data_str::dynamic_size()
{
	size_t bytes=0;
	for (size_t i=0; i < data.size(); i++) {
		bytes += data[i].size();
	}
	bytes += vecsize(fr_contig_glue_vec);
	return bytes;
}


/***************************** << operators ************************/

std::ostream& operator<<(std::ostream& os, contig_data_str& C_vec)
{
	os << C_vec.data;
	return os;
}

std::ostream& operator<<(std::ostream& os, fr_contig_glue_str& FR) {
	os << "Src: " << FR.src_contig_id 
		<< "\ttar: " << FR.tar_contig_id 
		<< "\tlen: " << FR.overlap_length 
		<< "\tcount: " << FR.overlap_node_count;
	return os;
}

std::ostream& operator<<(std::ostream& os, contig_data& CD) {
	os << "contig len: " << CD.contig.size() << std::endl;
	os << "no_neigh: " << BOOL(CD.no_neigh) << std::endl;
	os << "white node vec: " << CD.white_node_vec << std::endl;
	os << "contig label: " << CD.contig_label << std::endl;
	os << "Direction: " << Contig_Dir_String[CD.d] << std::endl;
	
	return os;
}
