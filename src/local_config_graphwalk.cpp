#General Params
#
############# The graph file (path relative to the output directory)
walk_input.txt
#
############# The log file
log_GraphWalk.txt
#
############# The output file
walk_output.txt
#
############# Print DB graph param (0 if DB graph not to be printed, else any other number)
0
#
############# Print Kmer to Unitig mapping table param (0 if not to be printed, else any other number)
1
#
############# Print Transformed DB constraints param (0 if not to be printed, else any other number)
0
#
############# Print Transformed and merged DB constraints param (0 if not to be printed, else any other number)
1
#
############# Print Final contigs param (0 if not to be printed, else any other number)
1
#
#Walk specific params, DON'T play if you don't know what you are doing
#
############# Max dist const error
3
#
############# Support power param (float)
1
#
############# Walk queue size
250
#
############# low value of walk queue size (value that is considered low enough to call for a clean up)
50
#
############# while cleaning up the queue, the amount of history to be preserved before the curr value
100
#
############# sufficient_score_diff: score diff enough to decide a clear winner. 
0.5
#
############# coverage_fudge. 
0.2
#
############# zero score streak: number of times we continuously allow a zero score while selecting next hop
3
#
############# Maximum number of iterations for walking
100000
#
############# enable_reverse_walking: after producing a contig, do you want to walk in reverse to produce longer contig (1 if yes, 0 if no)
1
#
############# all_paths_dist: dist to which we do all paths in case a decision can't be made on the next hop
40
#
#debug params
#
############# info_after_iterations
1000
