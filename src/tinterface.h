#ifndef TINTER_H
#define TINTER_H

#include "all_classes.h"
#include "misc.h"

// class for mapping Tony's lmer id to my lmer id
class lmer_id_mapping_type {
public:
	std::string lmer_id;
	unsigned int num_lmer_id;
};

// output operator for dist_const_table_type
std::ostream& operator<<(std::ostream& os, lmer_id_mapping_type& limt);

// function to search for a given lmer_id key in the vec
lmer_id_mapping_type const & search_lmer_id(	std::vector<lmer_id_mapping_type> const & map_vec, 
										std::string const & key);

class lmer_entry_type {
public:
	std::string lmer_id;
	std::string lmer;
	unsigned int len_lmer;
};

// output operator for lmer_entry_type
std::ostream& operator<<(std::ostream& os, lmer_entry_type& let);

class lmer_table_type {
public:
	std::vector<lmer_entry_type> Table;
	unsigned int k_;
	std::ofstream& log_file_;
	
	lmer_table_type(unsigned int k, std::ofstream& log_file):k_(k), log_file_(log_file) {};
	
	// read the lmer file and junction nodes file, and put lmer entries in the table
	void read_input(	std::ifstream& infile, 
// 					std::ifstream& junction_nodes_file,
					std::vector<std::string>& glob_jn_file_vec);
	
	// handle lmer entries
	void insert_lmer_entry(std::string& line);
	
	// handle junction node entries
	void insert_lmer_entry_jn(std::string& line);
	
	// search the lmer table for a given lmer_id
	lmer_entry_type& search_table(std::string& search_key);
};

// output operator for lmer_table_type
std::ostream& operator<<(std::ostream& os, lmer_table_type& ltt);


// data structure for kmer to unitig mapping, provided by Tony, might need tuning based on exact file format
class kmer_unitig_mapping_type {
public:
	std::string kmer;
	std::string lmer_id;
	unsigned int pos;
	bool is_on_positive;
	
	// experimental
	unsigned int lmer_length;
	unsigned int db_table_index;
	unsigned int pos_on_strand;
};

// output operator for kmer_unitig_mapping_type
std::ostream& operator<<(std::ostream& os, kmer_unitig_mapping_type& kutt);

// class for reading kmer to unitig mapping, provided by Tony, might need tuning based on exact file format
class kmer_unitig_mapping_table_type {
public:
	// vars
	std::vector<kmer_unitig_mapping_type> KUM_Table;
	std::ofstream& log_file_;
	
	// constructor
	kmer_unitig_mapping_table_type(std::ofstream& log_file): log_file_(log_file) {};
	
	// read the input mapping file and junction nodes file and create the KUM_Table
	void read_mapping_file(std::ifstream& kmer_unitig_map_file, 
						std::ifstream& junction_nodes_file, 
						std::vector<std::string>& glob_jn_file_vec,
						unsigned int& kmer_length);
	
	// break one line of input file into various fields, and insert entry in KUM_Table
	void insert_mapping_entry(std::string& line);
	
	// break one line of junction_nodes_file file into various fields, and insert entry in KUM_Table
	void insert_mapping_entry_jn(std::string& line);
	
	// search for a given cannonical kmer in the table
	kmer_unitig_mapping_type& search_table(std::string& search_kmer);
	
	// put length, index, pos on strand, currently experimental, not in use
	void put_experimental_stuff(std::vector< lmer_id_mapping_type >& lmer_id_mapping, std::vector< DB_Node_Type >& DB_Node_Table, unsigned int kmer_length);
};

// output operator for kmer_unitig_mapping_table_type
std::ostream& operator<<(std::ostream& os, kmer_unitig_mapping_table_type& kumtt);


class dist_const_type {
public:
	std::string src;
	std::string tar;
	int distance;
	int error;
	bool s_dir;
	bool e_dir;
	unsigned int freq;
	bool is_intra_read;
};

// comparison operators for dist_const_type
bool operator< (const dist_const_type& d1, const dist_const_type& d2);
bool operator== (const dist_const_type& d1, const dist_const_type& d2);

// output operator for dist_const_type
std::ostream& operator<<(std::ostream& os, dist_const_type& kdc);

class dist_const_table_type {
public:
	std::vector<dist_const_type> DC_Table;
	std::vector<unsigned long int> insert_est_data;
	// used for printing contig lens for 
	std::vector<unsigned long int> contig_len_data;
	std::vector<std::pair<unsigned long int, unsigned long int> > read_est_data;
	Params* P_;
	unsigned int insert_size_init = 100000;
	unsigned int k_;
	kmer_unitig_mapping_table_type& KUM_Table_;
	lmer_table_type& LTT_;
	std::ofstream& log_file_;
	std::ofstream& insert_hist_file_;
	std::ofstream& contig_len_hist_file_;
	
	// estimated insert size mean and deviation
	unsigned int est_ins_size_mean;
	unsigned int est_ins_size_dev;
	
	std::pair<unsigned int, unsigned int> est_read_size_mean;
	
	// constructor
	dist_const_table_type(	Params* P, 
						kmer_unitig_mapping_table_type& KUM_Table, 
						lmer_table_type& LTT,
						outfile_str& ostr):	P_(P), 
											KUM_Table_(KUM_Table), 
											LTT_(LTT),
											log_file_(ostr.log_file),
											insert_hist_file_(ostr.insert_size_hist_file),
											contig_len_hist_file_(ostr.contig_len_hist_file) {};
	
	// reads input files and creates the initial KDC table with distance constraints.
	void read_input_fasta(	std::ifstream& infile1, 
						std::ifstream& infile2, 
						std::ifstream& trunc_file);
	
	// get the next read from infile, return false if no read fetched, true otherwise
	bool get_read(std::ifstream& infile, std::string& read);
	
	// get the next read from file vector, return false if no read fetched, true otherwise
	bool get_read(std::vector<std::string>& vec, std::string& read, unsigned long int& index);
	
	// split an entry of read truncation into 4 parts
	void split_truncation_entry(std::vector< int >& trunc_pos_vec, std::string& line);
	
	// given a read, insert the appropriate intra read dist const in the table (kmer to kmer)
	void insert_intra_read(std::string& read, int pos1, int pos2, long unsigned int& count);
	
	// given a pair of reads, insert the appropriate inter read dist const in the table (kmer to kmer)
	void insert_inter_read(std::string& read1, std::string& read2, int pos1, int pos2, long unsigned int & count);
	
	// if the pair lies on the same contig, insert entry for pair size estimation, return true if insertion success
	bool insert_pair_estimation(std::string& src_kmer, 
							unsigned int src_pos, 
							bool src_positivity, 
							std::string& tar_kmer, 
							unsigned int tar_pos, 
							bool tar_positivity,
							unsigned int r1_size, 
							unsigned int r2_size);
	
	void estimate_insert_size();
	
	void print_insert_size();
	
	void print_contig_size();
	
	void estimate_read_size();
	
	// push the estimated insert size and dev into relevant DC table entries (KNOB)
	void update_inter_read_data();
	
	// return the first entry for a given key search_src
	dist_const_type& search_table(std::string& search_src);
	
	// in the DC_Table, transform constraints from kmer-kmer to lmer-lmer
	void kk_to_ll_transformation(	kmer_unitig_mapping_table_type& KUM_Table, 
								lmer_table_type& Lmer_Table);
	
	// accummulate same dist const and add up the frequency
	void accummulate_dist_const();
	
	// KNOB
	void update_maxdc_error();
};

// output operator for dist_const_table_type
std::ostream& operator<<(std::ostream& os, dist_const_table_type& dctt);

#endif // TINTER_H
