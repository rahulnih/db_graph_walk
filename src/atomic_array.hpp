/**
 * Quick and dirty dynamically allocated array of atomics.  does not allow resizing.
 * 
 */

#ifndef ATOMIC_ARRAY_H
#define ATOMIC_ARRAY_H

#include <atomic>
#include <mutex>
#include <typeinfo>
#include <xmmintrin.h>

/*Wrapper struct to create vector of atomic objects  - in sufficient because vector is not a concurrent structure.
 * copied from : https://stackoverflow.com/questions/13193484/how-to-declare-a-vector-of-atomic-in-c
 * 
 * INSTEAD CREATE An ARRAY OF ATOMICS.  no resizing capability
 */

namespace atomic 
{

template <typename T>
class atomic_array
{
    protected:

        ::std::atomic<T>* m_data;
        ::std::atomic<size_t> m_capacity;

    public:
        // ====================================================================================
        // these do not need to be protected by mutex.
        // constructors should all be called sequentially.
        atomic_array() : m_data(nullptr), m_capacity(0UL) {};
        atomic_array(size_t capacity) :  m_capacity(capacity) {
            m_data = static_cast<::std::atomic<T>*>(_mm_malloc(capacity * sizeof(::std::atomic<T>), 64));
        }
        atomic_array(size_t capacity, T const & val) : atomic_array(capacity) {
            for (size_t i = 0; i < capacity; ++i) {
				m_data[i].store(val);
			}
        }

		// ====================================================================================
		// these should be done by a single thread to avoid race condition or use_after_dtor.
        atomic_array(atomic_array const & other) : atomic_array(other.m_capacity.load()) {
			// just copy.
			memmove(m_data, other.m_data, m_capacity.load() * sizeof(T));
        }
        atomic_array(atomic_array && other) : m_capacity(other.m_capacity.load()) {
			m_data = other.m_data;
			other.m_data = nullptr;
        }

        atomic_array& operator=(atomic_array const & other) {
			if (m_data != nullptr) _mm_free(m_data);
			size_t capacity = other.m_capacity.load();
			m_data = static_cast<::std::atomic<T>*>(_mm_malloc(capacity * sizeof(::std::atomic<T>), 64));
			memmove(m_data, other.m_data, capacity * sizeof(T));
			m_capacity.store(capacity);

			return *this;
        }
        atomic_array& operator=(atomic_array && other) {
			::std::swap(m_data, other.m_data);
			size_t cap = m_capacity.exchange(other.m_capacity.load());
			other.m_capacity.store(cap);

			return *this;
        }

        // TODO: ensure that destructor is called by a single thread and all references to it are done before that.  use omp critical?
        ~atomic_array() {
			if (m_data != nullptr) _mm_free(m_data);
			m_data = nullptr;
			m_capacity = 0;
        }

		// ====================================================================================
		// accessors
		inline ::std::atomic<T>& operator[](size_t const & id) { return m_data[id]; }
		inline ::std::atomic<T> const & operator[](size_t const & id) const { return m_data[id]; }
	
		inline size_t size() const { return m_capacity.load(); }

		size_t mem_size() const { return size() * sizeof(std::atomic<T>) + sizeof(m_data) + sizeof(m_capacity); }

};

} // namespace atomic

#endif // ATOMIC_ARRAY_H
