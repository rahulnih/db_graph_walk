#ifndef DBGRAPH_H
#define DBGRAPH_H

#include "all_classes.h"
#include "misc.h"
#include "tinterface.h"

// an entry representing a distance constraint
// typedef struct
class Dist_Type{
public:
	
	// Vars
	
	std::string tar_kmer;
	unsigned int lmer_id; // std::numeric_limits<unsigned int>::max() means not assigned
	// expected distance of the tar_kmer
	// int because dist can be negative
	int distance; 
	// max error to be tolerated
	int error;
	
	// 3 LSBs represent s_ed and 4th LSB represents s_dir
	char start;
	
	// 3 LSBs represent e_ed and 4th LSB represents e_dir
	char end;
	unsigned int freq;
	
	// functions
	
	// function print the elements
	void print(std::ostream& os=std::cout) const;
	
	// tells if the dist const is intra read
	inline bool is_intra_read() const {return (error==0);}
	
	void read_from_file(std::ifstream& is);
	void write_to_file(std::ofstream& os) const;
	
	size_t size() const {return sizeof(Dist_Type);}
};

std::ostream& operator<<(std::ostream& os, Dist_Type const & D);

// an entry representing an lmer entry
// typedef struct 
class DB_Node_Type{
	
	// vars
	
private:
	// TODO: Change lmer to pointer lmer
	std::string lmer; // right now, lmer id will be the index in the DB_Node_Table
public:
	int len_lmer;
	// true if node has no edges whatsoever, used for graph partitioning
	bool is_singleton;
	mutable unsigned int visit_count=0;
	std::vector<unsigned int> prev_neigh;
	std::vector<unsigned int> next_neigh;
	
	// freq can be changed to a float if worth it
	unsigned long int freq;
	
	// range of occurance frequencies of various kmers in the lmer
	std::vector<unsigned long int> freq_range;
	std::vector<Dist_Type> Dist_Const_List;
	
	// bounds on how many times this node has to be visited
	// vector size=2: lower and upper bound
// 	std::vector<unsigned int> visit_bounds;
	
	// functions
	
	// function print the elements
	void print(std::ostream& os=std::cout) const;
	
// 	DB_Node_Type() {lmer = new std::string("");}
// 	DB_Node_Type(std::string& S) {lmer = &S;};
// 	std::string& get_lmer() { return (*lmer).substr(0, len_lmer);}
// 	void put_lmer(std::string& S) {lmer = &S;}
// 	unsigned int lmer_size() {return (*lmer).size();}

	DB_Node_Type() {lmer = "";}
	DB_Node_Type(std::string& S) {lmer = S;};
	std::string& get_lmer() { return lmer;}
	std::string const & get_lmer() const { return lmer;}
	void put_lmer(std::string& S) {lmer = S;}
	unsigned int lmer_size() const {return lmer.size();}
	
	void read_from_file(std::ifstream& is);
	void write_to_file(std::ofstream& os) const ;
	
	size_t dynamic_size() const ;
	size_t size() const  {return sizeof(DB_Node_Type) + dynamic_size();}

} ;

std::ostream& operator<<(std::ostream& os, DB_Node_Type const & D);

// used in transform distance constraints
struct lmer_comparator {
	inline bool operator() (const Dist_Type& d1, const Dist_Type& d2) {
		if (d1.lmer_id not_eq d2.lmer_id) {
			return d1.lmer_id < d2.lmer_id;
		}
		if (d1.start not_eq d2.start) {
			return d1.start < d2.start;
		}
		if (d1.end not_eq d2.end) {
			return d1.end < d2.end;
		}
		if (d1.distance not_eq d2.distance) {
			return d1.distance < d2.distance;
		}
		return d1.error < d2.error;
	}
};

class DB_Node_Table_Type {
	
		// vars
	
	private:
		std::vector<DB_Node_Type> DB_Node_Table;
		unsigned int kmer_length;
		std::ofstream& log_file_;
	public:
		unsigned int singleton_count=0;
		unsigned int num_edges=0;
		
		// functions
		
		DB_Node_Table_Type(std::ofstream& log_file):log_file_(log_file){};
		
		// resize the vector
		void resize(unsigned int s) {
			DB_Node_Table.resize(s);
		}
		
		// push an entry into the table
		void TablePush (DB_Node_Type const & a) {
			DB_Node_Table.push_back(a);
		}
		void TablePush (DB_Node_Type && a) {
			DB_Node_Table.push_back(a);
		}
		
		std::size_t size() const {
			return DB_Node_Table.size();
		}
		
		DB_Node_Type const & ElementAt(std::size_t pos) const {
			return DB_Node_Table.at(pos);
		}
		DB_Node_Type& ElementAt(std::size_t pos) {
			return DB_Node_Table.at(pos);
		}


		// read the db graph from an input stream
		void read_graph(Params* P, std::ifstream& graph_file);
		
		/*************************** tinterface functions*************************/
		
		// build graph from Tony's interface, file1...4 are named as convention. (main function) 
		void build_graph_from_tinterface(Params* P, tinfile_str& TinS, outfile_str& ostr, kmer_unitig_mapping_table_type& KUMTT);
		
		// supporting functions (function to read graph files provided by Tony)
		
		unsigned int read_unitig_nodes_file(std::ifstream& infile, dist_const_table_type& DCTT, lmer_table_type& LTT);
		unsigned int read_junction_nodes_file(/*std::ifstream& infile,*/ dist_const_table_type& DCTT, lmer_table_type& LTT, std::vector<std::string>& glob_jn_file_vec);
		
		// temporary function to create adj list of the graph
		void create_kahip_input(Params* P, 
							kmer_unitig_mapping_table_type& KUMTT_, 
							std::vector< std::vector< std::string > >& adj_list, 
							std::vector< std::string >& label_ref, 
							unsigned int& num_edges);
		
		// temporary function to write kahip input
		void write_kahip_input(std::vector< std::vector< std::string > >& adj_list, 
							std::vector<std::string>& label_ref, 
							unsigned int& num_edges);
		
		/************************* END: tinterface functions***********************/
		
		// Implemented in walk.cpp due to dependencies
		// In the list_nodes, and transform the dist const from target kmer to target lmer
		void Transform_dist_const(KU_Table_Type & Table, unsigned int k);
		
		// Given a unitig node, find the prev or the next based on if the side variable is 0 or 1. 
		unsigned int FindUnitigEdge (DB_Node_Type const & Node, unsigned int side) const;
		
		// check if a given node is a unitig or not
		bool isUnitig(DB_Node_Type const & Node) const;
		
		// given a DB node and indices of two distance constraints in it's structure, check if they are two occurances of the same dist constraint
		bool check_identical_constraints(DB_Node_Type const & D, unsigned int index1, unsigned int index2) const;
		
		// go through the list of nodes and merge identical constraints within each node
		void merge_distance_constraints();
		
		// Implemented in walk.cpp due to dependencies
		// set visit bounds for each db node
// 		void set_visit_bounds(KU_Table_Type& Table);
		
		// go through the graph and find out the longest dist const length
		unsigned int calc_longest_dist_const() const;
		
		unsigned int calc_longest_lmer_len() const;
		
		void update_sing_n_edges(DB_Node_Type & Node);
		
		// print the graph
		void print(std::ostream& os=std::cout) const ; 
		
		void read_from_file(std::ifstream& is);
		void write_to_file(std::ofstream& os) const ;
	
		std::size_t dynamic_size() const ;
		std::size_t memsize() const {return sizeof(DB_Node_Table) + dynamic_size();}
};


#endif // DBGRAPH_H
