#include "misc.h"
// TODO: following two included for compare_kut_kumtt only, if not needed, remove
#include "ku_table.h"
#include "tinterface.h"
// #include "db_graph.h"

std::string alphabet = "ACGT";
std::vector<std::string> Walk_Type_String = {"SER", "PAR", "MUL"};
std::vector<std::string> Evidence_Scaledown_String = {"LIN", "GAUSS"};
std::ofstream nullstream("/dev/null");
std::vector<std::string> Contig_Dir_String = {"FWD", "REV"};

// split a string str based on delimiter and return a vector of split elements
std::vector<std::string> split(std::string str, char delimiter) {
	std::vector<std::string> internal;
	std::stringstream ss(str); // Turn the string into a stream.
	std::string tok;
	
	while(getline(ss, tok, delimiter)) {
		internal.push_back(tok);
	}
	
	return internal;
}

// open output files
void outfile_str::open_output_files(Params* P)
{
	// open output file
	out_file.open((P->output_file).c_str());
	if (not out_file.is_open()) {
		std::cerr << "Unable to open output file" << std::endl;
		exit(0);
	}
	
	// open log file
	log_file.open((P->log_file + ".txt").c_str());
	if (not log_file.is_open()) {
		std::cerr << "Unable to open log file" << std::endl;
		exit(0);
	}
	
	// open stat file
	stat_file.open((P->stat_file).c_str());
	if (not stat_file.is_open()) {
		std::cerr << "Unable to open stat file" << std::endl;
		exit(0);
	}

	// open insert histogram file
	insert_size_hist_file.open("insert_size.hist");
	if (not insert_size_hist_file.is_open()) {
		std::cerr << "Unable to open insert size histogram file" << std::endl;
		exit(0);
	}
	
	// open contig len file
	contig_len_hist_file.open("contig_lens_ins.hist");
	if (not contig_len_hist_file.is_open()) {
		std::cerr << "Unable to open contig len file" << std::endl;
		exit(0);
	}

	// open start node stats file
	if (P->print_start_node_stats) {
		start_node_stats_file.open("start_node.stats");
		if (not start_node_stats_file.is_open()) {
			std::cerr << "Unable to open start node stats file" << std::endl;
			exit(0);
		}
	}
	
	// open score diff file
	if (P->print_score_diff_stats) {
		score_diff_hist_file.open("score_diff.hist");
		if (not score_diff_hist_file.is_open()) {
			std::cerr << "Unable to open score diff histogram file" << std::endl;
			exit(0);
		}
	}
	
	// open map stat file
	if (P->print_mini_all_paths_stats) {
		map_stat_file.open("map_stat.hist");
		if (not map_stat_file.is_open()) {
			std::cerr << "Unable to open map stat file" << std::endl;
			exit(0);
		}
	}
	
	// open warnings file
	warnings.open("warnings.txt");
	if (not warnings.is_open()) {
		std::cerr << "Unable to open warnings file" << std::endl;
		exit(0);
	}
	
	// open bubbles file
	bub_file.open("bubbles.txt");
	if (not bub_file.is_open()) {
		std::cerr << "Unable to open bubbles file" << std::endl;
		exit(0);
	}
}


// checks if the strand is lex smaller than it's reverse complement
bool is_positive(std::string & strand) {
	std::string s = strand;
	reverse_complement(s);
	if (strand.compare(s)<=0) {
		return true;
	}
	else {
		return false;
	}
}

// given a strand (kmer or lmer), return the lesser of it and it's reverse complement, and true/false accordingly
bool MakeNegative(std::string& strand) {
	std::string s = strand;
	reverse_complement(s);
	if (strand.compare(s)>=0) {
		return true;
	}
	else {
		strand=s;
		return false;
	}
}

// given a strand (kmer or lmer), return the lesser of it and it's reverse complement, and true/false accordingly
bool MakePositive(std::string& strand) {
	std::string s = strand;
	reverse_complement(s);
	if (strand.compare(s)<=0) {
		return true;
	}
	else {
		strand=s;
		return false;
	}
}


// start implicitly from 64, goes to 95
::std::string complement(".T.G...C......N.....A............t.g...c......n.....a..........." );

void reverse_complement(std::string& s) {
//TONY
	bool odd = ((s.size() % 2) == 1);
	size_t max = s.size();
	char left = 0, right = 0;

	for (size_t i=0; i < max / 2; ++i) {
		left = complement[(s[i] - 64)];
		right = complement[(s[max - i - 1] - 64)];

		if (left == '.') {
			std::cerr << "Error in reverse_complement: Unknown base s[" << i << "]=" 
					<< s[i] << " in the strand" 
					<< std::endl;
			exit(0);
		} 
		else if (right == '.') {
			std::cerr << "Error in reverse_complement: Unknown base s[" << (max - i - 1) << "]=" 
			<< s[max - i - 1] << " in the strand" 
			<< std::endl;
		exit(0);
		}

		s[i] = right;
		s[max - i - 1] = left;
	}
	if (odd) {
		left = complement[(s[max / 2] - 64)];

		if (left == '.') {
			std::cerr << "Error in reverse_complement: Unknown base s[" << (max / 2) << "]=" 
			<< s[max / 2] << " in the strand" 
			<< std::endl;
			exit(0);
		}
		s[max / 2] = left;
	}
}

std::string bytes2h(size_t bytes)
{
	std::string mem_string;
	std::stringstream ss;
	
	if (bytes > 1024*1024*1024) {
		ss << std::fixed << std::setprecision(2) << ((float)bytes)/(1024*1024*1024) << " GB";
	}
	else if (bytes > 1024*1024) {
		ss << std::fixed << std::setprecision(2) << ((float)bytes)/(1024*1024) << " MB";
	}
	else if (bytes > 1024) {
		ss << std::fixed << std::setprecision(2) << ((float)bytes)/(1024) << " KB";
	}
	else {
		ss << bytes << " Bytes";
	}
	mem_string = ss.str();
	return mem_string;
}

std::string num2h(long int num)
{
	std::stringstream ss;
	if (num<0) {
		ss << "-";
		num = abs(num);
	}
	
	if (num > 1000*1000*1000) {
		ss << std::fixed << std::setprecision(2) << ((float)num)/(1000*1000*1000) << " G";
	}
	else if (num > 1000*1000) {
		ss << std::fixed << std::setprecision(2) << ((float)num)/(1000*1000) << " M";
	}
	else if (num > 1000) {
		ss << std::fixed << std::setprecision(2) << ((float)num)/(1000) << " K";
	}
	else {
		ss << num;
	}
	
	return ss.str();
}


void Params::getParams(std::ifstream& is)
{
	// start reading input file;
	std::string line;
	std::vector<std::string> p;
	while(is.good()) {
		std::getline(is, line);
		if (line[0]=='#') {
			continue;
		}
		p.push_back(line);
	}
	
	// set up params from vector p
	
	unsigned int i=0;
	log_file = p[i++];
	
	output_file = p[i++];
	
	stat_file = p[i++];
	
	line=p[i++];
	print_contigs=true;
	if (atoi(line.c_str())==0) {
		print_contigs=false;
	}
	
	min_contig_len = atoi(p[i++].c_str());
	
	line=p[i++];
	print_kahip_graph=true;
	if (atoi(line.c_str())==0) {
		print_kahip_graph=false;
	}
	
	// reading params
	
	line=p[i++];
	read_kgp=false;
	if (atoi(line.c_str())==1) {
		read_kgp=true;
	}
	
	line=p[i++];
	write_kgp=false;
	if (atoi(line.c_str())==1) {
		write_kgp=true;
	}
	
	// parallelization params
	
	std::string walk_type_param = p[i++];
	if (walk_type_param=="SER") {
		wt = SER;
	}
	else if (walk_type_param=="PAR") {
		wt = PAR;
	}
	else if (walk_type_param=="MUL") {
		wt = MUL;
	}
	else {
		std::cerr << "Invalid walk type parameter, check config file" << std::endl;
		exit(0);
	}
	
	partition_file = p[i++];
	
	num_threads = atoi(p[i++].c_str());
	
	// walk params
	
	support_power = atof(p[i++].c_str());
	
	intra_support_scale = atof(p[i++].c_str());
	
	sufficient_score_diff_abs = atof(p[i++].c_str());
	
	sufficient_score_diff_norm = atof(p[i++].c_str());
	
	coverage_fudge = atof(p[i++].c_str());
	
	max_iterations = atol(p[i++].c_str());
	
	line=p[i++];
	enabRevWalk=true;
	if (atoi(line.c_str())==0) {
		enabRevWalk=false;
	}
	
	line=p[i++];
	enforce_neg_const=true;
	if (atoi(line.c_str())==0) {
		enforce_neg_const=false;
	}
	
	all_paths_dist = atoi(p[i++].c_str());
	
	all_paths_size = atoi(p[i++].c_str());
	
	max_bubble_diff = atoi(p[i++].c_str());
	
	min_magic_ratio = atof(p[i++].c_str());
	
	max_magic_ratio = atof(p[i++].c_str());
	
	error_scaling = atof(p[i++].c_str());
	
	intra_error = atoi(p[i++].c_str());
	
	min_dist_const = atoi(p[i++].c_str());
	
	min_start_node_len = atoi(p[i++].c_str());
	
	int ev_val=atoi(p[i++].c_str());
	if (ev_val==0) {
		evidence_scaledown_fn = LIN;
	}
	else if (ev_val==1) {
		evidence_scaledown_fn = GAUSS;
	}
	
	line=p[i++];
	doScaff=false;
	if (atoi(line.c_str())==1) {
		doScaff=true;
	}
	
	// debug params
	
	info_after_iterations = atoi(p[i++].c_str());
	
	walk_info_level = atoi(p[i++].c_str());
	
	base_wall_time = atoi(p[i++].c_str());
	
	line=p[i++];
	print_db_graph=true;
	if (atoi(line.c_str())==0) {
		print_db_graph=false;
	}
	
	line=p[i++];
	print_KU_table=true;
	if (atoi(line.c_str())==0) {
		print_KU_table=false;
	}
	
	line=p[i++];
	print_transformed_constraints=true;
	if (atoi(line.c_str())==0) {
		print_transformed_constraints=false;
	}
	
	line=p[i++];
	print_transformed_and_merged_constraints=true;
	if (atoi(line.c_str())==0) {
		print_transformed_and_merged_constraints=false;
	}
	
	line=p[i++];
	print_start_node_stats=true;
	if (atoi(line.c_str())==0) {
		print_start_node_stats=false;
	}
	
	line=p[i++];
	print_score_diff_stats=true;
	if (atoi(line.c_str())==0) {
		print_score_diff_stats=false;
	}
	
	line=p[i++];
	print_mini_all_paths_stats=true;
	if (atoi(line.c_str())==0) {
		print_mini_all_paths_stats=false;
	}
	
	line=p[i++];
	print_mem_map=true;
	if (atoi(line.c_str())==0) {
		print_mem_map=false;
	}
	
	print_mem_map_freq = atoi(p[i++].c_str());
	
	assert(i==p.size()-1);
	
} // END: Params::getParams(std::ifstream& is)

void Params::print(std::ostream& os /*= std::cout*/) {
	os << "Log file: " << log_file << std::endl;
	os << "Output file: " << output_file << std::endl;
	os << "Statistics file: " << stat_file << std::endl;
	
	os << "print_contigs: " << BOOL(print_contigs) << std:: endl;
	os << "min_contig_len: " << min_contig_len << std::endl;
	os << "print_kahip_graph: " << BOOL(print_kahip_graph) << std::endl;
	
	os << "read_kgp: " << BOOL(read_kgp) << std:: endl;
	os << "write_kgp: " << BOOL(write_kgp) << std:: endl;
	
	os << "walk_type: " <<Walk_Type_String[wt] << std::endl;
	os << "partition file: " << partition_file << std::endl;
	os << "num_threads: " << num_threads << std::endl;

	os << "support_power: " << support_power << std::endl;
	os << "intra_support_scale: " << intra_support_scale << std::endl;
	os << "sufficient_score_diff_abs: " <<  sufficient_score_diff_abs << std::endl;
	os << "sufficient_score_diff_norm: " << sufficient_score_diff_norm << std::endl;
	os << "coverage_fudge: " << coverage_fudge << std::endl;
	os << "max_iterations: " << max_iterations << std::endl;
	os << "enable_reverse_walking: " << BOOL(enabRevWalk) << std::endl;
	os << "enforce_neg_const: " << BOOL(enforce_neg_const) << std::endl;
	os << "all_paths_dist: " << all_paths_dist << std::endl;
	os << "all_paths_size: " << all_paths_size << std::endl;
	os << "max_bubble_diff: " << max_bubble_diff << std::endl;
	os << "min_magic_ratio: " << min_magic_ratio << std::endl;
	os << "max_magic_ratio: " << max_magic_ratio << std::endl;
	os << "error_scaling: " << error_scaling << std::endl;
	os << "intra_error: " << intra_error << std::endl;
	os << "min_dist_const: " << min_dist_const << std::endl;
	os << "min_start_node_len: " << min_start_node_len << std::endl;
	os << "Evidence scaledown type: " << Evidence_Scaledown_String[evidence_scaledown_fn] << std::endl;
	os << "Scaffolding: " << BOOL(doScaff) << std::endl;
	
	os << "info_after_iterations: " << info_after_iterations << std::endl;
	os << "walk_info_level: " << walk_info_level << std::endl;
	os << "base_wall_time: " << base_wall_time << "ms" << std::endl;
	os << "print_db_graph: " << BOOL(print_db_graph) << std:: endl;
	os << "print_KU_table: " << BOOL(print_KU_table) << std:: endl;
	os << "print_transformed_constraints: " << BOOL(print_transformed_constraints) << std:: endl;
	os << "print_transformed_and_merged_constraints: " 
		<< BOOL(print_transformed_and_merged_constraints) << std:: endl;
	os << "print_start_node_stats: " << BOOL(print_start_node_stats) << std:: endl;
	os << "print_score_diff_stats: " << BOOL(print_score_diff_stats) << std:: endl;
	os << "print_mini_all_paths_stats: " << BOOL(print_mini_all_paths_stats) << std:: endl;
	os << "print_mem_map: " << BOOL(print_mem_map) << std:: endl;
	os << "print_mem_map_freq: " << print_mem_map_freq << std::endl;

	return;
	
} // END: Params::print(std::ostream& os /*= std::cout*/)

void Params::print_graph_params(std::ostream& os)
{
	// TODO: write and read std dev instead
	os << max_dist_error << std::endl;
	os << k << std::endl;
	os << read1_length << std::endl;
	os << read2_length << std::endl;
	os << pair_distance << std::endl;
	os << insert_size << std::endl;
}

void Params::read_graph_params(std::istream& is)
{
	std::string line;
	
	std::getline(is, line);
	max_dist_error = atoi(line.c_str());

	std::getline(is, line);
	k = atoi(line.c_str());

	std::getline(is, line);
	read1_length = atoi(line.c_str());

	std::getline(is, line);
	read2_length = atoi(line.c_str());

	std::getline(is, line);
	pair_distance = atoi(line.c_str());

	std::getline(is, line);
	insert_size = atoi(line.c_str());
}


void tinfile_str::read_tinfiles(std::string input_id)
{
	kum_file.open((input_id + "_chain.components").c_str());
	if (not kum_file.good()) {
		std::cerr << "Unable to open chain components file" << std::endl;
		exit(0);
	}

	unitig_nodes_file.open((input_id + "_chain.edges").c_str());
	if (not unitig_nodes_file.good()) {
		std::cerr << "Unable to open chain edges file" << std::endl;
		exit(0);
	}

	lmer_sequences_file.open((input_id + "_chain.fasta").c_str());
	if (not lmer_sequences_file.good()) {
		std::cerr << "Unable to open chain fasta file" << std::endl;
		exit(0);
	}

	junction_nodes_file.open((input_id + "_branch.edges").c_str());
	if (not junction_nodes_file.good()) {
		std::cerr << "Unable to open branch edges file" << std::endl;
		exit(0);
	}

	read_truncation_file.open((input_id + "_trunc.edges").c_str());
	if (not read_truncation_file.good()) {
		std::cerr << "Unable to open truncation file" << std::endl;
		exit(0);
	}

	read_file1.open((input_id + "_1.fa").c_str());
	if (not read_file1.good()) {
		std::cerr << "Unable to open read file 1" << std::endl;
		exit(0);
	}
	
	read_file2.open((input_id + "_2.fa").c_str());
	if (not read_file2.good()) {
		std::cerr << "Unable to open read file 2" << std::endl;
		exit(0);
	}
}


// time_str constructor
time_str::time_str(Params* P)
{
	P_ = P;
	t_threads.resize(P_->num_threads);
}


unsigned int reverse_alphabet_index(char x) {
	switch(x) {
		case 'A': return 0;
		case 'C': return 1;
		case 'G': return 2;
		case 'T': return 3;
		default: return 4;
	}
}

// check if the position mid is in between start and end in a queue
bool check_mid(unsigned int start, unsigned int mid, unsigned int end)
{
	if (start <= end) {
		if (mid >= start and mid <= end) {
			return true;
		}
		else {
			return false;
		}
	}
	else {
		if (mid >= start or mid <= end) {
			return true;
		}
		else {
			return false;
		}
	}
}

void calc_lp_stats(	std::vector< lmer_partition_map >& LP_vec, 
				unsigned int num_par, 
				std::vector<std::size_t>& par_size)
{
	for (std::size_t i=0; i < LP_vec.size(); i++) {
		unsigned int p = LP_vec[i].partition_id;
		assert(p < num_par);
		par_size[p]++;
	}
}

void print_lp_stats(std::vector< std::size_t >& par_size, std::ofstream& out_file)
{
	out_file << "\nPartition sizes: " << std::endl;
	box_print_vec(par_size, 4, out_file);
	
	std::pair<std::vector<std::size_t>::iterator, std::vector<std::size_t>::iterator> mm_result = std::minmax_element(par_size.begin(), par_size.end());
	double bal = double(*(mm_result.second))/double(*(mm_result.first));
	out_file 	<< "min: " << *(mm_result.first) 
			<< ", max: " << *(mm_result.second) 
			<< ", bal: " << bal 
			<< std::endl;

}

// output operator for printing lmer partition map
std::ostream& operator<<(std::ostream& os, lmer_partition_map& L) {
	os << L.lmer_id << "\t" << L.index << "\t" << L.partition_id;
	return os;
}

// output operator for printing time
std::ostream& operator<<(std::ostream& os, time_str& T) {
	
	os << "\nStage wise run time: " << std::endl;
	
	os << std::fixed << std::setprecision(3);
	os << "\t\t|| Wall time (sec)\t|| CPU time (sec)" << std::endl;
	
	os 	<< "Prewalk\t||\t\t" 
		<< float(T.t_prewalk.count())/1000 
		<< "\t||\t" << (float)T.c_prewalk/CLOCKS_PER_SEC 
		<< std::endl;
		
	os 	<< "Walk\t\t||\t\t" 
		<< float(T.t_walk.count())/1000 
		<< "\t||\t" << (float)T.c_walk/CLOCKS_PER_SEC 
		<< std::endl;
		
	os 	<< "Postwalk\t||\t\t" 
		<< float(T.t_postwalk.count())/1000 
		<< "\t||\t" << (float)T.c_postwalk/CLOCKS_PER_SEC 
		<< std::endl;
	
	if (T.P_->wt == PAR) {
		os << "\nPer thread run time: " << std::endl;
		
		for (unsigned int i=0; i < T.t_threads.size(); i++) {
			os << float((T.t_threads[i]).count())/1000 << "\t";
		}
		os << std::endl;
	}
	return os;
}

// output operator for printing stats
std::ostream& operator<<(std::ostream& os, stat_str& S)
{
	os << "Num nodes: " << S.num_nodes << std::endl;
	os << "Num edges: " << S.num_edges << std::endl;
	os << "Num singletons: " << S.num_singletons << std::endl;
	
	return os;
}


// start the wall clock
void wall_time_wrap::start() {
	t_start=std::chrono::high_resolution_clock::now();
}

// stop the wall clock
void wall_time_wrap::stop() {
	t_end = std::chrono::high_resolution_clock::now();
	t_elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(t_end - t_start);
}

// convert the ticks into a time string
std::string /*wall_time_wrap::*/convert_t2s(std::chrono::milliseconds msec) {
	
	unsigned long int ticks = msec.count();
	
	unsigned long int dys = (ticks/(24*3600*1000));
	ticks = ticks - dys*24*3600*1000;
	assert(ticks < 24*3600*1000);
	
	unsigned long int hrs = (ticks/(3600*1000));
	ticks = ticks - hrs*3600*1000;
	assert(ticks < 3600*1000);
	
	unsigned long int mins = ticks/(60*1000);
	ticks = ticks - mins*60*1000;
	assert(ticks < 60*1000);
	
	unsigned long int secs = ticks/1000;
	ticks = ticks - secs*1000;
	assert(ticks<1000);
	
	unsigned long int msecs = ticks;
	
	std::string time_str="";
	if (dys not_eq 0) {
		time_str = time_str + std::to_string(dys) + "d ";
	}
	if (hrs not_eq 0) {
		time_str = time_str + std::to_string(hrs) + "h ";
	}
	if (mins not_eq 0) {
		time_str = time_str + std::to_string(mins) + "m";
	}
	if (secs not_eq 0) {
		if (dys==0) {
			if (mins not_eq 0) {
				time_str = time_str + " ";
			}
			time_str = time_str + std::to_string(secs) + "s";
		}
	}
	if (dys==0 and hrs==0 and mins==0) {
		if (secs not_eq 0) {
			time_str = time_str + " ";
		}
		time_str = time_str + std::to_string(msecs) + "ms";
	}
	return time_str;
}

void wall_time_wrap::reset() {
	// TODO: fill if needed
}

// print the time elapsed string
std::ostream& operator<<(std::ostream& os, wall_time_wrap& W)
{
	std::string T = convert_t2s(W.t_elapsed);
	os << T;
	return os;
}

bool operator<(const wall_time_wrap& w1, const wall_time_wrap& w2)
{
	return w1.t_elapsed < w2.t_elapsed;
}

bool operator>(const wall_time_wrap& w1, const wall_time_wrap& w2)
{
	return w1.t_elapsed > w2.t_elapsed;
}

bool operator>=(const wall_time_wrap& w1, const wall_time_wrap& w2)
{
	return w1.t_elapsed >= w2.t_elapsed;
}

void conditional_time_print(bool cond, wall_time_wrap W, std::ofstream& os, std::string fn_name)
{
	if (cond and W >= W_base) {
		os << fn_name << ": " << W << std::endl;
	}
	return;
}


void read_file_to_vec(std::ifstream& infile, std::vector< std::string >& vec, unsigned long int max_iter)
{
	std::string line;
	for (unsigned long int iter_count=0;;) {
	
		// get line from input file
		line.clear();
		std::getline(infile, line);
		
		// end of file reached, break
		if (line.empty()) {
			break;
		}
		
		if (iter_count++ == max_iter) { // hundred billion is a big number
			std::cerr << "Max iterations exceeded while reading input kmer to unitig file";
			break;
		}
		vec.push_back(line);
	}
}

std::ostream& operator<<(std::ostream& os, score_diff_str& SDS) {
	os << SDS.score_diff_abs << "\t" << SDS.score_diff_norm << "\t" << SDS.pass;
	return os;
}

void score_diff_vec::push(float sabs, float snorm, bool pass)
{
	score_diff_str S;
	S.score_diff_abs = sabs;
	S.score_diff_norm = snorm;
	S.pass = pass;
	
	m.lock();
	V.push_back(S);
	m.unlock();
}

std::ostream& operator<<(std::ostream& os, score_diff_vec& SDV)
{
	for (long unsigned int i=0; i < SDV.V.size(); i++) {
		os << SDV.V[i] << std::endl;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, map_stat_str& MSS) {
	os << MSS.num_nodes_returned << "\t" << MSS.buff_used;
	return os;
}

void map_stat_vec::push(short unsigned int num_nodes_returned, short unsigned int buff_used)
{
	map_stat_str M;
	M.num_nodes_returned = num_nodes_returned;
	M.buff_used = buff_used;
	
	m.lock();
	V.push_back(M);
	m.unlock();
}

std::ostream& operator<<(std::ostream& os, map_stat_vec& MSV)
{
	for (long unsigned int i=0; i < MSV.V.size(); i++) {
		os << MSV.V[i] << std::endl;
	}
	return os;
}

void map_stat_vec::sort()
{
	ALGO_NS::sort(	V.begin(), 
					V.end(), 
					[](	map_stat_str m1, 
						map_stat_str m2)
						{
							return m1.buff_used < m2.buff_used;
						}
					);
}

void start_node_len_vec::push(unsigned int len)
{
	m.lock();
	V.push_back(len);
	m.unlock();
}

void start_node_len_vec::sort()
{
	ALGO_NS::sort(V.begin(), V.end(), [](unsigned int l1, unsigned int l2){return l1 < l2;});
}

std::ostream& operator<<(std::ostream& os, start_node_len_vec& SLV)
{
	for (unsigned int i=0; i < SLV.V.size(); i++) {
		os << SLV.V[i] << std::endl;
	}
	return os;
}




void init_LP_map(DB_Node_Table_Type const & list_nodes, unsigned int k, std::vector<lmer_partition_map> & LP_map_,
 unsigned int nthreads, std::ifstream& partition_file_, std::ofstream & stat_file, std::ofstream& log_file_, unsigned int& walk_info_level_)
//void Walk_Queue_Par::init_LP_map(DB_Node_Table_Type const & list_nodes, Params* P)
{
	wall_time_wrap W;
	LP_map_.resize(list_nodes.size());
// #pragma omp parallel for
	for (unsigned int i=0; i < list_nodes.size(); i++) {
		DB_Node_Type const & D = list_nodes.ElementAt(i);
		std::string const & l = D.get_lmer();
		LP_map_[i].lmer_id = l.substr(0,k);
		LP_map_[i].is_singleton = D.is_singleton;
		LP_map_[i].index = i;
	}
	
	log_file_ << "Sort LP map on lmer id ..." << std::flush;
	W.start();
	
	// sort lexicographically by lmer id, so as to enable linear reading of partition id
	ALGO_NS::sort(	LP_map_.begin(), 
					LP_map_.end(), 
					[](lmer_partition_map L1, 
						lmer_partition_map L2){
						return L1.lmer_id < L2.lmer_id;
					});
	W.stop();
	log_file_ << "done, time: " << W << std::endl;
	
	std::string line;
	unsigned int inc=0;
	for (unsigned int i=0; i < LP_map_.size(); i++) {
		// TODO: check logic to handle singletons
		if (LP_map_[i].is_singleton) {
			// assign a random partition
			LP_map_[i].partition_id = (inc++) % nthreads;
		}
		else {
			std::getline(partition_file_, line);
			ASSERT_WITH_MESSAGE(not line.empty(), "Error: partition file has less lines than required, needed: " + std::to_string(LP_map_.size() - list_nodes.singleton_count) + ", current: " + std::to_string(i - inc));
			LP_map_[i].partition_id = atoi(line.c_str());
		}
		assert (LP_map_[i].partition_id < nthreads);
	}
	std::getline(partition_file_, line);
	assert (line.empty());
	
	log_file_ << "Sort LP map on index ..." << std::flush;
	W.start();
	ALGO_NS::sort(	LP_map_.begin(), 
					LP_map_.end(), 
					[](lmer_partition_map L1, 
						lmer_partition_map L2){
						return L1.index < L2.index;
					});
	W.stop();
	log_file_ << "done, time: " << W << std::endl;
	
	log_file_ << "Calculating stats on LP_map ..." << std::flush;
	W.start();
	
	std::vector<std::size_t> par_size(nthreads, 0);
	calc_lp_stats(LP_map_, nthreads, par_size);
	print_lp_stats(par_size, stat_file);
	
	W.stop();
	log_file_ << "time: " << W << std::endl;
	
	
	if (walk_info_level_>=2) {
		log_file_ << "Printing the LP map vector" << std::endl;
		for (unsigned int i=0; i < LP_map_.size(); i++) {
			log_file_ << LP_map_[i] << std::endl;
		}
	}
}




/******************************** Debug functions, currently not in use*******************/

bool compare_kut_kumtt(std::vector< kmer_unitig_mapping_type >& KXK, KU_Table_Type& KU_Table, std::ofstream& log_file) {
	if (not (KXK.size()==KU_Table.size())) {
		std::cout << "Size not equal" << std::endl;
		return false;
	}
	for (unsigned int i=0; i < KXK.size(); i++) {
		KU_TableEntry& K1 = KU_Table.ElementAt(i);
		kmer_unitig_mapping_type& K2 = KXK[i];
		if (K1.kmer not_eq K2.kmer 
			or K1.list_nodes_index not_eq K2.db_table_index
			or K1.PositiveLmer not_eq K2.is_on_positive
			or K1.pos not_eq K2.pos_on_strand
			or K1.lmer_length not_eq K2.lmer_length) {
				log_file << "Not equal case" << std::endl;
				log_file << "Index: " << i << std::endl;
				log_file << K1 << std::endl;
				log_file << K2 << std::endl;
				return false;
		}
	}
	return true;
}

/***************************** End: Debug functions, currently not in use*****************/

