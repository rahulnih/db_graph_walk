#ifndef MWALK_H
#define MWALK_H

#include "walk.h"

// Multithread walk wrapper class
class Walk_Queue_Mul{

	private:
		// vars
		
		Walk_Base *WBptr;
		
		// number of threads
		unsigned int NUM_THREADS_;
		
		// vector of pointers to queue objects
		std::vector<Walk_Queue*> Q_vec;
		
		std::vector<lmer_partition_map> LP_map_;
		
		// true if corresponding db node is in a path, false otherwise
		std::vector<bool> in_use;
		
	public:
		
		// constructor
		Walk_Queue_Mul(Params* P, DB_Node_Table_Type& list_nodes, KU_Table_Type& KU_Table, contig_data_str& CDS, std::ofstream& log_file, unsigned int NUM_THREADS);
		
		~Walk_Queue_Mul() {
			if (WBptr != nullptr) {
				delete WBptr;
			}
			for (auto x : Q_vec) {
				if (x != nullptr) delete x;
			}
		}
		// functions
		
		// the main parallel walk function
		void mwalk_db_graph();
		
		// select multiple starting nodes for parallel walk
		unsigned int mselect_starting_node(Walk_Queue& WQ, std::atomic<unsigned int>* sel_var);
		
		void mselect_next_hop(Walk_Queue& WQ, Walk_Queue_Element& WQE, int id);
};


#endif // MWALK_H
