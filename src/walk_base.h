#ifndef WALK_BASE_H
#define WALK_BASE_H

#include "all_classes.h"
#include "atomic_array.hpp"
#include "misc.h"
#include "db_graph.h"
#include "ku_table.h"
#include "contig_data.h"

class bubble_str {
public:
	// head offsets for two bubbles
	unsigned int head_offset1;
	unsigned int head_offset2;
	
	// lineage
	std::vector<unsigned int> tar_lmer_id_vec1;
	std::vector<unsigned int> tar_lmer_id_vec2;
};

std::ostream& operator<<(std::ostream& os, bubble_str& B);


class bubble_str_vec {
private:
	std::mutex m;
	std::vector<bubble_str> V;

public:
	
	// push bubble
	void push(bubble_str& B);
	
	// print all bubbles
	friend std::ostream& operator<<(std::ostream& os, bubble_str_vec& BSV);
};

class usage_data_str {
public:
	
	// TODO: check for useless locks, they will unnecessarily slow down the program
	
	// supporting data str to keep track of how many times a dist const can be used (for part1)
	std::vector< atomic::atomic_array<int > > dist_const_usage_left;
	std::vector< atomic::atomic_array<int > > expected_dist_const_usage_left;
	
	// supporting data str to keep track of how many times a dist const can be used (for part2)
	atomic::atomic_array<long int>  node_usage_left;
	atomic::atomic_array<long int>  expected_node_usage_left;
	
	// keep track of how many unupdated coverage instances of a node exist, due to unavailable outgoing dist const
	atomic::atomic_array<int>  expected_node_usage_left_unupdated;


	// constructors
	usage_data_str() {};
	usage_data_str(	DB_Node_Table_Type const & DB_Table, 
					const Params * P, 
					std::ofstream& log_file);
	
	void equate_dist_const_vec(std::vector<long unsigned int>& dcul_vec, 
							std::vector<long unsigned int>& nul_vec);
	size_t dynamic_size();
	size_t size() {return sizeof(usage_data_str) + dynamic_size();}
	
};


class Walk_Base {
	
	public:
		
		// VARS
		
		// a reference to the db graph
		DB_Node_Table_Type const & ref_db_node_table;
		
		// a reference to the ku_table
		KU_Table_Type const & ref_ku_table;
		
		// reference to the contig vector, main var in the main function
		contig_data_str& CDS_;
		
		std::vector<lmer_partition_map> const & LP_map_;
		
		outfile_str & ostr_;
		
		// ith entry denotes the index (in the DB_Node_Table) of ith longest lmer in the db node table
		std::vector<unsigned int> sorted_lmer_id_vec;
		
		// TODO: move all stats to a separate structure
		
		// start node len stats
		start_node_len_vec SLV;
		
		// structure to store all score diffs
		score_diff_vec SDV;
		
		// structure to store MAP stats
		map_stat_vec MSV;
		
		bubble_str_vec BSV;
		
		// after partitioning the graph, holds the partition id of each entry in the sorted_lmer_id_vec
// 		std::vector<unsigned int> partition_id;

		// total number of contigs produced till now
		std::atomic<unsigned int> contig_count;
		
		// TODO: see how it should be when reset
		usage_data_str UD;
		
		const Params* P_;
		
		// used in coverage calculations
		mutable float magic_ratio;
		
		mutable unsigned int walk_queue_size;
		mutable unsigned int walk_queue_low_size;
		mutable unsigned int walk_queue_preserve_history;
		
		// vec precomputed to store gaussian scaledown vals upto max error
		std::vector<double> gaussian_scaledown;
		
		// FUNCTIONS
		
		// constructor
		Walk_Base(	Params* P, 
					DB_Node_Table_Type const & list_nodes, 
					KU_Table_Type const & KU_Table, 
					contig_data_str& CDS, 
					std::vector<lmer_partition_map> const & LP_map, 
					outfile_str & ostr);
		
		// compute sorted_lmer_id_vec
		void init_lmer_id_vec();
		
	protected:
		void estimate_parameters();

	public:		
		// given the index of any node (long), initiate the expected coverage based on frequency (knob function)
		unsigned int init_expected_coverage(unsigned int index);
		
		// print start node stats
		void print_start_node_stats();
		
		// write score diff stats to file
		void print_score_diff_stats();
		
		// write MAP stats to file
		void print_map_stats();
		
		size_t dynamic_size();
		size_t size() {return sizeof(Walk_Base) + dynamic_size();}
};


#endif // WALK_BASE_H