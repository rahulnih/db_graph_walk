#ifndef WALK_H
#define WALK_H

#include "all_classes.h"
//#include "atomic_wrapper.h"
#include "atomic_array.hpp"
#include "misc.h"
#include "db_graph.h"
#include "ku_table.h"
#include "contig_data.h"
#include "walk_base.h"

#include "walk_support_files/select_next_hop.h"
// #include "walk_support_files/mini_all_paths.h"
#include "walk_support_files/clear_buffer.h"



// structure for cand_support and cand_dist_const vectors
class support_str {
public:
	int distance;
	unsigned int freq;

	// constructors
	support_str() = default;
	support_str(int const & _distance, unsigned int const & _freq) : distance(_distance), freq(_freq) {};
	support_str(support_str const & other) : distance(other.distance), freq(other.freq) {};
	support_str& operator=(support_str const & other) {
		distance = other.distance;
		freq = other.freq;

		return *this;
	}
	support_str(support_str && other) : distance(std::move(other.distance)), freq(std::move(other.freq)) {};
	support_str& operator=(support_str && other) {
		distance = std::move(other.distance);
		freq = std::move(other.freq);

		return *this;
	}

};
std::ostream& operator<<(std::ostream& os, support_str& cdist);
bool operator==(const support_str& lhs, const support_str& rhs);
// dist_const_str is identical to support_str
// typedef support_str dist_const_str;


// structure containing one node entry in the walking queue
class Walk_Node_Element {
public:
	// variables randomly initialized
	
	// to be managed by src_lmer_id
	// id (index in DB_Node_Table) of the lmer that put me here
	unsigned int src_lmer_id;
	// my id (index in DB_Node_Table)
	unsigned int tar_lmer_id;
	// my toleratable error
	int error=0;
	// how does src_lmer_id want the dist const from it to me to end
	char end=0;
	// self explanatory
	int distance=0;
	// flag saying if the dist const is intra read, used for determining coverage
	bool is_intra_read=false;
	// freq (of the dist const) and freq range data, directly from DB node, used for estimating coverage
	unsigned int freq=0;
	std::vector<long unsigned int> freq_range;
	
	// to be managed by tar_lmer_id
	// how do I want dist const from me to start, the dir depends on the end
	char start=0;
	
	// WHITE: Node is selected to proceed further
	// BROWN: Candidate for next hop. 
	// BLACK: Evidence in form of distance constraints, to convert one of the browns into white
	char color=0;
	
	// params used for coverage estimation
	
	unsigned int in_dc_inter=0;
	unsigned int out_dc_inter=0;
	
	unsigned int in_dc_intra=0;
	unsigned int out_dc_intra=0;
	
	// the forward distance constraints that have been satisfied, used in clear buffer to update the const usage:: pushed by forward white nodes into me
	std::vector<sat_const_str> sat_const_vec_fw;
	
	// the backword distance constraints that have been satisfied, used in clear buffer to update the const usage:: pushed into me by myself
	std::vector<sat_const_str> sat_const_vec_back;
	
	// while reverse walking, usage of the seed sequence doesn't need to be updated. This flag will be used for that.
// 	bool reverse_waiver=false;
	bool revWaiver=false;
	
	// FUNCTIONS
	
	size_t dynamic_size();
	size_t size() {return sizeof(Walk_Node_Element) + dynamic_size();}
	
	void revWalkInit(reverse_seed_structure& R);
	
};

// output operator for Walk_Node_Element
std::ostream& operator<<(std::ostream& os, Walk_Node_Element& WNE);
bool operator==( const Walk_Node_Element& w1, const Walk_Node_Element& w2);

// single element of the walking data structure to follow
class Walk_Queue_Element {
public:
	
	// vars
	
	char color;		// for walking purposes
	// used in the second walk function
	unsigned int white_index=MAX_UINT;
	std::vector<Walk_Node_Element> candidate_nodes;
	
	// functions
	
	void clear();
	
	size_t dynamic_size();
	size_t size() {return sizeof(Walk_Queue_Element) + dynamic_size();}
};

// output operator for Walk_Queue_Element
std::ostream& operator<<(std::ostream& os, Walk_Queue_Element& WQE);

class Traversal_support_str {
	public:
		
		// vars
		
		// last position in the queue where expected usage is updated
		unsigned int last_update_pos;
		
		// true if it's the start of the walk, false otherwiese
		bool start_of_walk;
		
		// when clearing buffer, last k-1 chars of the last node stored, used for sanity check on the first node that comes up next, also in wrapping up, hanging seq is appended at the end
		std::string hanging_seq;
		
		// our savior: if no covg deduced from current set of nodes: when clearing buffer, save the end coverage here, and use it
		unsigned int saved_base_num_dc;
		
		// functions
		
		// constructors
		Traversal_support_str() {};
		Traversal_support_str(Walk_Queue& WQ);
		
		void reset();
		
		size_t dynamic_size();
		size_t size() {return sizeof(Traversal_support_str) + dynamic_size();}
};

bool operator==(const Traversal_support_str& T1, const Traversal_support_str& T2);


class currContStr {
private:
	// current contig being produced
	std::string curr_contig;
	
	// true if current contig has no next nodes
	bool no_neigh;
	
	// if a fw contig is produced, it's id. useful for fr_contig_glue_str
	unsigned int fw_contig_id;
	
	// structures useful for collecting some stats
	unsigned int white_node_count;
	
	// structures for calculating the nodes visited
	std::vector<unsigned int> white_node_vec;
	
public:
	
	// FUNCTIONS
	
	currContStr();
	
	void reset();
	
	void clearContig();
	
	std::size_t contSize();
	
	friend class job_str;
	friend class select_next_hop_class;
	friend class Walk_Queue;
	friend class Walk_Queue_Par;
	friend class contig_data_str;

};

// used while creating the reverse contig
class reverse_seed_structure {
public:
	unsigned int lmer_id;
	char end;
	unsigned int coverage;
};

// output operator for reverse_seed_structure
std::ostream& operator<<(std::ostream& os, reverse_seed_structure& RSD);


class revWalkStr {
public:
	// vector acting as a seed for starting the reverse contig
	
	bool walkRev=false;
	unsigned int revSeedLen;
	unsigned int revSeedCount;
	std::vector<reverse_seed_structure> revSeedVec;
	
	// FUNCTIONS
	
	revWalkStr();
	
	void reset();
	
};

// structures relevant for mini_all_path routine (instead of just lmer ids, put the entire node, for more safety
class decidedHopStr {
public:
	
	std::vector<Walk_Node_Element> hops;
	std::vector<dc_str> decided_dc_inter;
	std::vector<dc_str> decided_dc_intra;
	
	void reset();
	
	std::size_t dynamic_size();
	
};

// data structure for walking algorithm
class Walk_Queue/*: public Walk_Base*/{
	
	private:
		
		Walk_Base *WBptr;
		
		unsigned int& walk_info_level_;
		
		// used only for the parallel case, spitting thread specific log information
		int thread_id;
		
		std::ofstream * t_log_file_ptr;
		
		std::ofstream& log_file_;
		
		// class for managing contig being traversed
		currContStr CCS;
		
		// the main walk queue
		std::vector<Walk_Queue_Element> Q;
		
		// position from which contig seq will be read after queue cleaning is needed
		unsigned int beg;
		
		// current position of examination
		unsigned int curr;
		
		// expected coverage at current traversal point
		// TODO: move to TSS
// 		unsigned int expected_coverage;
		unsigned int exp_covg;
		
		// vars assisting in graph traversal
		Traversal_support_str TSS;
		
		select_next_hop_class * SNHptr;
		
		revWalkStr RWS;
		
		decidedHopStr DHS;

		// TODO: assure that a thread is making only it's nodes dirty
		std::vector<long unsigned int> dirty_index_vec_dcul;
		std::vector<long unsigned int> dirty_index_vec_nul;
		
		// pointer to a str of scaffolding info
// 		scaff_info_str* scin_ptr;
		
		size_t mini_all_paths_mem;
		
		// TODO: move to TSS
		unsigned int sel_start_node_resume;
		
		// TODO: move it to walk base?
		unsigned int min_start_node_len;
		
		// indenting the log file
		unsigned int print_level;
		
		
		// PRIVATE FUNCTIONS
		
		// hard set the curr variable
		inline void set_curr(unsigned int new_curr) {
			curr = new_curr;
		}
		
		// jump ahead (or back) "jump" positions and return the new position
		inline unsigned int jump_ahead(int jump) {
			int icurr = (int)curr;
			icurr = (Q.size() + icurr + jump)%Q.size();
			return  (unsigned int)icurr;
		}
		
		//return the WQE at curr position 
		inline Walk_Queue_Element& CurrElement() {
			assert(curr >=0 and curr <= Q.size()-1);
			return Q.at(curr);
		}
		
		// return WQE at any arbitrary position
		inline Walk_Queue_Element& getElement(unsigned int pos) {
			assert(pos >=0 and pos <= Q.size()-1);
			return Q.at(pos);
		}
		
		// push an entry at a position relative to current position
		void EntryPush(Walk_Node_Element W, int pos) {
			unsigned int real_pos=jump_ahead(pos);
			Q[real_pos].candidate_nodes.push_back(W);
		}
		
		// clean all the data from the queue, leaving the size intact
		void clear() {
			for (unsigned int i=0; i<Q.size(); i++) {
				Q[i].clear();
			}
		}
		
		// print a snapshot of the queue in the range [print_start, print_end]
		void print_snapshot(	unsigned int print_start, 
							unsigned int print_end, 
							std::ostream& os = std::cout); 
		
		// select a starting node to walk the graph, or else return the max unsigned int
		unsigned int select_start_node ();
		
		/**********walk loop functions**************/
		
		// put the first node in the start queue, which will get things started
		void push_start_node(	unsigned int start_index, 
								int queue_index = 0);
		
		// for all WQE nodes, set the s_dir based on end
		void set_sdir (Walk_Queue_Element& WQE);
		
		void buff_check(Walk_Queue_Element& WQE, int shift);
		
		// set neigh nodes in the second walk function, set as RED
		unsigned int set_neigh_nodes(Walk_Queue_Element& WQE);
		
		// For the good node in WQE, set further dist constraints, return the longest distance set
		unsigned int set_next_dist_const(Walk_Queue_Element& WQE);
		
		// move the current pointer to the next brown list
		void move_pointer(Walk_Queue_Element& WQE);
		
		
		/**********END: walk loop functions**************/

		/************** supporting functions outside the walk loop *************/
		
		// given two candidate nodes, find if one is an evidence other's occurance
		bool match_evidence(	const Walk_Node_Element& ev_node, 
							const Walk_Node_Element& fixed_node, 
							const int obs_error);
		
		// given the support vector for a candidate, calculate the score
		float evidence_score(std::vector<support_str>& cand_support_inter_read, 
							std::vector<support_str>& cand_support_intra_read);
		
		bool is_good_score(	const float norm_score_b, 
							const float norm_score_sb, 
							const float abs_score_b, 
							const float abs_score_sb);
		
		// clear the buffer from beg to end, including beg but not including end
		void clear_buffer(unsigned int beg, unsigned int end);
		
		void update_contig_info();
		
		// after producing the contig, reset vars;
		void reset_vars();
		
		// after producing a forward contig, set up the queue for reverse walk
		void reverse_walking_init();
		
		// reset things to start a fresh contig, no reverse walking
		void square_zero_init();
		
		// given a db node D and a direction, put all the neighbors in that direction in the vector neighbors
		unsigned int get_neighbors(std::vector<Walk_Node_Element>& neighbors, 
								DB_Node_Type const & D, 
								char start);
		
		// for a given node current_WNE, gather support in the vicinity, put it in cand_support, meanwhile also update cand_dist_const for in_dist_const
		void gather_cand_support(std::vector<support_str>& support, 
								std::vector<dc_str>& dist_const, 
								std::vector<support_str>& support_intra_read,
								std::vector<dc_str>& dist_const_intra_read,
								Walk_Node_Element& current_WNE);
		
		
		
		
		// count the num of nodes of color "color" at WQE. color_index is the last index of that color
		unsigned int count_color(	Walk_Queue_Element& WQE ,
								char color, 
								unsigned int& color_index);
		
		/************** END: supporting functions outside the walk loop *************/
		
		
		size_t dynamic_size();
		
		/***************logger functions**********************/
		
		void log_msg_1();
		void log_msg_2();
		void log_msg_3();
// 		void log_msg_4(Walk_Node_Element& prev_white_node, sat_const_str& S);
// 		void log_msg_5(unsigned int max_dist_const_dest, unsigned int next_unup_pos);
// 		void log_msg_6();
		void log_msg_7(std::vector<dc_str>& dist_const);
		void log_msg_8(long unsigned int out_num_iterations);
		void log_msg_9(long unsigned int num_iterations);
		void log_msg_10(	unsigned int m, 
						long unsigned int num_iterations, 
						Walk_Queue_Element& current_WQE);
		void log_msg_11(long unsigned int num_iterations);
		void log_msg_12();
		void log_msg_13();
		void log_msg_14();
		void log_msg_15(	unsigned int empty_space, 
						unsigned int space_needed);
		void log_msg_16(unsigned int end_pos);
		void log_msg_17(Walk_Queue_Element& WQE);
// 		void log_msg_18(unsigned int brown_size);
// 		void log_msg_19();
// 		void log_msg_20(Walk_Node_Element& current_WNE);
// 		void log_msg_21(Walk_Node_Element& current_WNE);
// 		void log_msg_22(std::size_t idx, select_next_hop_data & SD, Walk_Node_Element& current_WNE);
// 		void log_msg_23();
		void log_msg_24();
		void log_msg_25();
		
		void beg_salutation(std::string fn_name);
		void end_salutation(std::string fn_name);
		
	public:
		
		// Walk_Queue constructor
		Walk_Queue(Params* P, 
					DB_Node_Table_Type const & list_nodes, 
					KU_Table_Type const & KU_Table, 
					contig_data_str& CDS, 
					std::vector<lmer_partition_map> const & LP_map, 
					outfile_str& ostr,
					Walk_Base *ptr); 
		
		~Walk_Queue() {
			if (t_log_file_ptr != nullptr) {
				// ERROR: double free or corruption
// 				delete t_log_file_ptr;
			}
			delete SNHptr;
		}
		
		// walk db graph to form contigs and write in the output file
		void walk_db_graph();
		
		size_t size() {return sizeof(Walk_Queue) + dynamic_size();}
		
		/*************** supporting classes ******************/
		
		friend class select_next_hop_data;
		friend class select_next_hop_class;
		friend class mini_all_paths_data;
		friend class clear_buffer_data;
		friend class Traversal_support_str;
		friend class Walk_Queue_Mul;
		friend class Walk_Queue_Par;
		friend class Walk_Queue_Par_Mem;
		friend class job_str;
		friend class contig_data_str;
		
		/*************** END: supporting classes ******************/
		
		// printing operator
		friend std::ostream& operator<<(std::ostream& os, Walk_Queue& WQ);
}; // END: class Walk_Queue


// output operator for Walk_Queue

bool comparable_candidate_lengths(unsigned int min_len, unsigned int max_len, Params* P);

/************ knobs**************/
// calculating coverage

bool check_acceptable_distance(int given_dist, int base_intra_dist, int base_inter_dist, int max_intra_err, int max_inter_err, bool is_intra_read);

#endif // WALK_H

