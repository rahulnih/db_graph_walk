#include "pwalk.h"

/***************************CRUCIAL TODO 
 * 1. Change refs to pointers when appropriate
 * 2. Mark thread common data structures exclusive
 * 4) assertion of stitch length
 * *****************************************/

// Walk_Queue_Par_Mem

Walk_Queue_Par_Mem::Walk_Queue_Par_Mem(Walk_Queue_Par& wqp_ref) : wqp_ref_(wqp_ref) {
	
	NUM_THREADS_ = wqp_ref.NUM_THREADS_;
	iter_vec = std::vector<size_t>(NUM_THREADS_, 0);
// 	mem_print = std::vector<bool>(NUM_THREADS_, false);
	mem_print_counter=1;
	Q_vec_mem = std::vector<size_t>(NUM_THREADS_, 0);
	JQS_vec_mem = std::vector<size_t>(NUM_THREADS_, 0);
	JQS_vec_num = std::vector<size_t>(NUM_THREADS_, 0);
	curr_job_vec_mem = std::vector<size_t>(NUM_THREADS_, 0);
}

Walk_Queue_Par_Mem::Walk_Queue_Par_Mem(Walk_Queue_Par_Mem& other):wqp_ref_(other.wqp_ref_)
{
	WB_mem = other.WB_mem;
	db_graph_mem = other.db_graph_mem;
	ku_table_mem = other.ku_table_mem;
	contigs_data_str_mem = other.contigs_data_str_mem;
	LP_map_mem = other.LP_map_mem;
	no_start_node_vec_mem = other.no_start_node_vec_mem;
	timing_vec_mem = other.timing_vec_mem;
	Q_vec_mem = other.Q_vec_mem;
	JQS_vec_mem = other.JQS_vec_mem;
	JQS_vec_num = other.JQS_vec_num;
	curr_job_vec_mem = other.curr_job_vec_mem;
}


void Walk_Queue_Par_Mem::calc_mem_map_parallel()
{
	int id = omp_get_thread_num();
	if ((iter_vec[id]++)%20 not_eq 0) {
		return;
	}
	
	Walk_Queue& my_wq = *(wqp_ref_.Q_vec[id]);
	std::ofstream& t_log = *(my_wq.t_log_file_ptr);
	
	wall_time_wrap W;
	W.start();
	
	shared_mem_m.lock();
	WB_mem = wqp_ref_.WBptr->size();
	db_graph_mem = wqp_ref_.WBptr->ref_db_node_table.memsize();
	ku_table_mem = wqp_ref_.WBptr->ref_ku_table.memsize();
	contigs_data_str_mem = wqp_ref_.WBptr->CDS_.size();
	LP_map_mem = vecsize(wqp_ref_.LP_map_);
	no_start_node_vec_mem = vecsize(wqp_ref_.no_start_node_vec);
	timing_vec_mem = vecsize(wqp_ref_.timing_vec);
	shared_mem_m.unlock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, t_log, "CALC SHARED MEM");
	
	W.start();
	
	Walk_Queue& my_wq_ref = *(wqp_ref_.Q_vec[id]);
	Q_vec_mem[id] = my_wq_ref.size();
	
	job_queue_str& my_jqs_ref = *(wqp_ref_.JQS_vec[id]);
	my_jqs_ref.lock();
	JQS_vec_mem[id] = my_jqs_ref.size();
	JQS_vec_num[id] = my_jqs_ref.num_jobs();
	my_jqs_ref.unlock();
	
	job_str& my_js_ref = *(wqp_ref_.curr_job_vec[id]);
	curr_job_vec_mem[id] = my_js_ref.size();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, t_log, "CALC IND MEM");
}


void Walk_Queue_Par_Mem::print_mem_map(std::ofstream& os)
{
	if (inc_mem_print()%40 not_eq 0) {
		return;
	}
	
	wall_time_wrap W;
	W.start();
	
	Walk_Queue_Par_Mem* local = new Walk_Queue_Par_Mem(*this);
	size_t qvs = (*local).Q_vec_sum();
	size_t jvs = (*local).JQS_vec_sum();
	size_t cvs = (*local).curr_job_vec_sum();
	size_t total = 	qvs + jvs + cvs 
				+ (*local).WB_mem 
				+ (*local).db_graph_mem 
				+ (*local).ku_table_mem 
				+ (*local).contigs_data_str_mem 
				+ (*local).LP_map_mem 
				+ (*local).no_start_node_vec_mem 
				+ (*local).timing_vec_mem;
	
	os << "--------------------------begin----------------------------" << std::endl;
	os << "WB_mem: " << bytes2h((*local).WB_mem) << std::endl;
	os << "db_graph_mem: " << bytes2h((*local).db_graph_mem) << std::endl;
	os << "ku_table_mem: " << bytes2h((*local).ku_table_mem) << std::endl;
	os << "contigs_data_str_mem: " << bytes2h((*local).contigs_data_str_mem) << std::endl;
	os << "LP_map_mem: " << bytes2h((*local).LP_map_mem) << std::endl;
	os << "no_start_node_vec_mem: " << bytes2h((*local).no_start_node_vec_mem) << std::endl;
	os << "timing_vec_mem: " << bytes2h((*local).timing_vec_mem) << std::endl;
	os << std::endl;
	
	os 	<< "Q_vec_mem: " 
		<< bytes2h(qvs) 
		<< std::endl;
	
	for (size_t i=0; i < (*local).Q_vec_mem.size(); i++) {
		os 	<< i << ": " 
			<< bytes2h((*local).Q_vec_mem[i]);
		if (i%4==3) {
			os << std::endl;
		}
		else {
			os << "\t";
		}
	}
	os << std::endl;

	os << "JQS_vec mem and num jobs: " << bytes2h(jvs) << std::endl;
	for (size_t i=0; i < (*local).JQS_vec_mem.size(); i++) {
		os 	<< i << ": " << bytes2h((*local).JQS_vec_mem[i]) 
			<< "," << (*local).JQS_vec_num[i];
		if (i%4==3) {
			os << std::endl;
		}
		else {
			os << "\t";
		}
	}
	os << std::endl;

	os << "curr_job_vec_mem: " << bytes2h(cvs) << std::endl;
	for (size_t i=0; i < (*local).curr_job_vec_mem.size(); i++) {
		os << i << ": " << bytes2h((*local).curr_job_vec_mem[i]);
		if (i%4==3) {
			os << std::endl;
		}
		else {
			os << "\t";
		}
	}
	os << std::endl;
	os << "Grand total memory: " << bytes2h(total) << std::endl;
	os << "---------------------------end-----------------------------" << std::endl;
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, os, "PRINT MEM");
	
	delete local;
}


// Walk_Queue_Par

// constructor
Walk_Queue_Par::Walk_Queue_Par(
	Params* P, 
	DB_Node_Table_Type const & list_nodes, 
	KU_Table_Type const & KU_Table, 
	contig_data_str& CDS, 
	std::vector<lmer_partition_map> const & LP_map, 
	outfile_str& ostr,
	std::ifstream& partition_file, 
	time_str& T) : NUM_THREADS_(P->num_threads), 
				LP_map_(LP_map), 
				partition_file_(partition_file), 
				log_file_(ostr.log_file),
				timing_vec(T.t_threads), 
				walk_info_level_(P->walk_info_level) {
		
		assert(partition_file_.is_open());
		
		WBptr = new Walk_Base(P, list_nodes, KU_Table, CDS, LP_map, ostr);
		Q_vec.resize(NUM_THREADS_);
		JQS_vec.resize(NUM_THREADS_);
		curr_job_vec.resize(NUM_THREADS_);
		no_start_node_vec.resize(NUM_THREADS_);
		no_ongoing_traversal.resize(NUM_THREADS_);
		
		log_file_ << "Init walk queue par...." << std::endl << std::flush;
		
#pragma omp parallel for num_threads(NUM_THREADS_)
		for (unsigned int i=0; i < NUM_THREADS_; i++) {
			Q_vec[i] = new Walk_Queue(P, list_nodes, KU_Table, CDS, LP_map, ostr, WBptr);
			
			JQS_vec[i] = new job_queue_str(Q_vec[i]->t_log_file_ptr);
			
			curr_job_vec[i] = new job_str(Q_vec[i]->t_log_file_ptr);
			
			// in the beginning, we have start nodes
			no_start_node_vec[i]=false;
			
			// in the beginning there is no ongoing traversal
			no_ongoing_traversal[i]=true;
		}
		
		Mem_str_ptr = new Walk_Queue_Par_Mem(*this);
		
		// initialize LP map vector
		// init_LP_map(list_nodes, P);  called before Walk_Queue_Par constructor now, in main.cpp
		
		std::vector<std::size_t> eligibility_vec(NUM_THREADS_, 0);
		calc_start_node_eligibility(eligibility_vec, NUM_THREADS_);
		WBptr->ostr_.stat_file << "\nStart node eligibility vector: " << std::endl;
		box_print_vec(eligibility_vec, 4, WBptr->ostr_.stat_file);
		
		unique_contig_id = 0;
		
		log_file_ << "Done init walk queue par...." << std::endl;
}

// the main parallel walk function
void Walk_Queue_Par::pwalk_db_graph()
{
// 	debug_file << "Test string" << std::endl;
	
	assert(timing_vec.size()==NUM_THREADS_);
	WBptr->init_lmer_id_vec();
	
	// TODO: Find out why this unnecessary loop is needed
	for (unsigned int i=0; i<NUM_THREADS_; i++) {
		no_ongoing_traversal[i]=true;
	}
	
	// var assists in parallel selection for starting nodes
		
#pragma omp parallel num_threads(NUM_THREADS_)
	{
		// start timing the thread
		std::chrono::high_resolution_clock::time_point s, e;
		std::chrono::milliseconds thread_time;
		s = std::chrono::high_resolution_clock::now();
		
		int id = omp_get_thread_num();
		
		// my walk queue structure
		Walk_Queue& WQ = *Q_vec[id];
		
		// my job queue structure
		job_queue_str& my_job_queue_str = *JQS_vec[id];
		
		console.lock();
		std::cout << "Starting thread: " << id << std::endl;
		console.unlock();
		
		// start producing contigs
		unsigned long int out_num_iterations=0;
		while(true) {
			// prelims
			WQ.clear();
			WQ.CCS.curr_contig="";
			// TODO: find why assert is not working
// 			ASSERT_WITH_MESSAGE(no_ongoing_traversal[id], "ID: " + std::to_string(id));
			
			// curr job, right now a dummy job
			job_str *curr_job_ptr = nullptr;
			
			// if the job queue is not empty, fetch job and process
			if (not my_job_queue_str.empty()) {
				
				// currently an ongoing traversal
				no_ongoing_traversal[id]=false;
				
				// pull a job out of of job queue and put it on the walk queue.
				curr_job_ptr = my_job_queue_str.pop_job_to_walk_queue(WQ);
			}
			else if (no_start_node_vec[id]) {
				
				// no local start nodes, so just check the exit condition
				if (check_exit_cond()) {
					break;
				}
				continue;
			}
			// else just proceed by selecting a start index in local partition
			else {
				unsigned int start_index =pselect_starting_node(WQ, id);
				
				if (start_index==MAX_UINT) {
					no_start_node_vec[id]=true;
					
					// calc time from beginning
					e = std::chrono::high_resolution_clock::now();
					thread_time = std::chrono::duration_cast<std::chrono::milliseconds >(e-s);
					timing_vec[id] = thread_time;
					
					// write out the timing
					console.lock();
					std::cout 	<< "Local start nodes finished for thread: " 
							<< id 
							<< ", " 
							<<  convert_t2s(thread_time) 
							<< std::endl;
					console.unlock();
					continue;
				}
				
				// current thread has an ongoing traversal
				no_ongoing_traversal[id]=false;
				
				WQ.push_start_node(start_index);

				curr_job_ptr = new job_str(WQ.t_log_file_ptr);
			}
			
			unsigned long int num_iterations=0;
			
			// produce one contig
			while(true) {
				
				// TODO: parametrize this calculation
// 				Mem_str_ptr->calc_mem_map_parallel();
// 				log_file_m.lock();
// 				Mem_str_ptr->print_mem_map(log_file_);
// 				log_file_m.unlock();
				
				
				Walk_Queue_Element& current_WQE = WQ.CurrElement();
				
				// set s_dir for all WHITE nodes in current_WQE based on end
				WQ.set_sdir( current_WQE );
				
				// check if there is enough space for further nodes
				WQ.buff_check(current_WQE, WBptr->walk_queue_preserve_history);
				
				// add neighbors of curr WHITE node, and make them BROWN
				unsigned int neigh_count = WQ.set_neigh_nodes( current_WQE );
				
				// add dist const of curr WHITE node, and make them BLACK
				unsigned int m = WQ.set_next_dist_const( current_WQE );
				
				// log some information
				if (num_iterations >= WBptr->P_->info_after_iterations and walk_info_level_>=2) {
					unsigned int l = WBptr->ref_db_node_table.ElementAt(
									current_WQE.candidate_nodes[ current_WQE.white_index ].tar_lmer_id
									).len_lmer;
					WQ.print_snapshot(WQ.curr, (WQ.curr + std::max(l,m))%WQ.Q.size(), *(WQ.t_log_file_ptr));
				}
				
				// move the current pointer to the next BROWN list
				WQ.move_pointer( current_WQE );
				
				// again get the current element after the pointer has been moved
				Walk_Queue_Element& next_WQE = WQ.CurrElement();
				
				unsigned int tar_partition_id = MAX_UINT;
				
				hop_return H = pselect_next_hop( WQ, 
											next_WQE, 
											neigh_count, 
											tar_partition_id, 
											id );
				
				// either way, this traversal has to end
				if (H==NO_HOP or H==DIFF_PAR_HOP) {
					
					bool my_contig = false;
					
					// if the curr job is empty, then it's my contig
					if ((*curr_job_ptr).no_job()) {
						my_contig = true;
					}
					
					// set the id for current contig
					unsigned int contig_id = MAX_UINT;
					if (my_contig) {
						if (not WQ.RWS.walkRev) {
							contig_id = get_unique_contig_id();
							WQ.CCS.fw_contig_id = contig_id;
						}
						else {
							contig_id = WQ.CCS.fw_contig_id;
						}
					}
					
					// if the node is from different partition, create a job and push
					if (H == DIFF_PAR_HOP) {
						
						assert(tar_partition_id not_eq (unsigned int)id);
						assert(tar_partition_id < NUM_THREADS_);
						
						// set sdir just so that there is no assertion error in clear buffer
						WQ.set_sdir(next_WQE);
						
						// will be used if this is a fw contig, else useless
						jcalculate_RSD(WQ);
						
						// partition id of the selected node
						
						// corresponding queue structure
						job_queue_str& tar_job_queue_str = *JQS_vec[ tar_partition_id ];
						
						// wrap up the current queue into job
						
						// create a new job from walk queue
						job_str *J_ptr = new job_str(WQ.t_log_file_ptr);
						(*J_ptr).create_job_from_walk_queue(WQ, curr_job_ptr, contig_id);
						
						// push the job ptr into target queue
						tar_job_queue_str.push_job(J_ptr);
						
					}
					
					if (H == NO_HOP) {
						
						// this contig is ending, so clear buffer
						WQ.SNHptr->reset_unupdated_vec();
						WQ.clear_buffer(WQ.beg, (WQ.curr + 1)%WQ.Q.size());
						WQ.CCS.curr_contig.append(WQ.TSS.hanging_seq);
						jupdate_contig_info(WQ, curr_job_ptr, contig_id);
						
					}
					WQ.reset_vars();
					
					// test if reverse walk is needed
					bool rev_walk_needed=false;
					if (my_contig) {
						if (not WQ.RWS.walkRev) {
							rev_walk_needed = true;
						}
						else {
							jpush_fw_rev_stitch_info(WQ, curr_job_ptr, contig_id);
						}
					}
					if (rev_walk_needed) {
						
						// do what you got to do to set up reverse walking
						WQ.reverse_walking_init();
						
						// oh yeah, now we walking reverse
						WQ.RWS.walkRev=true;
						
					}
					else {
						WQ.square_zero_init();
						
						// for now, no ongoing traversal
						no_ongoing_traversal[id]=true;
						
						break;
					}
						
				} // END: if (H==NO_HOP or H==DIFF_PAR_HOP) {
				if (num_iterations++ > WBptr->P_->max_iterations) {
#pragma omp critical
					{
						std::cerr 	<< "Iterations exceeded maximum limit in pwalk inner loop, tid: " 
								<< id 
								<< std::endl;
						exit(0);
					}
				}
			} // END: inner while()
			if (out_num_iterations++ > WBptr->P_->max_iterations) {
#pragma omp critical
				{
					std::cerr << "Iterations exceeded maximum limit in pwalk outer loop, tid: " << id << std::endl;
					exit(0);
				}
			}
			delete curr_job_ptr;
		} // END: outer while()
		e = std::chrono::high_resolution_clock::now();
		thread_time = std::chrono::duration_cast<std::chrono::milliseconds >(e-s);
		timing_vec[id] = thread_time;
		
		console.lock();
// 		std::cout << std::fixed << std::setprecision(3);
		std::cout << "Ending thread: " << id << ", " << /*float(thread_time.count())/1000 << " secs"*/ convert_t2s(thread_time) << std::endl;
		console.unlock();
		
	} // END: pragma omp parallel num_threads(NUM_THREADS_)
	
	// for now, rest of the part is serial
	
	wall_time_wrap W;
	
	pre_merge_fw_rev_contigs();
	
	WBptr->CDS_.merge_fw_rev_contigs();
	
	WBptr->print_start_node_stats();
	
	WBptr->print_score_diff_stats();
	
	WBptr->print_map_stats();
	
	WBptr->ostr_.bub_file << WBptr->BSV;
	
	// update visit_counts in list_nodes, just for analysis purpose
	for (unsigned int i=0; i < WBptr->CDS_.data.size(); i++) {
		std::vector<unsigned int>& white_vec_ref = WBptr->CDS_.data[i].white_node_vec;
		for (unsigned int j=0; j < white_vec_ref.size(); j++) {
			unsigned int lmer_id = white_vec_ref[j];
			DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(lmer_id);
			D.visit_count++;
		}
	}
	
	// TODO: after sorting by len, sort by lex and remove duplicates
	WBptr->CDS_.sort_data();
	
	if (walk_info_level_>=3) {
	
		unsigned int sing_zero=0;
		unsigned int nosing_zero=0;
		unsigned int sing_nozero=0;
		unsigned int nosing_nozero=0;
		
		for (unsigned int i=0; i < WBptr->ref_db_node_table.size(); i++) {
			DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(i);
			log_file_ << i << "\t" <<  BOOL(D.is_singleton) << "\t" << D.visit_count << std::endl;
			
			if (D.is_singleton and D.visit_count==0) {
				sing_zero++;
			}
			else if ((not D.is_singleton) and D.visit_count==0) {
				nosing_zero++;
			}
			else if (D.is_singleton and D.visit_count not_eq 0) {
				sing_nozero++;
			}
			else if ((not D.is_singleton) and D.visit_count not_eq 0) {
				nosing_nozero++;
			}
		}
		log_file_ 	<< "sing_zero: " << sing_zero 
							<< "\tnosing_zero: " << nosing_zero 
							<< "\tsing_nozero: " << sing_nozero 
							<< "\tnosing_nozero: " << nosing_nozero 
							<< std::endl;
	}
	
// 	std::cout << "No decision: " << WBptr->case1_termination << std::endl;
// 	std::cout << "Dead end, 0 DC: " << WBptr->case2_termination << std::endl;
// 	std::cout << "Dead end, sub5 DC: " << WBptr->case3_1_termination << std::endl;
// 	std::cout << "Dead end, sup5 DC: " << WBptr->case3_2_termination << std::endl;
	
} // END: pwalk_db_graph()

// select multiple starting nodes for parallel walk
unsigned int Walk_Queue_Par::pselect_starting_node(Walk_Queue& WQ, int id/*, std::atomic<unsigned int>* sel_var*/) {
	
	wall_time_wrap W;
	W.start();
	
	for (unsigned int i=WQ.sel_start_node_resume; i < WBptr->sorted_lmer_id_vec.size() ; ) {
		unsigned int index = WBptr->sorted_lmer_id_vec[i];
		if (LP_map_[index].partition_id not_eq (unsigned int)id) {
			++i;
			continue;
		}
		DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(index);
		
		// default
		bool negative_exists = true;
		
		// if neg const has to be enforced, then enforce it
		if (WBptr->P_->enforce_neg_const) {
			
			// test if there is at least one negative dist const
			negative_exists = false;
			for (unsigned int j=0; j < D.Dist_Const_List.size(); j++) {
				if (D.Dist_Const_List[j].distance < 0) {
					negative_exists = true;
				}
			}	
		}
		
		if ((!negative_exists) || (D.len_lmer < int(WQ.min_start_node_len))) {
			++i;
			continue;
		}

		//my_atomic_wrapper<long int>& expected_usage_left_entry = WBptr->UD.expected_node_usage_left[index];
		
		if (WBptr->UD.expected_node_usage_left[index].load() <= 0) {
			++i;
			continue;
		}
			
		long int old_val = WBptr->UD.expected_node_usage_left[index].load();
//TCP WHY MAX
		unsigned int cov = (unsigned int) std::max((size_t)old_val,  D.freq_range[1]);
		long int new_val = old_val > (long int)cov ? old_val - (long int)cov : 0;
		if (WBptr->UD.expected_node_usage_left[index].compare_exchange_strong(old_val, new_val)) {
			// not exchanged, then old_val needs to be used to update new_val.
			
			if (walk_info_level_ >=2) {
				*(WQ.t_log_file_ptr) << "Starting node " << index << " selected" << std::endl;
			}
			
			if (WBptr->P_->print_start_node_stats) {
				WBptr->SLV.push((unsigned int)D.len_lmer);
			}
			
			// update dirty index
			WQ.CCS.white_node_vec.push_back(index);
			WQ.dirty_index_vec_nul.push_back(index);
			WQ.sel_start_node_resume = i;
			WQ.exp_covg = cov;
			

			W.stop();
			conditional_time_print(PRINT_FN_TIMES, W, *(WQ.t_log_file_ptr), "PSELECT START NODE");
			
			// return
			return index;
		} // else repeat iteration


		// TCP Q for RAHUL:  WHY MAX instead of MIN?

		//WQ.expected_coverage = init_expected_coverage(index);
		//   same as (unsigned int)std::max((long unsigned int)WBptr->UD.expected_node_usage_left[index].val(), D.freq_range[1]);
		// if WBptr->UD.expected_node_usage_left[index] is max, then new entry is 0
		// if D.freq_range[1] is max, then expected_usage_left_entry is not bigger than D.freq_range[1].  so entry is also 0.

		// // obtain lock
		// expected_usage_left_entry.lock();
		
		// // reduce to zero
		// expected_usage_left_entry = expected_usage_left_entry > (long int)WQ.expected_coverage ? expected_usage_left_entry.val() - (long int)WQ.expected_coverage : 0;
		
		// // unlock
		// expected_usage_left_entry.unlock();
		
	}
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *(WQ.t_log_file_ptr), "PSELECT START NODE");
	return MAX_UINT;
}

// TODO: check this code and if it all makes sense
hop_return Walk_Queue_Par::pselect_next_hop(	Walk_Queue& WQ, 
										Walk_Queue_Element& WQE, 
										unsigned int neigh_count, 
										unsigned int& tar_par_id, 
										int id) {
	
	wall_time_wrap W;
	W.start();
	
	select_next_hop_data SD(WQ);
	
	bool found;
	if (neigh_count==0) {
		found=false;
		assert(not WQ.CCS.no_neigh);
		WQ.CCS.no_neigh=true;
	}
	else {
		// TODO: in modified version, if found==false, then check scaff str. If something, then there is a scaff.
		found = WQ.SNHptr->find_best_candidate(WQE, SD, PAR);
	}
	
	// if found, candidate present in SD.best_cand_position
	unsigned int lmer_id = MAX_UINT;
	if (found) {
		lmer_id = WQE.candidate_nodes[ SD.best_cand_position ].tar_lmer_id;
// 		*(WQ.t_log_file_ptr) /*<< "Tid: " << id*/ 
// 						<< ", next index: " << lmer_id 
// 						<< "(par: " << LP_map_[ lmer_id ].partition_id 
// 						<< ", len: " << WBptr->ref_db_node_table.ElementAt(lmer_id).len_lmer 
// 						<< "), b: " << WQ.beg  
// 						<< ", c: " << WQ.curr
// 						<< std::endl;
		
		WQ.SNHptr->select_best_candidate(WQE, SD);
		// if the new node is in the same partition
		if (LP_map_[ lmer_id ].partition_id == (unsigned int)id) {
			W.stop();
			conditional_time_print(PRINT_FN_TIMES, W, *WQ.t_log_file_ptr, "PSELECT NEXT HOP");
			return SAME_PAR_HOP;
		}
		else {
			tar_par_id = LP_map_[ lmer_id ].partition_id;
			W.stop();
			conditional_time_print(PRINT_FN_TIMES, W, *WQ.t_log_file_ptr, "PSELECT NEXT HOP");
			return DIFF_PAR_HOP;
		}
	} // END: if (found)
	
	// not found, return NO_HOP
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *WQ.t_log_file_ptr, "PSELECT NEXT HOP");
	return NO_HOP;
}

bool Walk_Queue_Par::check_exit_cond()
{
	exit_cond.lock();
	bool e=true;
	for (unsigned int i=0; i < no_start_node_vec.size(); i++) {
		if (not no_start_node_vec[i]) {
			e=false;
			break;
		}
	}
	
	bool t=true;
	for (unsigned int i=0; i < no_ongoing_traversal.size(); i++) {
		if (not no_ongoing_traversal[i]) {
			t=false;
			break;
		}
	}
	
	bool q=true;
	for (unsigned int i=0; i <JQS_vec.size(); i++) {
		job_queue_str& J = *(JQS_vec[i]);
		if (not J.empty()) {
			q=false;
			break;
		}
	}
	exit_cond.unlock();
	
	return e and t and q;
}

void Walk_Queue_Par::jupdate_contig_info(Walk_Queue& WQ, job_str *curr_job_ptr, unsigned int contig_id)
{
	wall_time_wrap W;
	W.start();
	WBptr->contig_count++;
	if (WBptr->contig_count <= 20000) {
		log_file_m.lock();
		log_file_ << "Tid: " << WQ.thread_id << ", num white nodes: " << WQ.CCS.white_node_count << std::endl;
		log_file_m.unlock();
	}
	if (walk_info_level_ >= 1) {
		jupdate_contig_log_print(WQ);
	}
// 	// white nodes can be zero only in reverse contig (for fwd, there will at least be starting node
	assert(WQ.RWS.walkRev or WQ.CCS.white_node_count!=0);
	
	
	push_contig_m.lock();
// 	debug_file << "contig_id: " << contig_id << std::endl;
// 	debug_file << "job_contig_id: " << (*curr_job_ptr).contig_num << std::endl;
// 	debug_file << "dir: " << (*curr_job_ptr).d << std::endl;
// 	if (WQ.curr_contig.size() >= WBptr->P_->min_contig_len) {
		WBptr->CDS_.push_contig_data(WQ, curr_job_ptr, contig_id);
// 	}
	push_contig_m.unlock();
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *(WQ.t_log_file_ptr), "JUPDATE CONTIG INFO");
}

void Walk_Queue_Par::jpush_fw_rev_stitch_info(Walk_Queue& WQ, job_str *curr_job_ptr, unsigned int contig_id)
{
	// if false, then first param has no relevance
	if (WBptr->P_->enabRevWalk) {
// 		if (WQ.walking_reverse /*and curr_job.no_job()*/) { // option 1
		
		currContStr& CCS_ = WQ.CCS;
		revWalkStr& RWS_ = WQ.RWS;
		
		assert(CCS_.fw_contig_id not_eq MAX_UINT);
		assert(curr_job_ptr->no_job());
		assert(RWS_.revSeedLen not_eq 0);
		
		// TODO: but curr job is empty
		
		fr_contig_glue_str F;
		
		F.src_contig_id = CCS_.fw_contig_id;
		F.tar_contig_id = CCS_.fw_contig_id;
		F.overlap_length = RWS_.revSeedLen;
		F.overlap_node_count = RWS_.revSeedCount;
		
		push_fr_glue_str_m.lock();
		WBptr->CDS_.fr_contig_glue_vec.push_back(F);
		push_fr_glue_str_m.unlock();
		
		CCS_.fw_contig_id = MAX_UINT;
	}
}

void Walk_Queue_Par::jcalculate_RSD(Walk_Queue& WQ)
{
	wall_time_wrap W;
	W.start();
	
	unsigned int beg = WQ.beg;
	unsigned int end = (WQ.curr+1)%WQ.Q.size();
	
	/************ calculate a coverage estimate ***************/
	// data structure for clear_buffer
	clear_buffer_data CBD(WQ);
	
	// first find out a base coverage
	CBD.calculate_base_coverage(beg, end, WQ);
		
	// go through each white node, update usage and overlap_lmers vector
	unsigned int iterations=0;
	for (unsigned int pos = beg; pos not_eq end; pos = (pos+1)%WQ.Q.size()) {
		
		// fetch the current white node
		Walk_Queue_Element& Wq = WQ.getElement(pos);
		if (Wq.white_index==MAX_UINT) {
			continue;
		}
		Walk_Node_Element& Wn = Wq.candidate_nodes[ Wq.white_index ];
		
		if (not CBD.hopeless) {
// 			CBD.curr_cov = (unsigned int)((float)(CBD.curr_cov) + ((float)Wn.out_dist_const_inter_read - (float)Wn.in_dist_const_inter_read)*WQ.WBptr->magic_ratio );
			CBD.curr_cov = (CBD.curr_cov) + ((float)Wn.out_dc_inter - (float)Wn.in_dc_inter)*WQ.WBptr->magic_ratio;
		}
		
		// if this is a forward walk and the reverse seed not sufficient, update it
		CBD.update_reverse_seed_str(Wn, WQ);
		
		// something going wrong, quit
		if (iterations++ > WBptr->P_->max_iterations) {
			std::cout << "\nIterations exceeded maximum limit in jcalculate_RSD while coverage update" << std::endl;
			exit(0);
		}
	} // end: for (unsigned int pos = beg; pos not_eq end; pos = (pos+1)%Q.size())
	
	// wrap up
	CBD.clear(WQ);
	
	W.stop();
	conditional_time_print(PRINT_FN_TIMES, W, *WQ.t_log_file_ptr, "JCALCULATE RSD");
}

void Walk_Queue_Par::pre_merge_fw_rev_contigs()
{
	// sort contigs based on contig labels
	ALGO_NS::sort(WBptr->CDS_.data.begin(), 
					WBptr->CDS_.data.end(), 
					[](contig_data cd1, contig_data cd2){ 
						return cd1.contig_label not_eq cd2.contig_label ? cd1.contig_label < cd2.contig_label : cd1.d==FWD;
					});
	
	// check basic assertions, and print some info
	log_file_ << "Contig data: " << std::endl;
	for (unsigned int i=0; i < WBptr->CDS_.data.size(); i++) {
		// basic assertions
		assert(WBptr->CDS_.data[i].contig_label == i/2);
		if (i%2==0) {
			assert(WBptr->CDS_.data[i].d==FWD);
		}
		else {
			assert(WBptr->CDS_.data[i].d==REV);
		}
		log_file_ << WBptr->CDS_.data[i] << std::endl;
	}
	
	// sort stitch vector based on contig labels
	ALGO_NS::sort(WBptr->CDS_.fr_contig_glue_vec.begin(), 
					WBptr->CDS_.fr_contig_glue_vec.end(), 
					[](fr_contig_glue_str f1, fr_contig_glue_str f2){
						return f1.src_contig_id < f2.src_contig_id;
					});
	
	
	log_file_ << "Stitch data: " << std::endl;
	for (unsigned int i=0; i < WBptr->CDS_.fr_contig_glue_vec.size(); i++) {
		// basic assertions after sorting
		assert(WBptr->CDS_.fr_contig_glue_vec[i].src_contig_id == i);
		assert(WBptr->CDS_.fr_contig_glue_vec[i].tar_contig_id==i);
		
		// transform stitch vector have correct src and tar ids
		WBptr->CDS_.fr_contig_glue_vec[i].src_contig_id = 2*i;
		WBptr->CDS_.fr_contig_glue_vec[i].tar_contig_id = 2*i + 1;
		
		// print
		log_file_ << WBptr->CDS_.fr_contig_glue_vec[i] << std::endl;
	}
	ASSERT_WITH_MESSAGE(2*WBptr->CDS_.fr_contig_glue_vec.size() == WBptr->CDS_.data.size(), 
			    "stitch size: " 
			    + std::to_string(WBptr->CDS_.fr_contig_glue_vec.size()) 
			    + ", contig size: " 
			    + std::to_string(WBptr->CDS_.data.size())
			   );
// 	assert(2*WBptr->CDS_.fr_contig_glue_vec.size() == WBptr->CDS_.data.size());

}

void Walk_Queue_Par::calc_start_node_eligibility(std::vector< std::size_t >& eligibility_vec, unsigned int num_par)
{
	for (unsigned int i=0; i < WBptr->ref_db_node_table.size() ; i++) {
		DB_Node_Type const & D = WBptr->ref_db_node_table.ElementAt(i);
		// test if there is at least one negative dist const
		bool negative_exists = false;
		for (unsigned int j=0; j < D.Dist_Const_List.size(); j++) {
			if (D.Dist_Const_List[j].distance < 0) {
				negative_exists = true;
			}
		}
		if (negative_exists) {
			eligibility_vec[LP_map_[i].partition_id]++;
		}
	}
}


void Walk_Queue_Par::jupdate_contig_log_print(Walk_Queue& WQ)
{
	// TODO: test summing unsigned ints to long ints
	unsigned long int j=0;
	std::vector<unsigned long int> j_vec(NUM_THREADS_, 0);
	unsigned int z=0;
	std::vector<unsigned int> z_vec(NUM_THREADS_, 0);
	if (WBptr->contig_count%1250==0) {
		wall_time_wrap W_local;
		W_local.start();
		for (std::size_t i =0; i < WBptr->UD.node_usage_left.size(); i++) {
			long usage_left = WBptr->UD.node_usage_left[i].load();
			j_vec[LP_map_[i].partition_id]+= usage_left;
			j += usage_left;
			if (usage_left not_eq 0) {
				z_vec[LP_map_[i].partition_id]++;
				z++;
			}
		}
		
		// TODO: assert that white nodes concat resembles contig
		unsigned int expected_contig_len = WBptr->P_->k - 1;
		log_file_m.lock();
		for (unsigned int i=0; i < WQ.CCS.white_node_vec.size(); i++) {
			log_file_ << WQ.CCS.white_node_vec[i] << "(" << WBptr->ref_db_node_table.ElementAt(WQ.CCS.white_node_vec[i]).len_lmer << ") ";
			expected_contig_len += WBptr->ref_db_node_table.ElementAt(WQ.CCS.white_node_vec[i]).len_lmer - (WBptr->P_->k - 1);
		}
// 			*(WQ.t_log_file_ptr) << std::endl;	
		log_file_ << std::endl;	
		log_file_ << "Expected len: " << expected_contig_len << ", actual len: " << WQ.CCS.curr_contig.size() << std::endl;
		W_local.stop();
		log_file_ 	<< "Tid: " << WQ.thread_id 
						<< "\tcontig: " << WBptr->contig_count 
						<< "\t usage sum: " << j 
						<< "\tnum nodes left: " << z 
						<< ", calculation time: " << W_local 
						<< std::endl;
		for (unsigned int i=0; i < NUM_THREADS_; i++) {
			log_file_ << i << "\t" << j_vec[i] << "\t" << z_vec[i] << std::endl;
		}
		log_file_m.unlock();
	}
}
