#ifndef ALLCLASSES_H
#define ALLCLASSES_H

// all header files
#include <algorithm>
#include <atomic>
#include <bitset>
#include <cassert>
#include <cstring>
#include <cmath>
#include <ctime>
#include <chrono>
#include <cstdlib>
#include <climits>
#include <dirent.h>
#include <deque>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <numeric>
#include <omp.h>

#ifndef SEQ_ALGO
#include <parallel/algorithm>
#include <parallel/numeric>
#define ALGO_NS __gnu_parallel

#else

#define ALGO_NS std

#endif

#include <queue>
#include <sstream>
#include <string>
#include <thread>
#include <utility>
#include <unistd.h>
#include <vector>

// all constants
// Imperative: WHITE < GREEN < RED < < BROWN < BLACK
#define DEBUG false
#define PRINT_FN_TIMES true
#define WHITE 10 // used in walk.h
#define GREEN 20 // used in walk.h
#define RED 30 // used in walk.h
#define BROWN 40 // used in walk.h
#define BLACK 50 // used in walk.h
#define DOUBLE_BLACK 60

// some constant values
#define HUNDRED_BILLION 100000000000
#define MAX_SIZE_T std::numeric_limits<std::size_t>::max()
#define MAX_UINT std::numeric_limits<unsigned int>::max()
#define MIN_FLOAT std::numeric_limits<float>::min()


// some stuff
#define ASSERT_WITH_MESSAGE(condition, message) do { \
if (!(condition)) { std::cout << message << std::endl; } \
assert ((condition)); } while(false)

#define assert_msg(condition, message) do { \
if (!(condition)) { std::cout << message << std::endl; } \
assert ((condition)); } while(false)

#define RET_COLOR(color) (color==10 ? "WHITE" : \
(color==20 ? "GREEN" : \
(color==30 ? "RED" : \
(color==40 ? "BROWN" : \
(color==50 ? "BLACK" : \
(color==60 ? "DOUBLE_BLACK" : "NO COLOR" \
))))))

#define BOOL(flag) (flag==true ? "TRUE" : "FALSE")


// declare all classes here to get rid of cyclic inclusion
class DB_Node_Table_Type;
class DB_Node_Type;
class KU_Table_Type;
class KU_TableEntry;
class Walk_Base;
class currContStr;
class Walk_Queue;
class Walk_Queue_Element;
class Walk_Node_Element;
class reverse_seed_structure;
class select_next_hop_class;
class Walk_Queue_Mul;
class Walk_Queue_Par;
class Walk_Queue_Par_Mem;
class support_str;
// typedef support_str dist_const_str;
typedef support_str dc_str;
class kmer_unitig_mapping_type;
class job_str;
class job_queue_str;
class usage_red_job_q_str;
class sat_const_str;
// flag to adapt serial walk functions for parallel walk
enum Walk_Type {SER, PAR, MUL};

enum Evidence_Scaledown_Type {LIN, GAUSS};

// useful for stitching parts of contigs produced by different threads
enum contig_dir {FWD, REV};

extern std::ofstream debug_file;

enum hop_return {NO_HOP, SAME_PAR_HOP, DIFF_PAR_HOP, SCAFF_HOP};

#endif // ALLCLASSES_H