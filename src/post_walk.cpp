#include "post_walk.h"

/***********TODO
 * 1. Add asserts at various places
 * 2. add approximate matching for errors : k-band algorithm
 * 3. Remove reads that are substrings of other reads
 */

// constructor for Post_Walk
Post_Walk::Post_Walk( Params* P, contig_data_str& CDS, std::ofstream& log_file ): CDS_(CDS), log_file_(log_file), walk_info_level_(P->walk_info_level) {
	P_ = P;
}

// constructor for match_structure
match_structure::match_structure() {
	pos = std::numeric_limits<unsigned int>::max();
	suff_pref_match=true;
	match_length = 0;
}

void Post_Walk::elongate_contigs() {
	for (unsigned int i=0; i < CDS_.data.size(); i++) {
		if (CDS_.data[i].no_neigh) {
			continue;
		}
		
		std::string& contig_i = CDS_.data[i].contig;
		
		if (contig_i.empty()) {
			continue;
		}
		for (unsigned int j=CDS_.data.size() - 1; j >i; j--) {
			
			if (CDS_.data[j].no_neigh) {
				continue;
			}
			
			if (CDS_.data[i].partition_id not_eq CDS_.data[j].partition_id) {
				continue;
			}
			
			std::string& contig_j = CDS_.data[j].contig;
			if (contig_j.empty()) {
				continue;
			}
			
			if (walk_info_level_>=2) {
				log_file_ << "Trying to merge " << i << " and " << j << "... " << std::flush;
			}
			
			bool merged = merge_contigs(contig_i, contig_j);
			if (merged) {
				if (walk_info_level_>=2) {
					log_file_ << "Success!!\n" << std::endl;
				}
				continue;
			}
			if (walk_info_level_>=2) {
				log_file_ << "Failed to merge!!\n" << std::endl;
			}
		}
	}
	
	CDS_.remove_empty();
	
// 	CDS_.push_contigs_to_vec(contigs_);
}

// void Post_Walk::elongate_contigs() {
// 	for (unsigned int i=0; i < contigs_.size(); i++) {
// 		if (contigs_[i].empty()) {
// 			continue;
// 		}
// 		for (unsigned int j=contigs_.size() - 1; j >i; j--) {
// 			if (contigs_[j].empty()) {
// 				continue;
// 			}
// 			
// 			log_file_ << "Trying to merge " << i << " and " << j << "... " << std::flush;
// 			
// 			bool merged = merge_contigs(contigs_[i], contigs_[j]);
// 			if (merged) {
// 				log_file_ << "Success!!\n" << std::endl;
// 				continue;
// 			}
// 			log_file_ << "Failed to merge!!\n" << std::endl;
// 		}
// 	}
// 	
// 	contigs_.erase(std::remove(contigs_.begin(), contigs_.end(), ""), contigs_.end());
// }


bool Post_Walk::merge_contigs(std::string& src, std::string& tar) {
	if (walk_info_level_>=3) {
		log_file_ << "Finding same strand longest match" << std::endl;
	}
	match_structure m1 = find_longest_match(src, tar);
	if (walk_info_level_>=3) {
		log_file_ << "Longest match found: " << m1.match_length << std::endl;
	}
	std::string tar_rc = tar;
	reverse_complement(tar_rc);
	
	if (walk_info_level_>=3) {
		log_file_ << "Finding opposite strand longest match" << std::endl;
	}
	match_structure m2 = find_longest_match(src, tar_rc);
	if (walk_info_level_>=3) {
		log_file_ << "Longest match found: " << m2.match_length << std::endl;
	}
	
// 	if (m1.match_length >= std::max(m2.match_length, P_->pair_distance)) {
	if (m1.match_length >= std::max(m2.match_length, P_->k)) {
		if (walk_info_level_>=3) {
			log_file_ << "Merging on same strands" << std::endl;
		}
		combine_contigs(src, tar, m1);
		return true;
	}
// 	else if (m2.match_length >= std::max(m1.match_length, P_->pair_distance)) {
	else if (m2.match_length >= std::max(m1.match_length, P_->k)) {
	if (walk_info_level_>=3) {
		log_file_ << "Merging on opposite strands" << std::endl;
	}
		combine_contigs(src, tar_rc, m2);
		// since we are passing tar_rc, we need to modify tar here
		tar="";
		return true;
	}
	return false;
}

// find the longest match of two contigs, and return the corresponding match_structure
match_structure Post_Walk::find_longest_match(const std::string& src, const std::string& tar) {
	std::string key = tar.substr(0, P_->k);
	std::vector<unsigned int> all_pos;
	VMS.clear();
	
	find_all_substrings(all_pos, src, key);
	if (walk_info_level_>=3) {
		log_file_ << all_pos.size() << " potential candidates found on one side" << std::endl;
	}
	for (unsigned int i=0; i < all_pos.size(); i++) {
		VMS.emplace_back(all_pos[i], true, src.size() - all_pos[i]);
	}
	
	// tar contained in src cases need to be removed
	key = tar.substr(tar.size() - P_->k, P_->k);
	all_pos.clear();
	find_all_substrings(all_pos, src, key);
	if (walk_info_level_>=3) {
		log_file_ << all_pos.size() << " potential candidates found on other side" << std::endl;
	}
	for (unsigned int i=0; i < all_pos.size(); i++) {
		if (all_pos[i] + P_->k < tar.size()) {
			VMS.emplace_back(all_pos[i], false, all_pos[i] + P_->k);
		}
	}
	
	ALGO_NS::sort(VMS.begin(), VMS.end(), [](match_structure m1, match_structure m2){return m1.match_length > m2.match_length;});
	
	for (unsigned int i=0; i < VMS.size();i++) {
		if (VMS.size() < P_->insert_size) {
			continue;
		}
		bool aligned=false;
		if (VMS[i].suff_pref_match) {
			aligned = align_strings(src, tar, VMS[i].pos);
		}
		else {
			aligned = align_strings(tar, src, VMS[i].pos);
		}
		if (aligned) {
			return VMS[i];
		}
	}
	
	match_structure dummy_ms;
	return dummy_ms;
}

void Post_Walk::find_all_substrings(std::vector<unsigned int>& pos, const std::string& main_string, const std::string& small_string) {
	if (walk_info_level_>=3) {
		log_file_ << "finding all substrings" << std::endl;
	}

	const char * needle = small_string.c_str();
	const char * haystack = main_string.c_str();
	const char * it = haystack;
	while ((it = strstr(it, needle)) != nullptr) {
	  pos.emplace_back(std::distance(haystack, it));
	  ++it;
	}

//  size_t position=0;
//  unsigned int start_search=0;
//	while(true) {
//	  position = main_string.find(small_string, start_search);
//
//	  if (position == std::string::npos) break;
//
////		if (main_string.size() < small_string.size()) {
////			break;
////		}
////		if (start_search >= main_string.size()) {
////			break;
////		}
////		position = main_string.find(small_string, start_search);
////// 		log_file_ << "Found position: " << position << std::endl;
////		if (position >= main_string.size()) {
////			break;
////		}
//		pos.push_back( position );
//		start_search = position + 1;
//	}
	if (walk_info_level_>=3) {
		log_file_ << "done" << std::endl;
	}
}

// align src and tar ass suff pref, with src suffix starting at pos
bool Post_Walk::align_strings (const std::string& src, const std::string& tar, unsigned int pos) {
	// right now assuming perfect reads
	if (pos + tar.size() - 1 <= src.size()-1) {

//		if (src.substr(pos, tar.size()).compare(tar) == 0) {
//			return true;
//		}

//	  return std::equal(src.cbegin() + pos, src.cbegin() + pos + tar.size(), tar.cbegin());


	  return strncmp(&(src[pos]), &(tar[0]), tar.size()) == 0;

	}
	else {
//		if (src.substr(pos, src.size() - pos).compare(tar.substr(0,src.size() - pos)) == 0) {
//			return true;
//		}
//    return std::equal(src.cbegin() + pos, src.cend(), tar.cbegin());
	  return strncmp(&(src[pos]), &(tar[0]), src.size() - pos) == 0;

	}
//	return false;
}

void Post_Walk::combine_contigs (std::string& src, std::string& tar, match_structure M) {
	if (M.suff_pref_match) {
		if (M.pos + tar.size() > src.size()) {
//			src += tar.substr(src.size() - M.pos -1, tar.size() + M.pos + 1 - src.size());
		  src.append(tar, src.size() - M.pos -1, tar.size() + M.pos + 1 - src.size());
		}
		tar="";
	}
	else {
		if (M.pos + src.size() > tar.size()) {
			//src = tar + src.substr(tar.size() - M.pos - 1, src.size() + M.pos + 1 - tar.size());
		  //(tar + src.substr(tar.size() - M.pos - 1, src.size() + M.pos + 1 - tar.size())).swap(src);
      tar.append(src, tar.size() - M.pos - 1, src.size() + M.pos + 1 - tar.size()).swap(src);
		}
		tar="";
	}
}

// bool Post_Walk::band_align(std::string& src, std::string& tar, unsigned int band_size, unsigned int max_mismatch)
// {
// 	assert (src.size()==tar.size());
// 	unsigned int s=src.size();
// 	
// 	// create the matrix and resize it
// 	std::vector<std::vector<int> > align_matrix;
// 	align_matrix.resize(2*band_size + 1);
// 	for (unsigned int i=0; i < align_matrix.size(); i++) {
// 		align_matrix[i].resize(s+1);
// 	}
// 	
// 	// initialize
// 	for (unsigned int i=0; i < align_matrix.size(); i++) {
// 		align_matrix[i][0] = std::numeric_limits<int>::min();
// 	}
// 	for (unsigned int i=0; i <band_size; i++) {
// 		for (unsigned int j=0; j <= band_size - i; j++) {
// 			align_matrix[i][j] = std::numeric_limits<int>::min();
// 		}
// 	}
// 
// }


std::ostream& operator<<(std::ostream& os, match_structure& M) {
	os << "Pos: " << M.pos << "\tForward match: " << BOOL(M.suff_pref_match) << "\tlength: " << M.match_length << std::endl;
	return os;
}
