#include "tinterface.h"
// only for put_experimental_stuff
#include "db_graph.h"

/*********************** TODO list
 * 1. read entire file in mem, and then do processing
 * 2. Time important steps
 * *******************************************/

void lmer_table_type::read_input(	std::ifstream& infile, 
// 							std::ifstream& junction_nodes_file, 
							std::vector<std::string>& glob_jn_file_vec)
{
	std::string line;
	std::vector<std::string> lmer_vec;
	
	wall_time_wrap W;
	
	std::cout << "Tint: Reading the lmer file (3) and putting entries in Lmer table ...." << std::endl << std::flush;
	unsigned int num_lmers=0;
	W.start();
	
	for (unsigned int iter_count=0;;) {
		
		// get line from input file
		line.clear();
		std::getline(infile, line);
		
		// end of file reached, break
		if (line.empty()) {
			break;
		}
		
		// read pair identifier, continue
		if (line[0]=='>') {
			continue;
		}
		
		if (++iter_count == std::numeric_limits<decltype(iter_count)>::max()) { // hundred billion is a big number
			std::cerr << "Max iterations exceeded while reading input kmer to unitig file";
			break;
		}
		
		// create an entry and insert into KUM_Table
		lmer_vec.push_back(line);
// 		insert_lmer_entry(line);
		num_lmers++;
	}
	for (unsigned int i=0; i < lmer_vec.size(); i++) {
		insert_lmer_entry(lmer_vec[i]);
	}
	W.stop();
	std::cout 	<< "Done, put " << num2h( num_lmers )
			<< " lmers, time: " << W 
			<< std::endl;
	
	std::cout 	<< "Tint: Reading glob_jn_file_vec and putting entries in Lmer table ..." 
			<< std::endl 
			<<  std::flush;
	W.start();
	
	for (unsigned int i=0; i < glob_jn_file_vec.size(); i++) {
		insert_lmer_entry_jn(glob_jn_file_vec[i]);
	}
	
	W.stop();
	std::cout 	<< "Done, put " << num2h( glob_jn_file_vec.size() )
			<< " entries, time: " << W 
			<< std::endl;
	
	// now sort the lmer table
	std::cout << "Tint: Inplace merging the lmer table..." << std::flush;
	W.start();

	std::inplace_merge(	Table.begin(), 
					Table.begin() + num_lmers, 
					Table.end(), 
					[](lmer_entry_type T1, 
					   lmer_entry_type T2)
					{
						return T1.lmer_id < T2.lmer_id;
					});
	
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
}

void lmer_table_type::insert_lmer_entry(std::string& line)
{
	// create a table entry
	lmer_entry_type le;
	le.lmer = line;
	le.len_lmer = le.lmer.size();
	le.lmer_id = (le.lmer).substr(0,k_);
	
	Table.push_back(le);
}

void lmer_table_type::insert_lmer_entry_jn(std::string& line)
{
	std::vector<std::string> line_elements;
	line_elements = split(line, '\t');
	
	// create a table entry
	lmer_entry_type le;
	le.lmer = line_elements[0];
	le.len_lmer = le.lmer.size();
	le.lmer_id = (le.lmer).substr(0,k_);
	
	Table.push_back(le);
}


lmer_entry_type& lmer_table_type::search_table(std::string& search_key)
{
	ASSERT_WITH_MESSAGE(search_key.size() == Table[0].lmer_id.size(), 
						"key size: " 
						+ std::to_string(search_key.size()) 
						+ "table entry size: " 
						+ std::to_string(Table[0].lmer_id.size()));
	
	lmer_entry_type key_obj;
	key_obj.lmer_id = search_key;
	
	std::pair<std::vector<lmer_entry_type>::iterator, std::vector<lmer_entry_type>::iterator> range;
	range = std::equal_range(	Table.begin(), 
							Table.end(), 
							key_obj, []
							(lmer_entry_type T1, lmer_entry_type T2) 
							{
								return T1.lmer_id < T2.lmer_id;
							});
	
// 	ASSERT_WITH_MESSAGE(std::distance(range.first, range.second) == 1, (*(range.first)).lmer_id);
	if (std::distance(range.first, range.second) not_eq 1) {
		std::cout << "search key: " << search_key << std::endl;
		std::cout << "distance: " << std::distance(range.first, range.second) << std::endl;
		std::cout << "distance from beg " << std::distance(Table.begin() , range.first) << std::endl;
		for (std::vector<lmer_entry_type>::iterator it=range.first; it not_eq range.second; it++) {
			std::cout << (*it).lmer_id << "\t" << (*it).lmer << "\t" << (*it).len_lmer << std::endl;
		}
		exit(0);
	}
	
	return *(range.first);
}


// read the input mapping file and create the KUM_Table
void kmer_unitig_mapping_table_type::read_mapping_file(std::ifstream& kmer_unitig_map_file, 
												std::ifstream& junction_nodes_file, 
												std::vector<std::string>& glob_jn_file_vec,
												unsigned int& kmer_length)
{
	std::string line;
	std::vector<std::string> kum_file_vec;
	wall_time_wrap W;
	
	// READ KUM FILE INTO A VEC
	
	std::cout 	<< "Tint: Reading the kmer-to-unitig-mapping file (1) to a vec ..." 
			<< std::endl 
			<< std::flush;
	
	// start timer
	W.start();
	
	// read file to vec
	read_file_to_vec(kmer_unitig_map_file, kum_file_vec, HUNDRED_BILLION);
	
	// stop timer
	W.stop();
	std::cout 	<< "Done, pushed " << num2h( kum_file_vec.size() )
			<< " lines in vec, time: " << W 
			<< std::endl;
	
	// PUSH VEC ENTRIES IN KUM TABLE
	
	std::cout 	<< "Tint: Pushing vec entries in KUM table..." 
			<< std::flush;
	
	// start timer
	W.start();
	
	for (unsigned int i=0; i < kum_file_vec.size(); i++) {
		insert_mapping_entry(kum_file_vec[i]);
	}
	
	// stop timer
	W.stop();
	
	std::cout << "Done, time: " << W << std::endl;
	kum_file_vec.clear();
	
	// READ JN FILE IN GLOB VEC
	
	std::cout 	<< "Tint: Reading the junction_nodes_file (4) in glob vec..." 
			<< std::endl 
			<< std::flush;
	
	// start timer
	W.start();
	
	read_file_to_vec(junction_nodes_file, glob_jn_file_vec, HUNDRED_BILLION);
	
	// stop timer
	W.stop();
	std::cout 	<< "Done, put " << num2h( glob_jn_file_vec.size() )
			<< " entries in vec, time: " << W 
			<< std::endl;
	
	// PUSH JN ENTRIES IN KUM TABLE
	
	std::cout 	<< "Tint: Pushing vec entries in KUM table..." 
			<< std::flush;
	
	// start timer
	W.start();
	
	for (unsigned int i=0; i < glob_jn_file_vec.size(); i++) {
		insert_mapping_entry_jn(glob_jn_file_vec[i]);
	}
	
	// stop timer
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
	
	kmer_length = KUM_Table[0].kmer.size();
	
	// now sort the KUM table
	std::cout 	<< "Tint: Sorting the mapping table (size: " << num2h( KUM_Table.size() ) <<")..." 
			<< std::flush;
			
	W.start();
	ALGO_NS::sort(	KUM_Table.begin(), 
					KUM_Table.end(), 
					[](kmer_unitig_mapping_type T1, 
					kmer_unitig_mapping_type T2)
					{
						return T1.kmer < T2.kmer;
					});
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
}

// break one line of input file into various fields, and insert entry in KUM_Table
void kmer_unitig_mapping_table_type::insert_mapping_entry(std::string& line)
{
	std::vector<std::string> line_elements;
	line_elements = split(line, '\t');
	
	// create a table entry
	kmer_unitig_mapping_type kumt;
	kumt.kmer = line_elements[0];
	kumt.lmer_id = line_elements[1];
	kumt.pos = atoi(line_elements[2].c_str());
	kumt.is_on_positive = (bool)atoi(line_elements[3].c_str());
	
	KUM_Table.push_back(kumt);
}

// break one line of junction_nodes_file file into various fields, and insert entry in KUM_Table
void kmer_unitig_mapping_table_type::insert_mapping_entry_jn(std::string& line)
{
	std::vector<std::string> line_elements;
	line_elements = split(line, '\t');
	
	// create a table entry
	kmer_unitig_mapping_type kumt;
	kumt.kmer = line_elements[0];
	kumt.lmer_id = line_elements[0];
	kumt.pos = 0;
	kumt.is_on_positive = true;
	
	KUM_Table.push_back(kumt);
}

void kmer_unitig_mapping_table_type::put_experimental_stuff(std::vector< lmer_id_mapping_type >& lmer_id_mapping, std::vector<DB_Node_Type>& DB_Node_Table, unsigned int kmer_length)
{
#pragma omp parallel for
	for (unsigned int i=0; i < KUM_Table.size(); i++) {
		kmer_unitig_mapping_type& KUMT = KUM_Table[i];
		std::string& lmer_id = KUMT.lmer_id;
		lmer_id_mapping_type const & LIMT = search_lmer_id(lmer_id_mapping, lmer_id);
		std::string& l = DB_Node_Table[ LIMT.num_lmer_id ].get_lmer();
		KUMT.lmer_length = l.size();
		KUMT.db_table_index = LIMT.num_lmer_id;
		if (KUMT.is_on_positive) {
			KUMT.pos_on_strand = KUMT.pos;
		}
		else {
			KUMT.pos_on_strand = KUMT.lmer_length - KUMT.pos - kmer_length;
		}
	}
}


// search for a given cannonical kmer in the table
kmer_unitig_mapping_type& kmer_unitig_mapping_table_type::search_table(std::string& search_kmer)
{
	assert(search_kmer.size() == KUM_Table[0].kmer.size());
	assert(is_positive(search_kmer));
	
	kmer_unitig_mapping_type key_obj;
	key_obj.kmer = search_kmer;
	
	std::pair<	std::vector<kmer_unitig_mapping_type>::iterator, 
			std::vector<kmer_unitig_mapping_type>::iterator
			> range;
			
	range = std::equal_range(	KUM_Table.begin(), 
							KUM_Table.end(), 
							key_obj, [](	kmer_unitig_mapping_type T1, 
										kmer_unitig_mapping_type T2) 
							{
								return T1.kmer < T2.kmer;
							});
	
	ASSERT_WITH_MESSAGE(std::distance(range.first, range.second) == 1, 
						"The range is : " 
						+ std::to_string(range.first - KUM_Table.begin()) 
						+ ":" 
						+ std::to_string(range.second - KUM_Table.begin()) 
						+ " for the kmer " 
						+ search_kmer  
						+ ", Max range: " 
						+ std::to_string(KUM_Table.end() - KUM_Table.begin())
			   );

	return *(range.first);
}

// given fasta files containing paired reads, read and insert the relevant distance constraints
void dist_const_table_type::read_input_fasta(	std::ifstream& infile1, 
										std::ifstream& infile2, 
										std::ifstream& trunc_file)
{
	k_ = P_->k;
	
	std::vector<std::string> readfile1_vec;
	std::vector<std::string> readfile2_vec;
	std::vector<std::string> truncfile_vec;
	wall_time_wrap W;
	
	std::cout << "Tint: Reading fasta and trunc files to vec..." << std::endl << std::flush;
	
	// read file 1
	W.start();
	read_file_to_vec(infile1, readfile1_vec, HUNDRED_BILLION);
	W.stop();
	std::cout 	<< "Tint: fasta 1 read, " 
			<< num2h( readfile1_vec.size() )
			<< " lines in vec, time: " 
			<< W 
			<< std::endl 
			<< std::flush;
	
	// read file 2
	W.start();
	read_file_to_vec(infile2, readfile2_vec, HUNDRED_BILLION);
	W.stop();
	std::cout 	<< "Tint: fasta 2 read, " 
			<< num2h( readfile2_vec.size() )
			<< " lines in vec, time: " 
			<< W 
			<< std::endl 
			<< std::flush;
	
	// read trunc_file
	W.start();
	read_file_to_vec(trunc_file, truncfile_vec, HUNDRED_BILLION);
	W.stop();
	std::cout 	<< "Tint: trunc read, " 
			<< num2h( truncfile_vec.size() )
			<< " entries in vec, time: " 
			<< W 
			<< std::endl 
			<< std::flush;
	
	std::cout << "Tint: Done reading fasta and trunc files" << std::endl << std::flush;
		
	std::string read1;
	std::string read2;
	std::string trunc_line;
	std::vector<int> trunc_pos_vec;
	long unsigned int inter_read_count=0;
	long unsigned int intra_read_count=0;
	
	std::cout << "Tint: Creating dist const table ... " << std::endl << std::flush;
	W.start();
	
	for (unsigned long int index1=0, index2=0, iter_count=0;;iter_count++) {
		bool fetched1 = get_read(readfile1_vec, read1, index1);
		bool fetched2 = get_read(readfile2_vec, read2, index2);
		assert(fetched1==fetched2);
		if (not fetched1) {
			break;
		}
		
		assert(iter_count<truncfile_vec.size());
		trunc_line = truncfile_vec[iter_count];
		split_truncation_entry(trunc_pos_vec, trunc_line);
		
		// reverse complement read2 to make it on the same strand as read1
		reverse_complement(read2);
		
		// now insert the 3 distance constraint sets for this pair
		insert_intra_read(	read1, 
						trunc_pos_vec[0], 
						trunc_pos_vec[1], 
						intra_read_count);
		insert_intra_read(	read2, 
						read2.size() - (trunc_pos_vec[3] + k_), 
						read2.size() - (trunc_pos_vec[2] + k_), 
						intra_read_count);
		insert_inter_read(	read1, 
						read2, 
						trunc_pos_vec[0], 
						read2.size() - (trunc_pos_vec[2] + k_), 
						inter_read_count);
		
		trunc_pos_vec.clear();
	}
	
	W.stop();
	std::cout << "Done creating dist const table, time: " << W << std::endl << std::flush;
	std::cout << num2h( intra_read_count ) << " intra consts and " << num2h( inter_read_count ) << " inter consts pushed" << std::endl << std::flush;
	
	estimate_insert_size();
	
	print_insert_size();
	
	// TODO: Introduce a bool variable to print this
	print_contig_size();
	
	update_inter_read_data();
	
	estimate_read_size();
	
	// TODO: print insert size list in file for histogram
	
	P_->read1_length = est_read_size_mean.first;
	P_->read2_length = est_read_size_mean.second;
	P_->insert_size = est_ins_size_mean;
	P_->pair_distance = est_ins_size_mean - (P_->read1_length + P_->read2_length) + 1;
	
	update_maxdc_error();
	
}



// get the next read from infile, return false if no read fetched, true otherwise
bool dist_const_table_type::get_read(std::ifstream& infile, std::string& read)
{
	std::string line;
	bool reading_complete=false;
	if (not read.empty()) {
		read.clear();
	}
	unsigned int iter=0;
	
	while(true) {
		std::getline(infile, line);
		
		if (line.empty()) {
			if (read.empty()) {
				return false;
			}
			else {
				return true;
			}
		}
		
		else if (line[0] == '>') {
			if (reading_complete) {
				return true;
			}
			else {
				continue;
			}
		}
		
		// presumably a part of the read
		else {
			reading_complete=true;
			read.append(line);
		}
		
		if (iter++==10000000) {
			std::cerr << "Error in get read, iterations exceeded maximum limit" << std::endl;
			exit(0);
		}
	}
}

bool dist_const_table_type::get_read(	std::vector< std::string >& vec, 
								std::string& read, 
								long unsigned int& index)
{
	std::string line;
	bool reading_complete=false;
	read.clear();
	unsigned int iter=0;
	
	while(true) {
		if (index>=vec.size()) {
			if (read.empty()) {
				return false;
			}
			else {
				return true;
			}
		}
		line=vec[index];
		index++;
		
		if (line[0] == '>') {
			if (reading_complete) {
				return true;
			}
			else {
				continue;
			}
		}
		
		// presumably a part of the read
		else {
			reading_complete=true;
			read.append(line);
		}
		
		if (iter++==10000000) {
			std::cerr << "Error in get read, iterations exceeded maximum limit" << std::endl;
			exit(0);
		}
	}
}


// split an entry of read truncation into 4 parts
void dist_const_table_type::split_truncation_entry(std::vector<int >& trunc_pos_vec, std::string& line)
{
	ASSERT_WITH_MESSAGE(trunc_pos_vec.empty(), "");
	
	std::vector<std::string> line_elements;
	line_elements = split(line, '\t');
	ASSERT_WITH_MESSAGE(line_elements.size()==4, "More than 4 numbers in truncation entry");
	
	for (unsigned int i=0; i < line_elements.size(); i++) {
		trunc_pos_vec.push_back(atoi(line_elements[i].c_str()));
	}
}

// given a read, insert the appropriate intra read dist const in the table
void dist_const_table_type::insert_intra_read(std::string& read, int pos1, int pos2, long unsigned int& count)
{
	unsigned int read_length=read.size();
	if (read_length < k_) {
		return;
	}
	
	// validity check
	if (	pos1==-1 
		or pos1 == int(read_length + 1 - k_) 
		or pos2==-1 
		or pos2 == int(read_length + 1 - k_)) {
			return;
	}
	

	dist_const_type read_intra;
	
	read_intra.src = read.substr(pos1, k_);
	bool src_positive = MakePositive(read_intra.src);
	
	read_intra.tar = read.substr(/*read_length - k_*/ pos2, k_);
	bool tar_positive = MakePositive(read_intra.tar);
	
	read_intra.distance = pos2 - pos1 - k_ + 1;
	read_intra.error = P_->intra_error;
	
	if (read_intra.distance < P_->min_dist_const) {
		return;
	}
	
	// true = 1, false = 0
	read_intra.s_dir = src_positive ? true : false;
	read_intra.e_dir = tar_positive ? false : true;
	
	read_intra.freq = 1;
	read_intra.is_intra_read = true;
	
	if (KUM_Table_.search_table(read_intra.src).lmer_id == KUM_Table_.search_table(read_intra.tar).lmer_id) {
		return;
	}
	
	DC_Table.push_back(read_intra);
	
	// now add the reverse entry
	std::swap(read_intra.src, read_intra.tar);
	std::swap(read_intra.s_dir, read_intra.e_dir);
	
	DC_Table.push_back(read_intra);
	
	count++;
	
	return;
}

// given a pair of reads, insert the appropriate inter read dist const in the table
void dist_const_table_type::insert_inter_read(	std::string& read1, std::string& read2, int pos1, int pos2, long unsigned int& count)
{
	unsigned int read1_length=read1.size();
	unsigned int read2_length=read2.size();
	
	if (read1_length < k_ or read2_length < k_) {
		return;
	}
	
	// validity check
	if (pos1==-1 or pos2 == int(read2_length + 1 - k_)) {
		return;
	}
	
	dist_const_type read_inter;
	
	read_inter.src = read1.substr(pos1, k_);
	bool src_positive = MakePositive(read_inter.src);
	
	read_inter.tar = read2.substr(pos2, k_);
	bool tar_positive = MakePositive(read_inter.tar);
	
	read_inter.distance = insert_size_init - 2*k_ + 1 - (pos1 + read2_length - pos2 - k_);
	
	if (read_inter.distance < P_->min_dist_const) {
		return;
	}
	
	// error will be entered later, after the estimation of insert size
	
	// true = 1, false = 0
	read_inter.s_dir = src_positive ? true : false;
	read_inter.e_dir = tar_positive ? false : true;
	
	read_inter.freq = 1;
	read_inter.is_intra_read = false;
	
	// see if a pair estimation entry is possible
	bool insertion_success = insert_pair_estimation(	read_inter.src, 
											pos1, 
											src_positive, 
											read_inter.tar, 
											pos2, 
											tar_positive, 
											read1_length, 
											read2_length);
	
	// if insertion success, src and tar on same strand, so no point of dist const in navigating
	if (insertion_success) {
		return;
	}
	
	DC_Table.push_back(read_inter);
	
	// now add the reverse entry
	std::swap(read_inter.src, read_inter.tar);
	std::swap(read_inter.s_dir, read_inter.e_dir);
	
	DC_Table.push_back(read_inter);
	
	count++;
	
	return;
}

// if the pair lies on the same contig, insert entry for pair size estimation, return true if insertion success
bool dist_const_table_type::insert_pair_estimation(	std::string& src_kmer, 
											unsigned int src_pos, 
											bool src_positivity, 
											std::string& tar_kmer, 
											unsigned int tar_pos, 
											bool tar_positivity, 
											unsigned int r1_size, 
											unsigned int r2_size)
{
	// push read size data
	std::pair<unsigned long int, unsigned long int> read_size_pair ((unsigned long int)r1_size, (unsigned long int)r2_size);
	read_est_data.push_back(read_size_pair);
	
	// find k-to-l entries for src and tar
	kmer_unitig_mapping_type& src_map_entry = KUM_Table_.search_table(src_kmer);
	kmer_unitig_mapping_type& tar_map_entry = KUM_Table_.search_table(tar_kmer);
	
	// if not on the same lmer, return
	if (src_map_entry.lmer_id not_eq tar_map_entry.lmer_id) {
		return false;
	}
	
	if (not ((src_positivity==tar_positivity) == (src_map_entry.is_on_positive == tar_map_entry.is_on_positive))) {
		return false;
	}
	
	unsigned int est=MAX_UINT;
	
	if (src_positivity==src_map_entry.is_on_positive) {
		assert(tar_positivity==tar_map_entry.is_on_positive);
		if (tar_map_entry.pos>=src_map_entry.pos) {
			est = (tar_map_entry.pos - src_map_entry.pos) + src_pos + r2_size - tar_pos;
		}
		else {
			return false;
		}
	}
	if (src_positivity not_eq src_map_entry.is_on_positive) {
		assert(tar_positivity not_eq tar_map_entry.is_on_positive);
		if (src_map_entry.pos>=tar_map_entry.pos) {
			est = (src_map_entry.pos - tar_map_entry.pos) + src_pos + r2_size - tar_pos;
		}
		else {
			return false;
		}
	}
	assert(est not_eq MAX_UINT);
	
	// push insert size data
// 	unsigned int est = 	abs(src_map_entry.pos - tar_map_entry.pos) 
// 					+ src_pos 
// 					+ r1_size 
// 					- tar_pos;
	insert_est_data.push_back((unsigned long int)est);
	
	// output the contig length used for estimating insert size
	lmer_entry_type& LET = LTT_.search_table(src_map_entry.lmer_id);
	contig_len_data.push_back(LET.len_lmer);
	
	return true;
}

void dist_const_table_type::estimate_insert_size()
{
	if (insert_est_data.size()==0) {
		std::cout << "Tint: no data to estimate insert size" << std::endl;
		return;
	}
	
	wall_time_wrap W;
	std::cout 	<< "Estimating insert size over: " << num2h( insert_est_data.size() ) << " entries"  
			<< std::endl 
			<< std::flush;
			
	W.start();
	
	// calculate mean
	unsigned long int sum = ALGO_NS::accumulate(	insert_est_data.begin(),
												insert_est_data.end(), 
												static_cast<unsigned long int>(0));
	unsigned long int avg = (unsigned long int)std::round((double)sum/(double)insert_est_data.size());
	
	std::pair<	std::vector<unsigned long int>::iterator, 
			std::vector<unsigned long int>::iterator
			> mm_result = std::minmax_element(	insert_est_data.begin(), 
											insert_est_data.end()
							 );
	std::cout 	<< "(min, max) = (" 
			<< *(mm_result.first) << ", " 
			<< *(mm_result.second) << ")" 
			<< std::endl;
	
	// calculate std dev
	std::vector<unsigned long int> diff_vec(insert_est_data.size());
	std::transform(	insert_est_data.begin(), 
				insert_est_data.end(), 
				diff_vec.begin(), [avg](unsigned long int x){
					return x>avg ? x - avg : avg - x;
				});
	unsigned long int sq_sum = std::inner_product(	diff_vec.begin(), 
											diff_vec.end(), 
											diff_vec.begin(), 
											static_cast<unsigned long int>(0));
	
	unsigned int std_dev = (unsigned int)std::round(std::sqrt((double)sq_sum/(double)diff_vec.size()));
	
	
	assert(avg < MAX_UINT);
	est_ins_size_mean = (unsigned int)avg;
	est_ins_size_dev = (unsigned int)(P_->error_scaling *(float)std_dev);
	
	W.stop();
	std::cout 	<<"Insert size = " << avg 
			<< ", std dev = " << std_dev 
			<< ", time:" << W 
			<< std::endl << std::flush;
}

void dist_const_table_type::print_insert_size()
{
	for (unsigned int i=0; i < insert_est_data.size(); i++) {
		insert_hist_file_ << insert_est_data[i] << std::endl;
	}
}

void dist_const_table_type::print_contig_size()
{
	for (unsigned int i=0; i < contig_len_data.size(); i++) {
		contig_len_hist_file_ << contig_len_data[i] << std::endl;
	}
}

void dist_const_table_type::estimate_read_size()
{
	if (read_est_data.size()==0) {
		std::cout << "Tint: no data to estimate read size" << std::endl;
		return;
	}
	
	wall_time_wrap W;
	std::cout 	<< "Estimating read size over: " 
			<< num2h( read_est_data.size() )
			<< " entries" << std::endl << std::flush;
	W.start();
			
	// TODO: parallelize the sum
	
	std::pair<unsigned long int, unsigned long int> sum = std::accumulate(
										read_est_data.begin(), 
										read_est_data.end(), 
										std::make_pair<unsigned long int, unsigned long int>(static_cast<unsigned long int>(0),
											static_cast<unsigned long int>(0)), 
										[](	std::pair<unsigned long int, unsigned long int>& p1, 
											std::pair<unsigned long int, unsigned long int>& p2)
										{
											return std::make_pair(	p1.first + p2.first, 
																p1.second + p2.second);
										});
	
	est_read_size_mean = std::make_pair(	(unsigned int)std::round((double)sum.first/(double)read_est_data.size()), 
									(unsigned int)std::round((double)sum.second/(double)read_est_data.size()));
	
	W.stop();
	std::cout 	<< "Done, size = " 
			<< est_read_size_mean.first 
			<< ", " 
			<< est_read_size_mean.second 
			<< ", time: " << W 
			<< std::endl << std::flush;
}



// search the dist const table for a src (kmer or lmer_id)
dist_const_type& dist_const_table_type::search_table(std::string& search_src)
{
	dist_const_type key_obj;
	key_obj.src = search_src;
	
	std::pair<	std::vector<dist_const_type>::iterator, 
			std::vector<dist_const_type>::iterator
			> range;
	range = std::equal_range(	DC_Table.begin(), 
							DC_Table.end(), 
							key_obj, 
							[](	dist_const_type T1, 
								dist_const_type T2) 
							{
								return T1.src < T2.src;
							});
	
	assert(std::distance(range.first, range.second) >= 1);
	
	return *(range.first);
}


// in the DC_Table, transform constraints from kmer-kmer to lmer-lmer
void dist_const_table_type::kk_to_ll_transformation(	kmer_unitig_mapping_table_type& KUM_Table, 
											lmer_table_type& Lmer_Table)
{
	wall_time_wrap W;
	std::cout 	<< "Tint: Scanning the DC table (size: " << num2h( DC_Table.size() )
			<< ") to transform kmer-kmer to lmer-lmer..." 
			<< std::flush;
	W.start();
	
#pragma omp parallel for
	for (unsigned int i=0; i < DC_Table.size(); i++) {
		
		dist_const_type& curr = DC_Table[i];
		
		// unitig mapping entries for src and tar
		kmer_unitig_mapping_type& src_map_entry = KUM_Table.search_table(curr.src);
		kmer_unitig_mapping_type& tar_map_entry = KUM_Table.search_table(curr.tar);
		
		// lmer entries for src and tar lmer, just to calculate lengths of lmers
		lmer_entry_type& src_lmer_entry = Lmer_Table.search_table(src_map_entry.lmer_id);
		lmer_entry_type& tar_lmer_entry = Lmer_Table.search_table(tar_map_entry.lmer_id);
		
		// alas, lmer lengths
		unsigned int src_lmer_len = src_lmer_entry.len_lmer;
		unsigned int tar_lmer_len = tar_lmer_entry.len_lmer;
// 		log_file_ << "src and tar lens: " << src_lmer_len << ", " << tar_lmer_len << std::endl;
		
		// change the src
		if (	(src_map_entry.is_on_positive and curr.s_dir == 1) 
			or 
			((not src_map_entry.is_on_positive) and curr.s_dir == 0)) {
				assert(src_lmer_len >= k_ + src_map_entry.pos);
				curr.distance -= (src_lmer_len - k_ - src_map_entry.pos);
				curr.s_dir=1;
		}
		else if (	(src_map_entry.is_on_positive and curr.s_dir == 0) 
				or 
				((not src_map_entry.is_on_positive) and curr.s_dir == 1)) {
					curr.distance -=  src_map_entry.pos;
					curr.s_dir=0;
		}
		else {
			// impossible case
			std::cerr << "Error" << std::endl;
			exit(0);
		}
		
		// change the tar
		if ((tar_map_entry.is_on_positive and curr.e_dir == 0) or ((not tar_map_entry.is_on_positive) and curr.e_dir == 1)) {
			curr.distance -= tar_map_entry.pos;
			curr.e_dir=0;
		}
		else if ((tar_map_entry.is_on_positive and curr.e_dir == 1) or ((not tar_map_entry.is_on_positive) and curr.e_dir == 0)) {
			assert(tar_lmer_len >= k_ + tar_map_entry.pos);
			curr.distance -= (tar_lmer_len - k_ - tar_map_entry.pos);
			curr.e_dir=1;
		}
		else {
			// impossible case
			std::cerr << "Error" << std::endl;
			exit(0);
		}
		
		// change the kmers to lmer_ids
		curr.src = src_map_entry.lmer_id;
		curr.tar = tar_map_entry.lmer_id;
		// TODO: think if this assert makes sense
// 		ASSERT_WITH_MESSAGE(curr.distance >= -((int)(P_->k + P_->max_dist_error) - 2), "dist: " + std::to_string(curr.distance) + ", src, tar: " + curr.src + " " + curr.tar);
// 		if (curr.distance < -((int)(P_->k + P_->max_dist_error) - 2)) {
// 			log_file_ << src_map_entry << std::endl;
// 			log_file_ << tar_map_entry << std::endl;
// 			log_file_ << curr << std::endl;
// 			exit(0);
// 		}
		
	} // END: for (unsigned int i=0; i < DC_Table.size(); i++) {
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
	
	std::cout << "Removing out of range dist constraints from DC table ..." << std::endl << std::flush;
	W.start();
	// TODO:temporarily erasing the dist const, that ideally shouldn't be there in the first place.
	DC_Table.erase(std::remove_if(DC_Table.begin(), DC_Table.end(), [this](dist_const_type& D){return (D.distance < -((int)(P_->k + P_->max_dist_error) - 2) or D.distance > int(P_->insert_size + P_->max_dist_error));}), DC_Table.end());
	W.stop();
	std::cout << "Done, remaining DC entries: " << num2h( DC_Table.size() ) << ", time: " << W << std::endl;
	
	// sort on src, so that can be proceeded to accumulate
	std::cout << "Tint: Sorting the DC table..." << std::flush;
	W.start();
	ALGO_NS::sort(	DC_Table.begin(), 
					DC_Table.end(), 
					[](dist_const_type d1, 
					   dist_const_type d2){
						return d1 < d2;
					});
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
} // END: kk_to_ll_transformation()

void dist_const_table_type::accummulate_dist_const()
{
	// TODO: can be parallelized, split the range into partitions with each thread on one partition
	wall_time_wrap W;
	std::cout << "Tint: Accumulating similar dist constraints in the DC table ... " << std::flush;
	W.start();
	unsigned int start_index=0;
	unsigned int end_index=0;
	while(true) {
		if (start_index>=DC_Table.size()) {
			break;
		}
		while(end_index<DC_Table.size()) {
			if (DC_Table[start_index] == DC_Table[end_index]) {
				end_index+=1;
			}
			else {
				break;
			}
		}
		for (unsigned int j=start_index+1;j<end_index;j++) {
			DC_Table[start_index].freq += DC_Table[j].freq;
			DC_Table[j].freq=0;
		}
		start_index=end_index;
	}
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
	
	// remove the zero freq entries
	std::cout << "Tint: Removing redundant entries from DC table ... ";
	W.start();
	DC_Table.erase(
		std::remove_if(
			DC_Table.begin(), 
			DC_Table.end(), 
			[](dist_const_type& D){
				return D.freq==0 or D.src == D.tar;
			}), 
		DC_Table.end()
	);
	W.stop();
	std::cout << "Done, time: " << W << std::endl;
}

lmer_id_mapping_type const & search_lmer_id(	std::vector< lmer_id_mapping_type > const & map_vec, 
										std::string const & key)
{
	lmer_id_mapping_type key_obj;
	key_obj.lmer_id = key;
	
	std::pair<	std::vector<lmer_id_mapping_type>::const_iterator, 
			std::vector<lmer_id_mapping_type>::const_iterator
			> 
		range = std::equal_range(	map_vec.begin(), 
							map_vec.end(), 
							key_obj, 
							[](	lmer_id_mapping_type const & T1, 
								lmer_id_mapping_type const & T2) 
							{
								return T1.lmer_id < T2.lmer_id;
							});
	
	assert(std::distance(range.first, range.second) == 1);
	
	return *(range.first);
}


inline bool operator<(const dist_const_type& d1, const dist_const_type& d2)
{
	if (d1.src != d2.src) {
		return d1.src < d2.src;
	}
	else {
		if (d1.tar != d2.tar) {
			return d1.tar < d2.tar;
		}
		else {
			if (d1.distance != d2.distance) {
				return d1.distance < d2.distance;
			}
			else {
				if (d1.error != d2.error) {
					return d1.error < d2.error;
				}
				else {
					if (d1.s_dir != d2.s_dir) {
						return d1.s_dir < d2.s_dir;
					}
					else {
						return d1.e_dir < d2.e_dir;
					}
				}
			}
		}
	}
}

bool operator==(const dist_const_type& d1, const dist_const_type& d2)
{
	if ((d1.src == d2.src) and (d1.tar == d2.tar) and (d1.distance == d2.distance) and (d1.error == d2.error) and (d1.s_dir == d2.s_dir) and (d1.e_dir == d2.e_dir)) {
		return true;
	}
	return false;
}

std::ostream& operator<<(std::ostream& os, lmer_entry_type& let)
{
	os << "lmer_id: " << let.lmer_id << "\tlength: " << let.len_lmer;
	if (let.len_lmer < 1000) {
		os << "\n" << let.lmer;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, lmer_table_type& ltt)
{
	os << "k: " << ltt.k_ << std::endl;
	for (unsigned int i=0; i < ltt.Table.size(); i++) {
		os << ltt.Table[i] << std::endl;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, kmer_unitig_mapping_type& kutt)
{
	os << /*"kmer: " <<*/ kutt.kmer << "\t" /*<< "\tlmer_id: " << kutt.lmer_id << "\tpos: "*/<< kutt.db_table_index << "\t" << kutt.pos << "\t" << /*"\tis_on_+ve: " <<*/ BOOL(kutt.is_on_positive);
	return os;
}

std::ostream& operator<<(std::ostream& os, kmer_unitig_mapping_table_type& kumtt)
{
	for (unsigned int i=0; i < kumtt.KUM_Table.size(); i++) {
		os << kumtt.KUM_Table[i] << std::endl;
	}
	return os;
}

std::ostream& operator<<(std::ostream& os, lmer_id_mapping_type& limt)
{
	os << "lmer_id: " << limt.lmer_id << "\tnum_lmer_id: " << limt.num_lmer_id;
	return os;
}

std::ostream& operator<<(std::ostream& os, dist_const_type& kdc)
{
	os 	<< "src: " << kdc.src 
		<< "\ttar: " << kdc.tar 
		<< "\td=" << kdc.distance 
		<< "\te=" << kdc.error 
		<< "\ts_dir: " << kdc.s_dir 
		<< "\te_dir: " << kdc.e_dir 
		<< "\tfreq=" << kdc.freq;
		
	return os;
}

std::ostream& operator<<(std::ostream& os, dist_const_table_type& dctt)
{
	os << "insert size: " << dctt.insert_size_init << "\tk: " << dctt.k_ << std::endl;
	for (unsigned int i=0; i < dctt.DC_Table.size(); i++) {
		os << dctt.DC_Table[i] << std::endl;
	}
	return os;
}
