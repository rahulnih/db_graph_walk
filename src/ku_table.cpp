#include "ku_table.h"

void KU_TableEntry::read_from_file(std::ifstream& is)
{
	std::string line;
	std::getline(is, line);
	std::vector<std::string> elements = split(line, '\t');
	assert(elements.size()==5);
	
	kmer = elements[0];
	list_nodes_index = atoi(elements[1].c_str());
	PositiveLmer = atoi(elements[2].c_str());
	pos = atoi(elements[3].c_str());
	lmer_length = atoi(elements[4].c_str());
}

void KU_TableEntry::write_to_file(std::ofstream& os)
{
	os 	<< kmer << "\t" 
		<< list_nodes_index << "\t"
		<< PositiveLmer << "\t"
		<< pos << "\t"
		<< lmer_length << std::endl;
}


void KU_Table_Type::Create_Kmer_2_Unitig_Table (DB_Node_Table_Type& list_nodes, unsigned int k) {
	KU_TableEntry E;
	for (unsigned int i=0; i < list_nodes.size(); i++) {
		DB_Node_Type& D=list_nodes.ElementAt(i);
		std::string& Dlmer = D.get_lmer();
		for (unsigned int j=0; j < Dlmer.size() - k + 1; j++) {
// 			KU_TableEntry E;
			E.kmer=Dlmer.substr(j, k);
			E.PositiveLmer=MakePositive(E.kmer);
			E.list_nodes_index = i;
			E.pos = j;
			E.lmer_length=Dlmer.size();
			// if it is on the negative lmer, then position is from the beginning of the negative lmer
			if (not E.PositiveLmer) {
				E.pos = Dlmer.size() - j - k;
			}
			KU_Table.push_back(E);
		}
	}
	// sort the table based on E.kmer, operator defined in KU_TableEntry structure
	ALGO_NS::sort(KU_Table.begin(), KU_Table.end());

}

// given a kmer (positive or negative), return the corresponding entry in the table
KU_TableEntry const & KU_Table_Type::search_table(std::string const & search_kmer) const {
	
	ASSERT_WITH_MESSAGE(search_kmer.size()==KU_Table[0].kmer.size(), "Error in searching KU table, incorrect kmer size");
	
	// make the search kmer positive, table only indexes positive kmers
	//MakePositive(search_kmer);
	
	KU_TableEntry key_obj;
	key_obj.kmer = search_kmer;

	MakePositive(key_obj.kmer);  // do this instead.
	std::pair<std::vector<KU_TableEntry>::const_iterator, std::vector<KU_TableEntry>::const_iterator>
		kmer_range = std::equal_range(KU_Table.begin(), KU_Table.end(), key_obj, [](KU_TableEntry E1, KU_TableEntry E2){return E1.kmer < E2.kmer;});
	assert(std::distance(kmer_range.first, kmer_range.second)==1);
	return *(kmer_range.first);
}

void KU_Table_Type::print(std::ostream& os/*=std::cout*/) {
	os << "Kmer\tLmer id\tOn strand\tposition" << std::endl;
	for (unsigned int i=0; i < KU_Table.size(); i++) {
		os << KU_Table[i] << std::endl;
	}
}

void KU_Table_Type::read_from_file(std::ifstream& is)
{
	std::string line;
	std::getline(is, line);
	size_t s = atoi(line.c_str());
	for (size_t i = 0; i < s; i++) {
		KU_TableEntry KUTE;
		KUTE.read_from_file(is);
		KU_Table.push_back(KUTE);
	}
}

void KU_Table_Type::write_to_file(std::ofstream& os)
{
	os << KU_Table.size() << std::endl;
	for (size_t i=0; i < KU_Table.size(); i++) {
		KU_TableEntry& KUTE = KU_Table[i];
		KUTE.write_to_file(os);
	}
}


size_t KU_Table_Type::dynamic_size() const
{
	return vecsize(KU_Table);
}


std::ostream& operator<<(std::ostream& os, KU_TableEntry& K)
{
		os << K.kmer << "\t" << K.list_nodes_index << "\t";
		if (K.PositiveLmer) {
			os << "Positive";
		}
		else {
			os << "Negative";
		}
		os << "\t" << K.pos;
		return os;
}
