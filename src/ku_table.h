#ifndef KUTABLE_H
#define KUTABLE_H

#include "all_classes.h"
#include "db_graph.h"

// an entry for mapping each kmer to the lmer_id
class KU_TableEntry{
public:
	
	// Vars
	
	std::string kmer;
	unsigned int list_nodes_index;
	bool PositiveLmer; // True if the positive kmer in question is on positive lmer
	unsigned int pos; // position of kmer on it's strand from the beginning
	unsigned int lmer_length;
	
	// Functions
	
	void read_from_file(std::ifstream& is);
	void write_to_file(std::ofstream& os);
	
	// operator used for sorting the KU_Table
	bool operator < (const KU_TableEntry& E) const {
		return std::strcmp(kmer.c_str(), E.kmer.c_str())<0;
	}
	
};

std::ostream& operator<<(std::ostream& os, KU_TableEntry& K);

class KU_Table_Type {
	private:
		std::vector<KU_TableEntry> KU_Table;
	public:
		// push an entry into the table
		void TablePush( KU_TableEntry a) {
			KU_Table.push_back(a);
		}

		void sort() {
			std::sort(KU_Table.begin(), KU_Table.end());
		}
		
		// creates mapping from kmer to unitig index in the db graph (contains list of nodes)
		void Create_Kmer_2_Unitig_Table (DB_Node_Table_Type& list_nodes, unsigned int k);
		
		// given a kmer (positive or negative), return the corresponding entry in the table
		KU_TableEntry const & search_table(std::string const & search_kmer) const;
		
		void print(std::ostream& os=std::cout);
		
		unsigned int size() const {
			return KU_Table.size();
		}
		
		KU_TableEntry& ElementAt(unsigned int index) {
			return KU_Table[ index ];
		}
		KU_TableEntry const & ElementAt(unsigned int index) const {
			return KU_Table[ index ];
		}
		
		void read_from_file(std::ifstream& is);
		void write_to_file(std::ofstream& os);
		
		size_t dynamic_size() const;
		size_t memsize() const {return sizeof(KU_Table_Type) + dynamic_size();}
};


#endif // KUTABLE_H
