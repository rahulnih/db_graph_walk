#ifndef POST_WALK_H
#define POST_WALK_H

#include "all_classes.h"
#include "misc.h"
#include "contig_data.h"

class match_structure {
public:
	unsigned int pos;
	bool suff_pref_match;
	unsigned int match_length;
	
	match_structure();
	
	match_structure(unsigned int const & _pos, bool const & _sp_match, unsigned int const & _match_len) :
		pos(_pos), suff_pref_match(_sp_match), match_length(_match_len) {};
		
	match_structure(match_structure const & other) :
		pos(other.pos), suff_pref_match(other.suff_pref_match), match_length(other.match_length) {}
		
	match_structure & operator=(match_structure const & other) {
		pos = other.pos;
		suff_pref_match = other.suff_pref_match;
		match_length = other.match_length;
		return *this;
	}
};
std::ostream& operator<<(std::ostream& os, match_structure& M);

class Post_Walk {
private:
	contig_data_str& CDS_;
	std::ofstream& log_file_;
	unsigned int& walk_info_level_;
	Params* P_;
	std::vector<match_structure> VMS;
	
public:
	Post_Walk(Params* P, contig_data_str& CDS, std::ofstream& log_file);
	
	// core function, called from main
	void elongate_contigs();
	
	bool merge_contigs(std::string& src, std::string& tar);
	
	// find the longest match of two contigs, and return the corresponding match_structure
	match_structure find_longest_match(const std::string& src, const std::string& tar);
	
	// given the main_string, find all occurances of small_string in that and fill the pos vector
	void find_all_substrings(std::vector<unsigned int>& pos, const std::string& main_string, const std::string& small_string);
	
	// try to keep tar at position pos in string src and return true if there is a good match
	bool align_strings (const std::string& src, const std::string& tar, unsigned int pos);
	
	void combine_contigs (std::string& src, std::string& tar, match_structure M);
	
// 	bool band_align(std::string& src, std::string& tar, unsigned int band_size, unsigned int max_mismatch);
};

#endif // POST_WALK_H
